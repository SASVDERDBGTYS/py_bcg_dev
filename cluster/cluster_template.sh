#!/bin/sh
#
#SBATCH --account=g_account_g # The account name for the job.
#SBATCH --job-name=g_job_name_g # The job name.
#SBATCH -c g_c_g # The number of cpu cores to use.
#SBATCH --time=g_time_g # The time the job will take to run.
#SBATCH --mem-per-cpu=g_mem_per_cpu_g # The memory the job will use per cpu core.

module load tensorflow/anaconda3-5.1.0
pip install numpy scipy pandas matplotlib seaborn fooof pycircstat noisyopt pykalman sympy keras


#Command to execute Python program
python cluster_functions.py --ix_sub g_ix_sub_g --arch g_arch_g --function g_function_g --epochs g_epochs_g --es_patience g_es_patience_g

#End of script
