#!/bin/sh
#
#SBATCH --account=dsi # The account name for the job.
#SBATCH --job-name=s00 # The job name.
#SBATCH -c 6 # The number of cpu cores to use.
#SBATCH --time=01:00:00 # The time the job will take to run.
#SBATCH --mem-per-cpu=4gb # The memory the job will use per cpu core.
#SBATCH --gres=gpu
#SBATCH --constraint=p100

module load cuda10.0/toolkit cuda10.0/blas
module load cudnn/cuda_10.0_v7.6.2 # guess this is temporary!

TMPDIR=/rigel/dsi/users/jrm2263/temp_piptf/
pip install --cache-dir=/rigel/dsi/users/jrm2263/temp_piptf/ --build /rigel/dsi/users/jrm2263/temp_piptf/ --user --upgrade tensorflow-gpu mne scipy numpy

python hyperas_optimizer_cudnn.py

#Attempt 1
# couldn't install in the right dir - maybe this will work if I can get cuit to change my home dir
#module load cuda80/toolkit cuda80/blas cudnn/5.1
#module load anaconda/3-5.1
#pip install numpy scipy seaborn mne tensorflow-gpu hyperas --ignore-installed --target=/rigel/dsi/users/jrm2263/pip_local
#pip install numpy scipy seaborn mne tensorflow-gpu hyperas --ignore-installed --install-option="--prefix=/rigel/dsi/users/jrm2263/pip_local"
#pip install numpy scipy seaborn mne hyperas

#paths:
#/home/mcintosh/Local/working_eegbcg
#/home/mcintosh/Habanero/Local/working_eegbcg/
#
#/home/mcintosh/Local/gitprojects/py_bcg_dev
#/home/mcintosh/Habanero/Local/gitprojects/py_bcg_dev

#Attempt 2
#Tried to use singularity, but in the end tf doesn't see the gpu
#module load singularity
#module load cuda90/toolkit cuda90/blas cudnn/7.0.5
#export SINGULARITY_CACHEDIR=/rigel/dsi/users/jrm2263/sing/
#export SINGULARITY_PULLFOLDER=/rigel/dsi/users/jrm2263/sing/
#singularity pull docker://tensorflow/tensorflow:latest-gpu-py3
#singularity shell --nv --writable-tmpfs tensorflow.simg
#pip install --user numpy scipy seaborn mne hyperas
#for testing: srun --pty -t 0-02:00:00 --gres=gpu:1 -A dsi /bin/bash
#but then ... Status: CUDA driver version is insufficient for CUDA runtime version
# the driver version is 384.111, which makes sense with cuda90 - so probably the docker version is too new

# ?
#TMPDIR=/rigel/dsi/users/jrm2263/temp_piptf/ pip install --cache-dir=/rigel/dsi/users/jrm2263/temp_piptf/ --build /rigel/dsi/users/jrm2263/temp_piptf/ --user --upgrade hyperas mne scipy numpy tensorflow-gpu


#Attempt 3
#srun --pty -t 0-02:00:00 --constraint=p100 -A dsi /bin/bash
#module load cuda90/toolkit cuda90/blas cudnn/7.0.5
#module load anaconda
#TMPDIR=/rigel/dsi/users/jrm2263/temp_piptf/ pip install --cache-dir=/rigel/dsi/users/jrm2263/temp_piptf/ --build /rigel/dsi/users/jrm2263/temp_piptf/ --user --upgrade tensorflow-gpu

#>>> import tensorflow as tf
#sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
#>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
#2019-07-04 16:13:17.986640: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
#2019-07-04 16:13:17.998436: I tensorflow/stream_executor/platform/default/dso_loader.cc:42] Successfully opened dynamic library libcuda.so.1
#2019-07-04 16:13:18.000725: E tensorflow/stream_executor/cuda/cuda_driver.cc:318] failed call to cuInit: CUDA_ERROR_NO_DEVICE: no CUDA-capable device is detected
#2019-07-04 16:13:18.000834: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:169] retrieving CUDA diagnostic information for host: node279
#2019-07-04 16:13:18.000882: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:176] hostname: node279
#2019-07-04 16:13:18.000989: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:200] libcuda reported version is: 384.111.0
#2019-07-04 16:13:18.001078: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:204] kernel reported version is: 384.111.0
#2019-07-04 16:13:18.001123: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:310] kernel version seems to match DSO: 384.111.0
#2019-07-04 16:13:18.003889: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2194895000 Hz
#2019-07-04 16:13:18.004075: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x40aa120 executing computations on platform Host. Devices:
#2019-07-04 16:13:18.004095: I tensorflow/compiler/xla/service/service.cc:175]   StreamExecutor device (0): <undefined>, <undefined>
#Device mapping:
#/job:localhost/replica:0/task:0/device:XLA_CPU:0 -> device: XLA_CPU device
#2019-07-04 16:13:18.004277: I tensorflow/core/common_runtime/direct_session.cc:296] Device mapping:
#/job:localhost/replica:0/task:0/device:XLA_CPU:0 -> device: XLA_CPU device
#
#nvidia-smi
#No devices were found

#Attempt 4
# Same as attempt 3 but with sbatch - works, but no cuda 10 at the moment on cluster

#Attempt 5
# Try TF 1.12.0, works, but stuff not supported:
#, line 388, in fmin                                                     [9/1986]
#    show_progressbar=show_progressbar,
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/base.py"
#, line 639, in fmin
#    show_progressbar=show_progressbar)
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/fmin.py"
#, line 407, in fmin
#    rval.exhaust()
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/fmin.py"
#, line 262, in exhaust
#    self.run(self.max_evals - n_done, block_until_done=self.asynchronous)
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/fmin.py$, line 227, in run
#    self.serial_evaluate()
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/fmin.py$, line 141, in serial_evaluate
#    result = self.domain.evaluate(spec, ctrl)
#  File "/rigel/home/jrm2263/.local/lib/python3.5/site-packages/hyperopt/base.py$, line 844, in evaluate
#    rval = self.fn(pyll_rval)
#  File "/rigel/dsi/users/jrm2263/Local/gitprojects/py_bcg_dev/py_bcg/temp_model$py", line 320, in keras_fmin_fnct
#TypeError: __init__() got an unexpected keyword argument 'restore_best_weights'
#^C

# Attempt 6 - works?
#srun --pty -t 0-02:00:00 --gres=gpu:1 -A dsi /bin/bash
#module load cuda10.0/toolkit cuda10.0/blas
# module load cudnn/cuda_10.0_v7.6.2 # guess this is temporary!
## or cudnn 7.0.5??
#module load anaconda
#TMPDIR=/rigel/dsi/users/jrm2263/temp_piptf/ pip install --cache-dir=/rigel/dsi/users/jrm2263/temp_piptf/ --build /rigel/dsi/users/jrm2263/temp_piptf/ --user --upgrade tensorflow-gpu
#>>> import tensorflow as tf
#>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
# nvidia-smi
