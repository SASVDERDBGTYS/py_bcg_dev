#!/bin/sh
#
#SBATCH --account=dsi # The account name for the job.
#SBATCH --job-name=s00 # The job name.
#SBATCH -c 3 # The number of cpu cores to use.
#SBATCH --time=12:00:00 # The time the job will take to run.
#SBATCH --mem-per-cpu=4gb # The memory the job will use per cpu core.
#SBATCH --gres=gpu
#SBATCH --constraint=p100

#Attempt 1
# couldn't install in the right dir - maybe this will work if I can get cuit to change my home dir
#module load cuda80/toolkit cuda80/blas cudnn/5.1
#module load anaconda/3-5.1
#pip install numpy scipy seaborn mne tensorflow-gpu hyperas --ignore-installed --target=/rigel/dsi/users/jrm2263/pip_local
#pip install numpy scipy seaborn mne tensorflow-gpu hyperas --ignore-installed --install-option="--prefix=/rigel/dsi/users/jrm2263/pip_local"
#pip install numpy scipy seaborn mne hyperas

#paths:
#/home/mcintosh/Local/working_eegbcg
#/home/mcintosh/Habanero/Local/working_eegbcg/
#
#/home/mcintosh/Local/gitprojects/py_bcg_dev
#/home/mcintosh/Habanero/Local/gitprojects/py_bcg_dev

#Attempt 2
#Tried to use singularity, but in the end tf doesn't see the gpu
#module load singularity
#module load cuda90/toolkit cuda90/blas cudnn/7.0.5
#export SINGULARITY_CACHEDIR=/rigel/dsi/users/jrm2263/sing/
#export SINGULARITY_PULLFOLDER=/rigel/dsi/users/jrm2263/sing/
#singularity pull docker://tensorflow/tensorflow:latest-gpu-py3
#singularity shell --nv --writable-tmpfs tensorflow.simg
#pip install --user numpy scipy seaborn mne hyperas
#for testing: srun --pty -t 0-02:00:00 --gres=gpu:1 -A dsi /bin/bash
#but then ... Status: CUDA driver version is insufficient for CUDA runtime version
# the driver version is 384.111, which makes sense with cuda90 - so probably the docker version is too new