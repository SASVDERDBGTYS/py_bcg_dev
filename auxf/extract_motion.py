from pathlib import Path
import re
from shutil import copyfile

from_local = True

if from_local:
    # %% FOLLOWING IS FOR LOCAL pre-prepared by Linbi

    d_proc = Path('/home/mcintosh/Cloud/DataPort/190327_eeg_fmri_linbi/proc')
    d_data = Path('/home/mcintosh/Cloud/DataPort/190327_eeg_fmri_linbi/proc_linbi/MC_files')
    par_file = d_data.rglob('??????_Sub??/?/mc/*prefiltered_func_data_mcf.par')
    par_file = [a for a in par_file]

    for ix_run in range(len(par_file)):
        f_source = par_file[ix_run]

        regex = re.compile(r'Sub(.*)\/(.*)\/mc')
        _, ix_sub, ix_run, _ = regex.split(str(f_source))

        print(f_source)
        print(ix_sub, ix_run)

        d_out = d_proc / 'proc_mcf/sub{:02d}'.format(int(ix_sub))
        d_out.mkdir(parents=True, exist_ok=True)

        f_target = d_out / 'sub{:02d}_r{:02d}_prefiltered_func_data_mcf.par'.format(int(ix_sub), int(ix_run))

        copyfile(f_source, f_target)
else:
    # %% FOLLOWING IS FOR EINSTEIN
    d_hopfield = Path('/home/mcintosh/einstein/Volumes/Hopfield')
    d_mbbi = Path('linbi/LIINC_After_Apr_2016/A4_Simultaneous_Pupil_EEG_fMRI/Data_Analysis/A4_fMRI_Analyses/MBBI')
    d_data = d_hopfield / d_mbbi
    # d_data = Path('/home/mcintosh/Cloud/DataPort/190327_eeg_fmri_linbi/temp/')
    d_proc = Path('/home/mcintosh/Cloud/DataPort/190327_eeg_fmri_linbi/proc')

    par_file = d_data.rglob('??????_Sub??/Run?.feat/mc/*prefiltered_func_data_mcf.par')
    par_file = [a for a in par_file]
    for ix_run in range(len(par_file)):

        f_source = par_file[ix_run]

        if 'Subs_to_be_discarded' in str(f_source):
            continue

        regex = re.compile(r'Sub(.*)\/Run(.*)\.feat')
        _, ix_sub, ix_run, _ = regex.split(str(f_source))

        print(f_source)
        print(ix_sub, ix_run)

        d_out = d_proc / 'proc_mcf/sub{:02d}'.format(int(ix_sub))
        d_out.mkdir(parents=True, exist_ok=True)

        f_target = d_out / 'sub{:02d}_r{:02d}_prefiltered_func_data_mcf.par'.format(int(ix_sub), int(ix_run))

        copyfile(f_source, f_target)
