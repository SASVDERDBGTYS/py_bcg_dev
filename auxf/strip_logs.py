from pathlib import Path
import numpy as np
from pprint import pprint
import re
import pandas as pd

path_log = Path(r'/home/mcintosh/Local/working_eegbcg/log')
pprint([str(x) for x in path_log.glob('*') if x.is_file()])

# filename = path_log / 'gru_arch_general44.log'

filename = path_log / 'as' / 'gru_arch_refined01_as.log'

filename = path_log / 'realtime_as' / 'gru_arch_realtime03.log'

with open(str(filename)) as f:
    content = f.read()

if filename.parts[-2] == 'realtime_as':
    print(filename.parts[-1])
    x = re.findall('Test Score (sub[0-9][0-9]) r(\d\d): (.+) RMSE', content)
    x = pd.DataFrame(x)
    x.columns = ['str_sub', 'ix_run', 'rmse']
    x = x.astype({'str_sub': 'category'})
    x = x.astype({'ix_run': 'int'})
    x = x.astype({'rmse': 'float'})
    y = x.groupby('str_sub').mean()
    y.drop('ix_run', axis=1)
    pprint(x)
    pprint(y)
    x.groupby('str_sub')['rmse'].plot()


elif filename.parts[-2] == 'as':
    print(filename.parts[-1])
    x = re.findall('(sub[0-9][0-9])_pre_training: ([0-9]+\.[0-9]+) RMSE', content)
    print('Pre')
    pprint(x)

    x = re.findall('(sub[0-9][0-9])_post_training: ([0-9]+\.[0-9]+) RMSE', content)
    print('Post')
    pprint(x)
else:
    x = re.findall('Test Score: (.+) RMSE', content)
    pprint(np.array(x).reshape(-1, 1))






