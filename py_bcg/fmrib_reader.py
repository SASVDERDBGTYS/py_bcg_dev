import set_env
import bcg_net
from epoch_rejection import single_subject_mabs
from pathlib import Path
import mne
import matplotlib.pyplot as plt
import numpy as np
import pickle
import copy
import gc
import contextlib


@ contextlib.contextmanager
def temp_seed(seed):
    state = np.random.get_state()
    np.random.seed(seed)
    try:
        yield
    finally:
        np.random.set_state(state)


def test_opt(opt):
    if opt is None:
        opt = bcg_net.TrainDefault()
        opt.epochs = 2500
        opt.ga_mc = False
        opt.p_arch = None
        opt.f_arch = None
        opt.target_ch = None
        opt.es_min_delta = 1e-5
        opt.es_patience = 25  # How many times does the validation not increase
        opt.early_stopping = True
        opt.resume = False
        opt.validation = 0.15
        opt.evaluation = 0.85
        opt.fig_num = 63
        opt.multi_ch = True
        opt.multi_run = True
        opt.multi_sub = False
        opt.use_rs_data = True
    elif isinstance(opt, dict):
        assert False, 'need to add this option?'

    return opt


if __name__ == '__main__':
    opt_user = test_opt(None)
    run_id_global = [1, 2, 3, 4, 5]

    if not opt_user.multi_run and not opt_user.multi_sub:
        p_out = Path.home() / 'Downloads'
        f_out = 'fmrib_rmse'

        for i in np.arange(11, 35):
            if i in [13, 15, 16, 17, 18, 19, 23, 24, 25, 26, 27, 28, 31]:
                continue
            else:
                str_sub = 'sub{}'.format(i)
                for run_id in np.arange(1, 6):
                    p_rs, f_rs = set_env.rs_path(str_sub, run_id)
                    p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)


                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, Epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = bcg_net.preprocessor_rs(
                        data_dir, opt_user.target_ch, opt=opt_user)

                    ecg_stats = [ecg_mean, ecg_std]
                    eeg_stats = [eeg_mean, eeg_std]

                    with temp_seed(1997):
                        x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test = bcg_net.generate_train_valid_test(Epoched_data, opt_user)

                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                    ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
                    if not opt_user.target_ch:
                        target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)

                    ecg_data = fmrib_raw.get_data()[ecg_ch, :]
                    eeg_data = fmrib_raw.get_data()[target_ch, :]
                    info = fmrib_raw.info

                    normalized_ecg_data = (ecg_data - ecg_stats[0]) / ecg_stats[1]
                    normalized_eeg_data = np.zeros(eeg_data.shape)
                    for i in range(eeg_data.shape[0]):
                        ds = eeg_data[i, :] - eeg_stats[0][i]
                        ds /= eeg_stats[1][i]
                        normalized_eeg_data[i, :] = ds

                    data = np.insert(normalized_eeg_data, ecg_ch, normalized_ecg_data, axis=0)
                    normalized_fmrib = mne.io.RawArray(data, info)

                    fmrib_epoched_data = bcg_net.dataset_epoch(dataset=normalized_fmrib, duration=3, epoch_rejection=False, good_ix=good_ix)
                    EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
                    EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_ix_slice_test, :, :], axes=(1, 0, 2))

                    test_score = np.sqrt(np.square(EEG_fmrib_test).mean(axis=(1, 2)))
                    rmse_overall = np.mean(test_score)

                    with open(p_out / f_out, 'a+') as handle:
                        handle.write('{:.3f}\n'.format(rmse_overall))
                    print('nothing')

    elif opt_user.multi_run and not opt_user.multi_sub:
        p_out = Path.home() / 'Downloads'
        f_out = 'fmrib_rmse'

        for k in np.arange(11, 35):
            if k in [13, 15, 16, 17, 18, 19, 23, 24, 25, 26, 27, 28, 31]:
                continue
            else:
                str_sub = 'sub{}'.format(k)
                run_id = run_id_global

                vec_normalized_raw = []
                vec_epoched_data = []
                vec_ecg_stats = []
                vec_eeg_stats = []
                vec_good_ix = []
                vec_fmrib_normalized = []

                for i in range(len(run_id)):
                    p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
                    p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
                    p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = bcg_net.preprocessor_rs(
                        data_dir, opt_user.target_ch, opt=opt_user)

                    ecg_stats = [ecg_mean, ecg_std]
                    eeg_stats = [eeg_mean, eeg_std]

                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                    ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
                    if not opt_user.target_ch:
                        target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)

                    ecg_data = fmrib_raw.get_data()[ecg_ch, :]
                    eeg_data = fmrib_raw.get_data()[target_ch, :]
                    info = fmrib_raw.info

                    normalized_ecg_data = (ecg_data - ecg_stats[0]) / ecg_stats[1]
                    normalized_eeg_data = np.zeros(eeg_data.shape)
                    for i in range(eeg_data.shape[0]):
                        ds = eeg_data[i, :] - eeg_stats[0][i]
                        ds /= eeg_stats[1][i]
                        normalized_eeg_data[i, :] = ds

                    data = np.insert(normalized_eeg_data, ecg_ch, normalized_ecg_data, axis=0)
                    normalized_fmrib = mne.io.RawArray(data, info)

                    normalized_fmrib_epoched_data = bcg_net.dataset_epoch(dataset=normalized_fmrib, duration=3, epoch_rejection=False, good_ix=good_ix)
                    normalized_fmrib_epochs = mne.EpochsArray(normalized_fmrib_epoched_data, info)

                    vec_normalized_raw.append(normalized_raw)
                    vec_epoched_data.append(epoched_data)
                    vec_ecg_stats.append(ecg_stats)
                    vec_eeg_stats.append(eeg_stats)
                    vec_good_ix.append(good_ix)
                    vec_fmrib_normalized.append(normalized_fmrib_epochs)

                epoched_data_combined = None
                for i in range(len(vec_epoched_data)):
                    if i == 0:
                        epoched_data_combined = vec_epoched_data[i]
                    else:
                        epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data[i]])

                with temp_seed(1997):
                    x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test = bcg_net.generate_train_valid_test(
                    epoched_data_combined, opt_user)


                epoched_fmrib_combined = None
                for i in range(len(vec_fmrib_normalized)):
                    if i == 0:
                        epoched_fmrib_combined = vec_fmrib_normalized[i]
                    else:
                        epoched_fmrib_combined = mne.concatenate_epochs([epoched_fmrib_combined, vec_fmrib_normalized[i]])

                ecg_ch = 32
                if not opt_user.target_ch:
                    target_ch = np.delete(np.arange(0, 64, 1), ecg_ch)
                epoched_fmrib_combined_data = epoched_fmrib_combined.get_data()
                eeg_fmrib_epochs = epoched_fmrib_combined_data[:, target_ch, :]
                EEG_fmrib_test = np.transpose(eeg_fmrib_epochs[vec_ix_slice_test, :, :], axes=(1, 0, 2))

                test_score = np.sqrt(np.square(EEG_fmrib_test).mean(axis=(1, 2)))
                rmse_overall = np.mean(test_score)

                with open(p_out / f_out, 'a+') as handle:
                    handle.write('{:.3f}\n'.format(rmse_overall))
                print('nothing')

                # vec_n_events = []
                # for i in range(len(vec_epoched_data)):
                #     n_events = len(vec_epoched_data[i].get_data())
                #     vec_n_events.append(n_events)
                # vec_n_events_cum = np.cumsum(vec_n_events)
                #
                # vec_run_id = []
                # for i in range(len(vec_ix_slice_test)):
                #     if vec_ix_slice_test[i] < vec_n_events_cum[0]:
                #         vec_run_id.append(1)
                #     else:
                #         for j in range(1, len(vec_good_ix)):
                #             if not vec_ix_slice_test[i] < vec_n_events_cum[j - 1] and vec_ix_slice_test[i] < vec_n_events_cum[j]:
                #                 vec_run_id.append(j + 1)
                #                 break
                # vec_run_id = np.array(vec_run_id)
                #
                # for j in np.unique(vec_run_id):
                #     vec_relative_ix = np.where(vec_run_id == j)[0]