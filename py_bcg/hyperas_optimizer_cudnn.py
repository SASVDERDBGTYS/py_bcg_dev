from __future__ import print_function
from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform
from pathlib import Path
import numpy as np
import os
import pickle
import mne
import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras import layers, callbacks
from tensorflow.python.keras import backend as K
from tensorflow.python.keras import regularizers
from functools import partial
import datetime
import contextlib
import gc
import set_env
from epoch_rejection import single_subject_mabs
from collections import namedtuple


def user_defined_opt(opt):
    if opt is None:
        opt = namedtuple('opt', ['epochs', 'str_sub', 'run_ix', 'es_min_delta',
                                 'es_patience', 'early_stopping', 'validation', 'evaluation',
                                 'multi_run', 'trials_step', 'total_evals', 'trials_step_initial'])
        opt.epochs = 2500  # 2500
        opt.str_sub = 'sub12'
        opt.run_ix = 1
        opt.es_min_delta = 1e-5
        opt.es_patience = 25  # How many times does the validation not increase
        opt.early_stopping = True
        opt.validation = 0.18
        opt.evaluation = 0.85
        opt.multi_run = False
        opt.trials_step = 50  # 50
        opt.total_evals = 800  # 800
        opt.trials_step_initial = 50  # 50
    else:
        raise Exception('Case not handled.')
    return opt


@ contextlib.contextmanager
def temp_seed(seed):
    state = np.random.get_state()
    np.random.seed(seed)
    try:
        yield
    finally:
        np.random.set_state(state)


def split_evaluation_test(epoched_data, per_evaluation):
    vec_ix = np.random.permutation(len(epoched_data))
    vec_ix_cutoff = int(np.round(len(epoched_data) * per_evaluation))
    vec_ix_slice_evaluation = vec_ix[:vec_ix_cutoff]
    epochs_evaluation = np.zeros(tuple([vec_ix_cutoff]) + epoched_data.shape[1:])
    for ix in vec_ix_slice_evaluation:
        epochs_evaluation[np.where(vec_ix_slice_evaluation == ix), :, :] = epoched_data[ix, :, :]

    vec_ix_slice_test = vec_ix[vec_ix_cutoff:]
    epochs_test = np.zeros(tuple([len(epoched_data) - vec_ix_cutoff]) + epoched_data.shape[1:])
    for ix in vec_ix_slice_test:
         epochs_test[np.where(vec_ix_slice_test == ix), :, :] = epoched_data[ix, :, :]

    return epochs_evaluation, epochs_test, vec_ix_slice_test


def split_train_validation(epochs_evaluation, es_validation):
    vec_ix = np.random.permutation(len(epochs_evaluation))
    vec_ix_cutoff = int(np.round(len(epochs_evaluation) * es_validation))
    vec_ix_slice_train = vec_ix[vec_ix_cutoff:]
    epochs_train = np.zeros(tuple([len(epochs_evaluation) - vec_ix_cutoff]) + epochs_evaluation.shape[1:])
    for ix in vec_ix_slice_train:
        epochs_train[np.where(vec_ix_slice_train == ix), :, :] = epochs_evaluation[ix, :, :]

    vec_ix_slice_val = vec_ix[:vec_ix_cutoff]
    epochs_validation = np.zeros(tuple([vec_ix_cutoff]) + epochs_evaluation.shape[1:])
    for ix in vec_ix_slice_val:
        epochs_validation[np.where(vec_ix_slice_val == ix), :, :] = epochs_evaluation[ix, :, :]
    return epochs_train, epochs_validation


def data():
    opt_local = user_defined_opt(None)
    str_sub = opt_local.str_sub
    run_id = opt_local.run_ix

    p_rs, f_rs = set_env.rs_path(str_sub, run_id)

    fs = 100

    data_dir = str(p_rs.joinpath(f_rs))
    rs_added_raw = mne.io.read_raw_eeglab(data_dir, preload=True, stim_channel=False)
    rs_added_raw.resample(fs)
    rs_removed_raw = rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    data = rs_removed_raw.get_data()
    info = rs_removed_raw.info
    normalizedData = np.zeros(data.shape)
    for i in range(data.shape[0]):
        ds = data[i, :] - np.mean(data[i, :])
        ds /= np.std(ds)
        normalizedData[i, :] = ds

    normalized_raw = mne.io.RawArray(normalizedData, info)

    duration = 3
    total_time_stamps = rs_removed_raw.get_data().shape[1]
    constructed_events =np.zeros(shape=(int(np.floor(total_time_stamps/fs)/duration), 3), dtype=int)

    for i in range(0, int(np.floor(total_time_stamps/fs))-duration, duration):
        ix = i/duration
        constructed_events[int(ix)] = np.array([i*fs, 0, 1])

    tmax = duration - 1/fs
    old_epoched_data = mne.Epochs(normalized_raw, constructed_events, tmin=0, tmax=tmax)

    threshold = 5
    ix = single_subject_mabs(rs_added_raw, threshold)
    good_ix = np.delete(np.arange(0, old_epoched_data.get_data().shape[0], 1), ix)
    good_data = old_epoched_data.get_data()[good_ix, :, :]
    Epoched_data = mne.EpochsArray(good_data, old_epoched_data.info)

    normalizedData = Epoched_data.get_data()
    ecg_ch = Epoched_data.info['ch_names'].index('ECG')

    num_epochs = normalizedData.shape[0]
    batch_size = normalizedData.shape[2]

    with temp_seed(1):
        s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
        ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
        s_ev_train, s_ev_va = split_train_validation(s_ev, opt_local.validation)

    x_train = s_ev_train[:, ecg_ch, :].reshape(
        ev_epoch_num - int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
    x_validation = s_ev_va[:, ecg_ch, :].reshape(int(np.round(ev_epoch_num * opt_local.validation)),
                                                    batch_size)
    x_test = s_test[:, ecg_ch, :].reshape(num_epochs - ev_epoch_num, batch_size)

    y_train = np.transpose(np.delete(s_ev_train, ecg_ch, axis=1), axes=(1, 0, 2))
    y_validation = np.transpose(np.delete(s_ev_va, ecg_ch, axis=1), axes=(1, 0, 2))
    y_test = np.transpose(np.delete(s_test, ecg_ch, axis=1), axes=(1, 0, 2))

    return x_train, y_train, x_validation, y_validation, x_test, y_test


def create_model(x_train, y_train, x_validation, y_validation, x_test, y_test):

    print('\n\n\nStarted model creation')
    print("The start time is {}/{}/{} {}:{}:{}\n\n\n".format(datetime.datetime.now().strftime("%Y"),
                                                       datetime.datetime.now().strftime("%m"),
                        datetime.datetime.now().strftime("%d"), datetime.datetime.now().strftime("%H"),
                           datetime.datetime.now().strftime("%M"), datetime.datetime.now().strftime("%S")))

    starttime = datetime.datetime.now()
    opt_user = user_defined_opt(None)
    model_gru = Sequential()
    model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={{choice([2, 4, 8, 16, 32])}}, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2({{uniform(0, 0.1)}}),
                                                       activity_regularizer=regularizers.l2({{uniform(0, 0.1)}})),
                                       input_shape=(None, 1)))

    if {{choice(['two_on', 'two_off'])}} == 'two_on':
        model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={{choice([2, 4, 8, 16, 32])}}, return_sequences=True,
                                                           recurrent_regularizer=regularizers.l2({{uniform(0, 0.1)}}),
                                                           activity_regularizer=regularizers.l2({{uniform(0, 0.1)}}))))

    if {{choice(['three_on', 'three_off'])}} == 'three_on':
        model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={{choice([2, 4, 8, 16, 32])}}, return_sequences=True,
                                                           recurrent_regularizer=regularizers.l2({{uniform(0, 0.1)}}),
                                                           activity_regularizer=regularizers.l2({{uniform(0, 0.1)}}))))

    if {{choice(['four_on', 'four_off'])}} == 'four_on':
        model_gru.add(
            layers.Dense(units={{choice([2, 4, 8, 16, 32])}}, activation='relu'))
        model_gru.add(layers.Dropout({{uniform(0, 0.5)}}))

    if {{choice(['five_on', 'five_off'])}} == 'five_on':
        model_gru.add(
            layers.Dense(units={{choice([2, 4, 8, 16, 32])}}, activation='relu'))
        model_gru.add(layers.Dropout({{uniform(0, 0.5)}}))

    if {{choice(['six_on', 'six_off'])}} == 'six_on':
        model_gru.add(
            layers.Dense(units={{choice([2, 4, 8, 16, 32])}}, activation='relu'))
        model_gru.add(layers.Dropout({{uniform(0, 0.5)}}))

    if {{choice(['seven_on', 'seven_off'])}} == 'seven_on':
        model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={{choice([2, 4, 8, 16, 32])}}, return_sequences=True,
                                                           recurrent_regularizer=regularizers.l2({{uniform(0, 0.1)}}),
                                                           activity_regularizer=regularizers.l2({{uniform(0, 0.1)}}))))

    if {{choice(['final_gru_on', 'final_gru_off'])}} == 'final_gru_on':
        model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={{choice([2, 4, 8, 16, 32, 64])}},
                                                           return_sequences=True,
                                                           recurrent_regularizer=regularizers.l2({{uniform(0, 0.1)}}),
                                                           activity_regularizer=regularizers.l2({{uniform(0, 0.1)}}))))

    model_gru.add(layers.Dense(units=63, activation='linear'))

    model_gru.compile(loss='mean_squared_error', optimizer='adam')

    def train_generator():
        ix = 0
        vec_ix = np.random.permutation(len(x_train))
        while True:
            xs = x_train[vec_ix[ix], :].reshape(1, -1, 1)
            ys = np.transpose(y_train[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            ix = ix % len(x_train)
            yield xs, ys

    def validation_generator():
        ix = 0
        vec_ix = np.random.permutation(len(x_validation))
        while True:
            xs = x_validation[vec_ix[ix], :].reshape(1, -1, 1)
            ys = np.transpose(y_validation[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            ix = ix % len(x_validation)
            yield xs, ys

    def evaluation_generator():
        ix = 0
        vec_ix = np.random.permutation(len(x_test))
        while True:
            xs = x_test[vec_ix[ix], :].reshape(1, -1, 1)
            ys = np.transpose(y_test[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            ix = ix % len(x_test)
            yield xs, ys

    steps_per_epoch_train = x_train.shape[0]
    steps_per_epoch_validation = x_validation.shape[0]
    print(opt_user.es_min_delta)
    callbacks_ = [callbacks.EarlyStopping(monitor='val_loss', min_delta=opt_user.es_min_delta,
                                          patience=opt_user.es_patience, verbose=0, mode='min',
                                          restore_best_weights=True)]

    model_gru.fit_generator(train_generator(), steps_per_epoch=steps_per_epoch_train,
                            epochs=opt_user.epochs, verbose=2,
                            validation_data=validation_generator(),
                            validation_steps=steps_per_epoch_validation,
                            callbacks=callbacks_)

    score = model_gru.evaluate_generator(evaluation_generator(), steps=len(x_test))

    if not np.isfinite(score):
        score = 20

    K.clear_session()
    gc.collect()

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    duration_in_s = duration.total_seconds()
    hours = round(divmod(duration_in_s, 3600)[0])
    if hours == 0:
        minutes = round(divmod(duration_in_s, 60)[0])
        seconds = round(divmod(duration_in_s, 60)[1])
    else:
        minutes = round(divmod(divmod(duration_in_s, 3600)[1], 60)[0])
        seconds = round(divmod(divmod(duration_in_s, 3600)[1], 60)[1])

    eval_time = (hours, minutes, seconds)

    print('\nTest score: {}\n'.format(score))
    print('\n\n\nFinished model creation')
    print("The start time is {}/{}/{} {}:{}:{}\n\n\n".format(datetime.datetime.now().strftime("%Y"),
                                                       datetime.datetime.now().strftime("%m"),
                        datetime.datetime.now().strftime("%d"), datetime.datetime.now().strftime("%H"),
                           datetime.datetime.now().strftime("%M"), datetime.datetime.now().strftime("%S")))

    return {'loss': score, 'status': STATUS_OK, 'eval_time': eval_time}


if __name__ == '__main__':

    def run_trials():
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        opt_local = user_defined_opt(None)
        str_sub = opt_local.str_sub
        run_id = opt_local.run_ix
        trials_step = opt_local.trials_step

        p_hyperas, f_hyperas, f_model = set_env.hyperas_path(str_sub, run_id)

        pf_hyperas = p_hyperas / f_hyperas
        if pf_hyperas.exists():
            trials = pickle.load(open(p_hyperas / f_hyperas, 'rb'))
            assert len(trials.trials) >= opt_local.trials_step_initial, "Maybe you are resuming from a test run?"
            max_evals = len(trials.trials) + trials_step
            print('Continuing from {} to {} trials'.format(len(trials.trials), len(trials.trials) + trials_step))
        else:
            max_evals = opt_local.trials_step_initial
            trials = Trials()
            print('Starting new trials...')
        n_startup_jobs = opt_local.trials_step_initial - 1

        if max_evals > opt_local.total_evals:
            exit()

        print('Started optimization')
        print("The start time is {}\n\n".format(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")))

        best_run = optim.minimize(
            model=create_model,
            data=data,
            algo=partial(tpe.suggest, n_startup_jobs=n_startup_jobs),
            functions=[user_defined_opt, split_evaluation_test, split_train_validation, temp_seed],
            max_evals=max_evals,
            trials=trials,
            keep_temp=False,
            eval_space=True
        )

        print('\n\n\nFinished optimization')
        print("The time is {}\n\n".format(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")))

        print('The best model is {}'.format(best_run))

        p_hyperas.mkdir(parents=True, exist_ok=True)

        with open(p_hyperas / f_hyperas, "wb") as f:
            pickle.dump(trials, f)

        with open(p_hyperas / f_model, 'wb') as handle:
            pickle.dump(best_run, handle)

        pf_hyperas_temp = p_hyperas.joinpath('temp')
        pf_hyperas_temp.mkdir(parents=True, exist_ok=True)
        with open(pf_hyperas_temp / '{}_{}.hyperopt'.format(f_hyperas.split('.')[0], datetime.datetime.now().strftime("%Y%m%d%H%M%S")), 'wb') as handle:
            pickle.dump(trials, handle)

        print('\n\n\nContinuing from {} trials to {} (+{}) trials'.format(len(trials.trials), max_evals, trials_step))

    while True:
        run_trials()
