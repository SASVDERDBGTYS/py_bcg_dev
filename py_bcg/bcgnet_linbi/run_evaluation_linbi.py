from set_env_linbi import *
from sp.sp_preprocessing import preprocessing, dataset_epoch
from sp.sp_split_dataset import *
from training.training import *
from prediction.predict import *
from io_ops.save_load_model import *
from io_ops.save_data import *
from io_ops.load_dataset import *
from utils.plotting import *
from utils.compute_rms import *
from utils.interpolate_dataset import *
from options import test_opt


# TODO: note that this script doesn't work anymore...
def run_evaluation_single_sub(str_sub, run_id, str_arch, f_arch, num_training_epoch):
    """


    :param str_sub:
    :param run_id:
    :param str_arch:
    :param num_training_epoch:
    :return:
    """

    """
    Setup
    """
    # Tensorflow session configuration
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)

    # Initialize the options object
    opt_user = test_opt(None)

    # Note that the input prompted in the terminal could be a list
    if isinstance(str_sub, list):
        str_sub = str_sub[0]

    if isinstance(run_id, list):
        run_id = run_id[0]

    # Path setup
    p_rs, f_rs = rs_path(str_sub, run_id)
    p_obs, f_obs = obs_path(str_sub, run_id)
    p_bcgnet, f_bcgnet = bcgnet_path(str_sub, run_id, str_arch, opt=opt_user)

    pfe_rs = str(p_rs / f_rs)
    pfe_obs = str(p_obs / f_obs)
    pfe_bcgnet = str(p_bcgnet / f_bcgnet)

    """
    Preparing the dataset
    """
    # Load, normalize and epoch the raw dataset
    normalized_epoched_raw_dataset, normalized_raw_dataset, epoched_raw_dataset, raw_dataset, \
        orig_sr_epoched_raw_dataset, orig_sr_raw_dataset, \
        ecg_stats, eeg_stats, good_idx = preprocessing(pfe_rs, duration=opt_user.epoch_duration,
                                                       threshold=opt_user.mad_threshold,
                                                       n_downsampling=opt_user.n_downsampling)

    orig_sr_epoched_raw_dataset = dataset_epoch(dataset=orig_sr_raw_dataset,
                                                duration=opt_user.epoch_duration, epoch_rejection=False,
                                                good_idx=good_idx)

    # Load and epoch the OBS-cleaned dataset
    orig_sr_epoched_obs_dataset, orig_sr_obs_dataset, _ = load_obs_dataset(pfe_obs, opt_user.epoch_duration,
                                                                           1, good_idx)

    # Load and epoch the BCGNET-cleaned dataset
    orig_sr_epoched_bcgnet_dataset, orig_sr_bcgnet_dataset, _ = load_bcgnet_dataset(pfe_bcgnet,
                                                                                    opt_user.epoch_duration,
                                                                                    1, good_idx)

    # Generate the train, validation and test sets and also obtain the index of epochs used in the validation
    # and test set
    xs, ys, vec_ix_slice = generate_train_valid_test(normalized_epoched_raw_dataset, opt=opt_user)

    """
    Load the model
    """

    # Load the trained model
    p_arch = arch_path(str_sub, run_id, str_arch, f_arch, num_training_epoch)
    model = load_model(f_arch, p_arch, str_arch)

    callbacks_ = get_callbacks_rnn(opt_user)

    """
    Predict using the loaded model
    """

    # Predict the cleaned dataset and epoch it for comparison later
    epoched_cleaned_dataset, cleaned_dataset = predict_time_series(model, callbacks_,
                                                                   normalized_raw_dataset, raw_dataset,
                                                                   ecg_stats, eeg_stats,
                                                                   opt_user.epoch_duration, good_idx)

    interpolated_cleaned_dataset = interpolate_raw_dataset(cleaned_dataset, orig_sr_raw_dataset)
    orig_sr_epoched_cleaned_dataset = dataset_epoch(dataset=interpolated_cleaned_dataset,
                                                    duration=opt_user.epoch_duration, epoch_rejection=False,
                                                    good_idx=good_idx)

    # Obtain the equivalent test epochs used during training from raw, OBS-cleaned and BCGNet-cleaned data
    vec_orig_sr_epoched_raw_dataset = split_epoched_dataset(orig_sr_epoched_raw_dataset, vec_ix_slice)
    vec_orig_sr_epoched_obs_dataset = split_epoched_dataset(orig_sr_epoched_obs_dataset, vec_ix_slice)
    vec_orig_sr_epoched_bcgnet_dataset = split_epoched_dataset(orig_sr_epoched_bcgnet_dataset, vec_ix_slice)
    vec_orig_sr_epoched_cleaned_dataset = split_epoched_dataset(orig_sr_epoched_cleaned_dataset, vec_ix_slice)

    orig_sr_epoched_raw_dataset_test = vec_orig_sr_epoched_raw_dataset[-1]
    orig_sr_epoched_obs_dataset_test = vec_orig_sr_epoched_obs_dataset[-1]
    orig_sr_epoched_bcgnet_dataset_test = vec_orig_sr_epoched_bcgnet_dataset[-1]
    orig_sr_epoched_cleaned_dataset_test = vec_orig_sr_epoched_cleaned_dataset[-1]

    """
    Verify validity
    """

    # if not np.allclose(orig_sr_epoched_bcgnet_dataset_test.get_data(), orig_sr_epoched_cleaned_dataset_test.get_data()):
    #     raise Exception('Not all close')

    """
    Saving file
    """
    p_test_epochs, f_test_epochs = test_epochs_path(str_sub, run_id, str_arch, opt_user)

    orig_sr_epoched_raw_data = orig_sr_epoched_raw_dataset.get_data()
    orig_sr_epoched_obs_data = orig_sr_epoched_obs_dataset.get_data()
    orig_sr_epoched_bcgnet_data = orig_sr_epoched_bcgnet_dataset.get_data()

    info = orig_sr_epoched_obs_dataset.info
    savemat(p_test_epochs / f_test_epochs,
            mdict={'met_obs': orig_sr_epoched_obs_data,
                   'met_net': orig_sr_epoched_bcgnet_data,
                   'met_gar': orig_sr_epoched_raw_data,
                   'ix_slice_test_py': vec_ix_slice[-1],
                   'srate': info['sfreq'],
                   'ch_names': info['ch_names']})


if __name__ == '__main__':
    vec_str_sub = ['sub32', 'sub35', 'sub36', 'sub37',
                   'sub38', 'sub39', 'sub40', 'sub42',
                   'sub43', 'sub44', 'sub45', 'sub46',
                   'sub47', 'sub48', 'sub49', 'sub50']

    run_id = 0
    str_arch = 'gru_arch_general4'
    vec_f_arch = ['gru_arch_general4_20200131013448',
                  'gru_arch_general4_20200131014335',
                  'gru_arch_general4_20200131015232',
                  'gru_arch_general4_20200131020050',
                  'gru_arch_general4_20200131021204',
                  'gru_arch_general4_20200131022146',
                  'gru_arch_general4_20200131023110',
                  'gru_arch_general4_20200131024018',
                  'gru_arch_general4_20200131025132',
                  'gru_arch_general4_20200131025933',
                  'gru_arch_general4_20200131031049',
                  'gru_arch_general4_20200131032103',
                  'gru_arch_general4_20200131032838',
                  'gru_arch_general4_20200131033715',
                  'gru_arch_general4_20200131035010',
                  'gru_arch_general4_20200131035826']

    vec_num_training_epoch = [63, 44, 44, 41,
                              56, 48, 49, 45,
                              55, 39, 58, 52,
                              38, 44, 66, 40]

    for i in range(len(vec_str_sub)):
        str_sub = vec_str_sub[i]
        f_arch = vec_f_arch[i]
        num_training_epoch = vec_num_training_epoch[i]

        run_evaluation_single_sub(str_sub, run_id, str_arch, f_arch, num_training_epoch)
