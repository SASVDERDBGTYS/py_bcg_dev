from pathlib import Path
import numpy as np
import re


def module_exists(module_name):
    """
    Using dnc_set_env to overwrite the current set_env file

    :param module_name: name of the dnc_set_env file
    :return: nothing
    """

    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True


# Configure the directory for loading the data and the name of the experiment and type of architecture
home = Path.home()
p_data = home / 'Local'
str_experiment = 'working_eegbcg'
str_proc = 'proc_full'
str_proc_rs = 'proc_rs'
str_proc_obs = 'proc_bcgobs'
str_proc_net = 'proc_net'
str_proc_net_cv = 'proc_net_cv'
str_proc_bcgnet_mat = 'proc_bcgnet_mat'
str_proc_bcgnet = 'proc_bcgnet'
str_proc_test_epochs = 'proc_test_epochs'
str_proc_cv_epochs = 'proc_cv_epochs'
str_proc_sssr = 'single_sub'
str_proc_ssmr = 'multi_run'
str_proc_msmr = 'multi_sub'
str_bash = 'bash'
str_log = 'log'
t_arch = 'gru_arch'

# Overwrite the set_env module if local modules exists
if module_exists('dnc_set_env'):
    import dnc_set_env

    home = dnc_set_env.home()
elif home.exists():
    print('')  # default case
else:
    raise Exception('Local directory not found?')


def rs_path(str_sub, run_id):
    """
    Obtain the path to the resampled dataset and the correct filename

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject

    :return: p_rs: pathlib.Path objects that holds the path to the resampled dataset
    :return: f_rs: filename of the resampled dataset
    """

    p_rs = p_data / str_experiment / str_proc / str_proc_rs / str_sub
    f_rs = '{}_r0{}_rs.set'.format(str_sub, run_id)
    return p_rs, f_rs


def obs_path(str_sub, run_id):
    """
    Obtain the path to the OBS cleaned dataset and the correct filename

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject

    :return: p_bcg: pathlib.Path objects that holds the path to the OBS cleaned dataset
    :return: f_bcg: filename of the OBS cleaned dataset
    """

    p_bcg = p_data / str_experiment / str_proc / str_proc_obs / str_sub
    f_bcg = '{}_r0{}_rmbcg.set'.format(str_sub, run_id)
    return p_bcg, f_bcg


def arch_path(str_sub, run_id, str_arch, f_arch=None, num_training_epoch=None, flag_root=False):
    """
    Return the path for saving the models

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories (e.g. gru_arch_general4)
    :param f_arch: filename of the architecture
    :param num_training_epoch: number of training epochs took to train the model
    :param flag_root: flag for whether or not to return the root directory for the current model

    :return: p_arch: pathlib.Path objects that holds the path to the directory to save all models
    """

    # Default case
    if flag_root:
        if isinstance(run_id, (list, tuple, np.ndarray)):
            # if single subject, multiple runs
            run_id_str = ''.join(str(x) for x in run_id)
            p_arch = p_data / str_experiment / str_proc / str_proc_net / t_arch / str_arch / str_sub / 'r0{}'\
                .format(run_id_str) / f_arch
            p_arch.mkdir(parents=True, exist_ok=True)

        else:
            # if single subject, single run
            p_arch = p_data / str_experiment / str_proc / str_proc_net / t_arch / str_arch / str_sub / 'r0{}'\
                .format(run_id) / f_arch
            p_arch.mkdir(parents=True, exist_ok=True)

        return p_arch

    # If across subject analysis
    if isinstance(str_sub, (list, tuple, np.ndarray)):

        str_sub_joined = ''.join(re.findall(r'\d+', x)[0] + '_' for x in str_sub)
        str_sub_joined = str_sub_joined[:-1]
        str_sub_local = 'sub{}'.format(str_sub_joined)

        p_arch = p_data / str_experiment / str_proc / str_proc_net / str_sub_local / t_arch / str_arch / 'r0{}'.format(
            run_id)
        p_arch.mkdir(parents=True, exist_ok=True)

        return p_arch

    # If single subject analysis
    else:
        if isinstance(run_id, (list, tuple, np.ndarray)):
            # if single subject, multiple runs
            run_id_str = ''.join(str(x) for x in run_id)
            p_arch = p_data / str_experiment / str_proc / str_proc_net / t_arch / str_arch / str_sub / 'r0{}'\
                .format(run_id_str) / f_arch / 'TEp{}'.format(num_training_epoch)

        else:
            # if single subject, single run
            p_arch = p_data / str_experiment / str_proc / str_proc_net / t_arch / str_arch / str_sub / 'r0{}'\
                .format(run_id) / f_arch / 'TEp{}'.format(num_training_epoch)
            p_arch.mkdir(parents=True, exist_ok=True)

    return p_arch


def arch_cv_path(str_sub, run_id, str_arch, f_arch, idx_fold=None, num_training_epoch=None, flag_root=False):
    """
    Return the path to save all the trained models in the case of the cross validation

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories
    :param idx_run: index of the current run
    :param idx_fold: index of the fold used as the test set data
    :param f_arch: name of the architecture
    :param num_training_epoch: number of epochs it took to train the network
    :param flag_root: flag for whether to return the root directory

    :return: p_arch
    """

    str_fold = 'fold{}'.format(idx_fold)

    if not flag_root:
        if isinstance(run_id, (list, tuple, np.ndarray)):
            # if single subject, multiple runs
            run_id_str = ''.join(str(x) for x in run_id)
            p_arch = p_data / str_experiment / str_proc / str_proc_net_cv / str_sub / t_arch / str_arch / 'r0{}'.format(
                run_id_str) / f_arch / str_fold / 'TEp{}'.format(num_training_epoch)
            p_arch.mkdir(parents=True, exist_ok=True)

        else:
            # if single subject, single run
            p_arch = p_data / str_experiment / str_proc / str_proc_net_cv / str_sub / t_arch / str_arch / 'r0{}'.format(
                run_id) / f_arch / str_fold / 'TEp{}'.format(num_training_epoch)
            p_arch.mkdir(parents=True, exist_ok=True)

    else:
        if isinstance(run_id, (list, tuple, np.ndarray)):
            # if single subject, multiple runs
            run_id_str = ''.join(str(x) for x in run_id)
            p_arch = p_data / str_experiment / str_proc / str_proc_net_cv / str_sub / t_arch / str_arch / 'r0{}'.format(
                run_id_str) / f_arch
            p_arch.mkdir(parents=True, exist_ok=True)

        else:
            # if single subject, single run
            p_arch = p_data / str_experiment / str_proc / str_proc_net_cv / str_sub / t_arch / str_arch / 'r0{}'.format(
                run_id) / f_arch
            p_arch.mkdir(parents=True, exist_ok=True)

    return p_arch


def figure_path(p_arch):
    """
    Return the path for saving the figures

    :param p_arch: pathlib.Path objects that hold the path to the model that was trained

    :return: p_figure: pathlib.Path objects that holds the path to the root directory to save all figures
    """

    p_figure = p_arch / 'figures'
    p_figure.mkdir(parents=True, exist_ok=True)

    return p_figure


def bcgnet_mat_path(str_sub, run_id, str_arch, mode=None, opt=None):
    """
    Obtain the path and filename to the file containing the cleaned data

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories
    :param mode: whether the model is pre-trained or after training on the left out subject in across subject analysis
    :param opt: option object containing all the options used in the study

    :return: p_out: pathlib.Path objects that holds the path to the directory to save all the cleaned data
    :return: f_out: filename of the .mat file that holds all the cleaned data
    """

    # If single subject analysis
    if not opt.multi_sub:
        if not opt.multi_run:
            # If single subject single run
            p_out = p_data / str_experiment / str_proc / str_proc_bcgnet_mat / str_proc_sssr / str_arch / str_sub
            p_out.mkdir(parents=True, exist_ok=True)

        else:
            # If single subject multiple run
            p_out = p_data / str_experiment / str_proc / str_proc_bcgnet_mat / str_proc_ssmr / str_arch / str_sub
            p_out.mkdir(parents=True, exist_ok=True)

        f_out = '{}_r0{}_bcgnet.mat'.format(str_sub, run_id)

    # If across subject analysis
    else:
        p_out = p_data / str_experiment / str_proc / str_proc_bcgnet_mat / str_proc_msmr / str_arch / str_sub
        p_out.mkdir(parents=True, exist_ok=True)

        if mode == 'pre-trained':
            f_out = '{}_r0{}_bcgnet_pre_trained.mat'.format(str_sub, run_id)
        else:
            f_out = '{}_r0{}_bcgnet.mat'.format(str_sub, run_id)

    return p_out, f_out


def bcgnet_path(str_sub, run_id, str_arch, mode=None, opt=None):
    """
    Obtain the path and filename to the file containing the cleaned dataset

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories
    :param mode: whether the model is pre-trained or after training on the left out subject in across subject analysis
    :param opt: option object containing all the options used in the study

    :return: p_out: pathlib.Path objects that holds the path to the directory to save all the cleaned dataset
    :return: f_out: filename of the .mat file that holds all the cleaned dataset
    """

    # If single subject analysis
    if not opt.multi_sub:
        if not opt.multi_run:
            # If single subject single run
            p_out = p_data / str_experiment / str_proc / str_proc_bcgnet_mat / str_proc_sssr / str_arch / str_sub
            p_out.mkdir(parents=True, exist_ok=True)

        else:
            # If single subject multiple run
            p_out = p_data / str_experiment / str_proc / str_proc_bcgnet_mat / str_proc_ssmr / str_arch / str_sub
            p_out.mkdir(parents=True, exist_ok=True)

        f_out = '{}_r0{}_bcgnet.set'.format(str_sub, run_id)

    # If across subject analysis
    else:
        p_out = p_data / str_experiment / str_proc / str_proc_bcgnet / str_proc_msmr / str_arch / str_sub
        p_out.mkdir(parents=True, exist_ok=True)

        if mode == 'pre-trained':
            f_out = '{}_r0{}_bcgnet_pre_trained.set'.format(str_sub, run_id)
        else:
            f_out = '{}_r0{}_bcgnet.set'.format(str_sub, run_id)

    return p_out, f_out


def test_epochs_path(str_sub, run_id, str_arch, opt=None):
    """

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories
    :param opt: option object containing all the options used in the study

    :return: p_test_epochs: path.Pathlib object holding the path to save the mat file holding test epoch data
    :return: f_test_epochs: name of the file to save
    """

    # If single subject analysis
    if not opt.multi_sub:
        if not opt.multi_run:
            # If single subject single run
            p_test_epochs = p_data / str_experiment / str_proc / str_proc_test_epochs / str_proc_sssr / str_arch / str_sub
            p_test_epochs.mkdir(parents=True, exist_ok=True)

        else:
            # If single subject multiple run
            p_test_epochs = p_data / str_experiment / str_proc / str_proc_test_epochs / str_proc_ssmr / str_arch / str_sub
            p_test_epochs.mkdir(parents=True, exist_ok=True)

    # If across subject analysis
    else:
        p_test_epochs = p_data / str_experiment / str_proc / str_proc_test_epochs / 'multi_sub' / str_arch / str_sub
        p_test_epochs.mkdir(parents=True, exist_ok=True)

    f_test_epochs = '{}_r0{}_test_epochs.mat'.format(str_sub, run_id)

    return p_test_epochs, f_test_epochs


def cv_epochs_path(str_sub, run_id, str_arch, opt=None):
    """

    :param str_sub: string for subject to run analysis on
    :param run_id: index of the run from the subject
    :param str_arch: name of the arch for making the directories
    :param opt: option object containing all the options used in the study

    :return:
    """
    # If single subject analysis
    if not opt.multi_sub:
        if not opt.multi_run:
            # If single subject single run
            p_cv_epochs = p_data / str_experiment / str_proc / str_proc_cv_epochs / str_proc_sssr / str_arch / str_sub
            p_cv_epochs.mkdir(parents=True, exist_ok=True)

        else:
            # If single subject multiple run
            p_cv_epochs = p_data / str_experiment / str_proc / str_proc_cv_epochs / str_proc_ssmr / str_arch / str_sub
            p_cv_epochs.mkdir(parents=True, exist_ok=True)

    # If across subject analysis
    else:
        p_cv_epochs = p_data / str_experiment / str_proc / str_proc_cv_epochs / str_proc_msmr / str_arch / str_sub
        p_cv_epochs.mkdir(parents=True, exist_ok=True)

    f_cv_epochs = '{}_r0{}_cv_epoch.mat'.format(str_sub, run_id)

    return p_cv_epochs, f_cv_epochs


def bash_path():
    """
    Return the path to save all the bash scripts for running the subjects and to save all the outputs

    :return: p_bash: pathlib.Path object holding the path to save all the bash scripts
    :return: p_log: pathlib.Path object holding the path to save all the outputs
    """

    p_bash = p_data / str_experiment / str_proc / str_bash
    p_bash.mkdir(parents=True, exist_ok=True)

    p_log = p_data.joinpath(str_experiment) / str_proc / str_log
    p_log.mkdir(parents=True, exist_ok=True)

    return p_bash, p_log


def python_path():
    """
    Return the path to the python environment

    :return: p_python: pathlib.Path object holding the path to the python environment
    """

    p_python = home / 'miniconda3/envs/py-bcg-gpu-custom/bin/python'

    return p_python


def git_path():
    """
    Return the path to the project

    :return: p_git: pathlib.Path object holding the path to the current project
    """

    p_git = home / 'Local/gitprojects/py_bcg_dev' / 'py_bcg' / 'bcgnet_linbi'

    return p_git

