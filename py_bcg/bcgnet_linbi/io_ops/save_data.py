from scipy.io import savemat


# TODO: see if you can save the opt as a struct or something that can be then saved into MATLAB EEGLab structure also
def save_data(dataset, p_out, f_out):
    """
    Save the data held in an mne dataset as a .mat file into the desired directory

    :param dataset: mne object that holds data
    :param p_out: pathlib.Path object holding the path to the directory to save the cleaned data
    :param f_out: filename of the .mat file to save the data
    """

    # Obtain the data
    data = dataset.get_data()

    # Save the dataset
    savemat(str(p_out / f_out), {'data': data})
