import mne
from sp.sp_preprocessing import *
from set_env_linbi import obs_path


def load_obs_dataset(dataset_dir, duration, n_downsampling, flag_use_motion_data, good_idx):
    """
    Load the OBS-cleaned dataset and return both the original EEG_bcgobs dataset and the epoched dataset that has
    equivalent epochs to the raw dataset after MAD-based epoch rejection

    :param dataset_dir: pathlib.Path object pointing to the source, which is a EEGlab format containing a single run
        from a single subject of shape (64, n_time_stamps)
    :param duration: duration of each epoch
    :param n_downsampling: factor of downsampling, if 1 then no downsampling is performed
    :param flag_use_motion_data: flag for whether or not to use the motion data in addition to using the ECG data
    :param good_idx: list containing the epochs that passed the epoch rejection from the preprocessing function

    :return: epoched_obs_dataset: epoched OBS-cleaned dataset
    :return: obs_dataset: original OBS-cleaned dataset
    :return: orig_sr_epoched_obs_dataset: mne.EpochArray object with epoched data using data where downsampling is not
        performed
    :return: orig_sr_obs_dataset: mne.RawArray object with raw data where downsampling is not performed
    """

    # Load in the OBS dataset
    obs_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Performs downsampling
    if n_downsampling != 1:
        fs_orig = obs_dataset.info['sfreq']
        fs = fs_orig / n_downsampling
        obs_dataset.resample(fs)

    # Load the dataset again and this time don't perform the downsampling
    orig_sr_obs_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Get rid of the motion data channels if flag not set to true
    if not flag_use_motion_data:
        obs_dataset.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        orig_sr_obs_dataset.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    # Obtain the data from the ground truth dataset that corresponds to epochs that passed the MAD rejection and that
    # are used in training the model
    epoched_obs_dataset = dataset_epoch(dataset=obs_dataset, duration=duration, epoch_rejection=False,
                                        good_idx=good_idx)

    # Epoch the dataset of the original sampling rate also for later uses
    orig_sr_epoched_obs_dataset = dataset_epoch(dataset=orig_sr_obs_dataset, duration=duration, epoch_rejection=False,
                                                good_idx=good_idx)

    return epoched_obs_dataset, obs_dataset, orig_sr_epoched_obs_dataset, orig_sr_obs_dataset


def load_obs_dataset_mr(str_sub, vec_run_id, duration, n_downsampling, flag_use_motion_data, vec_good_idx):
    """
    Wrapper function using the load_obs_dataset function for single runs to load multiple runs from the same subject

    :param str_sub: string for the subject
    :param vec_run_id: list containing the indices for al the runs to be analyzed
    :param duration: duration of each epoch
    :param n_downsampling: factor of downsampling, if 1 then no downsampling is performed
    :param flag_use_motion_data: flag for whether or not to use the motion data in addition to using the ECG data
    :param vec_good_idx: list of list where each sublist contain the epochs that passed the epoch rejection for a
        single run

    :return: vec_epoched_obs_dataset: list containing epoched OBS-cleaned datasets from separate runs
    :return: vec_obs_dataset: list containing original OBS-cleaned datasets from separate runs
    :return: vec_orig_sr_epoched_obs_dataset: list containing mne.EpochArray objects where each object is with epoched
        data using data where downsampling is not performed from a single run
    :return: vec_orig_sr_obs_dataset: list containing mne.RawArray objects where each object is with raw data where
        downsampling is not performed from a single run
    """
    # Load the data for each run and pack everything into lists
    vec_epoched_obs_dataset = []
    vec_obs_dataset = []
    vec_orig_sr_epoched_obs_dataset = []
    vec_orig_sr_obs_dataset = []

    for i in range(len(vec_run_id)):
        run_id = vec_run_id[i]
        good_idx = vec_good_idx[i]

        # path setup
        p_obs, f_obs = obs_path(str_sub, run_id)
        pfe_obs = str(p_obs / f_obs)

        # Load and epoch the OBS-cleaned dataset
        epoched_obs_dataset, obs_dataset, orig_sr_epoched_obs_dataset, orig_sr_obs_dataset \
            = load_obs_dataset(dataset_dir=pfe_obs, duration=duration,
                               n_downsampling=n_downsampling,
                               flag_use_motion_data=flag_use_motion_data,
                               good_idx=good_idx)

        vec_epoched_obs_dataset.append(epoched_obs_dataset)
        vec_obs_dataset.append(obs_dataset)
        vec_orig_sr_epoched_obs_dataset.append(orig_sr_epoched_obs_dataset)
        vec_orig_sr_obs_dataset.append(orig_sr_obs_dataset)

    return vec_epoched_obs_dataset, vec_obs_dataset, vec_orig_sr_epoched_obs_dataset, vec_orig_sr_obs_dataset


def load_bcgnet_dataset(dataset_dir, duration, n_downsampling, good_idx):
    """
    Load the BCGNET-cleaned dataset and return both the original EEG_bcgnet dataset and the epoched dataset that has
    equivalent epochs to the raw dataset after MAD-based epoch rejection

    :param dataset_dir: pathlib.Path object pointing to the source, which is a EEGlab format containing a single run
        from a single subject of shape (64, n_time_stamps)
    :param duration: duration of each epoch
    :param n_downsampling: factor of downsampling, if 1 then no downsampling is performed
    :param good_idx: list containing the epochs that passed the epoch rejection from the preprocessing function

    :return: epoched_bcgnet_dataset: epoched BCGNET-cleaned dataset
    :return: bcgnet_dataset: original BCGNET-cleaned dataset
    :return: orig_sr_bcgnet_dataset: mne Raw object with raw data where downsampling is not performed
    """

    # Load in the OBS dataset
    bcgnet_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Performs downsampling
    if n_downsampling != 1:
        fs_orig = bcgnet_dataset.info['sfreq']
        fs = fs_orig / n_downsampling
        bcgnet_dataset.resample(fs)

    # Load the dataset again and this time don't perform the downsampling
    orig_sr_bcgnet_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Obtain the data from the ground truth dataset that corresponds to epochs that passed the MAD rejection and that
    # are used in training the model
    epoched_bcgnet_dataset = dataset_epoch(dataset=bcgnet_dataset, duration=duration, epoch_rejection=False,
                                           good_idx=good_idx)

    return epoched_bcgnet_dataset, bcgnet_dataset, orig_sr_bcgnet_dataset
