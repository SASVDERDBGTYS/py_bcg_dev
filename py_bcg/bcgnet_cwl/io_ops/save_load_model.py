import datetime
import pickle
from training.training import get_arch_rnn


def save_model(model, p_arch, str_arch, f_arch, epochs):
    """
    Saving the trained model to the path specified in set_env

    :param model: model that was trained
    :param p_arch: path to save the architecture to
    :param str_arch: name of the architecture
    :param f_arch: filename of the architecture

    :param epochs: the number of epochs the model was trained for
    """

    # Obtain the weights of the model and save using pickle
    weights = model.get_weights()
    finished = True
    net_settings = {'weights': weights, 'arch': str_arch, 'epochs': epochs, 'finished': finished}
    with open(p_arch / '{}.pickle'.format(f_arch), 'wb') as handle:
        pickle.dump(net_settings, handle)


def load_model(f_arch, p_arch, str_arch, lr=1e-2):
    """
    Loading previously trained models

    :param f_arch: full filename of the model to be loaded
    :param p_arch: pathlib.Path objects that points to the directory where models are saved, and can be obtained
        via arch_path()
    :param str_arch: name of the architecture to load, in the form of gru_arch_XX
    :param lr: learning rate of the model

    :return: model: model whose weight is set to that of the trained model
    """

    # Obtain the weights from the previously saved pickle file
    with open(p_arch / '{}.pickle'.format(f_arch), 'rb') as handle:
        pickle_dict = pickle.load(handle)
    weights = pickle_dict['weights']

    model = get_arch_rnn(str_arch, lr)
    model.set_weights(weights)

    return model
