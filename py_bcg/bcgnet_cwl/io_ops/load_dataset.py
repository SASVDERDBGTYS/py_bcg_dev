import mne
from sp.sp_preprocessing import *


def load_obs_dataset(dataset_dir, duration, n_downsampling, good_idx):
    """
    Load the OBS-cleaned dataset and return both the original EEG_bcgobs dataset and the epoched dataset that has
    equivalent epochs to the raw dataset after MAD-based epoch rejection

    :param dataset_dir: pathlib.Path object pointing to the source, which is a EEGlab format containing a single run
        from a single subject of shape (64, n_time_stamps)
    :param duration: duration of each epoch
    :param n_downsampling: factor of downsampling, if 1 then no downsampling is performed
    :param good_idx: list containing the epochs that passed the epoch rejection from the preprocessing function

    :return: epoched_obs_dataset: epoched OBS-cleaned dataset
    :return: obs_dataset: original OBS-cleaned dataset
    :return: orig_sr_obs_dataset: mne Raw object with raw data where downsampling is not performed
    """

    # Load in the OBS dataset
    obs_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Performs downsampling
    if n_downsampling != 1:
        fs_orig = obs_dataset.info['sfreq']
        fs = fs_orig / n_downsampling
        obs_dataset.resample(fs)

    # Load the dataset again and this time don't perform the downsampling
    orig_sr_obs_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Obtain the data from the ground truth dataset that corresponds to epochs that passed the MAD rejection and that
    # are used in training the model
    epoched_obs_dataset = dataset_epoch(dataset=obs_dataset, duration=duration, epoch_rejection=False,
                                        good_idx=good_idx)

    # Epoch the dataset of the original sampling rate also for later uses
    orig_sr_epoched_obs_dataset = dataset_epoch(dataset=orig_sr_obs_dataset, duration=duration, epoch_rejection=False,
                                                good_idx=good_idx)

    return epoched_obs_dataset, obs_dataset, orig_sr_epoched_obs_dataset, orig_sr_obs_dataset


def load_bcgnet_dataset(dataset_dir, duration, n_downsampling, good_idx):
    """
    Load the BCGNET-cleaned dataset and return both the original EEG_bcgnet dataset and the epoched dataset that has
    equivalent epochs to the raw dataset after MAD-based epoch rejection

    :param dataset_dir: pathlib.Path object pointing to the source, which is a EEGlab format containing a single run
        from a single subject of shape (64, n_time_stamps)
    :param duration: duration of each epoch
    :param n_downsampling: factor of downsampling, if 1 then no downsampling is performed
    :param good_idx: list containing the epochs that passed the epoch rejection from the preprocessing function

    :return: epoched_bcgnet_dataset: epoched BCGNET-cleaned dataset
    :return: bcgnet_dataset: original BCGNET-cleaned dataset
    :return: orig_sr_bcgnet_dataset: mne Raw object with raw data where downsampling is not performed
    """

    # Load in the OBS dataset
    bcgnet_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Performs downsampling
    if n_downsampling != 1:
        fs_orig = bcgnet_dataset.info['sfreq']
        fs = fs_orig / n_downsampling
        bcgnet_dataset.resample(fs)

    # Load the dataset again and this time don't perform the downsampling
    orig_sr_bcgnet_dataset = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)

    # Obtain the data from the ground truth dataset that corresponds to epochs that passed the MAD rejection and that
    # are used in training the model
    epoched_bcgnet_dataset = dataset_epoch(dataset=bcgnet_dataset, duration=duration, epoch_rejection=False,
                                           good_idx=good_idx)

    return epoched_bcgnet_dataset, bcgnet_dataset, orig_sr_bcgnet_dataset
