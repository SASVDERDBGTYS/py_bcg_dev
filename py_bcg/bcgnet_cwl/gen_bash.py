import numpy as np
from pathlib import Path
import os
from set_env_cwl import *


if __name__ == '__main__':

    # Subject indices for all subjects and run indices for all runs
    vec_idx_sub = [11, 12, 13, 14, 15, 16, 17, 18,
                   21, 22, 23, 24, 25, 26, 27, 28]
    run_id = 0

    # Setup the path
    p_bash, p_log_root = bash_path()
    p_python = python_path()
    p_git = git_path()

    str_arch = 'gru_arch_general4'
    individual_script = True
    master_script = True

    if individual_script:
        for idx_sub in vec_idx_sub:
            str_name = 'run_bash_sub{}_r0{}.sh'.format(idx_sub, run_id)
            with open(p_bash / str_name, 'w') as rsh:
                rsh.write('#!/bin/bash\n'
                          '{} {} --str_sub sub{} --run_id {} --str_arch {}'.format(p_python,
                                                                                   str(p_git / 'run_training_linbi.py'),
                                                                                   idx_sub, run_id, str_arch))
        print('Individual bash scripts generated')

    if master_script:
        with open(p_bash / 'run_bash.sh', 'w+') as rmsh:
            rmsh.write('#!/bin/bash\n')
            for idx_sub in vec_idx_sub:
                p_log = p_log_root / 'sub{}'.format(idx_sub)
                p_log.mkdir(parents=True, exist_ok=True)

                f_log = 'log_sub{}_r0{}_{}.txt'.format(idx_sub, run_id, str_arch)
                rmsh.write('./run_bash_sub{}_r0{}.sh > {}\n'.format(idx_sub, run_id, str(p_log / f_log)))

            print('Master bash script generated')
