import argparse
import multiprocessing
import tensorflow as tf
import bcg_net
import set_env
import numpy as np


def user_defined_opt(opt):
    if opt is None:
        opt = bcg_net.TrainDefault()
        opt.epochs = 2500
        opt.ga_mc = False
        opt.p_arch = None
        opt.f_arch = None
        opt.target_ch = None
        opt.es_min_delta = 1e-5
        opt.es_patience = 25  # How many times does the validation not increase
        opt.early_stopping = True
        opt.resume = False
        opt.validation = 0.15
        opt.evaluation = 0.85
        opt.evaluate_model = False
        opt.figgen = False
        opt.dataset_gen = False
        opt.save_bcg_dataset = False
        opt.fig_num = 63
        opt.multi_ch = True
        opt.multi_run = True
        opt.multi_sub = False
        opt.use_rs_data = True
        opt.use_time_encoding = False
        opt.save_QRS_series = False
        opt.use_bcg_input = False
    elif isinstance(opt, dict):
        assert False, 'need to add this option?'

    return opt


opt = user_defined_opt(None)


def run_main_single_subject(str_sub, run_id, arch, opt, run_cpu_gpu):
    if run_cpu_gpu:
        n_cpu = multiprocessing.cpu_count()
        n_pcore = int(n_cpu/2)
        config = tf.ConfigProto()
        config.intra_op_parallelism_threads = 2
        config.inter_op_parallelism_threads = 2
        tf.Session(config=config)
    else:
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        tf.Session(config=config)

    str_sub_local = str_sub[0]
    if len(run_id) == 1:
        run_id_local = run_id[0]
    else:
        run_id_local = run_id

    if not isinstance(str_sub_local, str):
        raise Exception('For single-sub trials, str_sub should be a string')

    bcg_net.main_loop(str_sub_local, run_id_local, arch, opt)


def run_main_multi_subject(str_sub, run_id, arch, opt, run_cpu_gpu):
    if run_cpu_gpu:
        n_cpu = multiprocessing.cpu_count()
        n_pcore = int(n_cpu/2)
        config = tf.ConfigProto()
        config.intra_op_parallelism_threads = 2
        config.inter_op_parallelism_threads = 2
        tf.Session(config=config)
    else:
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        tf.Session(config=config)

    if not isinstance(str_sub, (list, tuple, np.ndarray)):
        raise Exception('For multi-sub trials, str_sub should be a list, tuple or numpy array')


if __name__ == '__main__':

    FUNCTION_MAP = {'run_main_single_subject': run_main_single_subject}
    parser = argparse.ArgumentParser(description='something')
    parser.add_argument('--function', choices=FUNCTION_MAP.keys(), default='run_main_single_subject')
    parser.add_argument('--str_sub', nargs='*', type=str, default='sub11')
    parser.add_argument('--run_id', nargs='*', type=int, default=1, help='')
    parser.add_argument('--arch', type=str, default='gru_arch_004')
    parser.add_argument('--opt', default=opt)
    parser.add_argument('--run_cpu_gpu', default=False)
    args = parser.parse_args()
    method = FUNCTION_MAP[args.function]

    method(str_sub=args.str_sub,
           run_id=args.run_id,
           arch=args.arch,
           opt=args.opt,
           run_cpu_gpu=args.run_cpu_gpu)