import argparse

from set_env_clayden import *
from sp.sp_preprocessing import preprocessing
from sp.sp_split_dataset import *
from training.training import *
from prediction.predict import *
from io_ops.save_load_model import *
from io_ops.save_data import *
from io_ops.load_dataset import *
from utils.plotting import *
from utils.compute_rms import *
from utils.interpolate_dataset import *
from options import test_opt


def run_training_single_sub(str_sub, run_id, str_arch):
    """
    Perform training on a single subject

    :param str_sub: name of the subject in the form subXX
    :param run_id: index of the run, in the form X
    :param str_arch: name of the architecture to run
    """

    """
    Setup
    """
    # Tensorflow session configuration
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)

    # Initialize the options object
    opt_user = test_opt(None)

    # Note that the input prompted in the terminal could be a list
    if isinstance(str_sub, list):
        str_sub = str_sub[0]

    if isinstance(run_id, list):
        run_id = run_id[0]

    # Path setup
    p_rs, f_rs = rs_path(str_sub, run_id)
    p_obs, f_obs = obs_path(str_sub, run_id)

    pfe_rs = str(p_rs / f_rs)
    pfe_obs = str(p_obs / f_obs)

    """
    Preparing the dataset
    """
    # Load, normalize and epoch the raw dataset
    normalized_epoched_raw_dataset, normalized_raw_dataset, epoched_raw_dataset, raw_dataset, \
        orig_sr_epoched_raw_dataset, orig_sr_raw_dataset, \
        ecg_stats, eeg_stats, good_idx = preprocessing(pfe_rs, duration=opt_user.epoch_duration,
                                                       threshold=opt_user.mad_threshold,
                                                       n_downsampling=opt_user.n_downsampling)

    # Load and epoch the OBS-cleaned dataset
    epoched_obs_dataset, obs_dataset, \
        orig_sr_epoched_obs_dataset, orig_sr_obs_dataset = load_obs_dataset(dataset_dir=pfe_obs,
                                                                            duration=opt_user.epoch_duration,
                                                                            n_downsampling=opt_user.n_downsampling,
                                                                            good_idx=good_idx)

    # Generate the train, validation and test sets and also obtain the index of epochs used in the validation
    # and test set
    xs, ys, vec_ix_slice = generate_train_valid_test(normalized_epoched_raw_dataset, opt=opt_user)

    # Obtain the training and validation generators
    training_generator = Defaultgenerator(xs[0], ys[0], batch_size=opt_user.batch_size, shuffle=True)
    validation_generator = Defaultgenerator(xs[1], ys[1], batch_size=opt_user.batch_size, shuffle=True)

    """
    Training
    """
    # Obtain the model and callback
    model = get_arch_rnn(str_arch, opt_user.lr)
    callbacks_ = get_callbacks_rnn(opt_user)

    # Fitting the model
    m = model.fit_generator(generator=training_generator, epochs=opt_user.epochs, verbose=2, callbacks=callbacks_,
                            validation_data=validation_generator)

    epochs = len(m.epoch)

    """
    Prediction
    """

    # Predict the cleaned dataset and epoch it for comparison later
    orig_sr_epoched_cleaned_dataset, orig_sr_cleaned_dataset, \
        epoched_cleaned_dataset, cleaned_dataset = predict_time_series(model, callbacks_,
                                                                       normalized_raw_dataset, raw_dataset,
                                                                       orig_sr_raw_dataset, ecg_stats, eeg_stats,
                                                                       opt_user.epoch_duration, good_idx)

    """
    Results
    """
    # Obtain the equivalent test epochs used during training from raw, OBS-cleaned and BCGNet-cleaned data
    vec_epoched_raw_dataset = split_epoched_dataset(orig_sr_epoched_raw_dataset, vec_ix_slice)
    vec_epoched_obs_dataset = split_epoched_dataset(orig_sr_epoched_obs_dataset, vec_ix_slice)
    vec_epoched_cleaned_dataset = split_epoched_dataset(orig_sr_epoched_cleaned_dataset, vec_ix_slice)

    orig_sr_epoched_raw_dataset_test = vec_epoched_raw_dataset[-1]
    orig_sr_epoched_obs_dataset_test = vec_epoched_obs_dataset[-1]
    orig_sr_epoched_cleaned_dataset_test = vec_epoched_cleaned_dataset[-1]

    # Computing the RMS using the test set
    vec_rms_test = compute_rms_epoched_dataset(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                                               orig_sr_epoched_cleaned_dataset_test)

    print("\n#############################################")
    print("#                  Results                  #")
    print("#############################################\n")
    print("RMS VALUES:")
    print("RMS Raw: {}".format(vec_rms_test[0]))
    print("RMS OBS: {}".format(vec_rms_test[1]))
    print("RMS BCGNet: {}".format(vec_rms_test[2]))

    # Compute the reduction in each power band
    print("\n\nFREQUENCY BAND POWER REDUCTION:")
    tabulate_band_power_reduction(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                                  orig_sr_epoched_cleaned_dataset_test)

    """
    Plotting Results
    """
    # Setup the filename of the model output
    f_arch = "{}_{}".format(str_arch, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))

    # Obtain the path of directory to save the model
    p_arch = arch_path(str_sub, run_id, str_arch, f_arch, epochs)

    # Obtain the root directory
    p_arch_root = arch_path(str_sub, run_id, str_arch, f_arch=f_arch, flag_root=True)

    if opt_user.training_figure_gen:
        # obtain the path to save all the figures to
        p_figure = figure_path(p_arch)

        # Plotting the training history
        plot_training_history(m, p_figure)

        # Plot a few random epochs
        plot_random_epoch(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                          orig_sr_epoched_cleaned_dataset_test,
                          vec_ix_slice[-1], p_figure, opt_user)

        # Plot a few sections of the time series
        plot_random_time_series_section(orig_sr_raw_dataset, orig_sr_obs_dataset, orig_sr_cleaned_dataset,
                                        p_figure, opt_user)

        # Now plot the PSD
        plot_psd(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                 orig_sr_epoched_cleaned_dataset_test, p_figure, opt_user)

    """
    Saving the trained model
    """
    save_model(model, p_arch, str_arch, f_arch, epochs)

    """
    Save the log
    """
    f_log = 'log_{}.pickle'.format(f_arch)
    with open(p_arch_root / f_log, 'wb') as handle:
        dict_log = dict([('history', m.history),
                         ('epochs', epochs),
                         ('vec_ix_slice', vec_ix_slice),
                         ('vec_rms_test', vec_rms_test)])

        pickle.dump(dict_log, handle)

    """
    Saving the cleaned time series data (optional)
    """
    if opt_user.training_dataset_gen:
        print('\n\nSaving cleaned dataset')

        # Obtain the path and filename of the output mat file
        p_out, f_out = bcgnet_mat_path(str_sub, run_id, str_arch, opt=opt_user)

        # Saving the cleaned data
        save_data(orig_sr_cleaned_dataset, p_out, f_out)

    """
    Saving the test epoch data (optional)
    """
    if opt_user.training_dataset_gen:
        print('\n\nSaving the epoched data')

        orig_sr_epoched_raw_data = orig_sr_epoched_raw_dataset.get_data()
        orig_sr_epoched_obs_data = orig_sr_epoched_obs_dataset.get_data()
        orig_sr_epoched_bcgnet_data = orig_sr_epoched_cleaned_dataset.get_data()

        p_test_epochs, f_test_epochs = test_epochs_path(str_sub, run_id, str_arch, opt_user)
        info = orig_sr_epoched_raw_dataset.info
        savemat(p_test_epochs / f_test_epochs,
                mdict={'met_obs': orig_sr_epoched_obs_data,
                       'met_net': orig_sr_epoched_bcgnet_data,
                       'met_gar': orig_sr_epoched_raw_data,
                       'ix_slice_test_py': vec_ix_slice[-1],
                       'srate': info['sfreq'],
                       'ch_names': info['ch_names']})


if __name__ == '__main__':
    FUNCTION_MAP = {'run_training_single_sub': run_training_single_sub}
    parser = argparse.ArgumentParser(description='something')
    parser.add_argument('--function', choices=FUNCTION_MAP.keys(), default='run_training_single_sub')
    parser.add_argument('--str_sub', nargs='*', type=str, default='sub32')
    parser.add_argument('--run_id', nargs='*', type=int, default=0, help='')
    parser.add_argument('--str_arch', type=str, default='gru_arch_general4')
    args = parser.parse_args()
    method = FUNCTION_MAP[args.function]

    method(str_sub=args.str_sub,
           run_id=args.run_id,
           str_arch=args.str_arch)
