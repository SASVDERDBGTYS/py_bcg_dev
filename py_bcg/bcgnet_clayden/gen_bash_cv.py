import numpy as np
from pathlib import Path
import os
from set_env_clayden import *


if __name__ == '__main__':

    # Subject indices for all subjects and run indices for all runs
    vec_idx_sub = [32, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50]
    run_id = 0

    # Setup the path
    p_bash, p_log_root = bash_path()
    p_python = python_path()
    p_git = git_path()

    p_bash = p_bash / 'cv'
    p_bash.mkdir(parents=True, exist_ok=True)

    str_arch = 'gru_arch_general4'
    individual_script = True
    master_script = True

    if individual_script:
        for idx_sub in vec_idx_sub:
            str_name = 'run_bash_sub{}_r0{}_cv.sh'.format(idx_sub, run_id)
            with open(p_bash / str_name, 'w') as rsh:
                rsh.write('#!/bin/bash\n'
                          '{} {} --str_sub sub{} --run_id {} --str_arch {}'.format(p_python,
                                                                                   str(p_git / 'run_training_cv_linbi.py'),
                                                                                   idx_sub, run_id, str_arch))
        print('Individual bash scripts generated')

    if master_script:
        with open(p_bash / 'run_bash_cv.sh', 'w+') as rmsh:
            rmsh.write('#!/bin/bash\n')
            for idx_sub in vec_idx_sub:
                p_log = p_log_root / 'cv' / 'sub{}'.format(idx_sub)
                p_log.mkdir(parents=True, exist_ok=True)

                f_log = 'log_sub{}_r0{}_{}.txt'.format(idx_sub, run_id, str_arch)
                rmsh.write('./run_bash_sub{}_r0{}_cv.sh > {}\n'.format(idx_sub, run_id, str(p_log / f_log)))

            print('Master bash script generated')
