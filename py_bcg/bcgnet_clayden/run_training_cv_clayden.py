import argparse
from scipy.stats import sem

from set_env_clayden import *
from sp.sp_preprocessing import preprocessing
from sp.sp_split_dataset import *
from training.training import *
from prediction.predict import *
from io_ops.save_load_model import *
from io_ops.save_data import *
from io_ops.load_dataset import *
from utils.plotting import *
from utils.compute_rms import *
from utils.combine_test_epochs import *
from utils.interpolate_dataset import *
from options import test_opt


def run_training_single_sub_cv(str_sub, run_id, str_arch):
    """
    Perform training on a single subject in a cross validation manner

    :param str_sub: name of the subject in the form subXX
    :param run_id: index of the run, in the form X
    :param str_arch: name of the architecture to run
    """

    """
    Setup
    """
    # Tensorflow session configuration
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)

    # Initialize the options object
    opt_user = test_opt(None)

    # Note that the input prompted in the terminal could be a list
    if isinstance(str_sub, list):
        str_sub = str_sub[0]

    if isinstance(run_id, list):
        run_id = run_id[0]

    # Path setup
    p_rs, f_rs = rs_path(str_sub, run_id)
    p_obs, f_obs = obs_path(str_sub, run_id)

    pfe_rs = str(p_rs / f_rs)
    pfe_obs = str(p_obs / f_obs)

    """
    Preparing the dataset
    """
    # Load, normalize and epoch the raw dataset
    normalized_epoched_raw_dataset, normalized_raw_dataset, epoched_raw_dataset, raw_dataset, \
        orig_sr_epoched_raw_dataset, orig_sr_raw_dataset, \
        ecg_stats, eeg_stats, good_idx = preprocessing(pfe_rs, duration=opt_user.epoch_duration,
                                                       threshold=opt_user.mad_threshold,
                                                       n_downsampling=opt_user.n_downsampling)

    # Load and epoch the OBS-cleaned dataset
    epoched_obs_dataset, obs_dataset, \
        orig_sr_epoched_obs_dataset, orig_sr_obs_dataset = load_obs_dataset(dataset_dir=pfe_obs,
                                                                            duration=opt_user.epoch_duration,
                                                                            n_downsampling=opt_user.n_downsampling,
                                                                            good_idx=good_idx)

    # Generate the train, validation and test sets and also obtain the index of epochs used in the validation
    # and test set
    vec_xs, vec_ys, mat_ix_slice = generate_train_valid_test_cv(normalized_epoched_raw_dataset, opt=opt_user)

    """
    Training (in CV manner)
    """

    # Train a separate model for each fold of data
    vec_model = []
    vec_callbacks = []
    vec_epochs = []
    vec_m = []
    vec_history = []

    # Loop through the folds
    for i in range(len(vec_xs)):
        print('\n\nStart training the {}-th fold\n'.format(i + 1))

        # Obtain data
        xs = vec_xs[i]
        ys = vec_ys[i]

        # Obtain the training and validation generators
        training_generator = Defaultgenerator(xs[0], ys[0], batch_size=opt_user.batch_size, shuffle=True)
        validation_generator = Defaultgenerator(xs[1], ys[1], batch_size=opt_user.batch_size, shuffle=True)

        # Obtain the model and callback
        model = get_arch_rnn(str_arch, opt_user.lr)
        callbacks_ = get_callbacks_rnn(opt_user)

        # Fitting the model
        m = model.fit_generator(generator=training_generator, epochs=opt_user.epochs, verbose=2, callbacks=callbacks_,
                                validation_data=validation_generator)

        epochs = len(m.epoch)

        # Append the relevant variables to the list
        vec_model.append(model)
        vec_callbacks.append(callbacks_)
        vec_epochs.append(epochs)
        vec_m.append(m)
        vec_history.append(m.history)

    """
    Prediction (in CV manner)
    """
    # Save the variables for later
    vec_orig_sr_epoched_raw_dataset_test = []
    vec_orig_sr_epoched_obs_dataset_test = []
    vec_orig_sr_epoched_cleaned_dataset_test = []
    vec_orig_sr_cleaned_dataset = []
    mat_rms_test = []

    # Loop through all the models trained and generate a prediction for each model
    for i in range(len(vec_model)):
        model = vec_model[i]
        callbacks_ = vec_callbacks[i]
        vec_ix_slice = mat_ix_slice[i]

        # Predict the cleaned dataset and epoch it for comparison later
        orig_sr_epoched_cleaned_dataset, orig_sr_cleaned_dataset, \
            epoched_cleaned_dataset, cleaned_dataset = predict_time_series(model, callbacks_,
                                                                           normalized_raw_dataset, raw_dataset,
                                                                           orig_sr_raw_dataset, ecg_stats, eeg_stats,
                                                                           opt_user.epoch_duration, good_idx)

        # Obtain the equivalent test epochs used during training from BCGNet-cleaned data
        vec_orig_sr_epoched_raw_dataset = split_epoched_dataset(orig_sr_epoched_raw_dataset, vec_ix_slice)
        vec_orig_sr_epoched_obs_dataset = split_epoched_dataset(orig_sr_epoched_obs_dataset, vec_ix_slice)
        vec_orig_sr_epoched_cleaned_dataset = split_epoched_dataset(orig_sr_epoched_cleaned_dataset, vec_ix_slice)

        orig_sr_epoched_raw_dataset_test = vec_orig_sr_epoched_raw_dataset[-1]
        orig_sr_epoched_obs_dataset_test = vec_orig_sr_epoched_obs_dataset[-1]
        orig_sr_epoched_cleaned_dataset_test = vec_orig_sr_epoched_cleaned_dataset[-1]

        # Computing the RMS using the test set
        vec_rms_test = compute_rms_epoched_dataset(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                                                   orig_sr_epoched_cleaned_dataset_test)

        # Append the variables to the list
        vec_orig_sr_epoched_raw_dataset_test.append(orig_sr_epoched_raw_dataset_test)
        vec_orig_sr_epoched_obs_dataset_test.append(orig_sr_epoched_obs_dataset_test)
        vec_orig_sr_epoched_cleaned_dataset_test.append(orig_sr_epoched_cleaned_dataset_test)

        vec_orig_sr_cleaned_dataset.append(orig_sr_cleaned_dataset)
        mat_rms_test.append(vec_rms_test)

    # Convert mat_rms_test to numpy array
    mat_rms_test = np.stack(mat_rms_test, axis=0)

    # Combine all test epochs into a single mne.EpochArray object
    combined_orig_sr_epoched_cleaned_dataset = combined_test_epochs(vec_orig_sr_epoched_cleaned_dataset_test,
                                                            mat_ix_slice, orig_sr_epoched_raw_dataset)


    print("\n#############################################")
    print("#                  Results                  #")
    print("#############################################\n")
    print("RMS VALUES:")
    print("Raw: AVG RMS: {}, SE: {}".format(np.mean(mat_rms_test, axis=0)[0], sem(mat_rms_test, axis=0)[0]))
    print("OBS: AVG RMS: {}, SE: {}".format(np.mean(mat_rms_test, axis=0)[1], sem(mat_rms_test, axis=0)[1]))
    print("BCGNet AVG RMS: {}, SE: {}".format(np.mean(mat_rms_test, axis=0)[2], sem(mat_rms_test, axis=0)[2]))

    # Compute the reduction in each power band
    print("\n\nFREQUENCY BAND POWER REDUCTION:")
    tabulate_band_power_reduction(orig_sr_epoched_raw_dataset, orig_sr_epoched_obs_dataset,
                                  combined_orig_sr_epoched_cleaned_dataset)

    """
    Plotting Results
    """
    # Setup the filename of the model output
    f_arch = "{}_{}".format(str_arch, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))

    # Create an empty list to save all the individual paths to each model
    vec_p_arch = []

    # Obtain the path of directory to save the model trained using each fold as test data
    for i in range(len(vec_model)):
        num_epochs = vec_epochs[i]
        idx_fold = i + 1
        p_arch = arch_cv_path(str_sub, run_id, str_arch, f_arch, idx_fold=idx_fold, num_training_epoch=num_epochs,
                              flag_root=False)

        vec_p_arch.append(p_arch)

    # Obtain the root directory also
    p_arch_root = arch_cv_path(str_sub, run_id, str_arch, f_arch, flag_root=True)

    # If user chooses to generate the figures
    if opt_user.training_cv_figure_gen:
        # Loop through all the individual models
        for i in range(len(vec_model)):
            # Obtain the corresponding variables from the all these lists
            m = vec_m[i]
            orig_sr_epoched_raw_dataset_test = vec_orig_sr_epoched_raw_dataset_test[i]
            orig_sr_epoched_obs_dataset_test = vec_orig_sr_epoched_obs_dataset_test[i]
            orig_sr_epoched_cleaned_dataset_test = vec_orig_sr_epoched_cleaned_dataset_test[i]
            orig_sr_cleaned_dataset = vec_orig_sr_cleaned_dataset[i]
            vec_ix_slice = mat_ix_slice[i]
            p_arch = vec_p_arch[i]

            # obtain the path to save all the figures to
            p_figure = figure_path(p_arch)

            # Plotting the training history
            plot_training_history(m, p_figure)

            # Plot a few random epochs
            plot_random_epoch(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                              orig_sr_epoched_cleaned_dataset_test, vec_ix_slice[-1], p_figure, opt_user)

            # Plot a few sections of the time series
            plot_random_time_series_section(orig_sr_raw_dataset, orig_sr_obs_dataset,
                                            orig_sr_cleaned_dataset, p_figure, opt_user)

            # Now plot the PSD
            plot_psd(orig_sr_epoched_raw_dataset_test, orig_sr_epoched_obs_dataset_test,
                     orig_sr_epoched_cleaned_dataset_test, p_figure, opt_user)

        # Plot the summary plots from the combined dataset
        p_figure_root = figure_path(p_arch_root)

        # Now plot the PSD
        plot_psd(orig_sr_epoched_raw_dataset, orig_sr_epoched_obs_dataset,
                 combined_orig_sr_epoched_cleaned_dataset, p_figure_root, opt_user)

    """
    Saving the trained model
    """

    # Save all the individual models to the corresponding directories
    for i in range(len(vec_model)):
        model = vec_model[i]
        p_arch = vec_p_arch[i]
        num_epochs = vec_epochs[i]

        save_model(model, p_arch, str_arch, f_arch, num_epochs)

    """
    Save the log
    """
    f_log = 'log_{}.pickle'.format(f_arch)
    with open(p_arch_root / f_log, 'wb') as handle:
        dict_log = dict([('vec_history', vec_history),
                         ('vec_epochs', vec_epochs),
                         ('mat_ix_slice', mat_ix_slice),
                         ('mat_rms_test', mat_rms_test)])

        pickle.dump(dict_log, handle)

    """
    Saving the epoched data (optional)
    """
    if opt_user.training_cv_dataset_gen:

        # Obtain the path and filename of the output mat file
        p_cv_epochs, f_cv_epochs = cv_epochs_path(str_sub, run_id, str_arch, opt=opt_user)

        # Obtain the data
        orig_sr_epoched_raw_data = orig_sr_epoched_raw_dataset.get_data()
        orig_sr_epoched_obs_data = orig_sr_epoched_obs_dataset.get_data()
        orig_sr_epoched_bcgnet_data = combined_orig_sr_epoched_cleaned_dataset.get_data()
        ix_slice_test = np.arange(0, orig_sr_epoched_raw_data.shape[0], 1)

        info = orig_sr_epoched_raw_dataset.info
        savemat(p_cv_epochs / f_cv_epochs,
                mdict={'met_obs': orig_sr_epoched_obs_data,
                       'met_net': orig_sr_epoched_bcgnet_data,
                       'met_gar': orig_sr_epoched_raw_data,
                       'ix_slice_test_py': ix_slice_test,
                       'mat_ix_slice': mat_ix_slice,
                       'srate': info['sfreq'],
                       'ch_names': info['ch_names']})


if __name__ == '__main__':
    FUNCTION_MAP = {'run_training_single_sub': run_training_single_sub_cv}
    parser = argparse.ArgumentParser(description='something')
    parser.add_argument('--function', choices=FUNCTION_MAP.keys(), default='run_training_single_sub')
    parser.add_argument('--str_sub', nargs='*', type=str, default='sub32')
    parser.add_argument('--run_id', nargs='*', type=int, default=0, help='')
    parser.add_argument('--str_arch', type=str, default='gru_arch_general4')
    args = parser.parse_args()
    method = FUNCTION_MAP[args.function]

    method(str_sub=args.str_sub,
           run_id=args.run_id,
           str_arch=args.str_arch)
