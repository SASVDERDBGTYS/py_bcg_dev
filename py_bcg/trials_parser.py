import set_env
import pickle
import matplotlib.pyplot as plt
import numpy as np


def load_trials(str_sub, run_id):
    p_hyperas, f_hyperas, f_model = set_env.hyperas_path(str_sub, run_id)
    with open(p_hyperas / f_hyperas, 'rb') as handle:
        trials = pickle.load(handle)

    return trials


def cudnn_mapping(trials, i):
    ts = trials.trials

    losses = []
    for j in range(len(ts)):
        losses.append(ts[j]['result']['loss'])

    inds = np.array(losses).argsort()
    ts_sorted = np.array(ts)[inds]
    val = ts_sorted[i]['misc']['vals']
    loss = ts_sorted[i]['result']['loss']

    layers = {0: 'on', 1: 'off'}
    units = {0: 2, 1: 4, 2: 8, 3: 16, 4: 32}
    units_gru = {0: 2, 1: 4, 2: 8, 3: 16, 4: 32, 5: 64}


    print('model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={}, return_sequences=True, recurrent_regularizer=regularizers.l2({}), activity_regularizer=regularizers.l2({})), input_shape=(None, 1)))'.format(units[val['units'][0]], val['l2'][0], val['l2_1'][0]))
    if layers[val['l2_2'][0]] == 'on':
        print('model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={}, return_sequences=True, recurrent_regularizer=regularizers.l2({}), activity_regularizer=regularizers.l2({}))))'.format(units[val['units_1'][0]], val['l2_3'][0], val['l2_4'][0]))

    if layers[val['l2_5'][0]] == 'on':
        print('model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={}, return_sequences=True, recurrent_regularizer=regularizers.l2({}), activity_regularizer=regularizers.l2({}))))'.format(units[val['units_2'][0]], val['l2_6'][0], val['l2_7'][0]))

    if layers[val['l2_8'][0]] == 'on':
        print("model_gru.add(layers.Dense(units={}, activation='relu')) ; model_gru.add(layers.Dropout({}))".format(units[val['units_3'][0]], val['Dropout'][0]))

    if layers[val['Dropout_1'][0]] == 'on':
        print("model_gru.add(layers.Dense(units={}, activation='relu')) ; model_gru.add(layers.Dropout({}))".format(units[val['units_4'][0]], val['Dropout_2'][0]))

    if layers[val['Dropout_3'][0]] == 'on':
        print("model_gru.add(layers.Dense(units={}, activation='relu')) ; model_gru.add(layers.Dropout({}))".format(units[val['units_5'][0]], val['Dropout_4'][0]))

    if layers[val['Dropout_5'][0]] == 'on':
        print('model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={}, return_sequences=True, recurrent_regularizer=regularizers.l2({}), activity_regularizer=regularizers.l2({}))))'.format(units[val['units_6'][0]], val['l2_9'][0], val['l2_10'][0]))

    if layers[val['l2_11'][0]] == 'on':
        print('model_gru.add(layers.Bidirectional(layers.CuDNNGRU(units={}, return_sequences=True, recurrent_regularizer=regularizers.l2({}), activity_regularizer=regularizers.l2({}))))'.format(units_gru[val['units_7'][0]], val['l2_12'][0], val['l2_13'][0]))

    print("model_gru.add(layers.Dense(units=63, activation='linear'))")
    print("model_gru.compile(loss='mean_squared_error', optimizer='adam')")
    print('Associated loss is {}\n'.format(loss))


if __name__ == '__main__':
    trials = load_trials('sub33', 1)
    plt.plot(np.arange(1, len(trials.losses()) + 1, 1), trials.losses())
    # plt.xticks(ticks=np.arange(1, len(trials.losses()) + 1, 1))
    plt.show()

    for i in range(10):
        cudnn_mapping(trials, i)
        print('\n')
    #
    #
    # cudnn_mapping(trials, 3)


    # cudnn_mapping(trials, 4)
    # cudnn_mapping(trials, -2)
    # cudnn_mapping(trials, -3)