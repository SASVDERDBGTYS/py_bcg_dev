import bcg_net
vec_str_sub = ['sub11', 'sub12', 'sub14',
               'sub15', 'sub17', 'sub18',
               'sub19', 'sub20', 'sub21',
               'sub22', 'sub23', 'sub24',
               'sub25', 'sub29', 'sub30',
               'sub32', 'sub33', 'sub34',
               'sub35']
str_sub = 'sub21'
opt = bcg_net.test_opt(None)
run_id = [1, 2, 3, 4, 5]
arch = 'gru_arch_general4'
opt.use_bcg_input = False
opt.figgen = False
opt.dataset_gen = False
opt.save_bcg_dataset = False
opt.multi_sub = True
opt.use_rs_data = True
opt.evaluate_model = False
bcg_net.main_loop_multi_sub_separate(vec_str_sub_full=vec_str_sub, run_id=run_id, str_sub_particular=str_sub,
                                     arch=arch, opt=opt)
