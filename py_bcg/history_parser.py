import glob
from pathlib import Path
import os
import pickle
import re
import numpy as np
import matplotlib.pyplot as plt
import set_env


def run_parser():
    p = set_env.arch_path(None, None)
    os.chdir(p)
    vec_subs = glob.glob('sub*')

    vec_models = []
    for i in vec_subs:
        os.chdir(p)
        os.chdir(Path(i))
        local_models = glob.glob('**/r01/*gru_arch_004*/*.dat', recursive=True)
        latest_model = max(local_models, key=os.path.getctime)
        vec_models.append(os.path.abspath((latest_model)))

    vec_filename = []
    for i in vec_models:
        os.chdir(Path(i).parents[0])
        vec_history = glob.glob('**/history_rmse')
        latest_history = max(vec_history, key=os.path.getctime)
        vec_filename.append(os.path.abspath(latest_history))

    hs = []
    ns = []
    vec_tep = []

    for i in range(len(vec_filename)):
        with open(vec_filename[i], 'rb') as handle:
            h1 = pickle.load(handle)
        hs.append(h1)

        sub_str1 = 'sub'
        sub_str2 = 'TEp'
        ns.append(list(filter(lambda x: sub_str1 in x, vec_filename[i].split('/')))[0])
        vec_tep.append(re.findall(r'\d+', list(filter(lambda x: sub_str2 in x, vec_filename[i].split('/')))[0]))

    vec_rmse = []
    vec_comptime = []
    for i in range(len(hs)):
        vec_rmse.append(hs[i]['rmse_overall'])
        vec_comptime.append(hs[i]['time'])

    vec_comptime_in_s = []
    for i in vec_comptime:
        vec_comptime_in_s.append(i[0] * 3600 + i[1] * 60 + i[2])

    vec_comptime_in_s = np.array(vec_comptime_in_s)
    vec_tep_fixed = np.array([int(i[0]) for i in vec_tep])
    vec_comptime_per_epoch = np.divide(vec_comptime_in_s, vec_tep_fixed)

    d = {}
    for i in range(len(ns)):
        d[ns[i]] = (vec_rmse[i], vec_comptime_per_epoch[i])

    ns_sorted = sorted(ns)
    rmse_sorted = [d[i][0] for i in ns_sorted]
    comptime_sorted = [d[i][1] for i in ns_sorted]

    plt.figure(figsize=(8, 6))
    plt.xticks(range(len(rmse_sorted)), ns_sorted)
    plt.bar(np.arange(len(rmse_sorted)),rmse_sorted)
    plt.title('RMSE for run 1 of all subjects')
    plt.xlabel('Subjects')
    plt.ylabel('RMSE (arb.)')
    plt.show()

    plt.figure(figsize=(8, 6))
    plt.xticks(range(len(rmse_sorted)), ns_sorted)
    plt.bar(np.arange(len(comptime_sorted)), comptime_sorted)
    plt.title('Computational time per epoch for run 1 of all subjects')
    plt.xlabel('Subjects')
    plt.ylabel('Time (s)')
    plt.show()
    print('All done')


if __name__ == '__main__':
    run_parser()