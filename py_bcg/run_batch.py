# For now we want 3 subs, each with 2 runs
import numpy as np
from pathlib import Path
import os
import set_env

if __name__ == '__main__':
    p, op = set_env.bash_path()
    p.mkdir(parents=True, exist_ok=True)

    arch = 'gru_arch_general4'
    individual_script = True
    multi_run = True
    master_script = True
    cpu_gpu = False

    if cpu_gpu:
        p_conda = set_env.conda_path_cpu()
    else:
        p_conda = set_env.conda_path_gpu()

    run_id = [1, 2, 3, 4, 5]
    if isinstance(run_id, (list, tuple, np.ndarray)):
        run_id_str = ''.join(str(x) for x in run_id)
        run_id_cmd = ''.join(str(x) + ' ' for x in run_id)

    # Total list [11, 12, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 29, 30, 32, 33, 34, 35]
    # Name holders [13, 16, 26, 27, 28, 31]

    # Ready for running - first batch [18, 24, 25, 35, 11, 12, 14]
    # First batch excluding [15, 17, 19, 20, 21, 22, 23, 29, 30, 32, 33, 34]

    # Ready for running - second batch [15, 17, 19, 20, 21, 22, 23, 29]
    # Second batch excluding [18, 24, 25, 35, 11, 12, 14, 30, 32, 33, 34]

    # Ready for running - third batch [30, 32, 33, 34]
    # Second batch excluding [18, 24, 25, 35, 11, 12, 14, 15, 17, 19, 20, 21, 22, 23, 29]

    if individual_script and not multi_run:
        for i in np.arange(11, 36):
            if i in [13, 16, 26, 27, 28, 31]:
                continue

            if i in [18, 24, 25, 35, 11, 12, 14, 15, 17, 19, 20, 21, 22, 23, 29]:
                continue
            else:
                str_name = 'run_bash_sub{}_{}.sh'.format(i, run_id)
                with open(p / str_name, 'w') as rsh:
                    rsh.write('#!/bin/bash\n'
                              '{} {} --str_sub sub{} --run_id {}'.format(p_conda,
                                                                         Path(os.path.realpath(__file__)).parents[0].joinpath('run_bcg_net.py'),
                                                                         i,
                                                                         run_id))
        print('Individual bash scripts generated')

    if individual_script and multi_run:
        for i in np.arange(11, 36):
            if i in [13, 16, 26, 27, 28, 31]:
                continue

            if i in [18, 24, 25, 35, 11, 12, 14, 15, 17, 19, 20, 21, 22, 23, 29]:
                continue
            else:
                str_name = 'run_bash_sub{}_{}.sh'.format(i, run_id_str)
                with open(p / str_name, 'w') as rsh:
                    rsh.write('#!/bin/bash\n'
                              '{} {} --str_sub sub{} --run_id {} --arch {}'.format(p_conda,
                                                                                   Path(os.path.realpath(__file__)).parents[0].joinpath('run_bcg_net.py'),
                                                                                   i,
                                                                                   run_id_cmd,
                                                                                   arch))

    if master_script and not multi_run:
        with open(p / 'run_bash.sh', 'w+') as rmsh:
            rmsh.write('#!/bin/bash\n')
            for i in np.arange(11, 36):
                if i in [13, 16, 26, 27, 28, 31]:
                    continue

                if i in [18, 24, 25, 35, 11, 12, 14, 15, 17, 19, 20, 21, 22, 23, 29]:
                    continue
                else:
                    p_op= op.joinpath('sub{}'.format(i))
                    p_op.mkdir(parents=True, exist_ok=True)
                    f_op = 'log_sub{}_{}_{}.txt'.format(i, run_id, arch)
                    rmsh.write('./run_bash_sub{}_{}.sh &> {}\n'.format(i, run_id, str(p_op.joinpath(f_op))))
        print('Master bash scripts generated')
    elif master_script and multi_run:
        with open(p / 'run_bash.sh', 'w+') as rmsh:
            rmsh.write('#!/bin/bash\n')
            for i in np.arange(11, 36):
                if i in [13, 16, 26, 27, 28, 31]:
                    continue

                if i in [18, 24, 25, 35, 11, 12, 14, 15, 17, 19, 20, 21, 22, 23, 29]:
                    continue
                else:
                    p_op= op.joinpath('sub{}'.format(i))
                    p_op.mkdir(parents=True, exist_ok=True)
                    f_op = 'log_sub{}_{}_{}.txt'.format(i, run_id_str, arch)
                    rmsh.write('./run_bash_sub{}_{}.sh &> {}\n'.format(i, run_id_str, str(p_op.joinpath(f_op))))