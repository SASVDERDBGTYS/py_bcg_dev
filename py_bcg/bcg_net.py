#!/usr/bin/env python
# -*- coding: utf-8 -*-
import set_env
from epoch_rejection import single_subject_mabs
import datetime
from pathlib import Path
import mne
import tensorflow as tf
from tensorflow.python.keras.models import Sequential, Model
from tensorflow.python.keras import layers, callbacks, regularizers, optimizers
from tensorflow.python.keras import backend as K
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy import interpolate
import scipy.io as sio
import pickle
import copy
import gc
import contextlib
from collections import namedtuple


@ contextlib.contextmanager
def temp_seed(seed):
    state = np.random.get_state()
    np.random.seed(seed)
    try:
        yield
    finally:
        np.random.set_state(state)


def preprocessor_ga(dataset, target_ch=[], opt=None):
    # Preprocessing the data if the data comes from fmrib GA removal script

    # Loading the raw input in EEGLAB format and downsampling it
    GA_removed_raw = mne.io.read_raw_eeglab(dataset, preload=True, stim_channel=False)
    # GA_removed_raw.plot()
    fs = 100
    GA_removed_raw.resample(fs)

    if not opt.multi_ch or opt is None:
        target_ch = target_ch
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data(GA_removed_raw, target_ch)

    elif target_ch is None:
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data_multi_ch(GA_removed_raw)

    else:
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data_multi_ch(GA_removed_raw, target_ch)

    '''
    # Using annotation to visual the events in the raw data
    Annotated_raw = GA_removed_raw.copy()
    n_events = len(constructed_events)

    onset = constructed_events[:,0]/Annotated_raw.info['sfreq']
    duration = np.repeat(3, n_events)
    description = ['events'] * n_events
    annotation = mne.Annotations(onset,duration,description)
    Annotated_raw.set_annotations(annotation)
    # Annotated_raw.plot(block=True)
    '''

    # Constructing events of duration 10s
    duration = 5
    total_time_stamps = GA_removed_raw.get_data().shape[1]
    constructed_events =np.zeros(shape=(int(np.floor(total_time_stamps/fs)/duration), 3), dtype=int)

    for i in range(0, int(np.floor(total_time_stamps/fs))-duration, duration):
        ix = i/duration
        constructed_events[int(ix)] = np.array([i*fs, 0, 1])

    # Epoching the data using the constructed event and plotting it
    epoched_data = mne.Epochs(normalized_raw, constructed_events, tmin=0, tmax=3)
    # epoched_data.plot(block=True)

    return normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std


# Preprocessing the data if the data comes from self-processed rs data
def preprocessor_rs(data_dir, target_ch=[], run_id=None, N=None, opt=None):
    opt_local = opt

    # Loading the raw input in EEGLAB format and downsampling it
    rs_added_raw = mne.io.read_raw_eeglab(data_dir, preload=True, stim_channel=False)
    # GA_removed_raw.plot()
    srate = rs_added_raw.info['sfreq']
    fs = srate/5
    rs_added_raw.resample(fs)

    rs_data = rs_added_raw.get_data()[64:, :]
    rs_removed_raw = rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    if not opt_local.multi_ch or opt_local is None:
        target_ch = target_ch
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data(rs_removed_raw, target_ch)

    elif target_ch is None:
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data_multi_ch(rs_removed_raw)
    else:
        normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std = normalize_raw_data_multi_ch(rs_removed_raw, target_ch)

    # Adding the motion data back into the eeg data
    rs_renorm = normalize_rs_data_multi_ch(rs_data, fs)
    normalized_raw.add_channels([rs_renorm], force_update_info=True)

    if opt_local.use_time_encoding:
        normalized_raw = modify_motion_data(normalized_raw, run_id, N, opt_local)
    elif opt_local.use_bcg_input:
        normalized_raw = modify_motion_data_with_bcg(normalized_raw, opt_local)

    epoched_data, good_ix = dataset_epoch(dataset=normalized_raw, duration=3, epoch_rejection=True,
                                          threshold=5, raw_dataset=rs_added_raw)

    if not opt_local.use_rs_data:
        normalized_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        epoched_data.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    return normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix


# Normalize the data by subtracting the mean and then dividing it by its std for single channel
def normalize_raw_data(raw_data, target_ch):
    # Assuming that raw data is an mne Raw object
    data = raw_data.get_data()
    info = raw_data.info
    ecg_ch = info['ch_names'].index('ECG')

    # used for reverting back to original data later
    ecg_mean = np.mean(data[ecg_ch, :])
    eeg_mean = np.mean(data[target_ch, :])

    normalizedData = np.zeros(data.shape)
    for i in range(data.shape[0]):
        ds = data[i, :] - np.mean(data[i, :])
        if i == ecg_ch:
            ecg_std = np.std(ds)
        elif i == target_ch:
            eeg_std = np.std(ds)

        ds /= np.std(ds)
        normalizedData[i, :] = ds

    normalized_raw = mne.io.RawArray(normalizedData, info)
    return normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std


# Normalize the data by subtracting the mean and then dividing it by its std for multiple channels
def normalize_raw_data_multi_ch(raw_data, target_ch=[]):
    # Assuming that raw data is an mne Raw object
    data = raw_data.get_data()
    info = raw_data.info
    ecg_ch = info['ch_names'].index('ECG')
    if not target_ch:
        target_ch = np.delete(np.arange(0, len(info['ch_names']), 1), ecg_ch)

    # used for reverting back to original data later
    ecg_mean = np.mean(data[ecg_ch, :])
    ecg_std = np.std(data[ecg_ch, :])
    eeg_mean = np.mean(data[target_ch, :], axis=1)
    eeg_std = np.std(data[target_ch, :], axis=1)

    normalizedData = np.zeros(data.shape)
    for i in range(data.shape[0]):
        ds = data[i, :] - np.mean(data[i, :])
        ds /= np.std(ds)
        normalizedData[i, :] = ds

    normalized_raw = mne.io.RawArray(normalizedData, info)
    return normalized_raw, ecg_mean, ecg_std, eeg_mean, eeg_std


# Normalizing the motion data
def normalize_rs_data_multi_ch(rs_data, fs):
    rs_info = mne.create_info(['t0', 't1', 't2', 'r0', 'r1', 'r2'], fs,
                              ['misc', 'misc', 'misc', 'misc', 'misc', 'misc'])
    transformational_data = rs_data[0:3, :]
    rotational_data = rs_data[3:, :]

    # transformational_data_renorm = np.append([[0], [0], [0]], np.diff(transformational_data, axis=1) * fs * 1e6, axis=1)
    transformational_data_renorm = transformational_data * 1e8
    # used to be 1e9 and derivatives
    rotational_data_renorm = rotational_data * 1e7
    # used to be 1e8

    rs_data_renorm = np.insert(transformational_data_renorm, 3, rotational_data_renorm, axis=0)
    rs_renorm = mne.io.RawArray(rs_data_renorm, rs_info)
    return rs_renorm


# Renormalization function that works for both epoched data and original time series, note that if target channel is not
# all eeg channels, then need to feed corresponding stats into the function
def renormalize(data, stats, multi_ch, multi_run=False, vec_run_id=None):
    if not multi_run:
        if not multi_ch:
            data_renorm = data*stats[1] + stats[0]
        else:
            data_renorm = np.zeros(data.shape)
            # If time series
            if len(data.shape) == 2:
                for i in range(data.shape[0]):
                    data_renorm[i, :] = data[i, :] * stats[1][i] + stats[0][i]
            # If epoch data
            else:
                for i in range(data.shape[0]):
                    data_renorm[i, :, :] = data[i, :, :] * stats[1][i] + stats[0][i]
    else:
        if not multi_ch:
            # For single channel epoch data
            vec_stats = stats
            data_renorm = np.zeros(data.shape)
            for i in range(data.shape[0]):
                data_renorm[i, :] = data[i, :] * vec_stats[vec_run_id[i] - 1][1] + vec_stats[vec_run_id[i] - 1][0]
        else:
            vec_stats = stats
            data_renorm = np.zeros(data.shape)
            for i in range(data.shape[1]):
                sub_stats = np.array(vec_stats[vec_run_id[i] - 1])
                sub_avg = np.repeat(sub_stats[0, :][:, np.newaxis], data.shape[2], axis=1)
                sub_std = sub_stats[1, :]
                data_renorm[:, i, :] = data[:, i, :] * sub_std[:, np.newaxis] + sub_avg

    return data_renorm


# Performing epoching on the raw data set that's provided
def dataset_epoch(dataset, duration, epoch_rejection, threshold=None, raw_dataset=None, good_ix=None):
    # Constructing events of duration 10s
    info = dataset.info
    fs = info['sfreq']

    total_time_stamps = dataset.get_data().shape[1]
    constructed_events = np.zeros(shape=(int(np.floor(total_time_stamps/fs)/duration), 3), dtype=int)

    for i in range(0, int(np.floor(total_time_stamps/fs))-duration, duration):
        ix = i/duration
        constructed_events[int(ix)] = np.array([i*fs, 0, 1])

    tmax = duration - 1/fs

    # Epoching the data using the constructed event and plotting it
    old_epoched_data = mne.Epochs(dataset, constructed_events, tmin=0, tmax=tmax)

    if epoch_rejection:
        # Epoch rejection based on median absolute deviation of mean of absolute values for individual epochs
        ix = single_subject_mabs(raw_dataset, threshold)
        good_ix = np.delete(np.arange(0, old_epoched_data.get_data().shape[0], 1), ix)
        good_data = old_epoched_data.get_data()[good_ix, :, :]
        epoched_data = mne.EpochsArray(good_data, old_epoched_data.info)

        return epoched_data, good_ix
    else:
        epoched_data = old_epoched_data.get_data()[good_ix, :, :]
        return epoched_data


def modify_motion_data_with_bcg(rs_set, opt, shift=None):
    opt_local = opt
    # if not opt_local.multi_sub and opt_local.use_rs_data and opt_local.multi_ch:
    if opt_local.use_rs_data and opt_local.multi_ch:
        data = rs_set.get_data()
        info = rs_set.info

        # makes me very nervous!
        eeg_data = data[0:64, :]

        # This length has to be 6 or else...
        electrode_list = ['F5', 'F6', 'P5', 'P6', 'TP9', 'TP10']  # also ugly place to put this

        bcg_input = np.zeros((len(electrode_list), np.shape(data)[1]))
        for ix, electrode in enumerate(electrode_list):
            bcg_input[ix, :] = data[info['ch_names'].index(electrode), :].reshape(1, -1)
            if shift:
                bcg_input[ix, :] = np.roll(bcg_input[ix, :], shift)  # need to check directions
                # not taking care of the edges - meh, this is just proof of concept

        modified_data = np.append(eeg_data, bcg_input, axis=0)
        modified_rs_raw = mne.io.RawArray(modified_data, info)

        return modified_rs_raw


def modify_motion_data(rs_set, run_id, N, opt):
    opt_local = opt
    if not opt_local.multi_sub and opt_local.use_rs_data and opt_local.multi_ch:
        data = rs_set.get_data()
        info = rs_set.info

        eeg_data = data[0 : 64, :]

        angle_diff = 2 * np.pi / N
        sub_encoding1 = np.cos(run_id * angle_diff)
        sub_encoding2 = np.sin(run_id * angle_diff)

        vec_sub_encoding1 = np.repeat(sub_encoding1, data.shape[1])
        vec_sub_encoding2 = np.repeat(sub_encoding2, data.shape[1])

        w0 = 2 * np.pi / data.shape[1]
        vec_time_encoding1 = np.array([np.sin(w0 * i) for i in range(data.shape[1])])
        vec_time_encoding2 = np.array([np.sin(2 * w0 * i) for i in range(data.shape[1])])
        vec_time_encoding3 = np.array([np.sin(4 * w0 * i) for i in range(data.shape[1])])
        vec_time_encoding4 = np.array([np.sin(8 * w0 * i) for i in range(data.shape[1])])

        time_sub_encoding = np.array([vec_sub_encoding1, vec_sub_encoding2, vec_time_encoding1, vec_time_encoding2,
                                      vec_time_encoding3, vec_time_encoding4])

        modified_data = np.append(eeg_data, time_sub_encoding, axis=0)
        modified_rs_raw = mne.io.RawArray(modified_data, info)

        return modified_rs_raw


# Obtain the architecture in use (Need to redefine)
def get_arch(arch, opt=None):
    if arch == "gru_arch_000":
        model = Sequential()
        model.add(layers.GRU(2, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.Dense(1, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_001':
        model = Sequential()
        model.add(layers.GRU(64, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.Dropout(0.1))
        model.add(layers.GRU(32, return_sequences=True))
        model.add(layers.Dropout(0.1))
        model.add(layers.GRU(16, return_sequences=True))
        model.add(layers.Dropout(0.1))
        model.add(layers.Dense(1, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_002':
        model = Sequential()
        model.add(layers.GRU(64, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.Dropout(0.1))
        model.add(layers.GRU(64, return_sequences=True))
        model.add(layers.Dropout(0.1))
        model.add(layers.GRU(32, return_sequences=True))
        model.add(layers.Dropout(0.1))
        model.add(layers.GRU(32, return_sequences=True))
        model.add(layers.Dropout(0.1))
        model.add(layers.Dense(1, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_003':
        model = Sequential()
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1),input_shape=(None, 1)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
        model.add(layers.Dense(1, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_004':
        config = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=2, inter_op_parallelism_threads=2)
        sess = tf.compat.v1.Session(config=config)

        model = Sequential()
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1),input_shape=(None, 1)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_005':
        model = Sequential()
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1, implementation=2), input_shape=(None, 1)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1, implementation=2)))
        model.add(layers.Bidirectional(layers.GRU(16, return_sequences=True, dropout=0.1, recurrent_dropout=0.1, implementation=2)))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_006':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)
        model = Sequential()
        model.add(layers.CuDNNLSTM(16, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_007':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)
        model = Sequential()
        model.add(layers.CuDNNGRU(16, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.CuDNNGRU(16, return_sequences=True))
        model.add(layers.CuDNNGRU(63, return_sequences=True))
        model.add(layers.Dense(63, activation='linear'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
    elif arch == 'gru_arch_008':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)
        model = Sequential()
        model.add(layers.CuDNNGRU(16, return_sequences=True, input_shape=(None, 1)))
        model.add(layers.CuDNNGRU(16, return_sequences=True))
        model.add(layers.Dense(16, activation='relu'))
        model.add(layers.Dense(16, activation='relu'))
        model.add(layers.CuDNNGRU(63, return_sequences=True))
        model.add(layers.Dense(63, activation='linear'))
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
    elif arch == 'gru_arch_009':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True, input_shape=(None, 1))))
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True)))
        model.add(layers.Dense(16, activation='relu'))
        model.add(layers.Dense(16, activation='relu'))
        model.add(layers.Bidirectional(layers.CuDNNGRU(63, return_sequences=True)))
        model.add(layers.Dense(63, activation='linear'))
        model.add(layers.Dropout(0.5))

        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')

    elif arch == 'gru_arch_010':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01)),
                                       input_shape=(None, 1)))
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01))))
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01))))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_011':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01)),
                                       input_shape=(None, 1)))
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01))))
        model.add(layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.01),
                                                       activity_regularizer=regularizers.l2(0.01))))
        model.add(layers.Dense(63, activation='linear'))
        model.compile(loss='mean_absolute_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_012':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_013':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_absolute_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_014':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        #
        # d1_out = layers.Dense(6, activation='relu')(rs_input)
        # d2_out = layers.Dense(6, activation='relu')(d1_out)
        # d3_out = layers.Dense(6, activation='relu')(d2_out)
        # rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        # input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_absolute_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_015':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        #
        # d1_out = layers.Dense(6, activation='relu')(rs_input)
        # d2_out = layers.Dense(6, activation='relu')(d1_out)
        # d3_out = layers.Dense(6, activation='relu')(d2_out)
        # rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        # input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.10),
                                                        activity_regularizer=regularizers.l2(0.10)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.00002),
                                                        activity_regularizer=regularizers.l2(0.02)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_absolute_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_016':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        #
        # d1_out = layers.Dense(6, activation='relu')(rs_input)
        # d2_out = layers.Dense(6, activation='relu')(d1_out)
        # d3_out = layers.Dense(6, activation='relu')(d2_out)
        # rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        # input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.10),
                                                        activity_regularizer=regularizers.l2(0.10)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.00002),
                                                        activity_regularizer=regularizers.l2(0.02)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_017':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        # d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d2_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_018':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        rs_6d = layers.Dense(6, activation='relu')(d2_out)
        # rs_3d = layers.Dense(3, activation='relu')(d2_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_6d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_019':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        rs_1d = layers.Dense(1, activation='relu')(d2_out)
        # rs_3d = layers.Dense(3, activation='relu')(d2_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_1d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_020':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d1_out_do = layers.Dropout(0.5)(d1_out)
        d2_out = layers.Dense(6, activation='relu')(d1_out_do)
        d2_out_do = layers.Dropout(0.5)(d2_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out_do)
        d3_out_do = layers.Dropout(0.5)(d3_out)
        rs_1d = layers.Dense(1, activation='relu')(d3_out_do)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_1d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru1_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_021':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='tanh')(rs_input)
        d1_out_do = layers.Dropout(0.2)(d1_out)
        # d2_out = layers.Dense(6, activation='tanh')(d1_out_do)
        # d2_out_do = layers.Dropout(0.2)(d2_out)

        gru0_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.01),
                                                        activity_regularizer=regularizers.l2(0.01)))(d1_out_do)

        rs_1d = layers.Dense(1, activation='tanh')(gru0_out)
        rs_1d = layers.Dropout(0.2)(rs_1d)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_1d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.1),
                                                        activity_regularizer=regularizers.l2(0.01)))(input_merge)

        d3_out = layers.Dense(8, activation='relu')(gru1_out)
        d3_out_do = layers.Dropout(0.3)(d3_out)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.02),
                                                        activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.00001),
                                                        activity_regularizer=regularizers.l2(0.05)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_022':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(16, activation='tanh')(rs_input)
        d1_out_do = layers.Dropout(0.2)(d1_out)
        d2_out = layers.Dense(8, activation='tanh')(d1_out_do)
        d2_out_do = layers.Dropout(0.2)(d2_out)

        rs_1d = layers.Dense(6, activation='tanh')(d2_out_do)
        rs_1d = layers.Dropout(0.2)(rs_1d)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_1d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNLSTM(16, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.05),
                                                         activity_regularizer=regularizers.l2(0.01)))(input_merge)

        d3_out = layers.Dense(8, activation='relu')(gru1_out)
        d3_out_do = layers.Dropout(0.3)(d3_out)

        gru2_out = layers.Bidirectional(layers.CuDNNLSTM(16, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.02),
                                                         activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru3_out = layers.Bidirectional(layers.CuDNNLSTM(64, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.01),
                                                         activity_regularizer=regularizers.l2(0.05)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_023':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')

        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.05),
                                                         activity_regularizer=regularizers.l2(0.01)))(input_merge)

        d3_out = layers.Dense(8, activation='relu')(gru1_out)
        d3_out_do = layers.Dropout(0.3)(d3_out)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.02),
                                                         activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                         recurrent_regularizer=regularizers.l2(0.01),
                                                         activity_regularizer=regularizers.l2(0.05)))(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()
    elif arch == 'gru_arch_good':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=16, return_sequences=True, recurrent_regularizer=regularizers.l2(0.08247666311637437),
                            activity_regularizer=regularizers.l2(0.0063296002987000815)), input_shape=(None, 1)))

        model.add(layers.Dense(units=8, activation='relu'))
        model.add(layers.Dropout(0.32724792858516555))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.023575941163834623),
                                                       activity_regularizer=regularizers.l2(0.06737730166064061))))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=64, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(2.482707313576898e-07),
                                                       activity_regularizer=regularizers.l2(0.055291176610193955))))

        model.add(layers.Dense(units=63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_middle':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=2, return_sequences=True, recurrent_regularizer=regularizers.l2(0.09989340988235419),
                            activity_regularizer=regularizers.l2(0.09659580808197461)), input_shape=(None, 1)))
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=32, return_sequences=True, recurrent_regularizer=regularizers.l2(0.03953060706288754),
                            activity_regularizer=regularizers.l2(0.027207130658326608))))

        model.add(layers.Dense(units=32, activation='relu'))
        model.add(layers.Dropout(0.03087421063561535))

        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=8, return_sequences=True, recurrent_regularizer=regularizers.l2(0.06672354357852925),
                            activity_regularizer=regularizers.l2(0.0002441743982632))))
        model.add(layers.Dense(units=63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_bad':
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=16, return_sequences=True, recurrent_regularizer=regularizers.l2(0.08471564059236229),
                            activity_regularizer=regularizers.l2(0.01600994252593328)), input_shape=(None, 1)))
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=4, return_sequences=True, recurrent_regularizer=regularizers.l2(0.07643595716579825),
                            activity_regularizer=regularizers.l2(0.04829116904332808))))
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=2, return_sequences=True, recurrent_regularizer=regularizers.l2(0.07105140847844676),
                            activity_regularizer=regularizers.l2(0.06573190194257307))))

        model.add(layers.Dense(units=32, activation='relu'))
        model.add(layers.Dropout(0.24840651514985188))

        model.add(layers.Dense(units=63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general':
        # Simple general architecture
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=16, return_sequences=True, recurrent_regularizer=regularizers.l2(0.096),
                            activity_regularizer=regularizers.l2(0.030)), input_shape=(None, 1)))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.090),
                                                       activity_regularizer=regularizers.l2(0.013))))

        model.add(layers.Dense(units=8, activation='relu'))
        model.add(layers.Dropout(0.327))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.024),
                                                       activity_regularizer=regularizers.l2(0.067))))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=64, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(2.48e-07),
                                                       activity_regularizer=regularizers.l2(0.055))))

        model.add(layers.Dense(units=63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general2':
        # Complex general architecture
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        model = Sequential()
        model.add(layers.Bidirectional(
            layers.CuDNNGRU(units=16, return_sequences=True, recurrent_regularizer=regularizers.l2(0.096),
                            activity_regularizer=regularizers.l2(0.030)), input_shape=(None, 1)))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.090),
                                                       activity_regularizer=regularizers.l2(0.013))))

        model.add(layers.Dense(units=8, activation='relu'))
        model.add(layers.Dropout(0.327))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=16, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.024),
                                                       activity_regularizer=regularizers.l2(0.067))))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=32, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(0.0004),
                                                       activity_regularizer=regularizers.l2(0.093))))

        model.add(layers.Bidirectional(layers.CuDNNGRU(units=64, return_sequences=True,
                                                       recurrent_regularizer=regularizers.l2(2.48e-07),
                                                       activity_regularizer=regularizers.l2(0.055))))

        model.add(layers.Dense(units=63, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general3':
        # Multi-run, no motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general4':
        # Multi-run, no motion, simple
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general4_0':
        # Multi-run, no motion, simple
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=ecg_input, outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general5':
        # Multi-run, with motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_input], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general6':
        # Multi-run, with motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general7':
        # Multi-run, with motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        gru0_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.10),
                                                        activity_regularizer=regularizers.l2(0.03)))(rs_input)
        rs_3d = layers.Dense(3, activation='relu')(gru0_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(input_merge)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general8':
        # Multi-run, unidirectional, without motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.CuDNNGRU(16, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.096),
                                   activity_regularizer=regularizers.l2(0.030))(ecg_input)

        gru2_out = layers.CuDNNGRU(16, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.090),
                                   activity_regularizer=regularizers.l2(0.013))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.CuDNNGRU(16, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.024),
                                   activity_regularizer=regularizers.l2(0.067))(d3_out_do)

        gru4_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.0004),
                                   activity_regularizer=regularizers.l2(0.093))(gru3_out)

        gru5_out = layers.CuDNNGRU(64, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(2.48e-07),
                                   activity_regularizer=regularizers.l2(0.055))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general9':
        # Multi-run, unidirectional, without motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.096),
                                   activity_regularizer=regularizers.l2(0.030))(ecg_input)

        gru2_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.090),
                                   activity_regularizer=regularizers.l2(0.013))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.024),
                                   activity_regularizer=regularizers.l2(0.067))(d3_out_do)

        gru4_out = layers.CuDNNGRU(64, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.0004),
                                   activity_regularizer=regularizers.l2(0.093))(gru3_out)

        gru5_out = layers.CuDNNGRU(128, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(2.48e-07),
                                   activity_regularizer=regularizers.l2(0.055))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general10':
        # Multi-run, unidirectional, with motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        d1_out = layers.Dense(6, activation='relu')(rs_input)
        d2_out = layers.Dense(6, activation='relu')(d1_out)
        d3_out = layers.Dense(6, activation='relu')(d2_out)
        rs_3d = layers.Dense(3, activation='relu')(d3_out)

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')
        input_merge = layers.concatenate([ecg_input, rs_3d], axis=-1)

        gru1_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.096),
                                   activity_regularizer=regularizers.l2(0.030))(input_merge)

        gru2_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.090),
                                   activity_regularizer=regularizers.l2(0.013))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.024),
                                   activity_regularizer=regularizers.l2(0.067))(d3_out_do)

        gru4_out = layers.CuDNNGRU(64, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.0004),
                                   activity_regularizer=regularizers.l2(0.093))(gru3_out)

        gru5_out = layers.CuDNNGRU(128, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(2.48e-07),
                                   activity_regularizer=regularizers.l2(0.055))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general11':
        # Multi-run, unidirectional, with motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.01),
                                   activity_regularizer=regularizers.l2(0.01))(ecg_input)

        gru2_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.01),
                                   activity_regularizer=regularizers.l2(0.01))(gru1_out)

        # d3_out = layers.Dense(8, activation='relu')(gru2_out)
        # d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.CuDNNGRU(32, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.01),
                                   activity_regularizer=regularizers.l2(0.01))(gru2_out)

        gru4_out = layers.CuDNNGRU(64, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.01),
                                   activity_regularizer=regularizers.l2(0.01))(gru3_out)

        gru5_out = layers.CuDNNGRU(128, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.01),
                                   activity_regularizer=regularizers.l2(0.01))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general12':
        # Multi-run, unidirectional, without motion, complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.CuDNNGRU(16, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.005),
                                   activity_regularizer=regularizers.l2(0.010))(ecg_input)

        gru2_out = layers.CuDNNGRU(16, return_sequences=True,
                                   recurrent_regularizer=regularizers.l2(0.005),
                                   activity_regularizer=regularizers.l2(0.01))(gru1_out)

        gru3_out = layers.CuDNNGRU(64, return_sequences=True)(gru2_out)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general13':
        # Multi-run, with motion (time encoding), complex
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        input_merge = layers.concatenate([gru2_out, rs_input], axis=-1)
        d3_out = layers.Dense(8, activation='relu')(input_merge)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general14':
        # Multi-run, no motion, complex, 1 Dense in middle
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(1, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(32, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.0004),
                                                        activity_regularizer=regularizers.l2(0.093)))(gru3_out)

        gru5_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru4_out)

        bcg_out = layers.Dense(63, activation='linear')(gru5_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_general15':
        # Multi-run, no motion, complex, 1 Dense in middle
        session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(1, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(d3_out_do)

        bcg_out = layers.Dense(63, activation='linear')(gru3_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()
    elif arch == 'gru_arch_general16':
        # Multi-run, no motion, simple, one-to-one output!
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        d3_out = layers.Dense(8, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)
        gru4_out = []
        for i in range(63):
            gru4_out_l1 = layers.Bidirectional(layers.CuDNNGRU(1, return_sequences=True,
                                                            recurrent_regularizer=regularizers.l2(2.48e-07),
                                                            activity_regularizer=regularizers.l2(0.055)))(gru3_out)

            gru4_out_l2 = layers.Dense(1, activation='linear')(gru4_out_l1)
            gru4_out.append(gru4_out_l2)

        bcg_out = layers.Concatenate(axis=-1)(gru4_out)

        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_refined01':
        # Multi-run, no motion, simple
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(gru1_out)

        d3_out = layers.Dense(16, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.5)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        activity_regularizer=regularizers.l2(0.05)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_custom01':
        import layers_custom
        # Multi-run, no motion, simple
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        # note that I had to set everything to 32 to get this to work for some reason!!!
        K.set_floatx('float32')
        rs_input = layers.Input(shape=(None, 6), dtype='float32', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float32', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(gru1_out)

        d3_out = layers_custom.ACC2(1, use_bias=False, return_sequences=True)(gru2_out)
        # ideally do this bidir, then average?

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(d3_out)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        activity_regularizer=regularizers.l2(0.05)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        optimizer = optimizers.Adam(lr=0.001, clipnorm=0.5)

        model.compile(loss='mean_squared_error', optimizer=optimizer)
        model.summary()

    elif arch == 'gru_arch_refined02':
        # Multi-run, no motion, simple
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(gru1_out)

        d3_out = layers.Dense(16, activation='relu')(gru2_out)
        d3_out_do = layers.Dropout(0.5)(d3_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        activity_regularizer=regularizers.l2(0.05)), merge_mode='ave')(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        # you might get away with clipnorm one order magntidue smaller (beyond that, things start to slow down)
        optimizer = optimizers.Adam(lr=0.001, clipnorm=0.001)

        model.compile(loss='mean_squared_error', optimizer=optimizer)
        model.summary()

    elif arch == 'gru_arch_bcginput01':
        assert opt.use_bcg_input, 'Change the use_bcg_input flag!'
        # Multi-run, BCG in input!!!
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        # Not allowed, disconnects the graph:
        # rs_input = tf.slice(rs_input, [0, 0, 0], [-1, -1, 4])

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(gru1_out)

        con = layers.Conv1D(18, kernel_size=25, activation='relu', padding='causal')(rs_input)

        con_avg = layers.Bidirectional(layers.CuDNNGRU(4, return_sequences=False,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(con)

        # con_avg = layers.MaxPool1D(pool_size=250)(con)
        #
        # con_flat = layers.Flatten()(con_avg)

        dense_compressed = layers.Dense(3)(con_avg)

        # The equivalent transofmations: A:[batch x time x units]
        # A = tf.ones([3, 100, 32])
        # B = tf.ones([3, 20]) # B: [batch x units]
        # C = tf.multiply(tf.ones([tf.shape(A)[1], 1, 1]), B) # broadcast to reshape
        # D = tf.transpose(C, [1, 0, 2])
        # E = tf.concat([A, D], 2)
        # print(tf.shape(E))

        C = tf.multiply(tf.ones([tf.shape(gru2_out)[1], 1, 1], dtype=tf.dtypes.float64), dense_compressed)
        D = tf.transpose(C, [1, 0, 2])
        E = tf.concat([gru2_out, D], 2)

        d3_out = layers.Dense(20, activation='relu')(E)
        d3_out_do = layers.Dropout(0.5)(d3_out)

        # d4_out = layers.Dense(8, activation='relu')(d3_out_do)
        # d4_out_do = layers.Dropout(0.25)(d4_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        activity_regularizer=regularizers.l2(0.05)), merge_mode='ave')(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        # you might get away with clipnorm one order magntidue smaller (beyond that, things start to slow down)
        optimizer = optimizers.Adam(lr=0.001, clipnorm=0.001)

        model.compile(loss='mean_squared_error', optimizer=optimizer)
        model.summary()

    elif arch == 'gru_arch_bcginput02':
        assert opt.use_bcg_input, 'Change the use_bcg_input flag!'
        # Multi-run, BCG in input!!!
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        # Not allowed, disconnects the graph:
        # rs_input = tf.slice(rs_input, [0, 0, 0], [-1, -1, 4])

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        con = layers.Conv1D(18, kernel_size=25, activation='relu', padding='causal')(rs_input)

        con_avg = layers.Bidirectional(layers.CuDNNGRU(4, return_sequences=False,
                                                        recurrent_regularizer=regularizers.l2(0.08),
                                                        activity_regularizer=regularizers.l2(0.05)))(con)

        # con_avg = layers.MaxPool1D(pool_size=250)(con)
        #
        # con_flat = layers.Flatten()(con_avg)

        dense_compressed = layers.Dense(3)(con_avg)

        # The equivalent transofmations: A:[batch x time x units]
        # A = tf.ones([3, 100, 32])
        # B = tf.ones([3, 20]) # B: [batch x units]
        # C = tf.multiply(tf.ones([tf.shape(A)[1], 1, 1]), B) # broadcast to reshape
        # D = tf.transpose(C, [1, 0, 2])
        # E = tf.concat([A, D], 2)
        # print(tf.shape(E))

        C = tf.multiply(tf.ones([tf.shape(gru2_out)[1], 1, 1], dtype=tf.dtypes.float64), dense_compressed)
        D = tf.transpose(C, [1, 0, 2])
        E = tf.concat([gru2_out, D], 2)

        d3_out = layers.Dense(12, activation='relu')(E)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        # d4_out = layers.Dense(8, activation='relu')(d3_out_do)
        # d4_out_do = layers.Dropout(0.25)(d4_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()

    elif arch == 'gru_arch_bcginput03':
        assert opt.use_bcg_input, 'Change the use_bcg_input flag!'
        # Multi-run, BCG in input!!!
        if int(tf.__version__[0]) > 1:
            session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
            sess = tf.compat.v1.Session(config=session_config)
        else:
            session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
            sess = tf.Session(config=session_config)

        K.set_floatx('float64')
        rs_input = layers.Input(shape=(None, 6), dtype='float64', name='rs_input')
        # Not allowed, disconnects the graph:
        # rs_input = tf.slice(rs_input, [0, 0, 0], [-1, -1, 4])

        ecg_input = layers.Input(shape=(None, 1), dtype='float64', name='ecg_input')

        gru1_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.096),
                                                        activity_regularizer=regularizers.l2(0.030)))(ecg_input)

        gru2_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.090),
                                                        activity_regularizer=regularizers.l2(0.013)))(gru1_out)

        con = layers.Conv1D(8, kernel_size=50, activation='relu', padding='causal')(rs_input)

        con_avg = layers.Bidirectional(layers.CuDNNGRU(4, return_sequences=False,
                                                        recurrent_regularizer=regularizers.l2(0.02),
                                                        activity_regularizer=regularizers.l2(0.01)))(con)

        # con_avg = layers.MaxPool1D(pool_size=250)(con)
        #
        # con_flat = layers.Flatten()(con_avg)

        dense_compressed = layers.Dense(4, activation='relu')(con_avg)

        # The equivalent transofmations: A:[batch x time x units]
        # A = tf.ones([3, 100, 32])
        # B = tf.ones([3, 20]) # B: [batch x units]
        # C = tf.multiply(tf.ones([tf.shape(A)[1], 1, 1]), B) # broadcast to reshape
        # D = tf.transpose(C, [1, 0, 2])
        # E = tf.concat([A, D], 2)
        # print(tf.shape(E))

        C = tf.multiply(tf.ones([tf.shape(gru2_out)[1], 1, 1], dtype=tf.dtypes.float64), dense_compressed)
        D = tf.transpose(C, [1, 0, 2])
        E = tf.concat([gru2_out, D], 2)

        d3_out = layers.Dense(12, activation='relu')(E)
        d3_out_do = layers.Dropout(0.327)(d3_out)

        # d4_out = layers.Dense(8, activation='relu')(d3_out_do)
        # d4_out_do = layers.Dropout(0.25)(d4_out)

        gru3_out = layers.Bidirectional(layers.CuDNNGRU(16, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(0.024),
                                                        activity_regularizer=regularizers.l2(0.067)))(d3_out_do)

        gru4_out = layers.Bidirectional(layers.CuDNNGRU(64, return_sequences=True,
                                                        recurrent_regularizer=regularizers.l2(2.48e-07),
                                                        activity_regularizer=regularizers.l2(0.055)))(gru3_out)

        bcg_out = layers.Dense(63, activation='linear')(gru4_out)
        model = Model(inputs=[ecg_input, rs_input], outputs=bcg_out)

        model.compile(loss='mean_squared_error', optimizer='adam')
        model.summary()
    else:
        raise Exception("Undefined network arch: {}".format(arch))

    return model


# Obtain the test set from the rest of the data
def split_evaluation_test(epoched_data, per_evaluation):
    vec_ix = np.random.permutation(len(epoched_data))
    vec_ix_cutoff = int(np.round(len(epoched_data) * per_evaluation))
    vec_ix_slice_evaluation = vec_ix[:vec_ix_cutoff]
    epochs_evaluation = epoched_data[vec_ix_slice_evaluation, :, :]

    vec_ix_slice_test = vec_ix[vec_ix_cutoff:]
    epochs_test = epoched_data[vec_ix_slice_test, :, :]

    return epochs_evaluation, epochs_test, vec_ix_slice_test


# Splits evaluation data into training set and validation set
def split_train_validation(epochs_evaluation, es_validation):
    vec_ix = np.random.permutation(len(epochs_evaluation))
    vec_ix_cutoff = int(np.round(len(epochs_evaluation) * es_validation))
    vec_ix_slice_train = vec_ix[vec_ix_cutoff:]
    epochs_train = epochs_evaluation[vec_ix_slice_train, :, :]

    vec_ix_slice_val = vec_ix[:vec_ix_cutoff]
    epochs_validation = epochs_evaluation[vec_ix_slice_val, :, :]
    return epochs_train, epochs_validation


def generate_train_valid_test(epoched_data, opt=None):
    opt_local = opt
    normalizedData = epoched_data.get_data()
    ecg_ch = epoched_data.info['ch_names'].index('ECG')

    if opt_local.use_rs_data:
        rs_ch = np.arange(64, 70, 1)

    if opt_local.target_ch is None:
        target_ch = np.delete(np.arange(0, len(epoched_data.info['ch_names']), 1), ecg_ch)

    num_epochs = normalizedData.shape[0]
    batch_size = normalizedData.shape[2]

    with temp_seed(1997):
        if not opt_local.multi_ch:
            if opt_local.validation is None:
                s_ev_train, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                x_test = s_test[:,ecg_ch,:].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)), batch_size)

                y_ev_train = s_ev_train[:, target_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                y_test = s_test[:,target_ch,:].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)), batch_size)

            else:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs)) / ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(ev_epoch_num - int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                x_ev_validation = s_ev_va[:, ecg_ch, :].reshape(int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                x_test = s_test[:,ecg_ch,:].reshape(num_epochs - ev_epoch_num, batch_size)

                y_ev_train = s_ev_train[:, target_ch, :].reshape(ev_epoch_num - int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                y_ev_validation = s_ev_va[:, target_ch, :].reshape(int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                y_test = s_test[:,target_ch,:].reshape(num_epochs - ev_epoch_num, batch_size)

        else:
            if opt_local.validation is None and not opt_local.use_rs_data:
                s_ev_train, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                x_test = s_test[:, ecg_ch, :].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)),
                                                      batch_size)

                y_ev_train = np.transpose(np.delete(s_ev_train, ecg_ch, axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, ecg_ch, axis=1), axes=(1, 0, 2))

            elif opt_local.validation is not None and not opt_local.use_rs_data:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs))/ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = s_ev_train[:, ecg_ch, :]
                x_ev_validation = s_ev_va[:, ecg_ch, :]
                x_test = s_test[:, ecg_ch, :]

                y_ev_train = np.transpose(np.delete(s_ev_train, ecg_ch, axis=1), axes=(1, 0, 2))
                y_ev_validation = np.transpose(np.delete(s_ev_va, ecg_ch, axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, ecg_ch, axis=1), axes=(1, 0, 2))

            elif opt_local.validation is not None and opt_local.use_rs_data:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs))/ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = np.transpose(s_ev_train[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))
                x_ev_validation = np.transpose(s_ev_va[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))
                x_test = np.transpose(s_test[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))

                y_ev_train = np.transpose(np.delete(s_ev_train, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))
                y_ev_validation = np.transpose(np.delete(s_ev_va, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))

    return x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test


#  Class used for setting hyperparameters of the training
class TrainDefault:
    def __init__(self):
        self._epochs = 75
        self._target_ch = None
        self._ga_mc = None  # Using the gradient or the motion dataset
        self._p_arch = None  # path of the architecture (mostly as a place holder)
        self._f_arch = None  # Name of the architecture, have to be named in the case of resuming or evaluating
        self._es_min_delta = 0  # Minimal change to be considered as an improvement by Keras
        self._es_patience = 10  # Patience factor for early stopping in Keras
        self._early_stopping = True  # Flag for turning on the early stopping feature
        self._resume = False  # Whether or not you are resuming the training of a particular model
        self._evaluate_model = False  # Evaluate a particular model: save the dataset and optionally generate all the figures
        self._dataset_gen = False  # Whether to generate the dataset during the training and evaluation
        self._save_bcg_dataset = False # Whether to save the predicted bcg into a .mat file during evaluation
        self._validation = None  # The percentage to be used as the validation set
        self._evaluation = None  # The percentage to be used as the evaluation (validation plus training) set
        self._fig_num = 1  # Number of figures to generate
        self._figgen = False  # Whether or not to generate figures while evaluating the model
        self._multi_ch = None  # Flag for whether or not you are training on multiple channels
        self._multi_run = None  # Flag for whether or not you are training on multiple runs
        self._multi_sub = None  # Flag for whether or not you are training on multiple subjects
        self._use_rs_data = False  # Flag for whether or not you are using the motion data in the training
        self._use_time_encoding = False  # Flag for whether or not you are using the time_encoding in the training
        self._save_QRS_series = False  # Flag for whether or not to save the output from the middle dense layer
        self._use_bcg_input = False  # Flag for using bcg in input
    @property
    def target_ch(self):
        return self._target_ch
    @property
    def epochs(self):
        return self._epochs

    @property
    def ga_mc(self):
        return self._ga_mc

    @property
    def p_arch(self):
        return self._p_arch

    @property
    def f_arch(self):
        return self._f_arch

    @property
    def es_min_delta(self):
        return self._es_min_delta

    @property
    def es_patience(self):
        return self._es_patience

    @property
    def early_stopping(self):
        return self._early_stopping

    @property
    def resume(self):
        return self._resume

    @property
    def evaluate_model(self):
        return self._evaluate_model

    @property
    def dataset_gen(self):
        return self._dataset_gen

    @property
    def save_bcg_dataset(self):
        return self._save_bcg_dataset

    @property
    def validation(self):
        validation = self._validation
        if validation is not None:
            if validation >= 1: raise Exception('es_validation cannot be greater than 1')
            if validation == 0: validation = None

        return validation

    @property
    def evaluation(self):
        evaluation = self._evaluation
        if evaluation is not None:
            if evaluation >= 1: raise Exception('percentage of evaluation cannot be greater than 1')
            if evaluation == 0: evaluation = None

        return evaluation

    @property
    def figgen(self):
        return self._figgen

    @property
    def fig_num(self):
        return self._fig_num

    @property
    def multi_ch(self):
        return self._multi_ch

    @property
    def multi_run(self):
        return self._multi_run

    @property
    def multi_sub(self):
        return self._multi_sub

    @property
    def use_rs_data(self):
        return self._use_rs_data

    @property
    def use_time_encoding(self):
        return self._use_time_encoding

    @property
    def save_QRS_series(self):
        return self._save_QRS_series

    @property
    def use_bcg_input(self):
        return self._use_bcg_input

    @epochs.setter
    def epochs(self, value):
        self._epochs = value

    @ga_mc.setter
    def ga_mc(self, value):
        self._ga_mc = value

    @p_arch.setter
    def p_arch(self, value):
        self._p_arch = value

    @f_arch.setter
    def f_arch(self, value):
        self._f_arch = value

    @target_ch.setter
    def target_ch(self, value):
        self._target_ch = value

    @es_min_delta.setter
    def es_min_delta(self, value):
        self._es_min_delta = value

    @es_patience.setter
    def es_patience(self, value):
        self._es_patience = value

    @resume.setter
    def resume(self, value):
        self._resume = value

    @evaluate_model.setter
    def evaluate_model(self, value):
        self._evaluate_model = value

    @dataset_gen.setter
    def dataset_gen(self, value):
        self._dataset_gen = value

    @save_bcg_dataset.setter
    def save_bcg_dataset(self, value):
        self._save_bcg_dataset = value

    @evaluation.setter
    def evaluation(self, value):
        self._evaluation = value

    @validation.setter
    def validation(self, value):
        self._validation = value

    @early_stopping.setter
    def early_stopping(self, value):
        self._early_stopping = value

    @figgen.setter
    def figgen(self, value):
        self._figgen = value

    @fig_num.setter
    def fig_num(self, value):
        self._fig_num = value

    @multi_ch.setter
    def multi_ch(self, value):
        self._multi_ch = value

    @multi_run.setter
    def multi_run(self, value):
        self._multi_run = value

    @multi_sub.setter
    def multi_sub(self, value):
        self._multi_sub = value

    @use_rs_data.setter
    def use_rs_data(self, value):
        self._use_rs_data = value

    @use_time_encoding.setter
    def use_time_encoding(self, value):
        self._use_time_encoding = value

    @save_QRS_series.setter
    def save_QRS_series(self, value):
        self._save_QRS_series = value

    @use_bcg_input.setter
    def use_bcg_input(self, value):
        self._use_bcg_input = value


# Generating the default setting for the training
def defaults(opt):
    if opt is None:
        opt = TrainDefault()
        opt.epochs = 100
        opt.target_ch = 0
        opt.ga_mc = True
        opt.p_arch = None
        opt.f_arch = None
        opt.es_min_delta = 1e-5
        opt.es_patience = 8 # How many times does the validation not increase
        opt.early_stopping = True
        opt.resume = False
        opt.evaluate_model = False
        opt.validation = 0.15
        opt.evaluation = 0.8
        opt.multi_ch = False
        opt.multi_run = False
    elif isinstance(opt, dict):
        assert False, 'need to add this option?'
    return opt


# Training the RNN specified by the user on the training set
def train_ds(epoched_data, target_ch, arch='gru_arch_000', overwrite=False, opt=None):
    opt_local = defaults(opt)

    if opt_local.use_rs_data:
        rs_ch = np.arange(64, 70, 1)

    if opt_local.f_arch is None:
        str_arch = 'net_{}_{}.dat'.format(arch, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        dir_arch = opt_local.p_arch.joinpath(Path(str_arch.split('.')[0]))
        dir_arch.mkdir(parents=True, exist_ok=True)
        opt_local.p_arch = dir_arch
        opt.f_arch = Path(str_arch)
        f_arch = opt_local.p_arch.joinpath(opt.f_arch)

    if isinstance(opt_local.f_arch, str):
        str_arch = opt_local.f_arch.split('.')[0]
        opt_local.p_arch = opt_local.p_arch.joinpath(str_arch)
        f_arch = opt_local.p_arch.joinpath(Path(opt_local.f_arch))


    normalizedData = epoched_data.get_data()
    ecg_ch = epoched_data.info['ch_names'].index('ECG')

    if opt_local.target_ch is None:
        target_ch = np.delete(np.arange(0, len(epoched_data.info['ch_names']), 1), ecg_ch)

    num_epochs = normalizedData.shape[0]
    batch_size = normalizedData.shape[2]

    with temp_seed(1997):
        if not opt_local.multi_ch:
            if opt_local.validation is None:
                s_ev_train, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                x_test = s_test[:,ecg_ch,:].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)), batch_size)

                y_ev_train = s_ev_train[:, target_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                y_test = s_test[:,target_ch,:].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)), batch_size)

            else:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs)) / ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(ev_epoch_num - int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                x_ev_validation = s_ev_va[:, ecg_ch, :].reshape(int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                x_test = s_test[:,ecg_ch,:].reshape(num_epochs - ev_epoch_num, batch_size)

                y_ev_train = s_ev_train[:, target_ch, :].reshape(ev_epoch_num - int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                y_ev_validation = s_ev_va[:, target_ch, :].reshape(int(np.round(ev_epoch_num * opt_local.validation)), batch_size)
                y_test = s_test[:,target_ch,:].reshape(num_epochs - ev_epoch_num, batch_size)

        else:
            if opt_local.validation is None and not opt_local.use_rs_data:
                s_ev_train, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                x_ev_train = s_ev_train[:, ecg_ch, :].reshape(int(np.round(num_epochs * opt_local.evaluation)), batch_size)
                x_test = s_test[:, ecg_ch, :].reshape(num_epochs - int(np.round(num_epochs * opt_local.evaluation)),
                                                      batch_size)

                y_ev_train = np.transpose(np.delete(s_ev_train, ecg_ch, axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, ecg_ch, axis=1), axes=(1, 0, 2))

            elif opt_local.validation is not None and not opt_local.use_rs_data:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs))/ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = s_ev_train[:, ecg_ch, :]
                x_ev_validation = s_ev_va[:, ecg_ch, :]
                x_test = s_test[:, ecg_ch, :]

                y_ev_train = np.transpose(np.delete(s_ev_train, ecg_ch, axis=1), axes=(1, 0, 2))
                y_ev_validation = np.transpose(np.delete(s_ev_va, ecg_ch, axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, ecg_ch, axis=1), axes=(1, 0, 2))

            elif opt_local.validation is not None and opt_local.use_rs_data:
                s_ev, s_test, vec_ix_slice_test = split_evaluation_test(normalizedData, opt_local.evaluation)
                ev_epoch_num = int(np.round(num_epochs * opt_local.evaluation))
                per_validation = int(np.round(opt_local.validation * num_epochs))/ev_epoch_num
                s_ev_train, s_ev_va = split_train_validation(s_ev, per_validation)

                x_ev_train = np.transpose(s_ev_train[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))
                x_ev_validation = np.transpose(s_ev_va[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))
                x_test = np.transpose(s_test[:, np.insert(rs_ch, 0, ecg_ch), :], axes=(1, 0, 2))

                y_ev_train = np.transpose(np.delete(s_ev_train, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))
                y_ev_validation = np.transpose(np.delete(s_ev_va, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))
                y_test = np.transpose(np.delete(s_test, np.insert(rs_ch, 0, ecg_ch), axis=1), axes=(1, 0, 2))

    model = get_arch(arch, opt=opt_local)

    # Generator for the training set, the output is a tuple of the xs and ys
    def train_generator():
        ix = 0
        if opt_local.use_rs_data:
            vec_ix = np.random.permutation(x_ev_train.shape[1])
        else:
            vec_ix = np.random.permutation(x_ev_train.shape[0])
        while True:
            if not opt_local.use_rs_data:
                xs = x_ev_train[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                xs_full = np.transpose(x_ev_train[:, vec_ix[ix], :]).reshape(1, -1, 7)
                xs_ecg = xs_full[0, :, 0].reshape(1, -1, 1)
                xs_rs = xs_full[0, :, 1:].reshape(1, -1, 6)
                xs = [xs_ecg, xs_rs]
            if not opt_local.multi_ch:
                ys = y_ev_train[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                ys = np.transpose(y_ev_train[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            if not opt_local.use_rs_data:
                ix = ix % x_ev_train.shape[0]
            else:
                ix = ix % x_ev_train.shape[1]
            yield xs, ys

    # Generator for the validation set, the output is a tuple of the xs and ys
    def validation_generator():
        ix = 0
        if opt_local.use_rs_data:
            vec_ix = np.random.permutation(x_ev_validation.shape[1])
        else:
            vec_ix = np.random.permutation(x_ev_validation.shape[0])
        while True:
            if not opt_local.use_rs_data:
                xs = x_ev_validation[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                xs_full = np.transpose(x_ev_validation[:, vec_ix[ix], :]).reshape(1, -1, 7)
                xs_ecg = xs_full[0, :, 0].reshape(1, -1, 1)
                xs_rs = xs_full[0, :, 1:].reshape(1, -1, 6)
                xs = [xs_ecg, xs_rs]
            if not opt_local.multi_ch:
                ys = y_ev_validation[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                ys = np.transpose(y_ev_validation[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            if not opt_local.use_rs_data:
                ix = ix % x_ev_validation.shape[0]
            else:
                ix = ix % x_ev_validation.shape[1]
            yield xs, ys

    if f_arch.exists() and not overwrite:
        # print('Loading {}'.format(f_arch))
        with open(str(f_arch), 'rb') as handle:
            net_settings = pickle.load(handle)

        weights = net_settings['weights']

        if 'finished' in net_settings:
            resume_overwrite = not(net_settings['finished'])
        else:
            resume_overwrite = False
        resume = opt_local.resume or resume_overwrite

        # you have to evaluate it before setting for some reason (maybe an older bug):
        model.set_weights(weights=weights)
    else:
        resume = False

    finished = True
    if not(f_arch.exists()) or resume or overwrite:
        print('Generating {}'.format(f_arch))
        if opt_local.epochs < 5:
            print('Only {} epochs selected... testing?'.format(opt_local.epochs))
        if not opt_local.use_rs_data:
            steps_per_epoch_train = x_ev_train.shape[0]
            steps_per_epoch_validation = x_ev_validation.shape[0]
        else:
            steps_per_epoch_train = x_ev_train.shape[1]
            steps_per_epoch_validation = x_ev_validation.shape[1]

        if opt_local.validation is None:
            model.fit_generator(train_generator(), steps_per_epoch=steps_per_epoch_train, epochs=opt_local.epochs, verbose=2)
        else:
            if opt_local.early_stopping:
                # 'an absolute change of less than min_delta, will count as no improvement'
                callbacks_ = [callbacks.EarlyStopping(monitor='val_loss', min_delta=opt_local.es_min_delta,
                                                      patience=opt_local.es_patience, verbose=0, mode='min', restore_best_weights=True)]
            else:
                callbacks_ = None

            m = model.fit_generator(train_generator(), steps_per_epoch=steps_per_epoch_train,
                                    epochs=opt_local.epochs, verbose=2,
                                    validation_data=validation_generator(),
                                    validation_steps=steps_per_epoch_validation,
                                    callbacks=callbacks_)

            # net_settings['finished']
            if np.max(m.epoch) + 1 >= opt_local.epochs:
                finished = False

        weights = model.get_weights()

        if resume:
            epochs = net_settings['epochs'] + len(m.history['loss'])  # don't like the structure here
        else:
            epochs = len(m.history['loss'])

        net_settings = {'weights': weights, 'arch': arch, 'epochs': epochs, 'finished': finished}

        # Don't like this code here
        if not resume:
            opt_local.p_arch.joinpath('TEp{}'.format(epochs)).mkdir(parents=True, exist_ok=True)
            opt.p_arch = opt_local.p_arch.joinpath('TEp{}'.format(epochs))
            opt.f_arch = f_arch
        else:
            opt_local.p_arch.joinpath('TEp{}'.format(epochs)).mkdir(parents=True, exist_ok=True)
            opt.p_arch = opt_local.p_arch.joinpath('TEp{}'.format(epochs))
            opt.f_arch = f_arch

        with open(str(f_arch), 'wb') as handle:
            pickle.dump(net_settings, handle)

        # Plotting the result of the training and saving the history file
        loss = m.history['loss']
        val_loss = m.history['val_loss']
        vec_epochs = range(1, len(loss) + 1)
        plt.figure(figsize=(6, 6))
        plt.plot(vec_epochs, loss, 'bo', label='Training loss')
        plt.plot(vec_epochs, val_loss, 'b', label='Validation loss')
        plt.title('Training and validation loss')
        plt.legend()

        fig = plt.gcf()
        fig.savefig(opt.p_arch / 'TrVa loss_TEp{}.svg'.format(epochs), format='svg')

        with open(str(opt.p_arch / 'history_rmse'), 'wb') as filename:
            pickle.dump(m.history, filename)

    return model, x_test, y_test, vec_ix_slice_test


def train_sp(x_ev_train, x_ev_validation, y_ev_train, y_ev_validation, arch='gru_arch_000', overwrite=False, held_out=False, opt=None):
    opt_local = defaults(opt)

    if opt_local.f_arch is None:
        str_arch = 'net_{}_{}.dat'.format(arch, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        dir_arch = opt_local.p_arch.joinpath(Path(str_arch.split('.')[0]))
        dir_arch.mkdir(parents=True, exist_ok=True)
        opt_local.p_arch = dir_arch
        opt.f_arch = Path(str_arch)
        f_arch = opt_local.p_arch.joinpath(opt.f_arch)

    if isinstance(opt_local.f_arch, str):
        str_arch = opt_local.f_arch.split('.')[0]
        opt_local.p_arch = opt_local.p_arch.joinpath(str_arch)
        f_arch = opt_local.p_arch.joinpath(Path(opt_local.f_arch))

    model = get_arch(arch, opt=opt_local)

    # Generator for the training set, the output is a tuple of the xs and ys
    def train_generator():
        ix = 0
        if opt_local.use_rs_data:
            vec_ix = np.random.permutation(x_ev_train.shape[1])
        else:
            vec_ix = np.random.permutation(x_ev_train.shape[0])
        while True:
            if not opt_local.use_rs_data:
                xs = x_ev_train[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                xs_full = np.transpose(x_ev_train[:, vec_ix[ix], :]).reshape(1, -1, 7)
                xs_ecg = xs_full[0, :, 0].reshape(1, -1, 1)
                xs_rs = xs_full[0, :, 1:].reshape(1, -1, 6)
                xs = [xs_ecg, xs_rs]
            if not opt_local.multi_ch:
                ys = y_ev_train[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                ys = np.transpose(y_ev_train[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            if not opt_local.use_rs_data:
                ix = ix % x_ev_train.shape[0]
            else:
                ix = ix % x_ev_train.shape[1]
            yield xs, ys

    # Generator for the validation set, the output is a tuple of the xs and ys
    def validation_generator():
        ix = 0
        if opt_local.use_rs_data:
            vec_ix = np.random.permutation(x_ev_validation.shape[1])
        else:
            vec_ix = np.random.permutation(x_ev_validation.shape[0])
        while True:
            if not opt_local.use_rs_data:
                xs = x_ev_validation[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                xs_full = np.transpose(x_ev_validation[:, vec_ix[ix], :]).reshape(1, -1, 7)
                xs_ecg = xs_full[0, :, 0].reshape(1, -1, 1)
                xs_rs = xs_full[0, :, 1:].reshape(1, -1, 6)
                xs = [xs_ecg, xs_rs]
            if not opt_local.multi_ch:
                ys = y_ev_validation[vec_ix[ix], :].reshape(1, -1, 1)
            else:
                ys = np.transpose(y_ev_validation[:, vec_ix[ix], :]).reshape(1, -1, 63)
            ix += 1
            if not opt_local.use_rs_data:
                ix = ix % x_ev_validation.shape[0]
            else:
                ix = ix % x_ev_validation.shape[1]
            yield xs, ys

    if f_arch.exists() and not overwrite:
        # print('Loading {}'.format(f_arch))
        with open(str(f_arch), 'rb') as handle:
            net_settings = pickle.load(handle)

        weights = net_settings['weights']

        if held_out:
            epochs_ori = net_settings['epochs']

        if 'finished' in net_settings:
            resume_overwrite = not (net_settings['finished'])
        else:
            resume_overwrite = False
        resume = opt_local.resume or resume_overwrite

        # you have to evaluate it before setting for some reason (maybe an older bug):
        model.set_weights(weights=weights)
    else:
        resume = False

    finished = True
    if not (f_arch.exists()) or resume or overwrite:
        print('Generating {}'.format(f_arch))
        if opt_local.epochs < 5:
            print('Only {} epochs selected... testing?'.format(opt_local.epochs))
        if not opt_local.use_rs_data:
            steps_per_epoch_train = x_ev_train.shape[0]
            steps_per_epoch_validation = x_ev_validation.shape[0]
        else:
            steps_per_epoch_train = x_ev_train.shape[1]
            steps_per_epoch_validation = x_ev_validation.shape[1]

        if opt_local.validation is None:
            model.fit_generator(train_generator(), steps_per_epoch=steps_per_epoch_train, epochs=opt_local.epochs,
                                verbose=2)
        else:
            if opt_local.early_stopping:
                # 'an absolute change of less than min_delta, will count as no improvement'
                callbacks_ = [callbacks.EarlyStopping(monitor='val_loss', min_delta=opt_local.es_min_delta,
                                                      patience=opt_local.es_patience, verbose=0, mode='min',
                                                      restore_best_weights=True)]
            else:
                callbacks_ = None

            m = model.fit_generator(train_generator(), steps_per_epoch=steps_per_epoch_train,
                                    epochs=opt_local.epochs, verbose=2,
                                    validation_data=validation_generator(),
                                    validation_steps=steps_per_epoch_validation,
                                    callbacks=callbacks_)

            # net_settings['finished']
            if np.max(m.epoch) + 1 >= opt_local.epochs:
                finished = False

        weights = model.get_weights()

        if resume:
            epochs = net_settings['epochs'] + len(m.history['loss'])  # don't like the structure here
        else:
            epochs = len(m.history['loss'])

        net_settings = {'weights': weights, 'arch': arch, 'epochs': epochs, 'finished': finished}

        # Don't like this code here
        if not resume:
            opt_local.p_arch.joinpath('TEp{}'.format(epochs)).mkdir(parents=True, exist_ok=True)
            opt.p_arch = opt_local.p_arch.joinpath('TEp{}'.format(epochs))
            opt.f_arch = f_arch
        else:
            if held_out:
                epochs = epochs_ori
            opt_local.p_arch.joinpath('TEp{}'.format(epochs)).mkdir(parents=True, exist_ok=True)
            opt.p_arch = opt_local.p_arch.joinpath('TEp{}'.format(epochs))
            opt.f_arch = f_arch

        with open(str(f_arch), 'wb') as handle:
            pickle.dump(net_settings, handle)

        # Plotting the result of the training and saving the history file
        loss = m.history['loss']
        val_loss = m.history['val_loss']
        vec_epochs = range(1, len(loss) + 1)
        # plt.figure(figsize=(6, 6))
        # plt.plot(vec_epochs, loss, 'bo', label='Training loss')
        # plt.plot(vec_epochs, val_loss, 'b', label='Validation loss')
        # plt.title('Training and validation loss')
        # plt.legend()
        #
        # fig = plt.gcf()
        if not held_out:
            # fig.savefig(opt.p_arch / 'TrVa_loss_TEp{}.svg'.format(epochs), format='svg')
            with open(str(opt.p_arch / 'history_rmse'), 'wb') as filename:
                pickle.dump(m.history, filename)
        else:
            # fig.savefig(opt.p_arch / 'TrVa_loss_ho_TEp{}.svg'.format(epochs), format='svg')
            with open(str(opt.p_arch / 'history_rmse'), 'rb') as handle:
                h1 = pickle.load(handle)

            h1['history_held_out_sub'] = m.history
            with open(str(opt.p_arch / 'history_rmse'), 'wb') as filename:
                pickle.dump(h1, filename)

        return model


# Predict the epochs in the test set
def predict_epoch(model, x_test, y_test, normalized_raw, vec_ix_slice_test, good_ix, opt=None):
    if opt is None:
        opt_local = defaults(opt)
    else:
        opt_local = opt

    ecg_ch = normalized_raw.info['ch_names'].index('ECG')
    if opt_local.target_ch is None and opt_local.multi_ch and not opt_local.use_rs_data:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']), 1), ecg_ch)
    elif opt_local.target_ch is None and opt_local.multi_ch and opt_local.use_rs_data:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

    if not opt_local.evaluate_model:
        # TODO: this is ugly
        with open(opt_local.p_arch / 'history_rmse', 'rb') as handle:
            h1 = pickle.load(handle)

    if not opt_local.multi_ch:
        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        BCG_predicted_raw = np.transpose(model.predict(ECG_raw_data.reshape(1, -1, 1)), axes=(0, 2, 1)).reshape(
            len(target_ch), -1)
        MNE_data_set = mne.io.RawArray(BCG_predicted_raw, normalized_raw.info)

        fs = normalized_raw.info['freq']
        duration = 3
        total_time_stamps = MNE_data_set.get_data().shape[1]
        constructed_events = np.zeros(shape=(int(np.floor(total_time_stamps / fs) / duration), 3), dtype=int)

        for i in range(0, int(np.floor(total_time_stamps / fs)) - duration, duration):
            ix = i / duration
            constructed_events[int(ix)] = np.array([i * fs, 0, 1])
        tmax = duration - 1/fs
        # Epoching the data using the constructed event and plotting it
        epoched_data = mne.Epochs(MNE_data_set, constructed_events, tmin=0, tmax=tmax)

        '''
        testEpochPredict = np.zeros([y_test.shape[1], y_test.shape[0], y_test.shape[2]])
        for i in range(y_test.shape[1]):
            testEpochPredict[i, :, :] = np.transpose(best_model.predict(x_test[i, :].reshape(1, -1, 1)),
                                                     axes=(0, 2, 1))
        testEpochPredict = np.transpose(testEpochPredict, axes=(1, 0, 2))
        test_score = np.mean(np.sqrt(np.square(np.subtract(y_test, testEpochPredict)).mean(axis=(1, 2))))
        '''

        raise Exception('Not implemented yet - needs fixing')
        testEpochPredict = np.zeros(y_test.shape)
        for i in range(len(y_test)):
            testEpochPredict[i, :] = model.predict(x_test[i, :].reshape(1, -1, 1)).reshape(1, -1)
        test_score = np.sqrt(np.square(np.subtract(y_test, testEpochPredict)).mean())
        print('Test Score: {:.3f} RMSE'.format(test_score))

    else:
        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]

        if not opt_local.use_rs_data:
            BCG_predicted_raw = np.transpose(model.predict(ECG_raw_data.reshape(1, -1, 1)), axes=(0, 2, 1)).reshape(
            len(target_ch), -1)

        else:
            ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
            rs_ch = np.arange(64, 70, 1)
            rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
            x_raw_data = [ECG_raw_data_reshape, rs_raw_data]
            BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(
                len(target_ch), -1)

        data_set = np.insert(BCG_predicted_raw, ecg_ch, ECG_raw_data, axis=0)

        if not opt_local.use_rs_data:
            MNE_data_set = mne.io.RawArray(data_set, normalized_raw.info)
        else:
            normalized_raw_copy = mne.io.RawArray(normalized_raw.get_data(), normalized_raw.info)
            normalized_raw_copy.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

            MNE_data_set = mne.io.RawArray(data_set, normalized_raw_copy.info)

        epoched_data = dataset_epoch(dataset=MNE_data_set, duration=3, epoch_rejection=False, good_ix=good_ix)

        if not opt_local.use_rs_data:
            if len(target_ch) != len(normalized_raw.info['ch_names']) - 1:
                epoched_data = np.transpose(epoched_data, axes=(1, 0, 2))[target_ch, :, :]
            else:
                epoched_data = np.delete(np.transpose(epoched_data, axes=(1, 0, 2)), ecg_ch, axis=0)
        else:
            if len(target_ch) != len(normalized_raw.info['ch_names']) - 7:
                epoched_data = np.transpose(epoched_data, axes=(1, 0, 2))[target_ch, :, :]
            else:
                epoched_data = np.delete(np.transpose(epoched_data, axes=(1, 0, 2)), ecg_ch, axis=0)

        testEpochPredict = epoched_data[:, vec_ix_slice_test, :]
        test_score = np.sqrt(np.square(np.subtract(y_test, testEpochPredict)).mean(axis=(1, 2)))
        print('Test Score: {:.3f} RMSE'.format(np.mean(test_score)))

        if not opt_local.evaluate_model:
            h1['rmse_overall'] = np.mean(test_score)
            h1['rmse_ch'] = test_score

    if not opt_local.evaluate_model:
        with open(opt_local.p_arch / 'history_rmse', 'wb') as handle:
            pickle.dump(h1, handle)

    return testEpochPredict


# Performs epoch prediction for the multi run version of the script
def predict_epoch_multi_run(model, y_test, vec_normalized_raw, vec_ix_slice_test, vec_good_ix, vec_n_events, sts=None, run_id_actual=None, opt=None):
    if opt is None:
        opt_local = defaults(opt)
    else:
        opt_local = opt

    if not opt_local.evaluate_model:
        # TODO: this is ugly
        with open(str(opt_local.p_arch / 'history_rmse'), 'rb') as handle:
            h1 = pickle.load(handle)

    vec_epoched_data = []
    for i in range(len(vec_normalized_raw)):
        normalized_raw = vec_normalized_raw[i]
        good_ix = vec_good_ix[i]

        ecg_ch = normalized_raw.info['ch_names'].index('ECG')
        if opt_local.target_ch is None:
            target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
        rs_ch = np.arange(64, 70, 1)
        rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
        x_raw_data = [ECG_raw_data_reshape, rs_raw_data]
        BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(len(target_ch), -1)
        data_set = np.insert(BCG_predicted_raw, ecg_ch, ECG_raw_data, axis=0)
        normalized_raw_copy = mne.io.RawArray(normalized_raw.get_data(), normalized_raw.info)
        normalized_raw_copy.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        MNE_data_set = mne.io.RawArray(data_set, normalized_raw_copy.info)

        epoched_data = dataset_epoch(dataset=MNE_data_set, duration=3, epoch_rejection=False, good_ix=good_ix)

        if len(target_ch) != len(normalized_raw.info['ch_names']) - 7:
            epoched_data = np.transpose(epoched_data, axes=(1, 0, 2))[target_ch, :, :]
        else:
            epoched_data = np.delete(np.transpose(epoched_data, axes=(1, 0, 2)), ecg_ch, axis=0)

        vec_epoched_data.append(epoched_data)

    epoched_data_combined = None
    for i in range(len(vec_epoched_data)):
        if i == 0:
            epoched_data_combined = vec_epoched_data[i]
        else:
            epoched_data_combined = np.append(epoched_data_combined, vec_epoched_data[i], axis=1)

    testEpochPredict = epoched_data_combined[:, vec_ix_slice_test, :]

    vec_n_events_cum = np.cumsum(vec_n_events)

    if len(vec_normalized_raw) == 5:
        vec_run_id = []
        for i in range(testEpochPredict.shape[1]):
            if vec_ix_slice_test[i] < vec_n_events_cum[0]:
                vec_run_id.append(1)
            else:
                for j in range(1, len(vec_good_ix)):
                    if not vec_ix_slice_test[i] < vec_n_events_cum[j - 1] and vec_ix_slice_test[i] < vec_n_events_cum[j]:
                        vec_run_id.append(j + 1)
                        break
        vec_run_id = np.array(vec_run_id)

    else:
        vec_run_id = []
        for i in range(testEpochPredict.shape[1]):
            if vec_ix_slice_test[i] < vec_n_events_cum[0]:
                vec_run_id.append(run_id_actual[0])
            else:
                for j in range(1, len(vec_good_ix)):
                    if not vec_ix_slice_test[i] < vec_n_events_cum[j - 1] and vec_ix_slice_test[i] < vec_n_events_cum[j]:
                        vec_run_id.append(run_id_actual[j])
                        break
        vec_run_id = np.array(vec_run_id)

    test_score = np.sqrt(np.square(np.subtract(y_test, testEpochPredict)).mean(axis=(1, 2)))

    if not opt_local.multi_sub:
        print('Test Score: {:.3f} RMSE'.format(np.mean(test_score)))
        if not opt_local.evaluate_model:
            h1['rmse_overall'] = np.mean(test_score)
            h1['rmse_ch'] = test_score
    else:
        print('Test Score {}: {:.3f} RMSE'.format(sts, np.mean(test_score)))
        f1 = 'rmse_overall_{}'.format(sts)
        f2 = 'rmse_ch_{}'.format(sts)
        if not opt_local.evaluate_model:
            h1[f1] = np.mean(test_score)
            h1[f2] = test_score

    if not opt_local.evaluate_model:
        with open(str(opt_local.p_arch / 'history_rmse'), 'wb') as handle:
            pickle.dump(h1, handle)

    return testEpochPredict, vec_run_id


# Predict based on the raw data, ECG stats = [ECG mean1, ECG mean2, ECG std] and EEG stats = ...
def predict_series(model, normalized_raw, target_ch, ecg_stats, eeg_stats, plot, opt=None):
    opt_local = opt
    ecg_ch = normalized_raw.info['ch_names'].index('ECG')
    if target_ch is None and opt_local.multi_ch and not opt_local.use_rs_data:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']), 1), ecg_ch)
    elif target_ch is None and opt_local.multi_ch and opt_local.use_rs_data:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

    if not opt_local.multi_ch:
        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        EEG_raw_data = normalized_raw.get_data()[target_ch, :].reshape(-1)
        BCG_predicted_raw = model.predict(ECG_raw_data.reshape(1, -1, 1)).reshape(-1)
        if plot:
            EEG_BCG_removed = EEG_raw_data - BCG_predicted_raw

        ECG_raw_renorm = renormalize(ECG_raw_data, ecg_stats, False)
        EEG_raw_renorm = renormalize(EEG_raw_data, eeg_stats, False)
        BCG_predicted_renorm = renormalize(BCG_predicted_raw, eeg_stats, False)
        EEG_BCG_removed_renorm = renormalize(EEG_BCG_removed, eeg_stats, False)
        if plot:
            series_prediction_plot(BCG_predicted_renorm, False, opt=opt_local)

    else:
        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        EEG_raw_data = normalized_raw.get_data()[target_ch, :]
        if not opt_local.use_rs_data:
            BCG_predicted_raw = np.transpose(model.predict(ECG_raw_data.reshape(1, -1, 1)), axes=(0, 2, 1)).reshape(len(target_ch), -1)
        else:
            ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
            rs_ch = np.arange(64, 70, 1)
            rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
            x_raw_data = [ECG_raw_data_reshape, rs_raw_data]

            if opt.use_bcg_input:
                BCG_predicted_raw = predict_bcginput(x_raw_data, model, target_ch)
            else:
                BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(
                len(target_ch), -1)

            if opt_local.save_QRS_series:
                get_QRS_output = K.function([model.layers[0].input],
                                                  [model.layers[2].output])
                QRS_output = get_QRS_output([x_raw_data])[0]

                with open(opt_local.p_arch / 'QRS_series.dat', 'wb') as handle:
                    pickle.dump(QRS_output, handle)

        EEG_BCG_removed = EEG_raw_data - BCG_predicted_raw

        ECG_raw_renorm = renormalize(ECG_raw_data, ecg_stats, False)
        EEG_raw_renorm = renormalize(EEG_raw_data, eeg_stats, True)
        BCG_predicted_renorm = renormalize(BCG_predicted_raw, eeg_stats, True)
        EEG_BCG_removed_renorm = renormalize(EEG_BCG_removed, eeg_stats, True)
        if plot:
            series_prediction_plot(BCG_predicted_renorm, True, opt=opt_local)

    return ECG_raw_renorm, BCG_predicted_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm


def predict_series_multi_run(model, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, sts=None, plot=True, vec_run_id=None, opt=None):
    opt_local = opt

    vec_ECG_raw_renorm = []
    vec_EEG_raw_renorm = []
    vec_BCG_predicted_renorm = []
    vec_EEG_BCG_removed_renorm = []

    for i in range(len(vec_normalized_raw)):
        normalized_raw = vec_normalized_raw[i]
        ecg_stats = vec_ecg_stats[i]
        eeg_stats = vec_eeg_stats[i]
        ecg_ch = normalized_raw.info['ch_names'].index('ECG')
        if opt_local.target_ch is None:
            target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        EEG_raw_data = normalized_raw.get_data()[target_ch, :]

        ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
        rs_ch = np.arange(64, 70, 1)
        rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
        x_raw_data = [ECG_raw_data_reshape, rs_raw_data]
        if opt.use_bcg_input:
            BCG_predicted_raw = predict_bcginput(x_raw_data, model, target_ch)

        else:
            BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(len(target_ch), -1)

        EEG_BCG_removed = EEG_raw_data - BCG_predicted_raw

        ECG_raw_renorm = renormalize(ECG_raw_data, ecg_stats, False, False)
        EEG_raw_renorm = renormalize(EEG_raw_data, eeg_stats, True, False)
        BCG_predicted_renorm = renormalize(BCG_predicted_raw, eeg_stats, True, False)
        EEG_BCG_removed_renorm = renormalize(EEG_BCG_removed, eeg_stats, True, False)

        if opt_local.save_QRS_series:
            get_QRS_output = K.function([model.layers[0].input],
                                        [model.layers[3].output])
            QRS_output = get_QRS_output([x_raw_data])[0].reshape(-1)

            f_out = 'QRS_series_r0{}.dat'.format(i + 1)

            with open(opt_local.p_arch / f_out, 'wb') as handle:
                pickle.dump(QRS_output, handle)

        if plot:
            if len(vec_normalized_raw) == 5:
                series_prediction_plot(BCG_predicted_renorm, True, sts, opt_local, i + 1)
            else:
                series_prediction_plot(BCG_predicted_renorm, True, sts, opt_local, np.unique(vec_run_id)[i])

        vec_ECG_raw_renorm.append(ECG_raw_renorm)
        vec_EEG_raw_renorm.append(EEG_raw_renorm)
        vec_BCG_predicted_renorm.append(BCG_predicted_renorm)
        vec_EEG_BCG_removed_renorm.append(EEG_BCG_removed_renorm)

    return vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm


# Plotting the prediction on a random in a random test epoch
def test_epoch_plot(x_test, y_test, testEpochPredict, ecg_stats, eeg_stats, opt):
    opt_local = opt
    x_test_renorm = renormalize(x_test, ecg_stats, False)
    y_test_renorm = renormalize(y_test, eeg_stats, False)
    testEpochPredict_renorm = renormalize(testEpochPredict, eeg_stats, False)
    y_test_rmbcg_renorm = renormalize(y_test - testEpochPredict, eeg_stats, False)
    vec_ix = np.random.permutation(len(y_test))

    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    for i in range(opt_local.fig_num):
        plt.figure(figsize=(5, 6))
        epoch_num = vec_ix[i]

        plt.subplot(311)
        plt.plot(x_test_renorm[epoch_num, :])
        plt.title('Original ECG data in epoch {}'.format(epoch_num + 1))

        plt.subplot(312)
        plt.plot(testEpochPredict_renorm[epoch_num, :])
        plt.title('Predicted BCG artifact in EEG channel, epoch {}'.format(epoch_num + 1))

        plt.subplot(313)
        plt.plot(y_test_renorm[epoch_num, :], label='EEG raw')
        plt.plot(y_test_rmbcg_renorm[epoch_num, :], label='EEG gru')
        plt.title('Original and cleaned EEG, epoch {}'.format(epoch_num + 1))

        plt.legend()
        plt.tight_layout()
        fig = plt.gcf()
        fig.savefig(opt_local.p_arch / 'Ep{}SCh_TEp{}.svg'.format(epoch_num + 1, epochs), format='svg')


# Plotting the prediction on a random in a random test epoch for multiple channels
def test_epoch_plot_multi_ch(x_test, y_test, testEpochPredict, ecg_stats, eeg_stats, opt=None):
    opt_local = opt
    if not opt_local.use_rs_data:
        x_test_renorm = renormalize(x_test, ecg_stats, False)
    else:
        x_test_renorm = renormalize(x_test[0, :], ecg_stats, False)
    y_test_renorm = renormalize(y_test, eeg_stats, True)
    testEpochPredict_renorm = renormalize(testEpochPredict, eeg_stats, True)
    y_test_rmbcg_renorm = renormalize(y_test - testEpochPredict, eeg_stats, True)

    vec_ix = np.random.permutation(testEpochPredict.shape[0])
    vec_jx = np.random.permutation(testEpochPredict.shape[1])

    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    for i in range(opt_local.fig_num):
        if opt_local.fig_num > testEpochPredict.shape[1]:
            epoch_ix = np.random.randint(0, testEpochPredict.shape[1] - 1, opt_local.fig_num)
            epoch_num = vec_jx[epoch_ix[i]]
        else:
            epoch_num = vec_jx[i]
        target_ch = vec_ix[i]

        plt.figure(figsize=(5, 6))
        plt.subplot(311)
        plt.plot(x_test_renorm[epoch_num, :] * 100000)
        plt.title('Original ECG data in epoch {}'.format(epoch_num + 1))
        plt.xlabel('Samples')
        plt.ylabel('Amplitude (μV)')

        plt.subplot(312)
        plt.plot(testEpochPredict_renorm[target_ch, epoch_num, :] * 100000)
        if target_ch < 31:
            plt.title('Predicted BCG artifact in EEG channel {}, epoch {}'.format(target_ch + 1, epoch_num + 1))
        else:
            plt.title('Predicted BCG artifact in EEG channel {}, epoch {}'.format(target_ch + 2, epoch_num + 1))
        plt.xlabel('Samples')
        plt.ylabel('Amplitude (μV)')

        plt.subplot(313)
        plt.plot(y_test_renorm[target_ch, epoch_num, :] * 100000, label='EEG raw')
        plt.plot(y_test_rmbcg_renorm[target_ch, epoch_num, :] * 100000, label='EEG gru')
        if target_ch < 31:
            plt.title('Original and cleaned EEG in channel {}, epoch {}'.format(target_ch + 1, epoch_num + 1))
        else:
            plt.title('Original and cleaned EEG in channel {}, epoch {}'.format(target_ch + 2, epoch_num + 1))
        plt.xlabel('Samples')
        plt.ylabel('Amplitude (μV)')
        plt.legend()

        p_epoch = Path(opt_local.p_arch).joinpath('Epoch plot')
        p_epoch.mkdir(parents=True, exist_ok=True)

        plt.tight_layout()
        fig = plt.gcf()
        if target_ch < 31:
            fig.savefig(p_epoch / 'Ch{}Ep{}_TEp{}.svg'.format(target_ch + 1, epoch_num + 1, epochs), format='svg')
        else:
            fig.savefig(p_epoch / 'Ch{}Ep{}_TEp{}.svg'.format(target_ch + 2, epoch_num + 1, epochs), format='svg')


def test_epoch_plot_multi_run(x_test, y_test, testEpochPredict, vec_ecg_stats, vec_eeg_stats, vec_run_id, vec_ix_slice_test, vec_n_events, sts=None, opt=None):
    opt_local = opt

    if len(np.unique(vec_run_id)) == 5:
        x_test_renorm = renormalize(x_test[0, :], vec_ecg_stats, False, True, vec_run_id)
        y_test_renorm = renormalize(y_test, vec_eeg_stats, True, True, vec_run_id)
        testEpochPredict_renorm = renormalize(testEpochPredict, vec_eeg_stats, True, True, vec_run_id)
        y_test_rmbcg_renorm = renormalize(y_test - testEpochPredict, vec_eeg_stats, True, True, vec_run_id)
    else:
        vec_run_id_uniq_renorm = np.arange(1, len(np.unique(vec_run_id)) + 1)
        vec_run_id_renorm = copy.deepcopy(vec_run_id)
        for idx_uniq in np.arange(len(np.unique(vec_run_id))):
            vec_run_id_renorm[vec_run_id == np.unique(vec_run_id)[idx_uniq]] = int(vec_run_id_uniq_renorm[idx_uniq])

        x_test_renorm = renormalize(x_test[0, :], vec_ecg_stats, False, True, vec_run_id_renorm)
        y_test_renorm = renormalize(y_test, vec_eeg_stats, True, True, vec_run_id_renorm)
        testEpochPredict_renorm = renormalize(testEpochPredict, vec_eeg_stats, True, True, vec_run_id_renorm)
        y_test_rmbcg_renorm = renormalize(y_test - testEpochPredict, vec_eeg_stats, True, True, vec_run_id_renorm)

    vec_ix = np.random.permutation(testEpochPredict.shape[0])
    vec_n_events_cum = np.cumsum(vec_n_events)

    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    for k in np.unique(vec_run_id):
        vec_relative_ix = np.where(vec_run_id == k)[0]
        vec_true_ix = vec_ix_slice_test[vec_relative_ix]
        vec_jx = np.random.permutation(vec_true_ix.shape[0])

        vec_jx_true_ix = vec_true_ix[vec_jx]
        epoch_ix = np.random.randint(0, vec_relative_ix.shape[0] - 1, opt_local.fig_num)
        if k != 1:
            vec_jx_true_ix = vec_jx_true_ix - vec_n_events_cum[k - 2]

        for i in range(opt_local.fig_num):
            if opt_local.fig_num > vec_relative_ix.shape[0]:
                epoch_num = vec_jx[epoch_ix[i]]
                epoch_num_true_ix = vec_jx_true_ix[epoch_ix[i]]
            else:
                epoch_num = vec_jx[i]
                epoch_num_true_ix = vec_jx_true_ix[i]

            target_ch = vec_ix[i]

            plt.figure(figsize=(6, 6))
            plt.subplot(311)
            plt.plot(x_test_renorm[vec_relative_ix[epoch_num], :] * 100000)
            plt.title('Original ECG data in run {}, epoch {}'.format(k, epoch_num_true_ix + 1))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')

            plt.subplot(312)
            plt.plot(testEpochPredict_renorm[target_ch, vec_relative_ix[epoch_num], :] * 100000)
            if target_ch < 31:
                plt.title('Predicted BCG artifact in EEG, run {}, channel {}, epoch {}'.format(k, target_ch + 1, epoch_num_true_ix + 1))
            else:
                plt.title('Predicted BCG artifact in EEG, run {}, channel {}, epoch {}'.format(k, target_ch + 2, epoch_num_true_ix + 1))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')

            plt.subplot(313)
            plt.plot(y_test_renorm[target_ch, vec_relative_ix[epoch_num], :] * 100000, label='EEG raw')
            plt.plot(y_test_rmbcg_renorm[target_ch, vec_relative_ix[epoch_num], :] * 100000, label='EEG gru')
            if target_ch < 31:
                plt.title('Original and cleaned EEG in run {}, channel {}, epoch {}'.format(k, target_ch + 1, epoch_num_true_ix + 1))
            else:
                plt.title('Original and cleaned EEG in run {}, channel {}, epoch {}'.format(k, target_ch + 2, epoch_num_true_ix + 1))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')
            plt.legend()

            if not opt_local.multi_sub:
                p_epoch = Path(opt_local.p_arch).joinpath('r0{}'.format(k)).joinpath('Epoch plot')
            else:
                p_epoch = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(k)).joinpath('Epoch plot')
            p_epoch.mkdir(parents=True, exist_ok=True)

            plt.tight_layout()
            fig = plt.gcf()
            if target_ch < 31:
                fig.savefig(p_epoch / 'Ch{}Ep{}_TEp{}.svg'.format(target_ch + 1, epoch_num_true_ix + 1, epochs), format='svg')
            else:
                fig.savefig(p_epoch / 'Ch{}Ep{}_TEp{}.svg'.format(target_ch + 2, epoch_num_true_ix + 1, epochs), format='svg')

            plt.close('all')

# Plot the BCG for target channel for the entire series
def series_prediction_plot(BCG_predicted, multi_ch, sts=None, opt=None, run_id=None):
    opt_local = opt
    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    if not multi_ch:
        plt.figure(figsize=(4, 2))
        plt.title('BCG prediction for target channel')
        plt.plot(BCG_predicted)
        plt.tight_layout()
        fig = plt.gcf()
        fig.savefig('BCG_target_Ch_TEp{}.svg'.format(epochs), format='svg')

    else:
        fig, axs = plt.subplots(8, 8, figsize=(20, 10))
        fig.tight_layout()
        for i in range(8):
            for j in range(8):
                if i == 7 and j == 7:
                    break
                ax = axs[i, j]
                ax.plot(BCG_predicted[i * 8 + j, :])
                if i * 8 + j < 31:
                    ax.set_title('Ch{}'.format(i * 8 + j + 1), fontsize=10)
                else:
                    ax.set_title('Ch{}'.format(i * 8 + j + 2), fontsize=10)

        if opt_local.multi_run and not opt_local.multi_sub:
            p_plot = Path(opt_local.p_arch).joinpath('r0{}'.format(run_id))
            p_plot.mkdir(parents=True, exist_ok=True)

            fig.savefig(p_plot / 'BCG_AllCh_TEp{}.svg'.format(epochs), format='svg')
        elif opt_local.multi_run and opt_local.multi_sub:
            p_plot = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(run_id))
            p_plot.mkdir(parents=True, exist_ok=True)

            fig.savefig(p_plot / 'BCG_AllCh_TEp{}.svg'.format(epochs), format='svg')
        elif not opt_local.multi_run and opt_local.multi_sub:
            p_plot = Path(opt_local.p_arch).joinpath(sts)
            p_plot.mkdir(parents=True, exist_ok=True)

            fig.savefig(p_plot / 'BCG_AllCh_TEp{}.svg'.format(epochs), format='svg')
        else:
            fig.savefig(opt_local.p_arch / 'BCG_AllCh_TEp{}.svg'.format(epochs), format='svg')

        plt.close('all')


# Visualize the waveform of original EEG, rmbcg from gru and rmbcg from fmrib in a 10s window
def series_section_plot(ECG_raw_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm, fmrib_raw, opt=None):
    opt_local = opt

    p_section_plot = Path(opt_local.p_arch).joinpath('Section plot')
    p_section_plot.mkdir(parents=True, exist_ok=True)

    length = 5  # length of window to look at in seconds
    fs = 100  # current sampling rate
    interval = length * fs  # interval of the window in samples

    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    if len(fmrib_raw.info['ch_names']) > 64:
        fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    if isinstance(opt_local.target_ch, int) and not opt_local.multi_ch:
        for i in range(opt_local.fig_num):
            vec_ix = np.random.permutation(int(np.floor(len(ECG_raw_renorm)/interval) - 1))
            # Plotting the time series
            plt.figure(figsize=(16, 8))
            plt.subplot(211)
            plt.plot(ECG_raw_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval])

            plt.title("ECG raw in 10s window, starting at {}s".format(vec_ix[0] * length))

            plt.subplot(212)
            plt.plot(EEG_raw_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG raw')
            plt.plot(EEG_BCG_removed_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG GRU')

            EEG_fmrib_raw = fmrib_raw.get_data()[opt_local.target_ch, :]
            EEG_fmrib_raw = EEG_fmrib_raw.reshape(-1)
            plt.plot(EEG_fmrib_raw[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG fmrib')
            plt.title("EEG data in 10s window, starting at {}s".format(vec_ix[0] * length))
            plt.legend()
            plt.tight_layout()
            fig = plt.gcf()
            fig.savefig(p_section_plot / '10s_series_prediction_S{}TEp{}.svg'.format(vec_ix[0], epochs), format='svg')

    elif opt_local.multi_ch and (opt_local.target_ch == [] or opt_local.target_ch is None):
        ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
        target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)
        vec_ix = np.random.permutation(len(target_ch))
        vec_jx = np.random.permutation(int(np.floor(len(ECG_raw_renorm) / interval) - 1))

        for i in range(opt_local.fig_num):
            if opt_local.fig_num > len(vec_jx):
                time_ix = np.random.randint(0, len(vec_jx) - 1, opt_local.fig_num)
                time = vec_jx[time_ix[i]]
            else:
                time = vec_jx[i]
            channel = vec_ix[i]

            plt.figure(figsize=(16, 6))
            # Plotting the time series
            plt.subplot(211)
            plt.plot(ECG_raw_renorm[time * interval: (time + 1) * interval] * 100000)
            plt.title("ECG raw in 5s window, starting at {}s".format(time * length))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')

            plt.subplot(212)
            plt.plot(EEG_raw_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG raw')
            plt.plot(EEG_BCG_removed_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG GRU')

            EEG_fmrib_raw = fmrib_raw.get_data()[target_ch, :]
            EEG_fmrib_raw = EEG_fmrib_raw[channel, :]
            plt.plot(EEG_fmrib_raw[time * interval: (time + 1) * interval] * 100000, label='EEG fmrib')
            if channel < 31:
                plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 1, time * length))
            else:
                plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 2, time * length))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')
            plt.legend()
            plt.tight_layout()
            fig = plt.gcf()
            if channel < 31:
                fig.savefig(p_section_plot / '10s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 1, time * length, epochs), format='svg')
            else:
                fig.savefig(p_section_plot / '10s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 2, time * length, epochs), format='svg')

    elif isinstance(opt_local.target_ch, list) and opt_local.target_ch != [] and opt_local.multi_ch:
        vec_ix = np.random.permutation(opt_local.target_ch)
        vec_jx = np.random.permutation(int(np.floor(len(ECG_raw_renorm) / interval) - 1))
        for i in range(opt_local.fig_num):
            plt.figure(figsize=(16, 8))
            # Plotting the time series
            plt.subplot(211)
            plt.plot(ECG_raw_renorm[vec_jx[i] * interval: (vec_jx[i] + 1) * interval] * 100000)
            plt.title("ECG raw in 10s window, starting at {}s".format(vec_jx[i] * length))

            plt.subplot(212)
            plt.plot(EEG_raw_renorm[vec_ix[i], vec_jx[i] * interval: (vec_jx[i] + 1) * interval] * 100000, label='EEG raw')
            plt.plot(EEG_BCG_removed_renorm[vec_ix[i], vec_jx[i] * interval: (vec_jx[i] + 1) * interval] * 100000, label='EEG GRU')

            EEG_fmrib_raw = fmrib_raw.get_data()[vec_ix[i], :]
            EEG_fmrib_raw = EEG_fmrib_raw.reshape(-1)
            plt.plot(EEG_fmrib_raw[vec_jx[i] * interval: (vec_jx[i] + 1) * interval], label='EEG fmrib')
            if vec_ix[i] < 31:
                plt.title("EEG data in channel {} in 10s window, starting at {}s".format(vec_ix[i] + 1, vec_jx[i] * length))
            else:
                plt.title("EEG data in channel {} in 10s window, starting at {}s".format(vec_ix[i] + 2, vec_jx[i] * length))
            plt.legend()
            plt.tight_layout()
            fig = plt.gcf()
            if vec_ix[i] < 31:
                fig.savefig(p_section_plot / '10s_series_prediction_Ch{}S{}TEp{}.svg'.format(vec_ix[i] + 1, vec_jx[i] * length, epochs), format='svg')
            else:
                fig.savefig(p_section_plot / '10s_series_prediction_Ch{}S{}TEp{}.svg'.format(vec_ix[i] + 2, vec_jx[i] * length, epochs), format='svg')
            plt.close('all')

def series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm, vec_fmrib_raw, sts=None, vec_run_id=None, opt=None):
    opt_local = opt

    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
        epochs = net_settings['epochs']

    length = 5  # length of window to look at in seconds
    fs = 100  # current sampling rate
    interval = length * fs  # interval of the window in samples

    for k in range(len(vec_EEG_BCG_removed_renorm)):
        ECG_raw_renorm = vec_ECG_raw_renorm[k]
        EEG_raw_renorm = vec_EEG_raw_renorm[k]
        EEG_BCG_removed_renorm = vec_EEG_BCG_removed_renorm[k]
        fmrib_raw = vec_fmrib_raw[k]

        if len(vec_EEG_BCG_removed_renorm) == 5:
            if not opt_local.multi_sub:
                p_section_plot = Path(opt_local.p_arch).joinpath('r0{}'.format(k + 1)).joinpath('Section plot')
            else:
                p_section_plot = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(k + 1)).joinpath('Section plot')
            p_section_plot.mkdir(parents=True, exist_ok=True)

        # Handles cases where the number of runs are not equal
        else:
            if not opt_local.multi_sub:
                p_section_plot = Path(opt_local.p_arch).joinpath('r0{}'.format(np.unique(vec_run_id)[k])).joinpath('Section plot')
            else:
                p_section_plot = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(np.unique(vec_run_id)[k])).joinpath('Section plot')
            p_section_plot.mkdir(parents=True, exist_ok=True)

        if len(fmrib_raw.info['ch_names']) > 64:
            fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        if isinstance(opt_local.target_ch, int) and not opt_local.multi_ch:
            for i in range(opt_local.fig_num):
                vec_ix = np.random.permutation(int(np.floor(len(ECG_raw_renorm)/interval) - 1))
                # Plotting the time series
                plt.figure(figsize=(16, 8))
                plt.subplot(211)
                plt.plot(ECG_raw_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval])

                plt.title("ECG raw in 5s window, starting at {}s".format(vec_ix[0] * length))

                plt.subplot(212)
                plt.plot(EEG_raw_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG raw')
                plt.plot(EEG_BCG_removed_renorm[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG GRU')

                EEG_fmrib_raw = fmrib_raw.get_data()[opt_local.target_ch, :]
                EEG_fmrib_raw = EEG_fmrib_raw.reshape(-1)
                plt.plot(EEG_fmrib_raw[vec_ix[0] * interval: (vec_ix[0] + 1) * interval], label='EEG fmrib')
                plt.title("EEG data in 5s window, starting at {}s".format(vec_ix[0] * length))
                plt.legend()
                plt.tight_layout()
                fig = plt.gcf()
                fig.savefig(p_section_plot / '5s_series_prediction_S{}TEp{}.svg'.format(vec_ix[0] * length, epochs), format='svg')
                plt.close('all')

        elif opt_local.multi_ch and (opt_local.target_ch == [] or opt_local.target_ch is None):
            ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
            target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)
            vec_ix = np.random.permutation(len(target_ch))
            vec_jx = np.random.permutation(int(np.floor(len(ECG_raw_renorm) / interval) - 1))

            for i in range(opt_local.fig_num):
                if opt_local.fig_num > len(vec_jx):
                    time_ix = np.random.randint(0, len(vec_jx) - 1, opt_local.fig_num)
                    time = vec_jx[time_ix[i]]
                else:
                    time = vec_jx[i]
                channel = vec_ix[i]

                plt.figure(figsize=(16, 6))
                # Plotting the time series
                plt.subplot(211)
                plt.plot(ECG_raw_renorm[time * interval: (time + 1) * interval] * 100000)
                plt.title("ECG raw in 5s window, starting at {}s".format(time * length))
                plt.xlabel('Samples')
                plt.ylabel('Amplitude (μV)')

                plt.subplot(212)
                plt.plot(EEG_raw_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG raw')
                plt.plot(EEG_BCG_removed_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG GRU')

                EEG_fmrib_raw = fmrib_raw.get_data()[target_ch, :]
                EEG_fmrib_raw = EEG_fmrib_raw[channel, :]
                plt.plot(EEG_fmrib_raw[time * interval: (time + 1) * interval] * 100000, label='EEG fmrib')
                if channel < 31:
                    plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 1, time * length))
                else:
                    plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 2, time * length))
                plt.xlabel('Samples')
                plt.ylabel('Amplitude (μV)')
                plt.legend()
                plt.tight_layout()
                fig = plt.gcf()
                if channel < 31:
                    fig.savefig(p_section_plot / '5s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 1, time * length, epochs), format='svg')
                else:
                    fig.savefig(p_section_plot / '5s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 2, time * length, epochs), format='svg')
                plt.close('all')

        elif (isinstance(opt_local.target_ch, list) and (opt_local.target_ch is not None or opt_local.target_ch != [])) and opt_local.multi_ch:
            target_ch = opt_local.target_ch
            vec_ix = np.random.permutation(target_ch)
            vec_jx = np.random.permutation(int(np.floor(len(ECG_raw_renorm) / interval) - 1))

            for i in range(opt_local.fig_num):
                if opt_local.fig_num > len(vec_jx):
                    time_ix = np.random.randint(0, len(vec_jx) - 1, opt_local.fig_num)
                    time = vec_jx[time_ix[i]]
                else:
                    time = vec_jx[i]
                channel = vec_ix[i]

                plt.figure(figsize=(16, 8))
                # Plotting the time series
                plt.subplot(211)
                plt.plot(ECG_raw_renorm[time * interval: (time + 1) * interval] * 100000)
                plt.title("ECG raw in 5s window, starting at {}s".format(time * length))

                plt.subplot(212)
                plt.plot(EEG_raw_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG raw')
                plt.plot(EEG_BCG_removed_renorm[channel, time * interval: (time + 1) * interval] * 100000, label='EEG GRU')

                EEG_fmrib_raw = fmrib_raw.get_data()[channel, :]
                EEG_fmrib_raw = EEG_fmrib_raw.reshape(-1)
                plt.plot(EEG_fmrib_raw[time * interval: (time + 1) * interval], label='EEG fmrib')
                if vec_ix[i] < 31:
                    plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 1, time * length))
                else:
                    plt.title("EEG data in channel {} in 5s window, starting at {}s".format(channel + 2, time * length))
                plt.legend()
                plt.tight_layout()
                fig = plt.gcf()
                if vec_ix[i] < 31:
                    fig.savefig(p_section_plot / '5s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 1, time * length, epochs), format='svg')
                else:
                    fig.savefig(p_section_plot / '5s_series_prediction_Ch{}S{}TEp{}.svg'.format(channel + 2, time * length, epochs), format='svg')

                plt.close('all')


def PSD_test_multi_ch(x_test, y_test, testEpochPredict, fmrib_raw, ecg_stats, eeg_stats, vec_ix_slice_test, good_ix=[], opt=None):
    opt_local = opt
    with open(str(opt_local.f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)
    epochs = net_settings['epochs']

    target_ch = opt_local.target_ch
    fig_num = opt_local.fig_num

    # Renormalize data
    if not opt_local.use_rs_data:
        ECG_test_raw = np.copy(x_test)
    else:
        ECG_test_raw = np.copy(x_test[0, :])
    EEG_test_raw = np.copy(y_test)

    ECG_test_renorm = renormalize(ECG_test_raw, ecg_stats, False)
    EEG_test_renorm = renormalize(EEG_test_raw, eeg_stats, True)
    EEG_test_nobcg_gru_renorm = renormalize(EEG_test_raw - testEpochPredict, eeg_stats, True)

    if len(fmrib_raw.info['ch_names']) > 64:
        fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    if isinstance(target_ch, list):
        vec_ix = np.random.permutation(y_test.shape[0])
        if fig_num > target_ch:
            raise Exception('PSD plot fails')

        for k in fig_num:
            f_ecg = []
            Pxx_ecg = []
            for i in range(ECG_test_raw.shape[0]):
                f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                f_ecg.append(f_i)
                Pxx_ecg.append((Pxx_ecg_i))

            f_eeg_raw = []
            Pxx_eeg_raw = []
            for i in range(EEG_test_raw.shape[1]):
                f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_raw.append(f_eeg_raw_i)
                Pxx_eeg_raw.append(Pxx_eeg_raw_i)

            f_eeg_gru = []
            Pxx_eeg_gru = []
            for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_gru.append(f_eeg_gru_i)
                Pxx_eeg_gru.append(Pxx_eeg_gru_i)

            fmrib_epoched_data = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)
            EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
            EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_ix_slice_test, :, :], axes=(1, 0, 2))

            f_eeg_fmrib = []
            Pxx_eeg_fmrib = []
            for i in range(EEG_fmrib_test.shape[1]):
                f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_fmrib.append(f_eeg_fmrib_i)
                Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

            Pxx_ecg = np.array(Pxx_ecg)
            Pxx_eeg_raw = np.array(Pxx_eeg_raw)
            Pxx_eeg_gru = np.array(Pxx_eeg_gru)
            Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

            Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
            Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
            Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
            Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

            plt.figure(figsize=(6, 6))
            plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
            plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
            plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
            plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
            plt.legend()
            plt.title('PSD on test data set')
            plt.xlim(0, 30)
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Power (arb.)')
            plt.tight_layout()
            fig = plt.gcf()
            fig.savefig(opt_local.p_arch / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k], epochs), format='svg')

    elif target_ch is None or target_ch == []:
        ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
        if not target_ch:
            target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)
        vec_ix = np.random.permutation(len(target_ch))

        fmrib_epoched_data = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)
        EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
        EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_ix_slice_test, :, :], axes=(1, 0, 2))

        for k in range(fig_num):
            f_ecg = []
            Pxx_ecg = []
            for i in range(ECG_test_raw.shape[0]):
                f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                f_ecg.append(f_i)
                Pxx_ecg.append((Pxx_ecg_i))

            f_eeg_raw = []
            Pxx_eeg_raw = []
            for i in range(EEG_test_raw.shape[1]):
                f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_raw.append(f_eeg_raw_i)
                Pxx_eeg_raw.append(Pxx_eeg_raw_i)

            f_eeg_gru = []
            Pxx_eeg_gru = []
            for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_gru.append(f_eeg_gru_i)
                Pxx_eeg_gru.append(Pxx_eeg_gru_i)

            f_eeg_fmrib = []
            Pxx_eeg_fmrib = []
            for i in range(EEG_fmrib_test.shape[1]):
                f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                f_eeg_fmrib.append(f_eeg_fmrib_i)
                Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

            vec_kx = np.random.permutation(ECG_test_renorm.shape[0])
            plt.figure(figsize=(6, 6))
            plt.subplot(211)
            plt.plot(ECG_test_renorm[vec_kx[0], :] * 100000)
            plt.title('ECG, epoch {}'.format(vec_kx[0] + 1))

            plt.subplot(212)
            plt.plot(EEG_test_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG raw')
            plt.plot(EEG_test_nobcg_gru_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG gru')
            plt.plot(EEG_fmrib_test[vec_ix[k], vec_kx[0], :] * 100000, label='EEG fmrib')
            if vec_ix[k] < 31:
                plt.title('EEG, channel {}, epoch {}'.format(vec_ix[k] + 1, vec_kx[0] + 1))
            else:
                plt.title('EEG, channel {}, epoch {}'.format(vec_ix[k] + 2, vec_kx[0] + 1))
            plt.xlabel('Samples')
            plt.ylabel('Amplitude (μV)')
            plt.legend()
            plt.tight_layout()
            fig = plt.gcf()

            p_test = Path(opt_local.p_arch).joinpath('Test')
            p_test.mkdir(parents=True, exist_ok=True)

            if vec_ix[k] < 31:
                fig.savefig(p_test / 'Test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 1, epochs), format='svg')
            else:
                fig.savefig(p_test / 'Test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 2, epochs), format='svg')

            Pxx_ecg = np.array(Pxx_ecg)
            Pxx_eeg_raw = np.array(Pxx_eeg_raw)
            Pxx_eeg_gru = np.array(Pxx_eeg_gru)
            Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

            Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
            Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
            Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
            Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

            p_psd = Path(opt_local.p_arch).joinpath('PSD')
            p_psd.mkdir(parents=True, exist_ok=True)

            plt.figure(figsize=(6, 6))
            plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
            plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
            plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
            plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
            plt.legend()
            if vec_ix[k] < 31:
                plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 1))
            else:
                plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 2))
            plt.xlim(0, 30)
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Power (arb.)')
            plt.tight_layout()
            fig = plt.gcf()
            if vec_ix[k] < 31:
                fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 1, epochs), format='svg')
            else:
                fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 2, epochs), format='svg')
    plt.close('all')


def PSD_test_multi_run(x_test, y_test, testEpochPredict, vec_fmrib_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id, vec_ix_slice_test, vec_good_ix, vec_n_events, sts=None, opt=None):
    opt_local = opt

    ECG_test_raw = np.copy(x_test[0, :])
    EEG_test_raw = np.copy(y_test)

    if len(np.unique(vec_run_id)) == 5:
        x_test_renorm = renormalize(ECG_test_raw, vec_ecg_stats, False, True, vec_run_id)
        y_test_renorm = renormalize(EEG_test_raw, vec_eeg_stats, True, True, vec_run_id)
        y_test_rmbcg_renorm = renormalize(EEG_test_raw - testEpochPredict, vec_eeg_stats, True, True, vec_run_id)

        vec_n_events_cum = np.cumsum(vec_n_events)

        with open(str(opt_local.f_arch), 'rb') as handle:
            net_settings = pickle.load(handle)
            epochs = net_settings['epochs']

        for j in np.unique(vec_run_id):
            vec_relative_ix = np.where(vec_run_id == j)[0]
            ECG_test_renorm = x_test_renorm[vec_relative_ix, :]
            EEG_test_renorm = y_test_renorm[:, vec_relative_ix, :]
            EEG_test_nobcg_gru_renorm = y_test_rmbcg_renorm[:, vec_relative_ix, :]
            fmrib_raw = vec_fmrib_raw[j - 1]
            good_ix = vec_good_ix[j - 1]
            vec_true_ix = vec_ix_slice_test[vec_relative_ix]

            if j != 1:
                vec_true_ix = vec_true_ix - vec_n_events_cum[j - 2]

            if isinstance(opt_local.target_ch, list):
                vec_ix = np.random.permutation(y_test.shape[0])
                if opt_local.fig_num > opt_local.target_ch:
                    raise Exception('PSD plot fails')
                target_ch = opt_local.target_ch

                for k in opt_local.fig_num:
                    f_ecg = []
                    Pxx_ecg = []
                    for i in range(ECG_test_renorm.shape[0]):
                        f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                        f_ecg.append(f_i)
                        Pxx_ecg.append((Pxx_ecg_i))

                    f_eeg_raw = []
                    Pxx_eeg_raw = []
                    for i in range(EEG_test_renorm.shape[1]):
                        f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_raw.append(f_eeg_raw_i)
                        Pxx_eeg_raw.append(Pxx_eeg_raw_i)

                    f_eeg_gru = []
                    Pxx_eeg_gru = []
                    for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                        f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100,
                                                                  nperseg=300)
                        f_eeg_gru.append(f_eeg_gru_i)
                        Pxx_eeg_gru.append(Pxx_eeg_gru_i)

                    epochs = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)
                    fmrib_epoched_data = epochs.get_data()
                    EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
                    EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_true_ix, :, :], axes=(1, 0, 2))

                    f_eeg_fmrib = []
                    Pxx_eeg_fmrib = []
                    for i in range(EEG_fmrib_test.shape[1]):
                        f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_fmrib.append(f_eeg_fmrib_i)
                        Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

                    Pxx_ecg = np.array(Pxx_ecg)
                    Pxx_eeg_raw = np.array(Pxx_eeg_raw)
                    Pxx_eeg_gru = np.array(Pxx_eeg_gru)
                    Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

                    Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
                    Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
                    Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
                    Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

                    if not opt_local.multi_sub:
                        p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('PSD')
                    else:
                        p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('PSD')
                    p_psd.mkdir(parents=True, exist_ok=True)

                    plt.figure(figsize=(6, 6))
                    plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
                    plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
                    plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
                    plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
                    plt.legend()
                    plt.title('PSD on test data set')
                    plt.xlim(0, 30)
                    plt.xlabel('Frequency (Hz)')
                    plt.ylabel('Power (arb.)')
                    plt.tight_layout()
                    fig = plt.gcf()
                    fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k], epochs), format='svg')
                    plt.close('all')

            elif opt_local.target_ch is None or opt_local.target_ch == []:
                ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
                target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)
                vec_ix = np.random.permutation(len(target_ch))

                fmrib_epoched_data = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False,
                                                   good_ix=good_ix)
                EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
                EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_true_ix, :, :], axes=(1, 0, 2))

                for k in range(opt_local.fig_num):
                    f_ecg = []
                    Pxx_ecg = []
                    for i in range(ECG_test_renorm.shape[0]):
                        f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                        f_ecg.append(f_i)
                        Pxx_ecg.append((Pxx_ecg_i))

                    f_eeg_raw = []
                    Pxx_eeg_raw = []
                    for i in range(EEG_test_renorm.shape[1]):
                        f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_raw.append(f_eeg_raw_i)
                        Pxx_eeg_raw.append(Pxx_eeg_raw_i)

                    f_eeg_gru = []
                    Pxx_eeg_gru = []
                    for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                        f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100,
                                                                  nperseg=300)
                        f_eeg_gru.append(f_eeg_gru_i)
                        Pxx_eeg_gru.append(Pxx_eeg_gru_i)

                    f_eeg_fmrib = []
                    Pxx_eeg_fmrib = []
                    for i in range(EEG_fmrib_test.shape[1]):
                        f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_fmrib.append(f_eeg_fmrib_i)
                        Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

                    vec_kx = np.random.permutation(ECG_test_renorm.shape[0])
                    vec_true_ix_per = vec_true_ix[vec_kx]
                    plt.figure(figsize=(6, 6))
                    plt.subplot(211)
                    plt.plot(ECG_test_renorm[vec_kx[0], :] * 100000)
                    plt.title('ECG, run{}, epoch {}'.format(j, vec_true_ix_per[0] + 1))

                    plt.subplot(212)
                    plt.plot(EEG_test_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG raw')
                    plt.plot(EEG_test_nobcg_gru_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG gru')
                    plt.plot(EEG_fmrib_test[vec_ix[k], vec_kx[0], :] * 100000, label='EEG fmrib')
                    if vec_ix[k] < 31:
                        plt.title('EEG, run {}, channel {}, epoch {}'.format(j, vec_ix[k] + 1, vec_true_ix_per[0] + 1))
                    else:
                        plt.title('EEG, run {}, channel {}, epoch {}'.format(j, vec_ix[k] + 2, vec_true_ix_per[0] + 1))
                    plt.xlabel('Samples')
                    plt.ylabel('Amplitude (μV)')
                    plt.legend()
                    plt.tight_layout()
                    fig = plt.gcf()

                    if len(np.unique(vec_run_id)) == 5:
                        if not opt_local.multi_sub:
                            p_test = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('Test')
                        else:
                            p_test = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('Test')
                        p_test.mkdir(parents=True, exist_ok=True)

                        if not opt_local.multi_sub:
                            p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('PSD')
                        else:
                            p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('PSD')
                        p_psd.mkdir(parents=True, exist_ok=True)

                    else:
                        if not opt_local.multi_sub:
                            p_test = Path(opt_local.p_arch).joinpath('r0{}'.format(np.unique(vec_run_id)[j])).joinpath(
                                'Test')
                        else:
                            p_test = Path(opt_local.p_arch).joinpath(sts).joinpath(
                                'r0{}'.format(np.unique(vec_run_id)[j])).joinpath('Test')
                        p_test.mkdir(parents=True, exist_ok=True)

                        if not opt_local.multi_sub:
                            p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(np.unique(vec_run_id)[j])).joinpath(
                                'PSD')
                        else:
                            p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath(
                                'r0{}'.format(np.unique(vec_run_id)[j])).joinpath('PSD')
                        p_psd.mkdir(parents=True, exist_ok=True)

                    if vec_ix[k] < 31:
                        fig.savefig(
                            p_test / 'Test_Ch{}Ep{}_TEp{}.svg'.format(vec_ix[k] + 1, vec_true_ix_per[0] + 1, epochs),
                            format='svg')
                    else:
                        fig.savefig(
                            p_test / 'Test_Ch{}Ep{}_TEp{}.svg'.format(vec_ix[k] + 2, vec_true_ix_per[0] + 1, epochs),
                            format='svg')

                    Pxx_ecg = np.array(Pxx_ecg)
                    Pxx_eeg_raw = np.array(Pxx_eeg_raw)
                    Pxx_eeg_gru = np.array(Pxx_eeg_gru)
                    Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

                    Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
                    Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
                    Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
                    Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

                    plt.figure(figsize=(6, 6))
                    plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
                    plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
                    plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
                    plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
                    plt.legend()
                    if vec_ix[k] < 31:
                        plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 1))
                    else:
                        plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 2))
                    plt.xlim(0, 30)
                    plt.xlabel('Frequency (Hz)')
                    plt.ylabel('Power (arb.)')
                    plt.tight_layout()
                    fig = plt.gcf()
                    if vec_ix[k] < 31:
                        fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 1, epochs), format='svg')
                    else:
                        fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 2, epochs), format='svg')
                    plt.close('all')

    # Handles the case where the run doesn't have 5 runs
    else:
        vec_run_id_uniq_renorm = np.arange(1, len(np.unique(vec_run_id)) + 1)
        vec_run_id_renorm = copy.deepcopy(vec_run_id)
        for idx_uniq in np.arange(len(np.unique(vec_run_id))):
            vec_run_id_renorm[vec_run_id == np.unique(vec_run_id)[idx_uniq]] = int(vec_run_id_uniq_renorm[idx_uniq])

        x_test_renorm = renormalize(ECG_test_raw, vec_ecg_stats, False, True, vec_run_id_renorm)
        y_test_renorm = renormalize(EEG_test_raw, vec_eeg_stats, True, True, vec_run_id_renorm)
        y_test_rmbcg_renorm = renormalize(EEG_test_raw - testEpochPredict, vec_eeg_stats, True, True, vec_run_id_renorm)

        vec_n_events_cum = np.cumsum(vec_n_events)

        with open(str(opt_local.f_arch), 'rb') as handle:
            net_settings = pickle.load(handle)
            epochs = net_settings['epochs']

        for j in np.unique(vec_run_id_uniq_renorm):
            vec_relative_ix = np.where(vec_run_id_renorm == j)[0]
            ECG_test_renorm = x_test_renorm[vec_relative_ix, :]
            EEG_test_renorm = y_test_renorm[:, vec_relative_ix, :]
            EEG_test_nobcg_gru_renorm = y_test_rmbcg_renorm[:, vec_relative_ix, :]
            fmrib_raw = vec_fmrib_raw[j - 1]
            good_ix = vec_good_ix[j - 1]
            vec_true_ix = vec_ix_slice_test[vec_relative_ix]

            if j != 1:
                vec_true_ix = vec_true_ix - vec_n_events_cum[j - 2]

            if isinstance(opt_local.target_ch, list):
                vec_ix = np.random.permutation(y_test.shape[0])
                if opt_local.fig_num > opt_local.target_ch:
                    raise Exception('PSD plot fails')
                target_ch = opt_local.target_ch

                for k in opt_local.fig_num:
                    f_ecg = []
                    Pxx_ecg = []
                    for i in range(ECG_test_renorm.shape[0]):
                        f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                        f_ecg.append(f_i)
                        Pxx_ecg.append((Pxx_ecg_i))

                    f_eeg_raw = []
                    Pxx_eeg_raw = []
                    for i in range(EEG_test_renorm.shape[1]):
                        f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_raw.append(f_eeg_raw_i)
                        Pxx_eeg_raw.append(Pxx_eeg_raw_i)

                    f_eeg_gru = []
                    Pxx_eeg_gru = []
                    for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                        f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_gru.append(f_eeg_gru_i)
                        Pxx_eeg_gru.append(Pxx_eeg_gru_i)

                    epochs = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)
                    fmrib_epoched_data = epochs.get_data()
                    EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
                    EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_true_ix, :, :], axes=(1, 0, 2))

                    f_eeg_fmrib = []
                    Pxx_eeg_fmrib = []
                    for i in range(EEG_fmrib_test.shape[1]):
                        f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_fmrib.append(f_eeg_fmrib_i)
                        Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

                    Pxx_ecg = np.array(Pxx_ecg)
                    Pxx_eeg_raw = np.array(Pxx_eeg_raw)
                    Pxx_eeg_gru = np.array(Pxx_eeg_gru)
                    Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

                    Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
                    Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
                    Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
                    Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

                    if not opt_local.multi_sub:
                        p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('PSD')
                    else:
                        p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('PSD')
                    p_psd.mkdir(parents=True, exist_ok=True)

                    plt.figure(figsize=(6, 6))
                    plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
                    plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
                    plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
                    plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
                    plt.legend()
                    plt.title('PSD on test data set')
                    plt.xlim(0, 30)
                    plt.xlabel('Frequency (Hz)')
                    plt.ylabel('Power (arb.)')
                    plt.tight_layout()
                    fig = plt.gcf()
                    fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k], epochs), format='svg')
                    plt.close('all')

            elif opt_local.target_ch is None or opt_local.target_ch == []:
                ecg_ch = fmrib_raw.info['ch_names'].index('ECG')
                target_ch = np.delete(np.arange(0, len(fmrib_raw.info['ch_names']), 1), ecg_ch)
                vec_ix = np.random.permutation(len(target_ch))

                fmrib_epoched_data = dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)
                EEG_fmrib_epochs = fmrib_epoched_data[:, target_ch, :]
                EEG_fmrib_test = np.transpose(EEG_fmrib_epochs[vec_true_ix, :, :], axes=(1, 0, 2))

                for k in range(opt_local.fig_num):
                    f_ecg = []
                    Pxx_ecg = []
                    for i in range(ECG_test_renorm.shape[0]):
                        f_i, Pxx_ecg_i = signal.welch(ECG_test_renorm[i, :], 100, nperseg=300)
                        f_ecg.append(f_i)
                        Pxx_ecg.append((Pxx_ecg_i))

                    f_eeg_raw = []
                    Pxx_eeg_raw = []
                    for i in range(EEG_test_renorm.shape[1]):
                        f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(EEG_test_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_raw.append(f_eeg_raw_i)
                        Pxx_eeg_raw.append(Pxx_eeg_raw_i)

                    f_eeg_gru = []
                    Pxx_eeg_gru = []
                    for i in range(EEG_test_nobcg_gru_renorm.shape[1]):
                        f_eeg_gru_i, Pxx_eeg_gru_i = signal.welch(EEG_test_nobcg_gru_renorm[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_gru.append(f_eeg_gru_i)
                        Pxx_eeg_gru.append(Pxx_eeg_gru_i)

                    f_eeg_fmrib = []
                    Pxx_eeg_fmrib = []
                    for i in range(EEG_fmrib_test.shape[1]):
                        f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(EEG_fmrib_test[vec_ix[k], i, :], 100, nperseg=300)
                        f_eeg_fmrib.append(f_eeg_fmrib_i)
                        Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_i)

                    vec_kx = np.random.permutation(ECG_test_renorm.shape[0])
                    vec_true_ix_per = vec_true_ix[vec_kx]
                    plt.figure(figsize=(6, 6))
                    plt.subplot(211)
                    plt.plot(ECG_test_renorm[vec_kx[0], :] * 100000)
                    plt.title('ECG, run{}, epoch {}'.format(j, vec_true_ix_per[0] + 1))

                    plt.subplot(212)
                    plt.plot(EEG_test_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG raw')
                    plt.plot(EEG_test_nobcg_gru_renorm[vec_ix[k], vec_kx[0], :] * 100000, label='EEG gru')
                    plt.plot(EEG_fmrib_test[vec_ix[k], vec_kx[0], :] * 100000, label='EEG fmrib')
                    if vec_ix[k] < 31:
                        plt.title('EEG, run {}, channel {}, epoch {}'.format(j, vec_ix[k] + 1, vec_true_ix_per[0] + 1))
                    else:
                        plt.title('EEG, run {}, channel {}, epoch {}'.format(j, vec_ix[k] + 2, vec_true_ix_per[0] + 1))
                    plt.xlabel('Samples')
                    plt.ylabel('Amplitude (μV)')
                    plt.legend()
                    plt.tight_layout()
                    fig = plt.gcf()

                    if len(vec_run_id) == 5:
                        if not opt_local.multi_sub:
                            p_test = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('Test')
                        else:
                            p_test = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('Test')
                        p_test.mkdir(parents=True, exist_ok=True)

                        if not opt_local.multi_sub:
                            p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(j)).joinpath('PSD')
                        else:
                            p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(j)).joinpath('PSD')
                        p_psd.mkdir(parents=True, exist_ok=True)

                    else:
                        if not opt_local.multi_sub:
                            p_test = Path(opt_local.p_arch).joinpath('r0{}'.format(np.unique(vec_run_id)[j - 1])).joinpath('Test')
                        else:
                            p_test = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(np.unique(vec_run_id)[j - 1])).joinpath('Test')
                        p_test.mkdir(parents=True, exist_ok=True)

                        if not opt_local.multi_sub:
                            p_psd = Path(opt_local.p_arch).joinpath('r0{}'.format(np.unique(vec_run_id)[j - 1])).joinpath('PSD')
                        else:
                            p_psd = Path(opt_local.p_arch).joinpath(sts).joinpath('r0{}'.format(np.unique(vec_run_id)[j - 1])).joinpath('PSD')
                        p_psd.mkdir(parents=True, exist_ok=True)

                    if vec_ix[k] < 31:
                        fig.savefig(p_test / 'Test_Ch{}Ep{}_TEp{}.svg'.format(vec_ix[k] + 1, vec_true_ix_per[0] + 1, epochs), format='svg')
                    else:
                        fig.savefig(p_test / 'Test_Ch{}Ep{}_TEp{}.svg'.format(vec_ix[k] + 2, vec_true_ix_per[0] + 1, epochs), format='svg')

                    Pxx_ecg = np.array(Pxx_ecg)
                    Pxx_eeg_raw = np.array(Pxx_eeg_raw)
                    Pxx_eeg_gru = np.array(Pxx_eeg_gru)
                    Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

                    Pxx_ecg_avg = np.mean(Pxx_ecg, axis=0)
                    Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
                    Pxx_eeg_gru_avg = np.mean(Pxx_eeg_gru, axis=0)
                    Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

                    plt.figure(figsize=(6, 6))
                    plt.semilogy(f_ecg[0], Pxx_ecg_avg, label='ECG')
                    plt.semilogy(f_ecg[0], Pxx_eeg_raw_avg, label='EEG raw')
                    plt.semilogy(f_ecg[0], Pxx_eeg_gru_avg, label='EEG gru')
                    plt.semilogy(f_ecg[0], Pxx_eeg_fmrib_avg, label='EEG fmrib')
                    plt.legend()
                    if vec_ix[k] < 31:
                        plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 1))
                    else:
                        plt.title('PSD on test data set for channel {}'.format(vec_ix[k] + 2))
                    plt.xlim(0, 30)
                    plt.xlabel('Frequency (Hz)')
                    plt.ylabel('Power (arb.)')
                    plt.tight_layout()
                    fig = plt.gcf()
                    if vec_ix[k] < 31:
                        fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 1, epochs), format='svg')
                    else:
                        fig.savefig(p_psd / 'PSD_test_Ch{}_TEp{}.svg'.format(vec_ix[k] + 2, epochs), format='svg')
                    plt.close('all')


def load_model(arch, mode=None, f_arch_in=None, opt=None):
    opt_local = opt

    if mode == 'pre-trained':
        str_arch = f_arch_in.split('.')[0]
        opt_local.p_arch = opt_local.p_arch.joinpath(str_arch)
        f_arch = opt_local.p_arch.joinpath(Path(opt_local.f_arch))

    else:
        str_arch = opt_local.f_arch.split('.')[0]
        opt_local.p_arch = opt_local.p_arch.joinpath(str_arch)
        f_arch = opt_local.p_arch.joinpath(Path(opt_local.f_arch))

    if not f_arch.exists():
        raise Exception('Specified architecture does not exist')

    with open(str(f_arch), 'rb') as handle:
        net_settings = pickle.load(handle)

    weights = net_settings['weights']

    model = get_arch(arch, opt=opt_local)
    model.set_weights(weights=weights)

    return model


#  TODO: Should merge use_rs_data and ga_mc into same tag
def generate_cleaned_set(model, normalized_raw, ecg_stats, eeg_stats, opt=None):
    opt_local = opt

    ecg_ch = normalized_raw.info['ch_names'].index('ECG')
    if not opt_local.use_rs_data:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']), 1), ecg_ch)
        raw_info = normalized_raw.info
    else:
        target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

        raw_data = normalized_raw.get_data()
        raw_info = normalized_raw.info
        normalized_raw_copy = mne.io.RawArray(raw_data, raw_info)
        normalized_raw_copy.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        raw_info = normalized_raw_copy.info

    ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
    EEG_raw_data = normalized_raw.get_data()[target_ch, :]
    if not opt_local.use_rs_data:
        BCG_predicted_raw = np.transpose(model.predict(ECG_raw_data.reshape(1, -1, 1)), axes=(0, 2, 1)).reshape(len(target_ch), -1)
    else:
        ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
        rs_ch = np.arange(64, 70, 1)
        rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
        x_raw_data = [ECG_raw_data_reshape, rs_raw_data]
        if opt.use_bcg_input:
            BCG_predicted_raw = predict_bcginput(x_raw_data, model, target_ch)
        else:
            BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(len(target_ch), -1)

    EEG_BCG_removed = EEG_raw_data - BCG_predicted_raw

    ECG_raw_renorm = renormalize(ECG_raw_data, ecg_stats, False)
    EEG_BCG_removed_renorm = renormalize(EEG_BCG_removed, eeg_stats, True)
    data = np.insert(EEG_BCG_removed_renorm, ecg_ch, ECG_raw_renorm, axis=0)
    bcg_net_raw = mne.io.RawArray(data, raw_info)

    return bcg_net_raw


def predict_bcginput(x_raw_data, model, target_ch):
    #  if we are using bcg_input, then we need to evaluate as if we had epochs...
    # because otherwise the bcg input side of the network will just spit out a single value for the entire time series
    # but we want this value to change over time...
    # BCG_predicted_raw = np.zeros((len(target_ch), x_raw_data[0].shape[1]))
    # len_s = x_raw_data[0].shape[1]
    # x_chunk = [0, 1]
    # half_epoch = 150  # should not be hardcoded
    # for ix_s in range(len_s):
    #     vec_slice = np.arange(ix_s - half_epoch, ix_s + half_epoch)
    #     vec_slice = vec_slice[vec_slice >= 0]
    #     vec_slice = vec_slice[vec_slice < len_s]
    #     x_chunk[0] = x_raw_data[0][:, vec_slice, :]
    #     x_chunk[1] = x_raw_data[1][:, vec_slice, :]
    #     BCG_predicted_raw[:, ix_s] = model.predict(x_chunk)[0, vec_slice == ix_s, :]

    # turns out that the above is actually pretty bad - maybe because the bcgnet side of the network is overfitted.
    # So replace the above method with standard prediction, until you think of something better.
    BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(len(target_ch), -1)

    return BCG_predicted_raw


def generate_cleaned_set_multi_run(model, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id=None, opt=None):
    opt_local = opt
    vec_bcg_net_raw = []

    for i in range(len(vec_normalized_raw)):
        normalized_raw = vec_normalized_raw[i]
        ecg_stats = vec_ecg_stats[i]
        eeg_stats = vec_eeg_stats[i]

        raw_data = normalized_raw.get_data()
        raw_info = normalized_raw.info
        normalized_raw_copy = mne.io.RawArray(raw_data, raw_info)
        normalized_raw_copy.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        raw_info = normalized_raw_copy.info

        ecg_ch = normalized_raw.info['ch_names'].index('ECG')
        if opt_local.target_ch is None:
            target_ch = np.delete(np.arange(0, len(normalized_raw.info['ch_names']) - 6, 1), ecg_ch)

        ECG_raw_data = normalized_raw.get_data()[ecg_ch, :]
        EEG_raw_data = normalized_raw.get_data()[target_ch, :]

        ECG_raw_data_reshape = ECG_raw_data.reshape(1, -1, 1)
        rs_ch = np.arange(64, 70, 1)
        rs_raw_data = np.transpose(normalized_raw.get_data()[rs_ch, :]).reshape(1, -1, 6)
        x_raw_data = [ECG_raw_data_reshape, rs_raw_data]
        if opt_local.use_bcg_input:
            BCG_predicted_raw = predict_bcginput(x_raw_data, model, target_ch)
        else:
            BCG_predicted_raw = np.transpose(model.predict(x_raw_data), axes=(0, 2, 1)).reshape(len(target_ch), -1)

        EEG_BCG_removed = EEG_raw_data - BCG_predicted_raw

        ECG_raw_renorm = renormalize(ECG_raw_data, ecg_stats, False, False)
        EEG_BCG_removed_renorm = renormalize(EEG_BCG_removed, eeg_stats, True, False)

        data = np.insert(EEG_BCG_removed_renorm, ecg_ch, ECG_raw_renorm, axis=0)

        bcg_net_raw = mne.io.RawArray(data, raw_info)
        vec_bcg_net_raw.append(bcg_net_raw)

    return vec_bcg_net_raw


def generate_bcg_dataset_multi_run(vec_ecg_raw_renorm, vec_bcg_predicted_renorm, vec_normalized_raw, opt=None):
    opt_local = opt
    vec_bcg_predicted_raw = []

    for i in range(len(vec_bcg_predicted_renorm)):
        ecg_raw_renorm = vec_ecg_raw_renorm[i]
        bcg_predicted_renorm = vec_bcg_predicted_renorm[i]
        normalized_raw = vec_normalized_raw[i]

        raw_data = normalized_raw.get_data()
        raw_info = normalized_raw.info
        normalized_raw_copy = mne.io.RawArray(raw_data, raw_info)
        normalized_raw_copy.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        raw_info = normalized_raw_copy.info

        ecg_ch = normalized_raw.info['ch_names'].index('ECG')
        data = np.insert(bcg_predicted_renorm, ecg_ch, ecg_raw_renorm, axis=0)

        bcg_predicted_raw = mne.io.RawArray(data, raw_info)
        vec_bcg_predicted_raw.append(bcg_predicted_raw)

    return vec_bcg_predicted_raw


def interpolator(bcg_net_raw, fmrib_raw):
    bcg_net_raw_data = bcg_net_raw.get_data()
    t_bcg_net_raw = bcg_net_raw.times
    t_fmrib_raw = fmrib_raw.times

    bcg_interpolator = interpolate.PchipInterpolator(t_bcg_net_raw, bcg_net_raw_data, axis=1, extrapolate=True)
    bcg_net_interpolated = bcg_interpolator(t_fmrib_raw, extrapolate=True)

    return bcg_net_interpolated


def interpolator_multi_run(vec_bcg_net_raw, vec_fmrib_raw):
    vec_bcg_net_interpolated = []

    for i in range(len(vec_bcg_net_raw)):
        bcg_net_raw = vec_bcg_net_raw[i]
        fmrib_raw = vec_fmrib_raw[i]

        bcg_net_raw_data = bcg_net_raw.get_data()
        t_bcg_net_raw = bcg_net_raw.times
        t_fmrib_raw = fmrib_raw.times

        bcg_interpolator = interpolate.PchipInterpolator(t_bcg_net_raw, bcg_net_raw_data, axis=1, extrapolate=True)
        bcg_net_interpolated_data = bcg_interpolator(t_fmrib_raw, extrapolate=True)

        vec_bcg_net_interpolated.append(bcg_net_interpolated_data)

    return vec_bcg_net_interpolated


def user_defined_opt(opt):
    if opt is None:
        opt = TrainDefault()
        opt.epochs = 500
        opt.ga_mc = False
        opt.p_arch = None
        opt.f_arch = None
        opt.target_ch = None
        opt.es_min_delta = 1e-5
        opt.es_patience = 25  # How many times does the validation not increase
        opt.early_stopping = True
        opt.resume = False
        opt.validation = 0.15
        opt.evaluation = 0.85
        opt.fig_num = 3
        opt.multi_ch = True
        opt.multi_run = False
        opt.use_rs_data = False
    elif isinstance(opt, dict):
        assert False, 'need to add this option?'
    return opt


# Main function loop
def main_loop(str_sub, run_id, arch, opt=None):
    print('Process starting')
    starttime = datetime.datetime.now()
    print("The start time is {}\n\n".format(starttime.strftime("%Y/%m/%d %H:%M:%S")))

    if opt is None:
        opt_user = user_defined_opt(None)
    else:
        opt_user = opt

    if not opt_user.evaluate_model:
        if not opt_user.multi_run:
            if not isinstance(run_id, int):
                raise Exception('For single-run trials, run_id should be an integer')

            for run_ix in range(run_id - 1, run_id):

                p_ga, f_ga = set_env.ga_path(str_sub, run_id)
                p_rs, f_rs = set_env.rs_path(str_sub, run_id)
                p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)
                p_arch = set_env.arch_path(str_sub, run_id)

                p_arch.mkdir(parents=True, exist_ok=True)
                opt_user.p_arch = p_arch  # Don't like this here, needs reorganization later

                if opt_user.target_ch is None and not opt_user.multi_ch:
                    raise Exception('Must choose a channel if not using the multiple channel mode')
                elif not(isinstance(opt_user.target_ch, int)) and not opt_user.multi_ch:
                    raise Exception('Can only choose 1 channel only if not using multiple channel mode')
                elif opt_user.multi_ch and not(isinstance(opt_user.target_ch, (int, tuple, np.ndarray))) and not opt_user.target_ch is None:
                    raise Exception('Must choose more than 1 channels if using the multiple channel mode')
                elif opt_user.resume and opt_user.f_arch is None:
                    raise Exception('Can only resume if have the structure')

                if opt_user.ga_mc:
                    data_dir = str(p_ga.joinpath(f_ga))
                    normalized_raw, Epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir, opt_user.target_ch, opt=opt_user)
                    good_ix = len(Epoched_data)
                else:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, Epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir, opt_user.target_ch, opt=opt_user)

                ecg_stats = [ecg_mean, ecg_std]
                eeg_stats = [eeg_mean, eeg_std]

                net_gru, x_test, y_test, vec_ix_slice_test = train_ds(Epoched_data, opt_user.target_ch, arch=arch, opt=opt_user)
                testEpochPredict = predict_epoch(net_gru, x_test, y_test, normalized_raw, vec_ix_slice_test, good_ix, opt=opt_user)

                if not opt_user.multi_ch:
                    test_epoch_plot(x_test, y_test, testEpochPredict, ecg_stats, eeg_stats, opt=opt_user)
                else:
                    test_epoch_plot_multi_ch(x_test, y_test, testEpochPredict, ecg_stats, eeg_stats, opt=opt_user)
                ECG_raw_renorm, BCG_predicted_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm = predict_series(net_gru, normalized_raw, opt_user.target_ch, ecg_stats, eeg_stats, True, opt=opt_user)

                fmrib_dir = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                series_section_plot(ECG_raw_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm, fmrib_raw, opt=opt_user)
                PSD_test_multi_ch(x_test, y_test, testEpochPredict, fmrib_raw, ecg_stats=ecg_stats, eeg_stats=eeg_stats, vec_ix_slice_test=vec_ix_slice_test, good_ix=good_ix, opt=opt_user)

                if opt_user.dataset_gen:
                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                    bcg_net_raw = generate_cleaned_set(net_gru, normalized_raw, ecg_stats, eeg_stats, opt=opt_user)
                    bcg_net_interpolated = interpolator(bcg_net_raw, fmrib_raw)

                    p_out, f_out = set_env.output_path(str_sub, run_id, arch, opt=opt_user)
                    p_out.mkdir(parents=True, exist_ok=True)

                    sio.savemat(str(p_out.joinpath(f_out)), {'data': bcg_net_interpolated})
        else:

            if not isinstance(run_id, (list, tuple, np.ndarray)):
                raise Exception('For multi-run trials, run_id should be a list, tuple or numpy array')

            p_arch = set_env.arch_path(str_sub, run_id)
            p_arch.mkdir(parents=True, exist_ok=True)
            opt_user.p_arch = p_arch

            vec_normalized_raw = []
            vec_epoched_data = []
            vec_ecg_stats = []
            vec_eeg_stats = []
            vec_good_ix = []
            vec_fmrib_raw = []

            run_id_actual = []

            for i in range(len(run_id)):
                p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
                p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
                p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                # Exception handling for subjects with less than 5 runs
                data_dir_rs = p_rs.joinpath(f_rs)
                data_dir_bcg = p_bcg.joinpath(f_bcg)

                if not data_dir_rs.exists() or not data_dir_bcg.exists():
                    continue

                if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                    continue

                try:
                    data_dir_rs = str(p_rs.joinpath(f_rs))
                    rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                    data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                except RuntimeWarning:
                    continue

                if opt_user.target_ch is None and not opt_user.multi_ch:
                    raise Exception('Must choose a channel if not using the multiple channel mode')
                elif not (isinstance(opt_user.target_ch, int)) and not opt_user.multi_ch:
                    raise Exception('Can only choose 1 channel only if not using multiple channel mode')
                elif opt_user.multi_ch and not (isinstance(opt_user.target_ch, (int, tuple, np.ndarray))) and not opt_user.target_ch is None:
                    raise Exception('Must choose more than 1 channels if using the multiple channel mode')
                elif opt_user.resume and opt_user.f_arch is None:
                    raise Exception('Can only resume if have the structure')
                elif opt_user.multi_run and not opt_user.use_rs_data:
                    raise Exception('Recommended to use motion data when training across runs')

                if (opt_user.ga_mc + opt_user.use_time_encoding + opt_user.use_bcg_input) > 1:
                    raise Exception('opt_user is overspecified')

                if opt_user.ga_mc:
                    data_dir = str(p_ga.joinpath(f_ga))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir, opt_user.target_ch, opt=opt_user)
                    good_ix = []

                elif opt_user.use_time_encoding:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                    data_dir, opt_user.target_ch, run_id=i, N=len(run_id), opt=opt_user)

                elif opt_user.use_bcg_input:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                    data_dir, opt_user.target_ch, opt=opt_user)

                else:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                        data_dir, opt_user.target_ch, opt=opt_user)

                ecg_stats = [ecg_mean, ecg_std]
                eeg_stats = [eeg_mean, eeg_std]

                fmrib_dir = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                vec_normalized_raw.append(normalized_raw)
                vec_epoched_data.append(epoched_data)
                vec_ecg_stats.append(ecg_stats)
                vec_eeg_stats.append(eeg_stats)
                vec_good_ix.append(good_ix)
                vec_fmrib_raw.append(fmrib_raw)

                run_id_actual.append(run_id[i])

            epoched_data_combined = None
            for i in range(len(vec_epoched_data)):
                if i == 0:
                    epoched_data_combined = vec_epoched_data[i]
                else:
                    epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data[i]])

            net_gru, x_test, y_test, vec_ix_slice_test = train_ds(epoched_data_combined, opt_user.target_ch,
                                                                  arch=arch, opt=opt_user)

            vec_n_events = []

            for i in range(len(vec_epoched_data)):
                n_events = len(vec_epoched_data[i].get_data())
                vec_n_events.append(n_events)

            testEpochPredict, vec_run_id = predict_epoch_multi_run(net_gru, y_test, vec_normalized_raw,
                                                                   vec_ix_slice_test, vec_good_ix,
                                                                   vec_n_events, run_id_actual=run_id_actual,
                                                                   opt=opt_user)
            print('<-{}'.format(str_sub))

            test_epoch_plot_multi_run(x_test, y_test, testEpochPredict, vec_ecg_stats, vec_eeg_stats, vec_run_id,
                                      vec_ix_slice_test, vec_n_events, opt=opt_user)

            vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(net_gru, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id=vec_run_id, opt=opt_user)

            series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
                                          vec_fmrib_raw, vec_run_id=vec_run_id, opt=opt_user)

            PSD_test_multi_run(x_test, y_test, testEpochPredict,
                               vec_fmrib_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id,
                               vec_ix_slice_test, vec_good_ix, vec_n_events, opt=opt_user)

            if opt_user.dataset_gen:
                # Had to reload the fmrib dataset to obtain the correct time stamps
                vec_fmrib_raw = []
                for i in range(len(run_id)):
                    p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
                    vec_fmrib_raw.append(fmrib_raw)

                vec_bcg_net_raw = generate_cleaned_set_multi_run(net_gru, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, opt=opt_user)
                vec_bcg_net_interpolated = interpolator_multi_run(vec_bcg_net_raw, vec_fmrib_raw)
                for i in range(len(run_id)):
                    p_out, f_out = set_env.output_path(str_sub, run_id[i], arch, opt=opt_user)
                    p_out.mkdir(parents=True, exist_ok=True)

                    sio.savemat(str(p_out.joinpath(f_out)), {'data': vec_bcg_net_interpolated[i]})

    else:
        #  Choosing to evaluate
        if not opt_user.multi_run:
            # Evaluating the single run trials
            if not isinstance(run_id, int):
                raise Exception('For single-run trials, run_id should be an integer')

            if opt_user.target_ch is not None or not opt_user.multi_ch:
                assert Exception("Can't generate all channels at the same time")

            p_ga, f_ga = set_env.ga_path(str_sub, run_id)
            p_rs, f_rs = set_env.rs_path(str_sub, run_id)
            p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)
            p_arch = set_env.arch_path(str_sub, run_id)
            opt_user.p_arch = p_arch

            if opt_user.ga_mc:
                data_dir = str(p_ga.joinpath(f_ga))
                normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir,
                                                                                                     opt_user.target_ch,
                                                                                                     opt=opt_user)
                good_ix = len(epoched_data)

            else:
                data_dir = str(p_rs.joinpath(f_rs))
                normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir,
                                                                                                              opt_user.target_ch,
                                                                                                              opt=opt_user)

            ecg_stats = [ecg_mean, ecg_std]
            eeg_stats = [eeg_mean, eeg_std]

            fmrib_dir = str(p_bcg.joinpath(f_bcg))
            fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
            fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

            model = load_model(arch, opt=opt_user)

            if opt_user.dataset_gen:
                bcg_net_raw = generate_cleaned_set(model, normalized_raw, ecg_stats, eeg_stats, opt=opt_user)
                bcg_net_interpolated = interpolator(bcg_net_raw, fmrib_raw)

                p_out, f_out = set_env.output_path(str_sub, run_id, arch, opt=opt_user)
                p_out.mkdir(parents=True, exist_ok=True)

                sio.savemat(str(p_out.joinpath(f_out)), {'data': bcg_net_interpolated})

                if not opt_user.figgen:
                    x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test = generate_train_valid_test(
                        epoched_data, opt=opt_user)

                    testEpochPredict = predict_epoch(model, x_test, y_test, normalized_raw, vec_ix_slice_test, good_ix,
                                                     opt=opt_user)

            if opt_user.figgen:

                fmrib_raw.resample(100)
                opt_user.f_arch = opt_user.p_arch.joinpath(opt_user.f_arch)
                with open(str(opt_user.f_arch), 'rb') as handle:
                    net_settings = pickle.load(handle)

                epochs = net_settings['epochs']
                opt_user.p_arch = opt_user.p_arch.joinpath('Fig_regen_TEp{}'.format(epochs))

                x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test = generate_train_valid_test(epoched_data, opt=opt_user)

                testEpochPredict = predict_epoch(model, x_test, y_test, normalized_raw, vec_ix_slice_test, good_ix, opt=opt_user)

                test_epoch_plot_multi_ch(x_test, y_test, testEpochPredict, ecg_stats, eeg_stats, opt=opt_user)

                ECG_raw_renorm, BCG_predicted_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm = predict_series(model, normalized_raw, opt_user.target_ch, ecg_stats, eeg_stats, True, opt_user)

                series_section_plot(ECG_raw_renorm, EEG_raw_renorm, EEG_BCG_removed_renorm, fmrib_raw, opt=opt_user)

                PSD_test_multi_ch(x_test, y_test, testEpochPredict, fmrib_raw, ecg_stats, eeg_stats, vec_ix_slice_test,
                                  good_ix=good_ix, opt=opt_user)
        else:
            #  Evaluating the multi-run trained models
            if opt_user.f_arch is None:
                raise Exception('Must specify an architecture to evaluate')
            if not isinstance(run_id, (list, tuple, np.ndarray)):
                raise Exception('For multi-run trials, run_id should be a list, tuple or numpy array')

            p_arch = set_env.arch_path(str_sub, run_id)
            opt_user.p_arch = p_arch

            vec_normalized_raw = []
            vec_epoched_data = []
            vec_ecg_stats = []
            vec_eeg_stats = []
            vec_good_ix = []
            vec_fmrib_raw = []

            run_id_actual = []

            for i in range(len(run_id)):
                p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
                p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
                p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                # Exception handling for subjects with less than 5 runs
                data_dir_rs = p_rs.joinpath(f_rs)
                data_dir_bcg = p_bcg.joinpath(f_bcg)

                if not data_dir_rs.exists() or not data_dir_bcg.exists():
                    continue

                if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                    continue

                try:
                    data_dir_rs = str(p_rs.joinpath(f_rs))
                    rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                    data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                except RuntimeWarning:
                    continue

                if opt_user.target_ch is None and not opt_user.multi_ch:
                    raise Exception('Must choose a channel if not using the multiple channel mode')
                elif not (isinstance(opt_user.target_ch, int)) and not opt_user.multi_ch:
                    raise Exception('Can only choose 1 channel only if not using multiple channel mode')
                elif opt_user.multi_ch and not (isinstance(opt_user.target_ch, (int, tuple, np.ndarray))) and not opt_user.target_ch is None:
                    raise Exception('Must choose more than 1 channels if using the multiple channel mode')
                elif opt_user.multi_run and not opt_user.use_rs_data:
                    raise Exception('Recommended to use motion data when training across runs')

                if opt_user.ga_mc:
                    data_dir = str(p_ga.joinpath(f_ga))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir, opt_user.target_ch, opt=opt_user)
                    good_id = []

                elif not opt_user.ga_mc and not opt_user.use_time_encoding:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir, opt_user.target_ch, opt=opt_user)

                elif not opt_user.ga_mc and opt_user.use_time_encoding:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir, opt_user.target_ch, run_id=i, N=len(run_id), opt=opt_user)

                ecg_stats = [ecg_mean, ecg_std]
                eeg_stats = [eeg_mean, eeg_std]

                fmrib_dir = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                vec_normalized_raw.append(normalized_raw)
                vec_epoched_data.append(epoched_data)
                vec_ecg_stats.append(ecg_stats)
                vec_eeg_stats.append(eeg_stats)
                vec_good_ix.append(good_ix)
                vec_fmrib_raw.append(fmrib_raw)

                run_id_actual.append(run_id[i])

            model = load_model(arch, opt=opt_user)

            epoched_data_combined = None
            for i in range(len(vec_epoched_data)):
                if i == 0:
                    epoched_data_combined = vec_epoched_data[i]
                else:
                    epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data[i]])

            x_ev_train, x_ev_validation, x_test, y_ev_train, y_ev_validation, y_test, vec_ix_slice_test = generate_train_valid_test(
                epoched_data_combined, opt=opt_user)

            vec_n_events = []
            for i in range(len(vec_epoched_data)):
                n_events = len(vec_epoched_data[i].get_data())
                vec_n_events.append(n_events)

            testEpochPredict, vec_run_id = predict_epoch_multi_run(model, y_test, vec_normalized_raw,
                                                                   vec_ix_slice_test, vec_good_ix,
                                                                   vec_n_events, run_id_actual=run_id_actual,
                                                                   opt=opt_user)

            if opt_user.dataset_gen:
                vec_bcg_net_raw = generate_cleaned_set_multi_run(model, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, opt=opt_user)
                vec_bcg_net_interpolated = interpolator_multi_run(vec_bcg_net_raw, vec_fmrib_raw)
                for i in range(len(vec_bcg_net_interpolated)):
                    rid = run_id_actual[i]
                    p_out, f_out = set_env.output_path(str_sub, rid, arch, opt=opt_user)
                    p_out.mkdir(parents=True, exist_ok=True)

                    sio.savemat(str(p_out.joinpath(f_out)), {'data': vec_bcg_net_interpolated[i]})

            if opt.save_bcg_dataset:
                opt_user.f_arch = opt_user.p_arch.joinpath(opt_user.f_arch)

                vec_ecg_raw_renorm, vec_eeg_raw_renorm, vec_bcg_predicted_renorm, vec_eeg_bcg_removed_renorm = predict_series_multi_run(
                    model, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats,
                    vec_run_id=vec_run_id, plot=False, opt=opt_user)

                vec_bcg_predicted_raw = generate_bcg_dataset_multi_run(vec_ecg_raw_renorm, vec_bcg_predicted_renorm,
                                                                       vec_normalized_raw, opt=opt_user)

                vec_bcg_predicted_interpolated = interpolator_multi_run(vec_bcg_predicted_raw, vec_fmrib_raw)

                for i in range(len(vec_bcg_predicted_interpolated)):
                    rid = run_id_actual[i]
                    p_out, f_out = set_env.bcg_output_path(str_sub, rid, arch, opt=opt_user)
                    p_out.mkdir(parents=True, exist_ok=True)

                    sio.savemat(str(p_out.joinpath(f_out)), {'data': vec_bcg_predicted_interpolated[i]})

            if opt_user.figgen:
                for i in range(len(vec_fmrib_raw)):
                    fmrib_raw = vec_fmrib_raw[i]
                    fmrib_raw.resample(100)

                opt_user.f_arch = opt_user.p_arch.joinpath(opt_user.f_arch)
                with open(str(opt_user.f_arch), 'rb') as handle:
                    net_settings = pickle.load(handle)

                epochs = net_settings['epochs']
                opt_user.p_arch = opt_user.p_arch.joinpath('Fig_regen_TEp{}'.format(epochs))

                testEpochPredict, vec_run_id = predict_epoch_multi_run(model, y_test, vec_normalized_raw,
                                                                       vec_ix_slice_test, vec_good_ix,
                                                                       vec_n_events, run_id_actual=run_id_actual,
                                                                       opt=opt_user)

                test_epoch_plot_multi_run(x_test, y_test, testEpochPredict, vec_ecg_stats, vec_eeg_stats, vec_run_id,
                                          vec_ix_slice_test, vec_n_events, opt=opt_user)

                vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
                    model, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id=vec_run_id, opt=opt_user)

                series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
                                              vec_fmrib_raw, vec_run_id=vec_run_id, opt=opt_user)

                PSD_test_multi_run(x_test, y_test, testEpochPredict,
                                   vec_fmrib_raw, vec_ecg_stats, vec_eeg_stats, vec_run_id,
                                   vec_ix_slice_test, vec_good_ix, vec_n_events, opt=opt_user)

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    duration_in_s = duration.total_seconds()
    hours = round(divmod(duration_in_s, 3600)[0])
    if hours == 0:
        minutes = round(divmod(duration_in_s, 60)[0])
        seconds = round(divmod(duration_in_s, 60)[1])
    else:
        minutes = round(divmod(divmod(duration_in_s, 3600)[1], 60)[0])
        seconds = round(divmod(divmod(duration_in_s, 3600)[1], 60)[1])

    if not opt_user.evaluate_model:
        with open(opt_user.p_arch / 'history_rmse', 'rb') as handle:
            h1 = pickle.load(handle)

        h1['time'] = (hours, minutes, seconds)

        with open(opt_user.p_arch / 'history_rmse', 'wb') as handle:
            pickle.dump(h1, handle)

    print('\n\nProcess ending')
    print("The end time is {}/{}/{} {}:{}:{}\n".format(datetime.datetime.now().strftime("%Y"),
                                                       datetime.datetime.now().strftime("%m"),
                        datetime.datetime.now().strftime("%d"), datetime.datetime.now().strftime("%H"),
                           datetime.datetime.now().strftime("%M"), datetime.datetime.now().strftime("%S")))
    print("The program took {:0>2}:{:0>2}:{:0>2} in total".format(hours, minutes, seconds))


def main_loop_multi_sub(vec_str_sub, run_id, arch, opt=None):
    if not isinstance(vec_str_sub, (list, tuple, np.ndarray)):
        raise Exception('For multi-run, str_sub must be a list, tuple or numpy array')

    if opt is None:
        opt = user_defined_opt(None)

    if not opt.multi_sub:
        raise Exception('For multi-sub runs, you must set multi_sub flag to true in opt')

    if not opt.evaluate_model:
        if not opt.multi_run:
            raise Exception('Not implemented yet')

        for sub_id in range(len(vec_str_sub)):
            print('Process starting')
            starttime = datetime.datetime.now()
            print("The start time is {}\n\n".format(starttime.strftime("%Y/%m/%d %H:%M:%S")))

            opt_user = copy.deepcopy(opt)

            vec_str_sub_local = vec_str_sub.copy()
            str_sub = vec_str_sub[sub_id]

            p_arch = set_env.arch_path(vec_str_sub, run_id, arch=arch)
            p_arch = p_arch.joinpath(str_sub)
            opt_user.p_arch = p_arch

            vec_str_sub_local.pop(sub_id)

            vec_normalized_raw_sub = []
            vec_epoched_data_sub = []
            vec_ecg_stats_sub = []
            vec_eeg_stats_sub = []
            vec_good_ix_sub = []
            vec_fmrib_raw_sub = []
            vec_sts = []

            vec_run_id_actual_sub = []

            for i in range(len(vec_str_sub_local)):
                sts = vec_str_sub_local[i]
                vec_normalized_raw = []
                vec_epoched_data = []
                vec_ecg_stats = []
                vec_eeg_stats = []
                vec_good_ix = []
                vec_fmrib_raw = []

                run_id_actual = []

                for j in range(len(run_id)):
                    sts = vec_str_sub_local[i]
                    rid = run_id[j]

                    p_ga, f_ga = set_env.ga_path(sts, rid)
                    p_rs, f_rs = set_env.rs_path(sts, rid)
                    p_bcg, f_bcg = set_env.bcg_path(sts, rid)

                    # Exception handling for subjects with less than 5 runs
                    data_dir_rs = p_rs.joinpath(f_rs)
                    data_dir_bcg = p_bcg.joinpath(f_bcg)

                    if not data_dir_rs.exists() or not data_dir_bcg.exists():
                        continue

                    if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                        continue

                    try:
                        data_dir_rs = str(p_rs.joinpath(f_rs))
                        rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                        data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                        fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                    except RuntimeWarning:
                        continue

                    if opt_user.target_ch is None and not opt_user.multi_ch:
                        raise Exception('Must choose a channel if not using the multiple channel mode')
                    elif not (isinstance(opt_user.target_ch, int)) and not opt_user.multi_ch:
                        raise Exception('Can only choose 1 channel only if not using multiple channel mode')
                    elif opt_user.multi_ch and not (isinstance(opt_user.target_ch, (int, tuple, np.ndarray))) and not opt_user.target_ch is None:
                        raise Exception('Must choose more than 1 channels if using the multiple channel mode')
                    elif opt_user.resume and opt_user.f_arch is None:
                        raise Exception('Can only resume if have the structure')
                    elif opt_user.multi_run and not opt_user.use_rs_data:
                        raise Exception('Recommended to use motion data when training across runs and subjects')

                    if opt_user.ga_mc:
                        data_dir = str(p_ga.joinpath(f_ga))
                        normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir,
                                                                                                             opt_user.target_ch,
                                                                                                             opt=opt_user)
                        good_ix = []
                    elif not opt_user.ga_mc and not opt_user.use_time_encoding:
                        data_dir = str(p_rs.joinpath(f_rs))
                        normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                            data_dir, opt_user.target_ch, opt=opt_user)

                    elif not opt_user.ga_mc and opt_user.use_time_encoding:
                        data_dir = str(p_rs.joinpath(f_rs))
                        normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                            data_dir, opt_user.target_ch, run_id=rid, N=len(run_id), opt=opt_user)

                    ecg_stats = [ecg_mean, ecg_std]
                    eeg_stats = [eeg_mean, eeg_std]

                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                    vec_normalized_raw.append(normalized_raw)
                    vec_epoched_data.append(epoched_data)
                    vec_ecg_stats.append(ecg_stats)
                    vec_eeg_stats.append(eeg_stats)
                    vec_good_ix.append(good_ix)
                    vec_fmrib_raw.append(fmrib_raw)

                    run_id_actual.append(rid)

                vec_normalized_raw_sub.append(vec_normalized_raw)
                vec_epoched_data_sub.append(vec_epoched_data)
                vec_ecg_stats_sub.append(vec_ecg_stats)
                vec_eeg_stats_sub.append(vec_eeg_stats)
                vec_good_ix_sub.append(vec_good_ix)
                vec_fmrib_raw_sub.append(vec_fmrib_raw)
                vec_sts.append(sts)

                vec_run_id_actual_sub.append(run_id_actual)

            vec_epoched_data_combined_sub = []
            for i in range(len(vec_epoched_data_sub)):
                vec_epoched_data = vec_epoched_data_sub[i]

                epoched_data_combined = None
                for j in range(len(vec_epoched_data)):
                    if j == 0:
                        epoched_data_combined = vec_epoched_data[j]
                    else:
                        epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data[j]])

                vec_epoched_data_combined_sub.append(epoched_data_combined)

            vec_n_events_sub = []
            for i in range(len(vec_epoched_data_sub)):
                vec_epoched_data = vec_epoched_data_sub[i]

                vec_n_events = []
                for j in range(len(vec_epoched_data)):
                    n_events = len(vec_epoched_data[j].get_data())
                    vec_n_events.append(n_events)
                vec_n_events_sub.append(vec_n_events)

            vec_x_train_sub = []
            vec_x_validation_sub = []
            vec_x_test_sub = []
            vec_y_train_sub = []
            vec_y_validation_sub = []
            vec_y_test_sub = []
            vec_ix_slice_test_sub = []
            for i in range(len(vec_epoched_data_combined_sub)):
                epoched_data_combined = vec_epoched_data_combined_sub[i]
                x_train, x_validation, x_test, y_train, y_validation, y_test, vec_ix_slice_test = generate_train_valid_test(epoched_data_combined, opt_user)

                vec_x_train_sub.append(x_train)
                vec_x_validation_sub.append(x_validation)
                vec_x_test_sub.append(x_test)
                vec_y_train_sub.append(y_train)
                vec_y_validation_sub.append(y_validation)
                vec_y_test_sub.append(y_test)
                vec_ix_slice_test_sub.append(vec_ix_slice_test)

            x_train_combined = None
            for i in range(len(vec_x_train_sub)):
                if i == 0:
                    x_train_combined = vec_x_train_sub[i]
                else:
                    x_train_combined = np.append(x_train_combined, vec_x_train_sub[i], axis=1)

            x_validation_combined = None
            for i in range(len(vec_x_validation_sub)):
                if i == 0:
                    x_validation_combined = vec_x_validation_sub[i]
                else:
                    x_validation_combined = np.append(x_validation_combined, vec_x_validation_sub[i], axis=1)

            x_test_combined = None
            for i in range(len(vec_x_test_sub)):
                if i == 0:
                    x_test_combined = vec_x_test_sub[i]
                else:
                    x_test_combined = np.append(x_test_combined, vec_x_test_sub[i], axis=1)

            y_train_combined = None
            for i in range(len(vec_y_train_sub)):
                if i == 0:
                    y_train_combined = vec_y_train_sub[i]
                else:
                    y_train_combined = np.append(y_train_combined, vec_y_train_sub[i], axis=1)

            y_validation_combined = None
            for i in range(len(vec_y_validation_sub)):
                if i == 0:
                    y_validation_combined = vec_y_validation_sub[i]
                else:
                    y_validation_combined = np.append(y_validation_combined, vec_y_validation_sub[i], axis=1)

            y_test_combined = None
            for i in range(len(vec_y_test_sub)):
                if i == 0:
                    y_test_combined = vec_y_test_sub[i]
                else:
                    y_test_combined = np.append(y_test_combined, vec_y_test_sub[i], axis=1)

            model_gru = train_sp(x_train_combined, x_validation_combined, y_train_combined, y_validation_combined,
                                 arch=arch, opt=opt_user)
            test_epoch_predict_sub = []
            vec_run_id_sub = []
            for i in range(len(vec_normalized_raw_sub)):
                x_test = vec_x_test_sub[i]
                y_test = vec_y_test_sub[i]
                vec_normalized_raw = vec_normalized_raw_sub[i]
                vec_fmrib_raw = vec_fmrib_raw_sub[i]
                vec_ecg_stats = vec_ecg_stats_sub[i]
                vec_eeg_stats = vec_eeg_stats_sub[i]
                vec_ix_slice_test = vec_ix_slice_test_sub[i]
                vec_good_ix = vec_good_ix_sub[i]
                vec_n_events = vec_n_events_sub[i]
                sts = vec_sts[i]

                run_id_actual = vec_run_id_actual_sub[i]

                test_epoch_predict, vec_run_id = predict_epoch_multi_run(model_gru, y_test, vec_normalized_raw,
                                                                         vec_ix_slice_test, vec_good_ix, vec_n_events,
                                                                         run_id_actual=run_id_actual,
                                                                         sts=sts, opt=opt_user)
                test_epoch_predict_sub.append(test_epoch_predict)
                vec_run_id_sub.append(vec_run_id)

                vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(model_gru, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, sts=sts, vec_run_id=vec_run_id, plot=True, opt=opt_user)

                test_epoch_plot_multi_run(x_test, y_test, test_epoch_predict, vec_ecg_stats, vec_eeg_stats,
                                          vec_run_id, vec_ix_slice_test, vec_n_events, sts=sts, opt=opt_user)

                series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
                                              vec_fmrib_raw, vec_run_id=vec_run_id, sts=sts, opt=opt_user)

                PSD_test_multi_run(x_test, y_test, test_epoch_predict, vec_fmrib_raw,
                                   vec_ecg_stats, vec_eeg_stats, vec_run_id, vec_ix_slice_test, vec_good_ix,
                                   vec_n_events, sts=sts, opt=opt_user)

                plt.close('all')

            # Loading the main subject for testing
            vec_normalized_raw_main = []
            vec_epoched_data_main = []
            vec_ecg_stats_main = []
            vec_eeg_stats_main = []
            vec_good_ix_main = []
            vec_fmrib_raw_main = []
            run_id_actual_main = []

            for i in range(len(run_id)):
                p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
                p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
                p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                # Exception handling for subjects with less than 5 runs
                data_dir_rs = p_rs.joinpath(f_rs)
                data_dir_bcg = p_bcg.joinpath(f_bcg)

                if not data_dir_rs.exists() or not data_dir_bcg.exists():
                    continue

                if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                    continue

                try:
                    data_dir_rs = str(p_rs.joinpath(f_rs))
                    rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                    data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                except RuntimeWarning:
                    continue

                if opt_user.ga_mc:
                    data_dir = str(p_ga.joinpath(f_ga))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir, opt_user.target_ch, opt=opt_user)
                    good_ix = []
                else:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir, opt_user.target_ch, opt=opt_user)

                ecg_stats = [ecg_mean, ecg_std]
                eeg_stats = [eeg_mean, eeg_std]

                fmrib_dir = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                vec_normalized_raw_main.append(normalized_raw)
                vec_epoched_data_main.append(epoched_data)
                vec_ecg_stats_main.append(ecg_stats)
                vec_eeg_stats_main.append(eeg_stats)
                vec_good_ix_main.append(good_ix)
                vec_fmrib_raw_main.append(fmrib_raw)

                run_id_actual_main.append(run_id[i])

            epoched_data_combined_main = None
            for i in range(len(vec_epoched_data_main)):
                if i == 0:
                    epoched_data_combined_main = vec_epoched_data_main[i]
                else:
                    epoched_data_combined_main = mne.concatenate_epochs([epoched_data_combined_main, vec_epoched_data_main[i]])

            vec_n_events_main = []
            for i in range(len(vec_epoched_data_main)):
                n_events = len(vec_epoched_data_main[i].get_data())
                vec_n_events_main.append(n_events)

            x_train_main, x_validation_main, x_test_main, y_train_main, y_validation_main, y_test_main, vec_ix_slice_test_main = generate_train_valid_test(epoched_data_combined_main, opt_user)
            sts = str_sub +'_pre_training'

            test_epoch_predict_main_pre, vec_run_id = predict_epoch_multi_run(model_gru, y_test_main, vec_normalized_raw_main,
                                                                              vec_ix_slice_test_main, vec_good_ix_main,
                                                                              vec_n_events_main, run_id_actual=run_id_actual_main,
                                                                              sts=sts, opt=opt_user)

            vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
                model_gru, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, sts=sts, vec_run_id=vec_run_id,
                plot=True, opt=opt_user)


            test_epoch_plot_multi_run(x_test_main, y_test_main, test_epoch_predict_main_pre, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
                                      vec_ix_slice_test_main, vec_n_events_main, sts=sts, opt=opt_user)

            series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm, vec_fmrib_raw_main,
                                          vec_run_id=vec_run_id, sts=sts, opt=opt_user)

            PSD_test_multi_run(x_test_main, y_test_main, test_epoch_predict_main_pre, vec_fmrib_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
                               vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main, sts=sts, opt=opt_user)

            plt.close('all')

            with open(opt_user.f_arch, 'rb') as handle:
                h1 = pickle.load(handle)

            net_name = str(Path(opt_user.f_arch).parts[-1])
            net_name_pre = net_name.split('.')[0] + '_pre_trained.' + net_name.split('.')[1]
            net_path = Path(opt_user.p_arch).parents[0]

            with open(net_path.joinpath(net_name_pre), 'wb') as handle:
                pickle.dump(h1, handle)

            opt_user.f_arch = Path(opt_user.f_arch).parts[-1]
            opt_user.p_arch = Path(opt_user.p_arch).parents[1]
            opt_user.resume = True

            model_gru_new = train_sp(x_train_main, x_validation_main, y_train_main, y_validation_main, arch=arch, overwrite=False, held_out=True, opt=opt_user)
            sts = str_sub +'_post_training'

            test_epoch_predict_main_post, vec_run_id = predict_epoch_multi_run(model_gru_new, y_test_main, vec_normalized_raw_main,
                                                                     vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main,
                                                                     run_id_actual=run_id_actual_main, sts=sts, opt=opt_user)

            vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
                model_gru_new, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id=vec_run_id,
                sts=sts, plot=True, opt=opt_user)


            test_epoch_plot_multi_run(x_test_main, y_test_main, test_epoch_predict_main_post, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
                                      vec_ix_slice_test_main, vec_n_events_main, sts=sts, opt=opt_user)

            series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm, vec_fmrib_raw_main,
                                          vec_run_id=vec_run_id, sts=sts, opt=opt_user)

            PSD_test_multi_run(x_test_main, y_test_main, test_epoch_predict_main_post, vec_fmrib_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
                               vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main, sts=sts, opt=opt_user)

            plt.close('all')
            K.clear_session()
            gc.collect()

            endtime = datetime.datetime.now()
            duration = endtime - starttime
            duration_in_s = duration.total_seconds()
            hours = round(divmod(duration_in_s, 3600)[0])
            if hours == 0:
                minutes = round(divmod(duration_in_s, 60)[0])
                seconds = round(divmod(duration_in_s, 60)[1])
            else:
                minutes = round(divmod(divmod(duration_in_s, 3600)[1], 60)[0])
                seconds = round(divmod(divmod(duration_in_s, 3600)[1], 60)[1])

            with open(opt_user.p_arch / 'history_rmse', 'rb') as handle:
                h1 = pickle.load(handle)

            h1['time'] = (hours, minutes, seconds)

            with open(opt_user.p_arch / 'history_rmse', 'wb') as handle:
                pickle.dump(h1, handle)

            print('\n\nProcess ending')
            print("The end time is {}/{}/{} {}:{}:{}\n".format(datetime.datetime.now().strftime("%Y"),
                                                               datetime.datetime.now().strftime("%m"),
                                datetime.datetime.now().strftime("%d"), datetime.datetime.now().strftime("%H"),
                                   datetime.datetime.now().strftime("%M"), datetime.datetime.now().strftime("%S")))
            print("The program took {:0>2}:{:0>2}:{:0>2} in total".format(hours, minutes, seconds))

    else:
        if not isinstance(opt.f_arch, (list, tuple, np.ndarray)):
            raise Exception('To evaluate multi-sub runs, f_arch must be a list, tuple or numpy array')

        if not opt.multi_run:
            raise Exception('Not implemented yet')

        else:
            for sub_id in range(len(vec_str_sub)):
                print('Process starting')
                print("The start time is {}/{}/{} {}:{}:{}\n\n".format(datetime.datetime.now().strftime("%Y"),
                                                                       datetime.datetime.now().strftime("%m"),
                                                                       datetime.datetime.now().strftime("%d"),
                                                                       datetime.datetime.now().strftime("%H"),
                                                                       datetime.datetime.now().strftime("%M"),
                                                                       datetime.datetime.now().strftime("%S")))

                starttime = datetime.datetime.now()

                opt_user = copy.deepcopy(opt)
                str_sub = vec_str_sub[sub_id]

                p_arch = set_env.arch_path(vec_str_sub, run_id, arch=arch)
                p_arch = p_arch.joinpath(str_sub)
                opt_user.p_arch = p_arch

                f_arch_pre = opt_user.f_arch[sub_id].split('.')[0] + '_pre_trained.' + opt_user.f_arch[sub_id].split('.')[1]
                f_arch_post = opt_user.f_arch[sub_id]
                opt_user.f_arch = f_arch_pre

                vec_normalized_raw_main = []
                vec_epoched_data_main = []
                vec_ecg_stats_main = []
                vec_eeg_stats_main = []
                vec_good_ix_main = []
                vec_fmrib_raw_main = []
                run_id_actual = []

                for i in range(len(run_id)):
                    p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
                    p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
                    p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

                    # Exception handling for subjects with less than 5 runs
                    data_dir_rs = p_rs.joinpath(f_rs)
                    data_dir_bcg = p_bcg.joinpath(f_bcg)

                    if not data_dir_rs.exists() or not data_dir_bcg.exists():
                        continue

                    if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                        continue

                    try:
                        data_dir_rs = str(p_rs.joinpath(f_rs))
                        rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                        data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                        fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                    except RuntimeWarning:
                        continue

                    if opt_user.ga_mc:
                        data_dir = str(p_ga.joinpath(f_ga))
                        normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir,
                                                                                                             opt_user.target_ch,
                                                                                                             opt=opt_user)
                        good_ix = []
                    else:
                        data_dir = str(p_rs.joinpath(f_rs))
                        normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                            data_dir, opt_user.target_ch, opt=opt_user)

                    ecg_stats = [ecg_mean, ecg_std]
                    eeg_stats = [eeg_mean, eeg_std]

                    fmrib_dir = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                    vec_normalized_raw_main.append(normalized_raw)
                    vec_epoched_data_main.append(epoched_data)
                    vec_ecg_stats_main.append(ecg_stats)
                    vec_eeg_stats_main.append(eeg_stats)
                    vec_good_ix_main.append(good_ix)
                    vec_fmrib_raw_main.append(fmrib_raw)
                    run_id_actual.append(run_id[i])

                epoched_data_combined_main = None
                for i in range(len(vec_epoched_data_main)):
                    if i == 0:
                        epoched_data_combined_main = vec_epoched_data_main[i]
                    else:
                        epoched_data_combined_main = mne.concatenate_epochs(
                            [epoched_data_combined_main, vec_epoched_data_main[i]])

                vec_n_events_main = []
                for i in range(len(vec_epoched_data_main)):
                    n_events = len(vec_epoched_data_main[i].get_data())
                    vec_n_events_main.append(n_events)

                x_train_main, x_validation_main, x_test_main, y_train_main, y_validation_main, y_test_main, vec_ix_slice_test_main = generate_train_valid_test(
                    epoched_data_combined_main, opt=opt_user)

                model_pre = load_model(arch, mode='pre-trained', f_arch_in=f_arch_post, opt=opt_user)

                opt_user.p_arch = p_arch
                opt_user.f_arch = f_arch_post
                model_post = load_model(arch, opt=opt_user)

                testEpochPredict_pre, vec_run_id_pre = predict_epoch_multi_run(model_pre, y_test_main,
                                                                               vec_normalized_raw_main,
                                                                               vec_ix_slice_test_main, vec_good_ix_main,
                                                                               vec_n_events_main,
                                                                               run_id_actual=run_id_actual,
                                                                               opt=opt_user)

                testEpochPredict, vec_run_id = predict_epoch_multi_run(model_post, y_test_main, vec_normalized_raw_main,
                                                                       vec_ix_slice_test_main, vec_good_ix_main,
                                                                       vec_n_events_main, run_id_actual=run_id_actual,
                                                                       sts=str_sub + ' post-trained',
                                                                       opt=opt_user)

                if opt_user.dataset_gen:
                    # Running the pre-trained network on the held-out subject
                    vec_bcg_net_raw_pre = generate_cleaned_set_multi_run(model_pre, vec_normalized_raw_main, vec_ecg_stats_main,
                                                                     vec_eeg_stats_main, opt=opt_user)
                    vec_bcg_net_interpolated_pre = interpolator_multi_run(vec_bcg_net_raw_pre, vec_fmrib_raw_main)

                    for i in range(len(vec_bcg_net_interpolated_pre)):
                        rid = run_id_actual[i]
                        p_out, f_out = set_env.output_path(str_sub, rid, arch, mode='pre-trained', opt=opt_user)
                        p_out.mkdir(parents=True, exist_ok=True)

                        sio.savemat(str(p_out.joinpath(f_out)), {'data': vec_bcg_net_interpolated_pre[i]})

                    # Running the post-trained network on the held-out subject
                    vec_bcg_net_raw_post = generate_cleaned_set_multi_run(model_post, vec_normalized_raw_main, vec_ecg_stats_main,
                                                                     vec_eeg_stats_main, opt=opt_user)

                    vec_bcg_net_interpolated_post = interpolator_multi_run(vec_bcg_net_raw_post, vec_fmrib_raw_main)

                    for i in range(len(vec_bcg_net_interpolated_post)):
                        rid = run_id_actual[i]
                        p_out, f_out = set_env.output_path(str_sub, rid, arch, opt=opt_user)
                        p_out.mkdir(parents=True, exist_ok=True)

                        sio.savemat(str(p_out.joinpath(f_out)), {'data': vec_bcg_net_interpolated_post[i]})

                if opt_user.figgen:
                    for i in range(len(vec_fmrib_raw_main)):
                        fmrib_raw = vec_fmrib_raw_main[i]
                        fmrib_raw.resample(100)

                    opt_user.f_arch = opt_user.p_arch.joinpath(f_arch_pre)
                    with open(str(opt_user.f_arch), 'rb') as handle:
                        net_settings = pickle.load(handle)

                    epochs = net_settings['epochs']
                    opt_user.p_arch = opt_user.p_arch.joinpath('Fig_regen_TEp{}'.format(epochs))

                    # Regenerating the pre-trained figures
                    testEpochPredict_pre, vec_run_id_pre = predict_epoch_multi_run(model_pre, y_test_main, vec_normalized_raw_main,
                                                                           vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main,
                                                                           run_id_actual=run_id_actual, sts=str_sub + ' pre-trained', opt=opt_user)

                    test_epoch_plot_multi_run(x_test_main, y_test_main, testEpochPredict_pre, vec_ecg_stats_main,
                                              vec_eeg_stats_main, vec_run_id_pre, vec_ix_slice_test_main,
                                              vec_n_events_main, sts=str_sub + '_pre_training', opt=opt_user)

                    vec_ECG_raw_renorm_pre, vec_EEG_raw_renorm_pre, vec_BCG_predicted_renorm_pre, vec_EEG_BCG_removed_renorm_pre = predict_series_multi_run(
                        model_pre, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main,
                        sts=str_sub + '_pre_training', vec_run_id=vec_run_id_pre, opt=opt_user)

                    series_section_plot_multi_run(vec_ECG_raw_renorm_pre, vec_EEG_raw_renorm_pre, vec_EEG_BCG_removed_renorm_pre,
                                                  vec_fmrib_raw_main, vec_run_id=vec_run_id_pre,
                                                  sts=str_sub + '_pre_training', opt=opt_user)

                    PSD_test_multi_run(x_test_main, y_test_main, testEpochPredict_pre, vec_fmrib_raw_main,
                                       vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id_pre,
                                       vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main,
                                       sts=str_sub + '_pre_training', opt=opt_user)

                    plt.close('all')

                    # Regenerating the post-trained figures
                    testEpochPredict_post, vec_run_id_post = predict_epoch_multi_run(model_post, y_test_main, vec_normalized_raw_main,
                                                                                     vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main,
                                                                                     run_id_actual=run_id_actual,
                                                                                     sts=str_sub + ' post-trained', opt=opt_user)

                    test_epoch_plot_multi_run(x_test_main, y_test_main, testEpochPredict_post, vec_ecg_stats_main,
                                              vec_eeg_stats_main, vec_run_id_post, vec_ix_slice_test_main,
                                              vec_n_events_main, sts=str_sub + '_post_training', opt=opt_user)

                    vec_ECG_raw_renorm_post, vec_EEG_raw_renorm_post, vec_BCG_predicted_renorm_post, vec_EEG_BCG_removed_renorm_post = predict_series_multi_run(
                        model_post, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main,
                        vec_run_id=vec_run_id_post, sts=str_sub + '_post_training', opt=opt_user)

                    series_section_plot_multi_run(vec_ECG_raw_renorm_post, vec_EEG_raw_renorm_post, vec_EEG_BCG_removed_renorm_post,
                                                  vec_fmrib_raw_main, vec_run_id=vec_run_id_post,
                                                  sts=str_sub + '_post_training', opt=opt_user)

                    PSD_test_multi_run(x_test_main, y_test_main, testEpochPredict_post, vec_fmrib_raw_main,
                                       vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id_post,
                                       vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main,
                                       sts=str_sub + '_post_training', opt=opt_user)

                    plt.close('all')

                    endtime = datetime.datetime.now()
                    duration = endtime - starttime
                    duration_in_s = duration.total_seconds()
                    hours = round(divmod(duration_in_s, 3600)[0])
                    if hours == 0:
                        minutes = round(divmod(duration_in_s, 60)[0])
                        seconds = round(divmod(duration_in_s, 60)[1])
                    else:
                        minutes = round(divmod(divmod(duration_in_s, 3600)[1], 60)[0])
                        seconds = round(divmod(divmod(duration_in_s, 3600)[1], 60)[1])

                    print('\n\nProcess ending')
                    print("The end time is {}/{}/{} {}:{}:{}\n".format(datetime.datetime.now().strftime("%Y"),
                                                                       datetime.datetime.now().strftime("%m"),
                                                                       datetime.datetime.now().strftime("%d"),
                                                                       datetime.datetime.now().strftime("%H"),
                                                                       datetime.datetime.now().strftime("%M"),
                                                                       datetime.datetime.now().strftime("%S")))
                    print("The program took {:0>2}:{:0>2}:{:0>2} in total".format(hours, minutes, seconds))


def main_loop_multi_sub_separate(vec_str_sub_full, run_id, str_sub_particular, arch, opt=None):
    '''
    :param vec_str_sub_full: List of all subjects (including the heldout)
    :param run_id: list of all runs (skips missing runs automatically)
    :param str_sub_particular: heldout subject
    :param arch: architecture to run
    :param opt: options
    :return: None, saves models to the file system
    '''

    # Exception handling
    if not isinstance(vec_str_sub_full, (list, tuple, np.ndarray)):
        raise Exception('For multi-run, str_sub must be a list, tuple or numpy array')

    opt_user = opt

    if not opt.multi_sub:
        raise Exception('For multi-sub runs, you must set multi_sub flag to true in opt')

    if not opt.evaluate_model:
        if not opt.multi_run:
            raise Exception('Not implemented yet')

        print('Process starting')
        starttime = datetime.datetime.now()
        print("The start time is {}\n\n".format(starttime.strftime("%Y/%m/%d %H:%M:%S")))
        vec_str_sub_local = vec_str_sub_full.copy()
        str_sub = str_sub_particular

        p_arch = set_env.arch_path(vec_str_sub_full, run_id, arch=arch)
        p_arch = p_arch.joinpath(str_sub)
        opt_user.p_arch = p_arch

        vec_str_sub_local.pop(vec_str_sub_local.index(str_sub))

        vec_normalized_raw_sub = []
        vec_epoched_data_sub = []
        vec_ecg_stats_sub = []
        vec_eeg_stats_sub = []
        vec_good_ix_sub = []
        vec_fmrib_raw_sub = []
        vec_sts = []

        vec_run_id_actual_sub = []

        for i in range(len(vec_str_sub_local)):
            sts = vec_str_sub_local[i]
            vec_normalized_raw = []
            vec_epoched_data = []
            vec_ecg_stats = []
            vec_eeg_stats = []
            vec_good_ix = []
            vec_fmrib_raw = []

            run_id_actual = []

            for j in range(len(run_id)):
                sts = vec_str_sub_local[i]
                rid = run_id[j]

                p_ga, f_ga = set_env.ga_path(sts, rid)
                p_rs, f_rs = set_env.rs_path(sts, rid)
                p_bcg, f_bcg = set_env.bcg_path(sts, rid)

                # Exception handling for subjects with less than 5 runs
                data_dir_rs = p_rs.joinpath(f_rs)
                data_dir_bcg = p_bcg.joinpath(f_bcg)

                if not data_dir_rs.exists() or not data_dir_bcg.exists():
                    continue

                if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                    continue

                try:
                    data_dir_rs = str(p_rs.joinpath(f_rs))
                    rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                    data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                    fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
                except RuntimeWarning:
                    continue

                if opt_user.target_ch is None and not opt_user.multi_ch:
                    raise Exception('Must choose a channel if not using the multiple channel mode')
                elif not (isinstance(opt_user.target_ch, int)) and not opt_user.multi_ch:
                    raise Exception('Can only choose 1 channel only if not using multiple channel mode')
                elif opt_user.multi_ch and not (
                isinstance(opt_user.target_ch, (int, tuple, np.ndarray))) and not opt_user.target_ch is None:
                    raise Exception('Must choose more than 1 channels if using the multiple channel mode')
                elif opt_user.resume and opt_user.f_arch is None:
                    raise Exception('Can only resume if have the structure')
                elif opt_user.multi_run and not opt_user.use_rs_data:
                    raise Exception('Recommended to use motion data when training across runs and subjects')

                if opt_user.ga_mc:
                    data_dir = str(p_ga.joinpath(f_ga))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir,
                                                                                                         opt_user.target_ch,
                                                                                                         opt=opt_user)
                    good_ix = []
                elif not opt_user.ga_mc and not opt_user.use_time_encoding:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                        data_dir, opt_user.target_ch, opt=opt_user)

                elif not opt_user.ga_mc and opt_user.use_time_encoding:
                    data_dir = str(p_rs.joinpath(f_rs))
                    normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(
                        data_dir, opt_user.target_ch, run_id=rid, N=len(run_id), opt=opt_user)

                ecg_stats = [ecg_mean, ecg_std]
                eeg_stats = [eeg_mean, eeg_std]

                fmrib_dir = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
                fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
                fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

                vec_normalized_raw.append(normalized_raw)
                vec_epoched_data.append(epoched_data)
                vec_ecg_stats.append(ecg_stats)
                vec_eeg_stats.append(eeg_stats)
                vec_good_ix.append(good_ix)
                vec_fmrib_raw.append(fmrib_raw)

                run_id_actual.append(rid)

            vec_normalized_raw_sub.append(vec_normalized_raw)
            vec_epoched_data_sub.append(vec_epoched_data)
            vec_ecg_stats_sub.append(vec_ecg_stats)
            vec_eeg_stats_sub.append(vec_eeg_stats)
            vec_good_ix_sub.append(vec_good_ix)
            vec_fmrib_raw_sub.append(vec_fmrib_raw)
            vec_sts.append(sts)

            vec_run_id_actual_sub.append(run_id_actual)

        vec_epoched_data_combined_sub = []
        for i in range(len(vec_epoched_data_sub)):
            vec_epoched_data = vec_epoched_data_sub[i]

            epoched_data_combined = None
            for j in range(len(vec_epoched_data)):
                if j == 0:
                    epoched_data_combined = vec_epoched_data[j]
                else:
                    epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data[j]])

            vec_epoched_data_combined_sub.append(epoched_data_combined)

        vec_n_events_sub = []
        for i in range(len(vec_epoched_data_sub)):
            vec_epoched_data = vec_epoched_data_sub[i]

            vec_n_events = []
            for j in range(len(vec_epoched_data)):
                n_events = len(vec_epoched_data[j].get_data())
                vec_n_events.append(n_events)
            vec_n_events_sub.append(vec_n_events)

        vec_x_train_sub = []
        vec_x_validation_sub = []
        vec_x_test_sub = []
        vec_y_train_sub = []
        vec_y_validation_sub = []
        vec_y_test_sub = []
        vec_ix_slice_test_sub = []
        for i in range(len(vec_epoched_data_combined_sub)):
            epoched_data_combined = vec_epoched_data_combined_sub[i]
            x_train, x_validation, x_test, y_train, y_validation, y_test, vec_ix_slice_test = generate_train_valid_test(
                epoched_data_combined, opt_user)

            vec_x_train_sub.append(x_train)
            vec_x_validation_sub.append(x_validation)
            vec_x_test_sub.append(x_test)
            vec_y_train_sub.append(y_train)
            vec_y_validation_sub.append(y_validation)
            vec_y_test_sub.append(y_test)
            vec_ix_slice_test_sub.append(vec_ix_slice_test)

        x_train_combined = None
        for i in range(len(vec_x_train_sub)):
            if i == 0:
                x_train_combined = vec_x_train_sub[i]
            else:
                x_train_combined = np.append(x_train_combined, vec_x_train_sub[i], axis=1)

        x_validation_combined = None
        for i in range(len(vec_x_validation_sub)):
            if i == 0:
                x_validation_combined = vec_x_validation_sub[i]
            else:
                x_validation_combined = np.append(x_validation_combined, vec_x_validation_sub[i], axis=1)

        x_test_combined = None
        for i in range(len(vec_x_test_sub)):
            if i == 0:
                x_test_combined = vec_x_test_sub[i]
            else:
                x_test_combined = np.append(x_test_combined, vec_x_test_sub[i], axis=1)

        y_train_combined = None
        for i in range(len(vec_y_train_sub)):
            if i == 0:
                y_train_combined = vec_y_train_sub[i]
            else:
                y_train_combined = np.append(y_train_combined, vec_y_train_sub[i], axis=1)

        y_validation_combined = None
        for i in range(len(vec_y_validation_sub)):
            if i == 0:
                y_validation_combined = vec_y_validation_sub[i]
            else:
                y_validation_combined = np.append(y_validation_combined, vec_y_validation_sub[i], axis=1)

        y_test_combined = None
        for i in range(len(vec_y_test_sub)):
            if i == 0:
                y_test_combined = vec_y_test_sub[i]
            else:
                y_test_combined = np.append(y_test_combined, vec_y_test_sub[i], axis=1)

        model_gru = train_sp(x_train_combined, x_validation_combined, y_train_combined, y_validation_combined,
                             arch=arch, opt=opt_user)
        print('model done')
        test_epoch_predict_sub = []
        vec_run_id_sub = []
        for i in range(len(vec_normalized_raw_sub)):
            x_test = vec_x_test_sub[i]
            y_test = vec_y_test_sub[i]
            vec_normalized_raw = vec_normalized_raw_sub[i]
            vec_fmrib_raw = vec_fmrib_raw_sub[i]
            vec_ecg_stats = vec_ecg_stats_sub[i]
            vec_eeg_stats = vec_eeg_stats_sub[i]
            vec_ix_slice_test = vec_ix_slice_test_sub[i]
            vec_good_ix = vec_good_ix_sub[i]
            vec_n_events = vec_n_events_sub[i]
            sts = vec_sts[i]

            run_id_actual = vec_run_id_actual_sub[i]

            test_epoch_predict, vec_run_id = predict_epoch_multi_run(model_gru, y_test, vec_normalized_raw,
                                                                     vec_ix_slice_test, vec_good_ix, vec_n_events,
                                                                     run_id_actual=run_id_actual,
                                                                     sts=sts, opt=opt_user)
            test_epoch_predict_sub.append(test_epoch_predict)
            vec_run_id_sub.append(vec_run_id)

            # vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
            #     model_gru, vec_normalized_raw, vec_ecg_stats, vec_eeg_stats, sts=sts, vec_run_id=vec_run_id, plot=True,
            #     opt=opt_user)

            # test_epoch_plot_multi_run(x_test, y_test, test_epoch_predict, vec_ecg_stats, vec_eeg_stats,
            #                           vec_run_id, vec_ix_slice_test, vec_n_events, sts=sts, opt=opt_user)
            #
            # series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
            #                               vec_fmrib_raw, vec_run_id=vec_run_id, sts=sts, opt=opt_user)
            #
            # PSD_test_multi_run(x_test, y_test, test_epoch_predict, vec_fmrib_raw,
            #                    vec_ecg_stats, vec_eeg_stats, vec_run_id, vec_ix_slice_test, vec_good_ix,
            #                    vec_n_events, sts=sts, opt=opt_user)

            # plt.close('all')

        # Loading the main subject for testing
        vec_normalized_raw_main = []
        vec_epoched_data_main = []
        vec_ecg_stats_main = []
        vec_eeg_stats_main = []
        vec_good_ix_main = []
        vec_fmrib_raw_main = []
        run_id_actual_main = []

        for i in range(len(run_id)):
            p_ga, f_ga = set_env.ga_path(str_sub, run_id[i])
            p_rs, f_rs = set_env.rs_path(str_sub, run_id[i])
            p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id[i])

            # Exception handling for subjects with less than 5 runs
            data_dir_rs = p_rs.joinpath(f_rs)
            data_dir_bcg = p_bcg.joinpath(f_bcg)

            if not data_dir_rs.exists() or not data_dir_bcg.exists():
                continue

            if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0:
                continue

            try:
                data_dir_rs = str(p_rs.joinpath(f_rs))
                rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

                data_dir_bcg = str(p_bcg.joinpath(f_bcg))
                fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
            except RuntimeWarning:
                continue

            if opt_user.ga_mc:
                data_dir = str(p_ga.joinpath(f_ga))
                normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std = preprocessor_ga(data_dir,
                                                                                                     opt_user.target_ch,
                                                                                                     opt=opt_user)
                good_ix = []
            else:
                data_dir = str(p_rs.joinpath(f_rs))
                normalized_raw, epoched_data, ecg_mean, ecg_std, eeg_mean, eeg_std, good_ix = preprocessor_rs(data_dir,
                                                                                                              opt_user.target_ch,
                                                                                                              opt=opt_user)

            ecg_stats = [ecg_mean, ecg_std]
            eeg_stats = [eeg_mean, eeg_std]

            fmrib_dir = str(p_bcg.joinpath(f_bcg))
            fmrib_raw = mne.io.read_raw_eeglab(fmrib_dir, preload=True, stim_channel=False)
            fmrib_raw.resample(100)  # Sampling rate is 500Hz and downsampled to 100Hz
            fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

            vec_normalized_raw_main.append(normalized_raw)
            vec_epoched_data_main.append(epoched_data)
            vec_ecg_stats_main.append(ecg_stats)
            vec_eeg_stats_main.append(eeg_stats)
            vec_good_ix_main.append(good_ix)
            vec_fmrib_raw_main.append(fmrib_raw)

            run_id_actual_main.append(run_id[i])

        epoched_data_combined_main = None
        for i in range(len(vec_epoched_data_main)):
            if i == 0:
                epoched_data_combined_main = vec_epoched_data_main[i]
            else:
                epoched_data_combined_main = mne.concatenate_epochs(
                    [epoched_data_combined_main, vec_epoched_data_main[i]])

        vec_n_events_main = []
        for i in range(len(vec_epoched_data_main)):
            n_events = len(vec_epoched_data_main[i].get_data())
            vec_n_events_main.append(n_events)

        x_train_main, x_validation_main, x_test_main, y_train_main, y_validation_main, y_test_main, vec_ix_slice_test_main = generate_train_valid_test(
            epoched_data_combined_main, opt_user)
        sts = str_sub + '_pre_training'

        test_epoch_predict_main_pre, vec_run_id = predict_epoch_multi_run(model_gru, y_test_main,
                                                                          vec_normalized_raw_main,
                                                                          vec_ix_slice_test_main, vec_good_ix_main,
                                                                          vec_n_events_main,
                                                                          run_id_actual=run_id_actual_main,
                                                                          sts=sts, opt=opt_user)

        # vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
        #     model_gru, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, sts=sts, vec_run_id=vec_run_id,
        #     plot=True, opt=opt_user)
        #
        # test_epoch_plot_multi_run(x_test_main, y_test_main, test_epoch_predict_main_pre, vec_ecg_stats_main,
        #                           vec_eeg_stats_main, vec_run_id,
        #                           vec_ix_slice_test_main, vec_n_events_main, sts=sts, opt=opt_user)
        #
        # series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
        #                               vec_fmrib_raw_main,
        #                               vec_run_id=vec_run_id, sts=sts, opt=opt_user)
        #
        # PSD_test_multi_run(x_test_main, y_test_main, test_epoch_predict_main_pre, vec_fmrib_raw_main,
        #                    vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
        #                    vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main, sts=sts, opt=opt_user)
        #
        # plt.close('all')

        with open(str(opt_user.f_arch), 'rb') as handle:
            h1 = pickle.load(handle)

        net_name = str(Path(opt_user.f_arch).parts[-1])
        net_name_pre = net_name.split('.')[0] + '_pre_trained.' + net_name.split('.')[1]
        net_path = Path(opt_user.p_arch).parents[0]

        with open(str(net_path.joinpath(net_name_pre)), 'wb') as handle:
            pickle.dump(h1, handle)

        opt_user.f_arch = Path(opt_user.f_arch).parts[-1]
        opt_user.p_arch = Path(opt_user.p_arch).parents[1]
        opt_user.resume = True

        model_gru_new = train_sp(x_train_main, x_validation_main, y_train_main, y_validation_main, arch=arch,
                                 overwrite=False, held_out=True, opt=opt_user)
        sts = str_sub + '_post_training'

        # test_epoch_predict_main_post, vec_run_id = predict_epoch_multi_run(model_gru_new, y_test_main,
        #                                                                    vec_normalized_raw_main,
        #                                                                    vec_ix_slice_test_main, vec_good_ix_main,
        #                                                                    vec_n_events_main,
        #                                                                    run_id_actual=run_id_actual_main, sts=sts,
        #                                                                    opt=opt_user)
        #
        # vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_BCG_predicted_renorm, vec_EEG_BCG_removed_renorm = predict_series_multi_run(
        #     model_gru_new, vec_normalized_raw_main, vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id=vec_run_id,
        #     sts=sts, plot=True, opt=opt_user)
        #
        # test_epoch_plot_multi_run(x_test_main, y_test_main, test_epoch_predict_main_post, vec_ecg_stats_main,
        #                           vec_eeg_stats_main, vec_run_id,
        #                           vec_ix_slice_test_main, vec_n_events_main, sts=sts, opt=opt_user)
        #
        # series_section_plot_multi_run(vec_ECG_raw_renorm, vec_EEG_raw_renorm, vec_EEG_BCG_removed_renorm,
        #                               vec_fmrib_raw_main,
        #                               vec_run_id=vec_run_id, sts=sts, opt=opt_user)
        #
        # PSD_test_multi_run(x_test_main, y_test_main, test_epoch_predict_main_post, vec_fmrib_raw_main,
        #                    vec_ecg_stats_main, vec_eeg_stats_main, vec_run_id,
        #                    vec_ix_slice_test_main, vec_good_ix_main, vec_n_events_main, sts=sts, opt=opt_user)
        #
        # plt.close('all')
        K.clear_session()
        gc.collect()

        endtime = datetime.datetime.now()
        duration = endtime - starttime
        duration_in_s = duration.total_seconds()
        hours = round(divmod(duration_in_s, 3600)[0])
        if hours == 0:
            minutes = round(divmod(duration_in_s, 60)[0])
            seconds = round(divmod(duration_in_s, 60)[1])
        else:
            minutes = round(divmod(divmod(duration_in_s, 3600)[1], 60)[0])
            seconds = round(divmod(divmod(duration_in_s, 3600)[1], 60)[1])

        with open(str(opt_user.p_arch / 'history_rmse'), 'rb') as handle:
            h1 = pickle.load(handle)

        h1['time'] = (hours, minutes, seconds)

        with open(str(opt_user.p_arch / 'history_rmse'), 'wb') as handle:
            pickle.dump(h1, handle)

        print('\n\nProcess ending')
        print("The end time is {}/{}/{} {}:{}:{}\n".format(datetime.datetime.now().strftime("%Y"),
                                                           datetime.datetime.now().strftime("%m"),
                                                           datetime.datetime.now().strftime("%d"),
                                                           datetime.datetime.now().strftime("%H"),
                                                           datetime.datetime.now().strftime("%M"),
                                                           datetime.datetime.now().strftime("%S")))
        print("The program took {:0>2}:{:0>2}:{:0>2} in total".format(hours, minutes, seconds))

    else:
        raise Exception('Not implemented yet')


def test_opt(opt):
    if opt is None:
        opt = TrainDefault()
        opt.epochs = 2500
        opt.ga_mc = False
        opt.p_arch = None
        opt.f_arch = None
        opt.target_ch = None
        opt.es_min_delta = 1e-5
        opt.es_patience = 25  # How many times does the validation not increase
        opt.early_stopping = True
        opt.resume = False
        opt.validation = 0.15
        opt.evaluation = 0.85
        opt.evaluate_model = False
        opt.figgen = False
        opt.dataset_gen = False
        opt.save_bcg_dataset = False
        opt.fig_num = 63
        opt.multi_ch = True
        opt.multi_run = True
        opt.multi_sub = True
        opt.use_rs_data = True
        opt.use_time_encoding = False
        opt.save_QRS_series = False
        opt.use_bcg_input = False
    elif isinstance(opt, dict):
        assert False, 'need to add this option?'

    return opt


if __name__ == '__main__':

    # 1. Single sub
    str_sub = 'sub35'
    run_id = [1, 2, 3, 4, 5]
    arch = 'gru_arch_general4'
    opt = test_opt(None)
    opt.evaluate_model = False
    opt.figgen = False
    opt.dataset_gen = False
    opt.save_bcg_dataset = False
    # opt.f_arch = 'net_gru_arch_general4_20191004004526.dat'
    main_loop(str_sub=str_sub, run_id=run_id, arch=arch, opt=opt)

    # vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
    # for str_sub in vec_str_sub:
    #     run_id = [1, 2, 3, 4, 5]
    #     arch = 'gru_arch_general17'
    #     opt = test_opt(None)
    #     # opt.f_arch = 'net_gru_arch_general4_20190723080149.dat'
    #     main_loop(str_sub=str_sub, run_id=run_id, arch=arch, opt=opt)

    # vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']

    # for str_sub in vec_str_sub:
    #     run_id = [1, 2, 3, 4, 5]
    #     # arch = 'gru_arch_general17'
    #     opt = test_opt(None)
    #     arch = 'gru_arch_bcginput01'
    #     # opt.use_bcg_input = True
    #
    #     # opt.f_arch = 'net_gru_arch_general4_20190723080149.dat'
    #     main_loop(str_sub=str_sub, run_id=run_id, arch=arch, opt=opt)


    # 2. Multi-sub
    # vec_str_sub = ['sub11', 'sub12', 'sub14',
    #                'sub15', 'sub17', 'sub18',
    #                'sub19', 'sub20', 'sub21',
    #                'sub22', 'sub23', 'sub24',
    #                'sub25', 'sub29', 'sub30',
    #                'sub32', 'sub33', 'sub34',
    #                'sub35']
    # run_id = [1, 2, 3, 4, 5]
    # # arch = 'gru_arch_bcginput01'
    # arch = 'gru_arch_general4'
    # opt = test_opt(None)

    # opt.f_arch = ['net_gru_arch_general4_20190813182604.dat',
    #               'net_gru_arch_general4_20190814014942.dat',
    #               'net_gru_arch_general4_20190814083905.dat',
    #               'net_gru_arch_general4_20190814135247.dat',
    #               'net_gru_arch_general4_20190814233820.dat',
    #               'net_gru_arch_general4_20190815055912.dat',
    #               'net_gru_arch_general4_20190815132623.dat',
    #               'net_gru_arch_general4_20190815194624.dat',
    #               'net_gru_arch_general4_20190816032045.dat',
    #               'net_gru_arch_general4_20190816105154.dat',
    #               'net_gru_arch_general4_20190816194105.dat']

    # opt.f_arch = ['net_gru_arch_bcginput01_20190903155646.dat',
    #               'net_gru_arch_bcginput01_20190904145908.dat',
    #               'net_gru_arch_bcginput01_20190904214713.dat',
    #               'net_gru_arch_bcginput01_20190905025959.dat',
    #               'net_gru_arch_bcginput01_20190905080751.dat',
    #               'net_gru_arch_bcginput01_20190905125325.dat',
    #               'net_gru_arch_bcginput01_20190905182744.dat',
    #               'net_gru_arch_bcginput01_20190905235015.dat',
    #               'net_gru_arch_bcginput01_20190906041829.dat',
    #               'net_gru_arch_bcginput01_20190906090814.dat',
    #               'net_gru_arch_bcginput01_20190906144657.dat']
    # opt.use_bcg_input = False
    #
    # main_loop_multi_sub(vec_str_sub=vec_str_sub, run_id=run_id, arch=arch, opt=opt)

    # # 3. Separate multi_sub
    # vec_str_sub = ['sub11', 'sub12', 'sub14',
    #                'sub15', 'sub17', 'sub18',
    #                'sub19', 'sub20', 'sub21',
    #                'sub22', 'sub23', 'sub24',
    #                'sub25', 'sub29', 'sub30',
    #                'sub32', 'sub33', 'sub34',
    #                'sub35']
    # str_sub = 'sub15'
    # run_id = [1, 2, 3, 4, 5]
    # # arch = 'gru_arch_bcginput01'
    # arch = 'gru_arch_general4'
    # opt = test_opt(None)
    # main_loop_multi_sub_separate(vec_str_sub_full=vec_str_sub, run_id=run_id, str_sub_particular=str_sub,
    #                              arch=arch, opt=opt)