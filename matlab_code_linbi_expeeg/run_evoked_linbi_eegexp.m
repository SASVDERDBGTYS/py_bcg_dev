clearvars -except switch_case;
close all;
set_env_linbi_eegexp;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'an_results')));
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'sp_preproc')));
% addpath('sp_preproc');
% addpath('an_results');

%%
d_project = 'working_eegbcg';
t_pct_baseline = NaN;t_low = -0.5;t_hig = 1.5; % defaults
%%
reref = 'laplace';
% spectral_band = 'alpha_beta1_beta2'; %n.b. order matters!
% spectral_band = 'alpha_beta1'; % examples
% spectral_band = 'alpha_square'; % examples
% spectral_band = 'alpha'; % examples
spectral_band = ''; % examples
% spectral_band = 'beta-motor_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25]; % you don't need to notch!!!
spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
% spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];

% spectral_band = 'beta-motor_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
% spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
% spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
proc = getenv_check_empty('R_PROC');

%%
if (exist('switch_case', 'var')==1)
    switch switch_case
        case 1, spectral_band = 'beta-motor_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 2, spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 3, spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 4, spectral_band = 'beta-motor_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
        case 5, spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
        case 6, spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
    end
end
%%
overwrite = false;

%%
data_source_gar = fullfile('proc_rs');
d_proc_bcg_gar = fullfile(getenv('D_OUT'), d_project, proc, data_source_gar);
[str_source_gar, str_type_gar] = gen_source(data_source_gar);

%%

[d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar] = an_epoch(d_project, d_proc_bcg_gar, ...
    'reref', reref, 'str_source', str_source_gar, 'str_merged', str_type_gar, ...
    't_low', t_low, 't_hig', t_hig, 'spectral_band', spectral_band);

%%

[std_standard_gar, std_target_gar, mea_standard_gar, mea_target_gar, med_standard_gar, med_target_gar, ...
    se2_standard_gar, se2_target_gar, t, eeg_labels, topo_locs] = an_plot_evoked(d_project, ...
    d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar, 'str_source', str_source_gar, 't_baseline', t_pct_baseline);

%%
vec_sub = vec_sub_gar;
es = '';
if all(isinf(t_pct_baseline))
    es = sprintf('%s%s', es, '_inf_baseline');
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
%%

coi = {'T8', 'Pz'};
coi_full = { ...
    'Fz','F1','F2','F3','F4', ...
    'FC1','FC2','FC3','FC4', ...
    'FT7', 'FT8', ...
    'Cz','C1','C2','C3','C4', ...
    'T7', 'T8', ...
    'CPz','CP1','CP2','CP3','CP4', ...
    'TP7', 'TP8', ...
    'Pz','P1','P2','P3','P4', ...
    'Oz','O1','O2', ...
    };

if strcmpi(reref, 'laplace')
    a_units = 'µV/m²';
else
    a_units = 'µV';
end
%%
if isempty(spectral_band)
    str_spectral_band = '';
else
    str_spectral_band = sprintf('_%s', spectral_band);
    cell_spectral_band = strsplit(spectral_band, '_');
    if contains(spectral_band, 'ersp')
    else
        coi_temp = {};
        for ix_sb = 1:length(cell_spectral_band)
            for ix_coi = 1:length(coi)
                coi_temp_instance = sprintf('%s_%s', coi{ix_coi}, cell_spectral_band{ix_sb});
                coi_temp = [coi_temp, coi_temp_instance];
            end
        end
        coi = coi_temp;
    end
end

for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    if ix_vec_sub == 1
        nb_chan = size(std_standard_gar.(str_sub), 1);
        nb_methods = 3; %obs, net_based
        nb_stimuli = 2; % standard, target
        mat_mea = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_med = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_std = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_se2 = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        
    end
    mat_mea(:, :, ix_vec_sub, 1, 1) = mea_standard_gar.(str_sub);
    mat_mea(:, :, ix_vec_sub, 1, 2) = mea_target_gar.(str_sub);
    
    mat_med(:, :, ix_vec_sub, 1, 1) = med_standard_gar.(str_sub);
    mat_med(:, :, ix_vec_sub, 1, 2) = med_target_gar.(str_sub);
    
    mat_std(:, :, ix_vec_sub, 1, 1) = std_standard_gar.(str_sub);
    mat_std(:, :, ix_vec_sub, 1, 2) = std_target_gar.(str_sub);
    
    mat_se2(:, :, ix_vec_sub, 1, 1) = se2_standard_gar.(str_sub);
    mat_se2(:, :, ix_vec_sub, 1, 2) = se2_target_gar.(str_sub);
end

%%

% p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'proc_epoch', 'figures', ...
%     sprintf('c%s_vs%s%s', gen_source(data_source_obs), gen_source(data_source_net), str_spectral_band));
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig06', ...
    sprintf('c_%s%s%s', reref, gen_source(data_source_gar), str_spectral_band));
% p_fig = '~/Desktop/temp/';

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'png';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
print_local = @(h, dim) printForPub(h, sprintf('f_evo_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

cmap = [c.obs_st; c.net_st; c.obs_ta; c.net_ta; c.neutral];

%%
coi_motor = {'C3', 'Cz', 'C2', 'Pz'};
lw =2 ;
%
cell_met = {'GAR'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('time_res_coi_%s_band', str_met));clf;
    ns = numSubplots(length(coi_motor));
    for ix_coi = 1:length(coi_motor)
        if ix_coi==length(coi_motor)+1
            case_ch = 1:length(eeg_labels);
            title_ = strrep(sprintf('%s', 'ALL'), '_', ' ');
        else
            case_ch = strcmpi(eeg_labels, coi_motor{ix_coi});
            title_ = strrep(sprintf('%s', coi_motor{ix_coi}), '_', ' ');
        end
        
        subplot(ns(1), ns(2), ix_coi);cla;hold on;
        
        if strcmpi(str_met, 'GAR')
            mea_standard = mean(mat_med(case_ch, :, :, 1, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 1, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 1, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 1, 2), 0, 3);
            c_st = c.gar_st;
            c_ta = c.gar_ta;
        elseif strcmpi(str_met, 'OBS')
            mea_standard = mean(mat_med(case_ch, :, :, 2, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 2, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 2, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 2, 2), 0, 3);
            c_st = c.obs_st;
            c_ta = c.obs_ta;
        elseif strcmpi(str_met, 'BCGNet')
            mea_standard = mean(mat_med(case_ch, :, :, 3, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 3, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 3, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 3, 2), 0, 3);
            c_st = c.net_st;
            c_ta = c.net_ta;
        end
        ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
        ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
        
        
        h_standard = plot(t, mea_standard, '-',  'Color', c_st);
        h_standard.LineWidth = lw;
        h_target = plot(t, mea_target, '-', 'Color', c_ta);
        h_target.LineWidth = lw;
        title(title_);
        
        if isempty(spectral_band)
            s_f = [18.1 * (9/12), 4];
        else
            s_f = [18.1 * (9/12), 9];
        end
        xlabel('Time (s)');
        ylabel(sprintf('Amplitude (%s)', a_units))
        plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
        axis tight;
        xlim([-0.25, 2.75])
    end
    legend([h_standard, h_target], ...
        {sprintf('%s standard', str_met), sprintf('%s target', str_met)}, 'Location', 'SouthEast');
    legend('boxoff');
    print_local(h_f, s_f);
end
%%

ns = numSubplots(length(coi_full));
lw = 1;
cell_met = {'GAR'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('topo_time_res_coi_%s_band', str_met));clf;
    for ix_coi = 1:length(coi_full)+1
        if ix_coi==length(coi_full)+1
            subplot('Position',[ ...
                0.8, 0.1, ...
                topo_locs.topoW, ...
                topo_locs.topoH, ...
                ]);hold on;
            title('bits');
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            legend([h_standard, h_target], ...
                {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
            legend('boxoff');
        else
            case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
            title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
            
            
            %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
            ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
            
            cla;hold on;
            if strcmpi(str_met, 'GAR')
                mea_standard = mean(mat_med(case_ch, :, :, 1, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 1, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 1, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 1, 2), 0, 3);
                c_st = c.gar_st;
                c_ta = c.gar_ta;
            elseif strcmpi(str_met, 'OBS')
                mea_standard = mean(mat_med(case_ch, :, :, 2, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 2, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 2, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 2, 2), 0, 3);
                c_st = c.obs_st;
                c_ta = c.obs_ta;
            elseif strcmpi(str_met, 'BCGNet')
                mea_standard = mean(mat_med(case_ch, :, :, 3, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 3, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 3, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 3, 2), 0, 3);
                c_st = c.net_st;
                c_ta = c.net_ta;
            end
            ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
            ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
            
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            title(title_);
            xlim([t(1) t(end)])
            
            if ix_coi==length(coi_full)+1
                xlabel('Time (s)');
                ylabel(sprintf('Amplitude (%s)', a_units))
            else
                %             axis off;
                ax.Color = 'none';
                ax.XColor = 'none';
                %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
            end
        end
        axis tight;
        if isempty(spectral_band)
            %         ylim(40 * [-1, 1])
            s_f = [18.1, 22];
            
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        else
            %         ylim(0.41 * [-1, 1])
            s_f = [18.1, 22];
            xlim([-0.25, 2.5])
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        end
    end
    
    print_local(h_f, s_f);
end
%%
for ix_vec_sub = 1:length(vec_sub)
ns = numSubplots(length(coi_full));
lw = 1;
cell_met = {'GAR'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('_sub%02d_topo_time_res_coi_%s_band', vec_sub(ix_vec_sub), str_met));clf;
    for ix_coi = 1:length(coi_full)+1
        if ix_coi==length(coi_full)+1
            subplot('Position',[ ...
                0.8, 0.1, ...
                topo_locs.topoW, ...
                topo_locs.topoH, ...
                ]);hold on;
            title('bits');
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            legend([h_standard, h_target], ...
                {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
            legend('boxoff');
        else
            case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
            title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
            
            
            %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
            ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
            
            cla;hold on;
            if strcmpi(str_met, 'GAR')
                mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 1, 1), 3);
                mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 1, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 1, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 1, 2), 0, 3);
                c_st = c.gar_st;
                c_ta = c.gar_ta;
            elseif strcmpi(str_met, 'OBS')
                mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 2, 1), 3);
                mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 2, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 2, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 2, 2), 0, 3);
                c_st = c.obs_st;
                c_ta = c.obs_ta;
            elseif strcmpi(str_met, 'BCGNet')
                mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 3, 1), 3);
                mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 3, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 3, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 3, 2), 0, 3);
                c_st = c.net_st;
                c_ta = c.net_ta;
            end
%             ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
%             ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
            
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            title(title_);
            xlim([t(1) t(end)])
            
            if ix_coi==length(coi_full)+1
                xlabel('Time (s)');
                ylabel(sprintf('Amplitude (%s)', a_units))
            else
                %             axis off;
                ax.Color = 'none';
                ax.XColor = 'none';
                %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
            end
        end
        axis tight;
        if isempty(spectral_band)
            %         ylim(40 * [-1, 1])
            s_f = [18.1, 22];
            
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        else
            %         ylim(0.41 * [-1, 1])
            s_f = [18.1, 22];
            xlim([-0.25, 2.5])
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        end
    end
    
    print_local(h_f, s_f);
end
end