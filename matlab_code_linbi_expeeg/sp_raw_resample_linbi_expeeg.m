clearvars;
close all;
set_env_linbi_eegexp;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;

%%
proc = getenv_check_empty('R_PROC');

d_project = 'working_eegbcg';
v.fs_rs = 500;
data_source_raw = fullfile(getenv_check_empty('D_DATA'), d_project, 'raw_linbi_eegexp');

overwrite = true;

f_cell = glob(fullfile(data_source_raw, '**.bdf'));
cell_sub = {};
for ix_f_cell = 1:length(f_cell)
    m = regexp(f_cell{ix_f_cell},'.+\((.+)\)/EEG/aud-(\d+).bdf', 'tokens');
    if not(isempty(m))
        cell_sub = [cell_sub, m{1}{1}];
    end
end
cell_sub = unique(cell_sub);
cell_sub = sort(cell_sub);
sub_key = struct;
for ix_sub = 1:length(cell_sub)
    sub_key.(cell_sub{ix_sub}) = sprintf('sub%02d', ix_sub);
end

for ix_f_cell = 1:length(f_cell)
    m = regexp(f_cell{ix_f_cell},'.+\((.+)\)/EEG/aud-(\d+).bdf', 'tokens');
    if contains(f_cell{ix_f_cell}, '2011-07-01_EEG_Eyelink(BC)')
        continue
    end
    if not(isempty(m))
        str_sub = sub_key.(m{1}{1});
        str_run = m{1}{2};
        str_out = sprintf('%s_r%s_rs', str_sub, str_run);
        
        d_out = fullfile(getenv_check_empty('D_DATA'), d_project, proc, 'proc_rs', str_sub, str_out);
        f_out = fullfile(d_out, str_out);
        
        if not(exist([f_out, '.set'], 'file')==2)||overwrite
            EEG = pop_readbdf(f_cell{ix_f_cell});
            
            EEG.data(66:end, :) = [];
            
            ch_ev = find(strcmpi({EEG.chanlocs.labels}, 'Status'));
            
            EEG.data(ch_ev, :) = EEG.data(ch_ev, :) - min(EEG.data(ch_ev, :));
            EEG.data(ch_ev, EEG.data(ch_ev, :) == max(EEG.data(ch_ev, :))) = -1;
            
            EEG = pop_chanevent(EEG, ch_ev, 'edge', 'leading', 'edgelen', 0);
            
            %             255 - begin
            %             150 - response
            %             100 - target
            %             50 - standard
            
            v.l_standards = 'S  8';
            v.l_targets = 'S 32';
            v.l_response = 'R';
            v.l_begin = 'B';
            for ix_ev = 1:length(EEG.event)
                if EEG.event(ix_ev).type == 50
                    EEG.event(ix_ev).type = v.l_standards;
                elseif EEG.event(ix_ev).type == 100
                    EEG.event(ix_ev).type = v.l_targets;
                elseif EEG.event(ix_ev).type == 150
                    EEG.event(ix_ev).type = v.l_response;
                elseif EEG.event(ix_ev).type == 255
                    EEG.event(ix_ev).type = v.l_begin;
                end
            end
            
            EEG=pop_chanedit(EEG, 'lookup', fullfile(getenv('D_EEGLAB'), 'plugins', 'dipfit', 'standard_BEM', 'elec', 'standard_1005.elc'), ...
                'load', fullfile(getenv('D_DATA'), d_project, 'raw_linbi_eegexp', 'biosemi64.sph'));
            
            EEG.data = double(EEG.data);
            EEG = pop_resample(EEG, v.fs_rs);
            
            EEG.etc.sp_raw_resample = v;
            EEG.subject = str_sub;
            
            EEG = eeg_checkset( EEG );
            [~, setn] = fileparts(f_out);
            EEG.setname = setn;
            if not(exist(d_out, 'dir')==7), mkdir(d_out);end
            pop_saveset(EEG, 'filename', f_out);
        end
    end
end