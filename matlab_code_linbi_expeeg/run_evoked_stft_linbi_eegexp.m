clearvars;
close all;
set_env_linbi_eegexp;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'an_results')));
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'sp_preproc')));
% addpath('sp_preproc');
% addpath('an_results');

%%
d_project = 'working_eegbcg';
reref = 'laplace';
% spectral_band = 'stft_ersp'; t_low = -1.5; t_hig = 3.5;% if you add ersp it should work here...
spectral_band = 'stft-pls_ersp'; t_low = -1.5; t_hig = 3.5;% if you add ersp it should work here...\
lowpass = false;
str_spectral_band = spectral_band;
proc = getenv_check_empty('R_PROC');
%%
overwrite = false;

%%
data_source_gar = fullfile('proc_rs');
d_proc_bcg_gar = fullfile(getenv('D_OUT'), d_project, proc, data_source_gar);
[str_source_gar, str_type_gar] = gen_source(data_source_gar);

%%
[d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar] = an_epoch(d_project, d_proc_bcg_gar, ...
    'reref', reref, 'str_source', str_source_gar, 'str_merged', str_type_gar, 'spectral_band', spectral_band, ...
    't_low',t_low, 't_hig',t_hig, 'lowpass', lowpass);

%%

[std_standard_gar, std_target_gar, mea_standard_gar, mea_target_gar, med_standard_gar, med_target_gar, ...
    se2_standard_gar, se2_target_gar, t, f, eeg_labels, topo_locs] = an_plot_evoked_stft(d_project, ...
    d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar, 'str_source', str_source_gar);

%%
vec_sub = vec_sub_gar;
%%

cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';

%%

coi = {'T8', 'Pz'};
coi_full = { ...
    'Fz','F1','F2','F3','F4', ...
    'FC1','FC2','FC3','FC4', ...
    'FT7', 'FT8', ...
    'Cz','C1','C2','C3','C4', ...
    'T7', 'T8', ...
    'CPz','CP1','CP2','CP3','CP4', ...
    'TP7', 'TP8', ...
    'Pz','P1','P2','P3','P4', ...
    'Oz','O1','O2', ...
    };

if strcmpi(reref, 'laplace')
    a_units = 'µV/m²';
else
    a_units = 'µV';
end
%%
for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    if ix_vec_sub == 1
        nb_chan = size(std_standard_gar.(str_sub), 3);
        nb_methods = 3; %obs, net_based
        nb_stimuli = 2; % standard, target
        nb_freqs = size(std_standard_gar.(str_sub), 1);
        mat_med = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        mat_std = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        mat_se2 = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        
    end
    mat_med(:, :, :, ix_vec_sub, 1, 1) = med_standard_gar.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 1, 2) = med_target_gar.(str_sub);
    
    mat_std(:, :, :, ix_vec_sub, 1, 1) = std_standard_gar.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 1, 2) = std_target_gar.(str_sub);
    
    mat_se2(:, :, :, ix_vec_sub, 1, 1) = se2_standard_gar.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 1, 2) = se2_target_gar.(str_sub);
end

%%

% p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'proc_epoch', 'figures', ...
%     sprintf('c%s_vs%s%s', gen_source(data_source_obs), gen_source(data_source_net), str_spectral_band));
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig06', ...
    sprintf('c_%s%s_vs%s%s', reref, gen_source(data_source_gar), '', str_spectral_band));
% p_fig = '~/Desktop/temp/';
es = '';
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'svg';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
print_local = @(h, dim) printForPub(h, sprintf('f_evo_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

cmap = [c.obs_st; c.net_st; c.obs_ta; c.net_ta; c.neutral];
%%
h_f = figure('Name', sprintf('%s', 'GAR'));clf;

ns = numSubplots(length(coi_full)+1);
fprintf('---------\n');
clims = [-0.3, 0.3];
for ix_coi = 1:length(coi_full)+1
    if ix_coi==length(coi_full)+1
        ax = subplot('Position',[ ...
            0.8, 0.1, ...
            topo_locs.topoW*2, ...
            topo_locs.topoH, ...
            ]);hold on;
        title('bits');
        ax_im = imagesc(ax, 'XData', t,'YData', f,'CData', x, clims);
        colorbar;
        xlabel('Time (s)');
        ylabel('Frequency (Hz)')
        xlim([0, 3])
        ylim([0 30])
    else
        case_ch = strcmpi(eeg_labels, coi_full{ix_coi});
        title_ = sprintf('%s', coi_full{ix_coi});
        
        ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);hold on;
        
        % needs to be checked!
        
        mea_standard_gar = mean(mat_med(:, :, case_ch, :, 1, 1), 4);
        mea_target_gar = mean(mat_med(:, :, case_ch, :, 1, 2), 4);
        med_target_gar = median(mat_med(:, :, case_ch, :, 1, 2), 4);
        se2_standard_gar = std_error(mat_med(:, :, case_ch, :, 1, 1), 0, 4);
        se2_target_gar = std_error(mat_med(:, :, case_ch, :, 1, 2), 0, 4);
        
        x = squeeze(med_target_gar);
        [min(x(:)), max(x(:))]
        x = smoothn(x, 0.15);
        ax_im = imagesc(ax, 'XData', t, 'YData', f, 'CData', x, clims);
        
        title(title_);
        
        axis tight;
        plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
        xlim([0, 3])
        ylim([1 30])
    end
end
s_f = [18.1, 22];

fprintf('---------\n');
print_local(h_f, [18.1, 22]);
