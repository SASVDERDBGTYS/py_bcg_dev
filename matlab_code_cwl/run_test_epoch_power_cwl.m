clearvars;
close all;
set_env_cwl;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'an_results')));
use_cv = true;
%%

d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';
%%
arch = 'gru_arch_general4';
if strcmpi(getenv('R_PROC'), 'proc_cwl')
    proc_net = fullfile('proc_cv_epochs', 'single_sub', arch);
end
T_full = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg);
%% correct for CWL subject encoding
% T_full.ix_typesub = T_full.ix_sub;
T_full.ix_type = floor(T_full.ix_sub/10);
T_full.ix_sub_real = T_full.ix_sub - T_full.ix_type*10;
%%
% T = T_full(T_full.ix_type==1, :);es = 'in-scan_hpump-on';
% T = T_full(T_full.ix_type==2, :);es = 'in-scan_hpump-off';
% T = T_full(T_full.ix_type==4, :);es = 'in-noscan_hpump-off'; % 

%% Load up outside before and after data sets.
ix_type_outside_before = 7;
ix_type_outside_after = 8;
T.p_out_after = T.p_net;
T.p_out_before = T.p_net;
t_epoch = 3.0;
for ix_ix_sub_real = 1:length(T.ix_sub_real)
    ix_sub_real = T.ix_sub_real(ix_ix_sub_real);
    
    str_sub = T.str_sub{ix_ix_sub_real};
    str_sub_outside_before = sprintf('sub%d%d', ix_type_outside_before, ix_sub_real);
    str_sub_outside_after = sprintf('sub%d%d', ix_type_outside_after, ix_sub_real);
    
    f_ob_rs = sprintf('%s_r00_rs.set', str_sub_outside_before);
    f_ob_rs = fullfile(getenv('D_DATA'), d_project, proc, 'proc_rs', str_sub_outside_before, f_ob_rs);
    
    if exist(f_ob_rs, 'file')==2
        EEG = pop_loadset(f_ob_rs);
        EEG = eeg_regepochs(EEG, t_epoch, [0, t_epoch], NaN);
        psd_mat = sp_return_psd(EEG);
    else
        psd_mat = [];
    end
    T.p_out_before{ix_ix_sub_real} = psd_mat;
    
    f_oa_rs = sprintf('%s_r00_rs.set', str_sub_outside_after);
    f_oa_rs = fullfile(getenv('D_DATA'), d_project, proc, 'proc_rs', str_sub_outside_after, f_oa_rs);
    
    if exist(f_oa_rs, 'file')==2
        EEG = pop_loadset(f_oa_rs);
        EEG = eeg_regepochs(EEG, t_epoch, [0, t_epoch], NaN);
        psd_mat = sp_return_psd(EEG);
    else
        psd_mat = [];
    end
    T.p_out_after{ix_ix_sub_real} = psd_mat;
    
    %         if ix_ix_sub_real == 1
    %                     theta = -pi/2;
    %         topo = [[EEG.chanlocs.X]',[EEG.chanlocs.Y]']*[cos(theta),-sin(theta);sin(theta),cos(theta)]*1.0;
    %         topoX = topo(:,1);
    %         topoY = topo(:,2);
    %         topoW = 0.08;
    %         topoH = 0.08;
    %         %prototypical subject
    %         topo_locs.topoX = topoX;
    %         topo_locs.topoY = topoY;
    %         topo_locs.topoW = topoW;
    %         topo_locs.topoH = topoH;
    %         end
    
    
end

%%
eeg_labels = T.Properties.UserData.chlabels;
topo = load(fullfile(getenv('D_OUT'), d_project, 'proc_full', 'topo_locs.mat'));
topo_locs.topoW = topo.topo_locs.topoW;
topo_locs.topoH = topo.topo_locs.topoH;
topo_locs.topoX = nan(length(eeg_labels), 1);
topo_locs.topoY = nan(length(eeg_labels), 1);
for ix_eeg_labels = 1:length(eeg_labels)
    ix_topo = strcmpi(eeg_labels{ix_eeg_labels}, topo.labels);
    if sum(ix_topo)==0
    else
        topo_locs.topoX(ix_eeg_labels) = topo.topo_locs.topoX(ix_topo);
        topo_locs.topoY(ix_eeg_labels) = topo.topo_locs.topoY(ix_topo);
    end
end
%%
cell_band = {'delta', 'theta', 'alpha'};
met_comparison = 'div';
cell_coi = {'T8', 'Pz', 'AVG'};
cell_coi_alt = {'T8', 'POz', 'Oz', 'Cz'};
coi_full = { ...
    'Fz','F3','F4', 'F7', 'F8', ...
    'Cz','C3','C4', ...
    'CP5', 'CP6', ...
    'T7', 'T8', ...
    'TP9', 'TP10', ...
    'P4', 'P7', 'P8', ...
    'POz', 'PO3', 'PO4', ...'PO9', 'PO10',...
    'Oz',...
    };
% coi_full = { ...
%     'Fz','F3','F4', 'F7', 'F8', ...
%     'Fpz', ...
%     'FC5','FC6', ...
%     'Cz','C3','C4', ...
%     'CP5', 'CP6', ...
%     'T7', 'T8', ...
%     'TP9', 'TP10', ...
%     'P4', 'P7', 'P8', ...
%     'POz', 'PO3', 'PO4', 'PO9', 'PO10',...
%     'Oz',...
%     };

% if strcmpi(reref, 'laplace')
%     a_units = 'µV/m²';
% else
%     a_units = 'µV';
% end
[T, T_sub] = get_power_difference(T, cell_band, cell_coi, met_comparison, ...
    v.str_ecg, 'str_sub');
%%
vec_sub = T_sub.ix_sub';
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
%% unlike other scripts, this one does direct operations on the whole of T/T_sub
% so just remove unused subjects if there are any
ix_keep_T = false(height(T), 1);
ix_keep_T_sub = false(height(T_sub), 1);
for ix_sub = 1:length(cell_sub)
    ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
    ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
end
T = T(ix_keep_T, :);
T_sub = T_sub(ix_keep_T_sub, :);

%%
v.figformat = 'svg';
v.figdir = pwd;
v.figsave = true;
% v.proc = 'proc';
v.fontsizeAxes = 7;
v.fontsizeText = 6;

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'fig_folder', 'fig04');
% p_fig = '~/Desktop/temp2/';
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end

v.figdir = p_fig;

print_local = @(h, dim) printForPub(h, sprintf('f_pow_%s_%s%s', arch, h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];
%%
h_f = figure('Name', sprintf('epoch_power'));clf;

ns = [1, length(cell_coi)];
fprintf('---------\n');
for ix_coi = 1:length(cell_coi)
    coi = cell_coi{ix_coi};
    
    
    if strcmpi(met_comparison, 'diff')
        met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', coi, band);
        met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', coi, band);
    elseif strcmpi(met_comparison, 'div')
        met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', coi, band);
        met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', coi, band);
    else
        error('');
    end
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    
    vec_obs_delta = T_sub.(met1('delta'));
    vec_net_delta = T_sub.(met2('delta'));
    vec_obs_theta = T_sub.(met1('theta'));
    vec_net_theta = T_sub.(met2('theta'));
    vec_obs_alpha = T_sub.(met1('alpha'));
    vec_net_alpha = T_sub.(met2('alpha'));
    X = [vec_obs_delta; vec_net_delta; vec_obs_theta; vec_net_theta; vec_obs_alpha; vec_net_alpha];
    
    hold on;
    G = [1 * ones(size(vec_obs_delta)); 2 * ones(size(vec_net_delta)); 3 * ones(size(vec_obs_theta)); 4 * ones(size(vec_net_theta)); 5 * ones(size(vec_obs_alpha)); 6 * ones(size(vec_net_alpha))];
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', repmat([cmap(2, :);cmap(3, :)], 3, 1), ...
        'labels',repmat({'OBS', 'BCGNet'}, 1, 3), 'Symbol', '');
    box off;
    set(h_box, {'linew'}, {1.75});
    av = findobj(gca, 'tag', 'Lower Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    av = findobj(gca, 'tag', 'Upper Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    
    c_grey = 0.7*ones(1, 3);
    c_alpha = 0.4;
    MarkerSize = 6;
    MarkerEdgeColor = [1,1,1];
    % note that in inkscape you then need to add a 0.1mm border to each
    % point (find property circle)
    
    for ix_vec_sub = 1:length(cell_sub)
        h = plot([1, 2], [vec_obs_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        h = plot([3, 4], [vec_obs_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        h = plot([5, 6], [vec_obs_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        
        for ix = 1:6
            switch ix
                case 1, vec_local = vec_obs_delta(ix_vec_sub);
                case 2, vec_local = vec_net_delta(ix_vec_sub);
                case 3, vec_local = vec_obs_theta(ix_vec_sub);
                case 4, vec_local = vec_net_theta(ix_vec_sub);
                case 5, vec_local = vec_obs_alpha(ix_vec_sub);
                case 6, vec_local = vec_net_alpha(ix_vec_sub);
            end
            
            plot(ix, vec_local, ...
                '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
                'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
        end
    end
    
    if strcmpi(coi, 'AVG')
        ylim([-0.05, 1.1   ])
        p_line_height = 1.;
    else
        ylim([-0.05, 1.5])
        p_line_height = 1.35;
    end
    
    y_range = [min(X(:)), max(X(:))];
    d = (diff(y_range)*0.075);
    tooth_length = d * 0.25;
    line_height = y_range(2) + d;
    
    [p_obs_net_delta, ~, stats] = signrank(vec_obs_delta, vec_net_delta);
    [p_obs_net_theta, ~, stats] = signrank(vec_obs_theta, vec_net_theta);
    [p_obs_net_alpha, ~, stats] = signrank(vec_obs_alpha, vec_net_alpha);
    
    [p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha] = ...
        deal_bonf_holm([p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha]);
    
    [p_obs_net_delta, ~, md_obs_net_delta] = plot_stars(vec_obs_delta, vec_net_delta, [1, 2], p_line_height, tooth_length, p_obs_net_delta);
    [p_obs_net_theta, ~, md_obs_net_theta] = plot_stars(vec_obs_theta, vec_net_theta, [3, 4], p_line_height, tooth_length, p_obs_net_theta);
    [p_obs_net_alpha, ~, md_obs_net_alpha] = plot_stars(vec_obs_alpha, vec_net_alpha, [5, 6], p_line_height, tooth_length, p_obs_net_alpha);
    
    
    
    %         xtickangle(45)
    
    title(coi);
    fprintf('---\n');
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'xtick',[1.5, 3.5, 5.5])
    set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})
    
    fprintf('%s:\n(', coi);
    fprintf('delta MD = %0.2f, p = %s; ', md_obs_net_delta, p_to_str(p_obs_net_delta));
    fprintf('theta MD = %0.2f, p = %s; ', md_obs_net_theta, p_to_str(p_obs_net_theta));
    fprintf('alpha MD = %0.2f, p = %s', md_obs_net_alpha, p_to_str(p_obs_net_alpha));
    fprintf(')\n');
    % ylabel('AUC')
    
    ylabel('Power ratio')
    
    
end
print_local(h_f, [18.1, 5]);
%%
ns = numSubplots(length(cell_coi_alt));
lw = 2;
% cell_met = {'before', 'after'};
ix_sub = 8;
% for ix_met = 1:length(cell_met)
str_met = 'x';
h_f = figure('Name', sprintf('ind_sub%d_%s_fft', ix_sub, str_met));clf;
for ix_coi = 1:length(cell_coi_alt)
    
    case_ch = strcmpi(cell_coi_alt{ix_coi}, eeg_labels);
    title_ = strrep(sprintf('%s', cell_coi_alt{ix_coi}), '_', ' ');
    
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    %         ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
    
    cla;hold on;
    f = T.Properties.UserData.f;
    p_net = squeeze(T.p_net{ix_sub}(ix_coi, :, :));
    p_obs = squeeze(T.p_obs{ix_sub}(ix_coi, :, :));
    p_ob = squeeze(T.p_out_before{ix_sub}(ix_coi, :, :));
    if isempty(T.p_out_after{ix_sub})
        p_oa = nan(size(p_ob));
    else
        p_oa = squeeze(T.p_out_after{ix_sub}(ix_coi, :, :));
    end
    mea_obs = nanmedian(p_obs, 2);
    mea_net = nanmedian(p_net, 2);
    mea_ob = nanmedian(p_ob, 2);
    mea_oa = nanmedian(p_oa, 2);
    
    %         ciplot(mea_net - se2_standard, mea_net + se2_standard, t, c_st, 0.15);
    %         ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
    h_obs = plot(f, log(mea_obs), '-', 'Color', c.obs_ta);
    h_obs.LineWidth = lw;
    h_net = plot(f, log(mea_net), '-',  'Color', c.net_st);
    h_net.LineWidth = lw;
    h_ob = plot(f, log(mea_ob), '-', 'Color', c.neutral);
    h_ob.LineWidth = lw;
    h_oa = plot(f, log(mea_oa), '-', 'Color', c.neutral_dark);
    h_oa.LineWidth = lw;
    title(title_);
    
    ax.Color = 'none';
    ax.XColor = 'none';
    axis tight;
    
    xlim([1, 20])
    ylim([0 20])
end
s_f = [18.1, 10];
print_local(h_f, s_f);
%%
ns = numSubplots(length(vec_sub));
lw = 2;
% cell_met = {'before', 'after'};
% for ix_met = 1:length(cell_met)
str_met = 'x';
h_f = figure('Name', sprintf('sub_%s_%s_fft', 'avg', str_met));clf;
for ix_sub = 1:length(vec_sub)
    

%     case_ch = strcmpi(cell_coi_alt{ix_coi}, eeg_labels);
    title_ = strrep(sprintf('%d', ix_sub), '_', ' ');
    
    subplot(ns(1), ns(2), ix_sub);cla;hold on;
    %         ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
    
    cla;hold on;
    f = T.Properties.UserData.f;
    p_net = squeeze(T.p_net{ix_sub}(:, :, :));
    p_obs = squeeze(T.p_obs{ix_sub}(:, :, :));
    p_ob = squeeze(T.p_out_before{ix_sub}(:, :, :));
    if isempty(T.p_out_after{ix_sub})
        p_oa = nan(size(p_ob));
    else
        p_oa = squeeze(T.p_out_after{ix_sub}(:, :, :));
    end
    mea_obs = squeeze(nanmedian(p_obs, [1, 3]));
    mea_net = squeeze(nanmedian(p_net, [1, 3]));
    mea_ob = squeeze(nanmedian(p_ob, [1, 3]));
    mea_oa = squeeze(nanmedian(p_oa, [1, 3]));
    
    %         ciplot(mea_net - se2_standard, mea_net + se2_standard, t, c_st, 0.15);
    %         ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
    h_obs = plot(f, log(mea_obs), '-', 'Color', c.obs_ta);
    h_obs.LineWidth = lw;
    h_net = plot(f, log(mea_net), '-',  'Color', c.net_st);
    h_net.LineWidth = lw;
    h_ob = plot(f, log(mea_ob), '-', 'Color', c.neutral);
    h_ob.LineWidth = lw;
    h_oa = plot(f, log(mea_oa), '-', 'Color', c.neutral_dark);
    h_oa.LineWidth = lw;
    title(title_);
    
    ax.Color = 'none';
    ax.XColor = 'none';
    axis tight;
    
    xlim([1, 50])

%     ylim([-20 20])

end
s_f = [40.1, 10];
print_local(h_f, s_f);
%%
ns = numSubplots(length(coi_full));
lw = 1;
% cell_met = {'before', 'after'};
ix_sub = 1;
% for ix_met = 1:length(cell_met)
str_met = 'x';
h_f = figure('Name', sprintf('topo_sub%d_%s_fft', ix_sub, str_met));clf;
for ix_coi = 1:length(coi_full)+1
    if ix_coi==length(coi_full)+1
        subplot('Position',[ ...
            0.8, 0.1, ...
            topo_locs.topoW, ...
            topo_locs.topoH, ...
            ]);hold on;
        title('bits');
        %             h_standard = plot(t, mea_standard, '-',  'Color', c_st);
        %             h_standard.LineWidth = lw;
        %             h_target = plot(t, mea_target, '-', 'Color', c_ta);
        %             h_target.LineWidth = lw;
        %             legend([h_standard, h_target], ...
        %                 {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
        %             legend('boxoff');
    else
        case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
        title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
        
        %         topo_locs = EEG.chanlocs;
        %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
        ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
        

        f = T.Properties.UserData.f;
        p_net = squeeze(T.p_net{ix_sub}(ix_coi, :, :));
        p_obs = squeeze(T.p_obs{ix_sub}(ix_coi, :, :));
        p_ob = squeeze(T.p_out_before{ix_sub}(ix_coi, :, :));
        if isempty(T.p_out_after{ix_sub})
            p_oa = nan(size(p_ob));
        else
            p_oa = squeeze(T.p_out_after{ix_sub}(ix_coi, :, :));
        end
        mea_obs = nanmedian(p_obs, 2);
        mea_net = nanmedian(p_net, 2);
        mea_ob = nanmedian(p_ob, 2);
        mea_oa = nanmedian(p_oa, 2);
        
        %         ciplot(mea_net - se2_standard, mea_net + se2_standard, t, c_st, 0.15);
        %         ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
        h_obs = plot(f, log(mea_obs), '-', 'Color', c.obs_ta);
        h_obs.LineWidth = lw;
        h_net = plot(f, log(mea_net), '-',  'Color', c.net_st);
        h_net.LineWidth = lw;
        h_ob = plot(f, log(mea_ob), '-', 'Color', c.neutral);
        h_ob.LineWidth = lw;
        h_oa = plot(f, log(mea_oa), '-', 'Color', c.neutral_dark);
        h_oa.LineWidth = lw;

    end
    axis tight;
    xlim([3, 20])

end
s_f = [18.1, 22];
print_local(h_f, s_f);
% end
%%
