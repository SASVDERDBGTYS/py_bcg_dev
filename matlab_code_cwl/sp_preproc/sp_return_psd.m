function psd_mat = sp_return_psd(EEG)
ch_names = {EEG.chanlocs.labels};
srate = EEG.srate;
X_met = permute(EEG.data, [3, 1, 2]);
for ix_chan = 1:length(ch_names)
    Y = squeeze(X_met(:, ix_chan, :))';
    [psd, f] = welch_custom(Y, srate);
    if ix_chan == 1
        psd_mat = nan(length(ch_names), length(f), size(psd, 2));
    end
    psd_mat(ix_chan, :, :) = psd;
end
end