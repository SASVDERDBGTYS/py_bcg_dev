% sp_resample_clayden: Performs resampling of the original dataset
% based on specfied downsampling ratio and uses either acausal/causal
% filtering based on a flag provided by the user. Saves output to proc_rs
% folder

% Inputs:
%   EEG: EEGLAB format data in which gradient artifact has been removed
%
%   varargin: struct for overwritting the default parameters
%       str_ecg: name of the ECG channel
%
%       fs_rs: desired sampling rate after resampling
%
%       flag_causal: flag for whether to use causal sampling
%           0: acausal
%           1: causal
%
%   v: setting struct from sp_rmbcg_clayden

function sp_resample_cwl(d_project, varargin)
%% Initial environment exception handling

assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');


%% Parameter setup

% default parameters
d.str_ecg = 'ECG';                       % name of the ECG channel
d.str_eeg = 'EEG';
d.overwrite = false;                      % flag for whether to overwrite existing data
d.d_overwrite = struct;                  % overwriting struct provided by user
d.proc = getenv_check_empty('R_PROC');   % obtain the proc directory

d.fs_rs = 500;
d.proc_rs = 'proc_rs';
d.hpf_cutoff = 0.25;
d.flag_causal = 0;

% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Obtain the path information

d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);
d_rmga = fullfile(d_proc, 'proc_ga');
d_rs = fullfile(d_proc, v.proc_rs);

%% Performs resampling

% obtain all dataset with gradient artifact removed
file_list = glob(fullfile(d_rmga, '**_r0[0-9]_rmga.set'));

% loop through all the datasets
for ix_sub = 1:length(file_list)
    %% obtain subject and run index
    
    % obtain file name, subject idx, run idx, etc
    [p_rmga, f_rmga, ext_rmga] = fileparts(file_list{ix_sub});
    rng(string2hash(f_rmga));
    f_rmga = sprintf('%s%s', f_rmga, ext_rmga);
    [~,folder_rmga,~] = fileparts(p_rmga);
    ix_run = extractBetween(f_rmga, '_r0', '_rmga');
    ix_run = str2double(ix_run{end});
    str_sub = lower(folder_rmga);
    
    %untested
    ix_typesub = regexp(str_sub, 'sub(.+)', 'tokens');
    ix_typesub = str2double(ix_typesub{1}{1});
    type_ix = floor(ix_typesub/10);
    sub_ix = ix_typesub - 10*type_ix;
    
    % define output directory and file name and test whether to
    % perform gradient removal
    f_out_rs = sprintf('%s_r%02d_rs', str_sub, ix_run);
    p_out_rs = fullfile(d_rs, str_sub);
    d_out_rs = fullfile(p_out_rs, f_out_rs);
    
    do_rs = generate_check_eeg(d_out_rs, v.overwrite);
    
    % if perform resampling
    if do_rs
        %% preprocessing
        % load dataset
        fprintf('\n\n\nLoading:\n%s\n\n', f_rmga);
        EEG_rmga = pop_loadset(f_rmga, p_rmga);
        
        % convert all time stamps and data to double
        EEG_rmga.times = double(EEG_rmga.times);
        EEG_rmga.data = double(EEG_rmga.data);
        
        %% extract data between second and second last TR marker
        
        % here extract between these TR markers due to artifacts before and
        % after the last TR marker
        
        if type_ix<=2  % only do this for data that has GA though
            EEG_extract = sp_extract_data_cwl(EEG_rmga, 'str_sub', ...
                str_sub, 'ix_run', ix_run);
        else
            EEG_extract = EEG_rmga;
        end
        %% resampling
        
        fprintf('\nPerforming resampling\n');
        
        % perform resampling
        EEG_rs = pop_resample(EEG_extract, v.fs_rs);
        
        %% perform HPF on the data
        fprintf('\nHighpass filtering the data at %f Hz\n', v.hpf_cutoff);
        if ~v.flag_causal
            y_ecg = EEG_rs.data(strcmpi({EEG_rs.chanlocs.labels}, v.str_ecg), :);
            EEG_rs = pop_eegfiltnew(EEG_rs, v.hpf_cutoff, [], [], 0, [], 1);
        else
            warning('NOT IMPLEMENTED CURRENTLY!!');
        end
        
        % selecting the EEG and ECG channels only
        idx_channels = 1:length(EEG_rmga.chanlocs);
        idx_eeg = idx_channels(strcmpi({EEG_rmga.chanlocs.type}, ...
            v.str_eeg));
        idx_ecg = idx_channels(strcmpi({EEG_rmga.chanlocs.type}, ...
            v.str_ecg));
        EEG_rs = pop_select( EEG_rs, 'channel', [idx_eeg, idx_ecg]);
        
        % store the processing conditions and set setname
        EEG_rs.etc.sp_resample_cwl = v;
        EEG_rs.setname = f_out_rs;
        
        % check set again
        EEG_rs = eeg_checkset(EEG_rs);
        
        % save dataset
        pop_saveset(EEG_rs, 'filename', f_out_rs, 'filepath', p_out_rs, 'version', '6');
        
    end
end
end