%% Setup

clear;
clc;
close all;

set_env_cwl;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'sp_preproc')));
if isempty(which('eeglab.m')),addpath(getenv('D_EEGLAB'));eeglab;close;end
addpath('sp_preproc');

d_project = 'working_eegbcg';

%% Perform BCG removal

sp_rmbcg_cwl(d_project);
