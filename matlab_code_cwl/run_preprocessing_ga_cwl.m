%% Setup

clear;
clc;
close all;

set_env_cwl;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
if isempty(which('eeglab.m')),addpath(getenv('D_EEGLAB'));eeglab;close;end
addpath('sp_preproc');

d_project = 'working_eegbcg';

%% Perform GA removal
% note that for CWL this also sorts data that doesn't need to have GA
% performed on it.
sp_rmga_cwl(d_project);

%%