
function set_env_cwl
if ispc
    userDir = winqueryreg('HKEY_CURRENT_USER',...
        ['Software\Microsoft\Windows\CurrentVersion\' ...
        'Explorer\Shell Folders'],'Personal');
    userDir = fileparts(userDir);
else
    userDir = char(java.lang.System.getProperty('user.home'));
end
%%
if isempty(which('dnc_set_env_cwl'))
    % Defaults
    setenv('D_USER', userDir);
    setenv('D_EEGLAB', fullfile(userDir,'Cloud','Research','matlab','liblarge','eeglab14_1_2b_desktop_ubuntu'));
    setenv('PYENV_RTMATLAB', fullfile(userDir,'miniconda2','envs','fooof','bin','python'));
    setenv('D_DATA', fullfile(userDir, 'Local'));
    setenv('D_OUT', fullfile(userDir, 'Local'));
    setenv('D_DIPFIT', fullfile('plugins', 'dipfit2.3'));
%     /home/mcintosh/Local/working_eegbcg/proc_clayden
    setenv('D_GIT', fullfile(userDir, 'Local', 'gitprojects', 'py_bcg_dev'));
    setenv('R_PROC', 'proc_cwl'); % R for relative
else
    dnc_set_env_cwl(userDir);
end
%%
% addpath(fullfile(getenv('D_RTMATLAB'),'ipparser'));
end

%%
% example dnc_set_env.m - put this in your own local function!:

% function dnc_set_env(userDir)
% use_hpc = true;
% if exist(userDir, 'dir')==7
%     if strcmpi(userDir, '/home/mcintosh')
%         use_hpc = false;
%         setenv('D_USER', userDir);
%         setenv('D_EEGLAB', fullfile(userDir,'Cloud','Research','matlab','liblarge','eeglab14_1_2b'));
%         setenv('PYENV_RTMATLAB', fullfile(userDir,'miniconda2','envs','fooof','bin','python'));
%         setenv('D_DATA', fullfile(userDir, 'Local', 'DataPort'));
%     end
% end
% if use_hpc
%     error('HPC not setup!');
% end
% end