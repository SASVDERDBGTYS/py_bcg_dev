from py_bcg import bcg_net

if __name__ == '__main__':
    str_sub = 'sub32'
    run_id = 1
    arch = 'gru_arch_004'

    bcg_net.main_loop(str_sub=str_sub, run_id=run_id, arch=arch)
