import mne
from py_bcg import set_env
import numpy as np
from pathlib import Path
import pickle
from scipy import signal

vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]


trial_type = 'multi_run'
vec_raw_sub = []
vec_cleaned_raw_sub = []
vec_fmrib_raw_sub = []

for str_sub in vec_str_sub:
    vec_raw = []
    vec_cleaned_raw = []
    vec_fmrib_raw = []

    for run_id in vec_run_ix:
        print('not finished')

