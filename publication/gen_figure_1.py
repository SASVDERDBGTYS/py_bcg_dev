import mne
from py_bcg import set_env
from matplotlib import rc
import numpy as np
import scipy.io as sio

rc('font', **{'family': 'sans-serif', 'sans-serif': ['Verdana'], 'size': 10})

import matplotlib.pyplot as plt

vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]

str_sub = 'sub33'
run_id = 1

p_rs, f_rs = set_env.rs_path(str_sub, run_id)
data_dir = str(p_rs.joinpath(f_rs))

rs_added_raw = mne.io.read_raw_eeglab(data_dir, preload=True, stim_channel=False)
rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
ch = rs_added_raw.info['ch_names']

ecg_ch = 31
pz_ch = 18
t8_ch = 13
# o2_ch = 9

ecg_data_raw = rs_added_raw.get_data()[ecg_ch, :]
pz_data_raw = rs_added_raw.get_data()[pz_ch, :]
t8_data_raw = rs_added_raw.get_data()[t8_ch, :]
# o2_data_raw = rs_added_raw.get_data()[o2_ch, :]

fig, ax = plt.subplots(nrows=3, ncols=1, figsize=(6, 6))

start = 29.75
duration = 2.5
srate = int(rs_added_raw.info['sfreq'])
unit = 1e6
time_stamps = np.linspace(0, duration, int(duration * srate))

ax[0].plot(time_stamps, ecg_data_raw[int(start * srate): int((start + duration) * srate)] * unit)

ax[1].plot(time_stamps, t8_data_raw[int(start * srate): int((start + duration) * srate)] * unit)
ax[1].set_ylim([-75, 75])

ax[2].plot(time_stamps, pz_data_raw[int(start * srate): int((start + duration) * srate)] * unit)
ax[2].set_ylim([-75, 75])

plt.show()

p_figure = set_env.figure_path(1)
p_figure.mkdir(parents=True, exist_ok=True)
f_figure = 'all_data.mat'
data = rs_added_raw.get_data()[:, int(start * srate): int((start + duration) * srate)] * unit

sio.savemat(str(p_figure.joinpath(f_figure)), {'data': data})

# plt.show()
#
# raw_info = rs_added_raw.info
# data = rs_added_raw.get_data()[:, int(start * srate): int((start + duration) * srate)] * unit
# evoked_raw = mne.EvokedArray(data, raw_info, 0)

# ax[3].plot(time_stamps, o2_data_raw[int(start * srate): int((start + duration) * srate)] * unit)

# times = [0.7, 0.95, 1.1, 1.4]
# evoked_raw.plot_topomap(times, ch_type='eeg', time_unit='s')


print('nothing')