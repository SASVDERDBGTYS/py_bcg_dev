import mne
from py_bcg import set_env
from py_bcg import bcg_net
from matplotlib import rcParams
import numpy as np
from pathlib import Path
import pickle
from scipy import signal
from scipy import stats

params = {
   'axes.labelsize': 10,
   'legend.title_fontsize': 10,
   'legend.fontsize': 10,
   'xtick.labelsize': 10,
   'ytick.labelsize': 10,
   'text.usetex': False,
   }
rcParams.update(params)

rcParams['font.family'] = 'sans-serif'
rcParams['mathtext.default'] = 'regular'
rcParams['font.sans-serif'] = ['Verdana']
rcParams["legend.loc"] = 'upper right'

import matplotlib.pyplot as plt

vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]

arch = 'gru_arch_general3'
trial_type = 'multi_run'

vec_raw_sub = []
vec_cleaned_raw_sub = []
vec_fmrib_raw_sub = []
ecg_ch = 31
pz_ch = 18
fz_ch = 16
unit = 1e6
evaluation = 0.85
p_figure = set_env.figure_path(5)
p_figure.mkdir(parents=True, exist_ok=True)

# Variables corresponding to all EEG channels
f_eeg_raw_sub = []
f_eeg_cleaned_sub = []
f_eeg_fmrib_sub = []

Pxx_eeg_raw_sub = []
Pxx_eeg_cleaned_sub = []
Pxx_eeg_fmrib_sub = []

# Variables corresponding to channel Fz
f_fz_raw_sub = []
f_fz_cleaned_sub = []
f_fz_fmrib_sub = []

Pxx_fz_raw_sub = []
Pxx_fz_cleaned_sub = []
Pxx_fz_fmrib_sub = []

# Variables corresponding to channel Pz
f_pz_raw_sub = []
f_pz_cleaned_sub = []
f_pz_fmrib_sub = []

Pxx_pz_raw_sub = []
Pxx_pz_cleaned_sub = []
Pxx_pz_fmrib_sub = []

for str_sub in vec_str_sub:
    vec_epoch_raw = []
    vec_epoch_cleaned = []
    vec_epoch_fmrib = []

    for run_id in vec_run_ix:
        p_rs, f_rs = set_env.rs_path(str_sub, run_id)
        p_cleaned, f_cleaned = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
        p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)

        data_dir_raw = str(p_rs.joinpath(f_rs))
        data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
        data_dir_bcg = str(p_bcg.joinpath(f_bcg))

        rs_added_raw = mne.io.read_raw_eeglab(data_dir_raw, preload=True, stim_channel=False)
        cleaned_raw = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)
        srate = cleaned_raw.info['sfreq']

        rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
        rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
        fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        epoched_data_cleaned, good_ix = bcg_net.dataset_epoch(dataset=cleaned_raw, duration=3, epoch_rejection=True,
                                                              threshold=5, raw_dataset=rs_added_raw_copy)

        epoched_d_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False,
                                                 good_ix=good_ix)
        raw_info = rs_added_raw.info
        epoched_data_raw = mne.EpochsArray(epoched_d_raw, raw_info)

        epoched_d_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False,
                                                   good_ix=good_ix)
        fmrib_info = fmrib_raw.info
        epoched_data_fmrib = mne.EpochsArray(epoched_d_fmrib, fmrib_info)

        vec_epoch_raw.append(epoched_data_raw)
        vec_epoch_cleaned.append(epoched_data_cleaned)
        vec_epoch_fmrib.append(epoched_data_fmrib)

    # if str_sub == 'sub14':
    #     print('Subject 14 and run 2 for testing')
    #     r2_raw = vec_epoch_raw[1]
    #     r2_cleaned = vec_epoch_cleaned[1]
    #     r2_fmrib = vec_epoch_fmrib[1]
    #
    #     epoch_num = 68
    #     ecg_ch = 31
    #     ch_num = 7
    #     unit = 1e6
    #
    #     plt.subplot(211)
    #     plt.plot(r2_raw.get_data()[epoch_num, ecg_ch, :] * unit)
    #
    #     plt.subplot(212)
    #     plt.plot(r2_raw.get_data()[epoch_num, ch_num, :] * unit, label='raw')
    #     plt.plot(r2_cleaned.get_data()[epoch_num, ch_num, :] * unit, label='BCGNet')
    #     plt.plot(r2_fmrib.get_data()[epoch_num, ch_num, :] * unit, label='OBS')
    #     plt.legend()
    #     plt.show()
    #     print('nothing')

    # Code for combining epochs based on subjects starts here
    epoch_combined_raw = None
    for i in range(len(vec_epoch_raw)):
        if i == 0:
            epoch_combined_raw = vec_epoch_raw[i]
        else:
            epoch_combined_raw = mne.concatenate_epochs([epoch_combined_raw, vec_epoch_raw[i]])

    epoch_data_combined_raw = epoch_combined_raw.get_data()
    num_epochs = epoch_data_combined_raw.shape[0]
    batch_size = epoch_data_combined_raw.shape[2]
    with bcg_net.temp_seed(1997):
        s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(epoch_data_combined_raw, evaluation)

    epoch_combined_cleaned = None
    for i in range(len(vec_epoch_cleaned)):
        if i == 0:
            epoch_combined_cleaned = vec_epoch_cleaned[i]
        else:
            epoch_combined_cleaned = mne.concatenate_epochs([epoch_combined_cleaned, vec_epoch_cleaned[i]])
    epoch_data_combined_cleaned = epoch_combined_cleaned.get_data()

    epoch_combined_fmrib = None
    for i in range(len(vec_epoch_fmrib)):
        if i == 0:
            epoch_combined_fmrib = vec_epoch_fmrib[i]
        else:
            epoch_combined_fmrib = mne.concatenate_epochs([epoch_combined_fmrib, vec_epoch_fmrib[i]])
    epoch_combined_fmrib = epoch_combined_fmrib.get_data()

    vec_n_events = []
    for i in range(len(vec_epoch_raw)):
        n_events = vec_epoch_raw[i].get_data().shape[0]
        vec_n_events.append(n_events)

    vec_n_events_cum = np.cumsum(vec_n_events)

    # Variables corresponding to all EEG channels
    f_eeg_raw_run = []
    f_eeg_cleaned_run = []
    f_eeg_fmrib_run = []

    Pxx_eeg_raw_run = []
    Pxx_eeg_cleaned_run = []
    Pxx_eeg_fmrib_run = []

    # Variables corresponding to channel Fz
    f_fz_raw_run = []
    f_fz_cleaned_run = []
    f_fz_fmrib_run = []

    Pxx_fz_raw_run = []
    Pxx_fz_cleaned_run = []
    Pxx_fz_fmrib_run = []

    # Variables corresponding to channel Pz
    f_pz_raw_run = []
    f_pz_cleaned_run = []
    f_pz_fmrib_run = []

    Pxx_pz_raw_run = []
    Pxx_pz_cleaned_run = []
    Pxx_pz_fmrib_run = []

    for run_id in vec_run_ix:
        if run_id == 1:
            ix_slice_test = vec_ix_slice_test[np.where(vec_ix_slice_test < vec_n_events_cum[run_id - 1])]
            ix_slice_test = np.sort(ix_slice_test)
        else:
            ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 2] <= vec_ix_slice_test,
                                                                      vec_ix_slice_test < vec_n_events_cum[
                                                                          run_id - 1]))]
            ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 2])

        epoched_raw = vec_epoch_raw[run_id - 1]
        epoched_cleaned = vec_epoch_cleaned[run_id - 1]
        epoched_fmrib = vec_epoch_fmrib[run_id - 1]
        srate = epoched_raw.info['sfreq']

        epoched_data_raw = epoched_raw.get_data()
        epoched_data_cleaned = epoched_cleaned.get_data()
        epoched_data_fmrib = epoched_fmrib.get_data()

        # Obtaining all EEG data
        target_ch = np.delete(np.arange(0, 64, 1), ecg_ch)

        eeg_raw_all = epoched_data_raw[:, target_ch, :] * unit
        eeg_raw_test = eeg_raw_all[ix_slice_test, :, :]

        eeg_cleaned_all = epoched_data_cleaned[:, target_ch, :] * unit
        eeg_cleaned_test = eeg_cleaned_all[ix_slice_test, :, :]

        eeg_fmrib_all = epoched_data_fmrib[:, target_ch, :] * unit
        eeg_fmrib_test = eeg_fmrib_all[ix_slice_test, :, :]

        # Obtaining data for channel Fz
        fz_raw_test = epoched_data_raw[ix_slice_test, fz_ch, :] * unit
        fz_cleaned_test = epoched_data_cleaned[ix_slice_test, fz_ch, :] * unit
        fz_fmrib_test = epoched_data_fmrib[ix_slice_test, fz_ch, :] * unit

        # Obtaining data for channel Pz
        pz_raw_test = epoched_data_raw[ix_slice_test, pz_ch, :] * unit
        pz_cleaned_test = epoched_data_cleaned[ix_slice_test, pz_ch, :] * unit
        pz_fmrib_test = epoched_data_fmrib[ix_slice_test, pz_ch, :] * unit

        # Variables corresponding to all EEG Channels
        f_eeg_raw = []
        f_eeg_cleaned = []
        f_eeg_fmrib = []

        Pxx_eeg_raw = []
        Pxx_eeg_cleaned = []
        Pxx_eeg_fmrib = []

        # Variables corresponding to Channel Fz
        f_fz_raw = []
        f_fz_cleaned = []
        f_fz_fmrib = []

        Pxx_fz_raw = []
        Pxx_fz_cleaned = []
        Pxx_fz_fmrib = []

        # Variables corresponding to Channel Pz
        f_pz_raw = []
        f_pz_cleaned = []
        f_pz_fmrib = []

        Pxx_pz_raw = []
        Pxx_pz_cleaned = []
        Pxx_pz_fmrib = []

        for ix_epoch in range(eeg_raw_test.shape[0]):
            # Generating the PSD for all channels
            f_eeg_raw_chs = []
            f_eeg_cleaned_chs = []
            f_eeg_fmrib_chs = []

            Pxx_eeg_raw_chs = []
            Pxx_eeg_cleaned_chs = []
            Pxx_eeg_fmrib_chs = []
            for ix_ch in range(eeg_raw_test.shape[1]):
                eeg_raw_data = eeg_raw_test[ix_epoch, ix_ch, :]
                eeg_cleaned_data = eeg_cleaned_test[ix_epoch, ix_ch, :]
                eeg_fmrib_data = eeg_fmrib_test[ix_epoch, ix_ch, :]

                f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(eeg_raw_data, srate, nperseg=srate * 5)
                f_eeg_cleaned_i, Pxx_eeg_cleaned_i = signal.welch(eeg_cleaned_data, srate, nperseg=srate * 5)
                f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(eeg_fmrib_data, srate, nperseg=srate * 5)

                f_eeg_raw_chs.append(f_eeg_raw_i)
                Pxx_eeg_raw_chs.append(Pxx_eeg_raw_i)

                f_eeg_cleaned_chs.append(f_eeg_cleaned_i)
                Pxx_eeg_cleaned_chs.append(Pxx_eeg_cleaned_i)

                f_eeg_fmrib_chs.append(f_eeg_fmrib_i)
                Pxx_eeg_fmrib_chs.append(Pxx_eeg_fmrib_i)

            f_eeg_raw_chs = np.array(f_eeg_raw_chs)
            f_eeg_cleaned_chs = np.array(f_eeg_cleaned_chs)
            f_eeg_fmrib_chs = np.array(f_eeg_fmrib_chs)

            Pxx_eeg_raw_chs = np.array(Pxx_eeg_raw_chs)
            Pxx_eeg_cleaned_chs = np.array(Pxx_eeg_cleaned_chs)
            Pxx_eeg_fmrib_chs = np.array(Pxx_eeg_fmrib_chs)

            f_eeg_raw_chs_avg = np.mean(f_eeg_raw_chs, axis=0)
            f_eeg_cleaned_chs_avg = np.mean(f_eeg_cleaned_chs, axis=0)
            f_eeg_fmrib_chs_avg = np.mean(f_eeg_fmrib_chs, axis=0)

            Pxx_eeg_raw_chs_avg = np.mean(Pxx_eeg_raw_chs, axis=0)
            Pxx_eeg_cleaned_chs_avg = np.mean(Pxx_eeg_cleaned_chs, axis=0)
            Pxx_eeg_fmrib_chs_avg = np.mean(Pxx_eeg_fmrib_chs, axis=0)

            f_eeg_raw.append(f_eeg_raw_chs_avg)
            f_eeg_cleaned.append(f_eeg_cleaned_chs_avg)
            f_eeg_fmrib.append(f_eeg_fmrib_chs_avg)

            Pxx_eeg_raw.append(Pxx_eeg_raw_chs_avg)
            Pxx_eeg_cleaned.append(Pxx_eeg_cleaned_chs_avg)
            Pxx_eeg_fmrib.append(Pxx_eeg_fmrib_chs_avg)

            # Generating the PSD for channel Fz
            fz_raw_data = fz_raw_test[ix_epoch, :]
            fz_cleaned_data = fz_cleaned_test[ix_epoch, :]
            fz_fmrib_data = fz_fmrib_test[ix_epoch, :]

            f_fz_raw_i, Pxx_fz_raw_i = signal.welch(fz_raw_data, srate, nperseg=srate * 5)
            f_fz_cleaned_i, Pxx_fz_cleaned_i = signal.welch(fz_cleaned_data, srate, nperseg=srate * 5)
            f_fz_fmrib_i, Pxx_fz_fmrib_i = signal.welch(fz_fmrib_data, srate, nperseg=srate * 5)

            f_fz_raw.append(f_fz_raw_i)
            f_fz_cleaned.append(f_fz_cleaned_i)
            f_fz_fmrib.append(f_fz_fmrib_i)

            Pxx_fz_raw.append(Pxx_fz_raw_i)
            Pxx_fz_cleaned.append(Pxx_fz_cleaned_i)
            Pxx_fz_fmrib.append(Pxx_fz_fmrib_i)

            # Generating the PSD for channel Pz
            pz_raw_data = pz_raw_test[ix_epoch, :]
            pz_cleaned_data = pz_cleaned_test[ix_epoch, :]
            pz_fmrib_data = pz_fmrib_test[ix_epoch, :]

            f_pz_raw_i, Pxx_pz_raw_i = signal.welch(pz_raw_data, srate, nperseg=srate * 5)
            f_pz_cleaned_i, Pxx_pz_cleaned_i = signal.welch(pz_cleaned_data, srate, nperseg=srate * 5)
            f_pz_fmrib_i, Pxx_pz_fmrib_i = signal.welch(pz_fmrib_data, srate, nperseg=srate * 5)

            f_pz_raw.append(f_pz_raw_i)
            f_pz_cleaned.append(f_pz_cleaned_i)
            f_pz_fmrib.append(f_pz_fmrib_i)

            Pxx_pz_raw.append(Pxx_pz_raw_i)
            Pxx_pz_cleaned.append(Pxx_pz_cleaned_i)
            Pxx_pz_fmrib.append(Pxx_pz_fmrib_i)

        # Variables corresponding to all EEG channels
        f_eeg_raw = np.array(f_eeg_raw)
        f_eeg_cleaned = np.array(f_eeg_cleaned)
        f_eeg_fmrib = np.array(f_eeg_fmrib)

        Pxx_eeg_raw = np.array(Pxx_eeg_raw)
        Pxx_eeg_cleaned = np.array(Pxx_eeg_cleaned)
        Pxx_eeg_fmrib = np.array(Pxx_eeg_fmrib)

        f_eeg_raw_avg = np.mean(f_eeg_raw, axis=0)
        f_eeg_cleaned_avg = np.mean(f_eeg_cleaned, axis=0)
        f_eeg_fmrib_avg = np.mean(f_eeg_fmrib, axis=0)

        Pxx_eeg_raw_avg = np.mean(Pxx_eeg_raw, axis=0)
        Pxx_eeg_cleaned_avg = np.mean(Pxx_eeg_cleaned, axis=0)
        Pxx_eeg_fmrib_avg = np.mean(Pxx_eeg_fmrib, axis=0)

        f_eeg_raw_run.append(f_eeg_raw_avg)
        f_eeg_cleaned_run.append(f_eeg_cleaned_avg)
        f_eeg_fmrib_run.append(f_eeg_fmrib_avg)

        Pxx_eeg_raw_run.append(Pxx_eeg_raw_avg)
        Pxx_eeg_cleaned_run.append(Pxx_eeg_cleaned_avg)
        Pxx_eeg_fmrib_run.append(Pxx_eeg_fmrib_avg)

        # Variables corresponding to Channel Fz
        f_fz_raw = np.array(f_fz_raw)
        f_fz_cleaned = np.array(f_fz_cleaned)
        f_fz_fmrib = np.array(f_fz_fmrib)

        Pxx_fz_raw = np.array(Pxx_fz_raw)
        Pxx_fz_cleaned = np.array(Pxx_fz_cleaned)
        Pxx_fz_fmrib = np.array(Pxx_fz_fmrib)

        f_fz_raw_avg = np.mean(f_fz_raw, axis=0)
        f_fz_cleaned_avg = np.mean(f_fz_cleaned, axis=0)
        f_fz_fmrib_avg = np.mean(f_fz_fmrib, axis=0)

        Pxx_fz_raw_avg = np.mean(Pxx_fz_raw, axis=0)
        Pxx_fz_cleaned_avg = np.mean(Pxx_fz_cleaned, axis=0)
        Pxx_fz_fmrib_avg = np.mean(Pxx_fz_fmrib, axis=0)

        f_fz_raw_run.append(f_fz_raw_avg)
        f_fz_cleaned_run.append(f_fz_cleaned_avg)
        f_fz_fmrib_run.append(f_fz_fmrib_avg)

        Pxx_fz_raw_run.append(Pxx_fz_raw_avg)
        Pxx_fz_cleaned_run.append(Pxx_fz_cleaned_avg)
        Pxx_fz_fmrib_run.append(Pxx_fz_fmrib_avg)

        # Variables corresponding to Channel Pz
        f_pz_raw = np.array(f_pz_raw)
        f_pz_cleaned = np.array(f_pz_cleaned)
        f_pz_fmrib = np.array(f_pz_fmrib)

        Pxx_pz_raw = np.array(Pxx_pz_raw)
        Pxx_pz_cleaned = np.array(Pxx_pz_cleaned)
        Pxx_pz_fmrib = np.array(Pxx_pz_fmrib)

        f_pz_raw_avg = np.mean(f_pz_raw, axis=0)
        f_pz_cleaned_avg = np.mean(f_pz_cleaned, axis=0)
        f_pz_fmrib_avg = np.mean(f_pz_fmrib, axis=0)

        Pxx_pz_raw_avg = np.mean(Pxx_pz_raw, axis=0)
        Pxx_pz_cleaned_avg = np.mean(Pxx_pz_cleaned, axis=0)
        Pxx_pz_fmrib_avg = np.mean(Pxx_pz_fmrib, axis=0)

        f_pz_raw_run.append(f_pz_raw_avg)
        f_pz_cleaned_run.append(f_pz_cleaned_avg)
        f_pz_fmrib_run.append(f_pz_fmrib_avg)

        Pxx_pz_raw_run.append(Pxx_pz_raw_avg)
        Pxx_pz_cleaned_run.append(Pxx_pz_cleaned_avg)
        Pxx_pz_fmrib_run.append(Pxx_pz_fmrib_avg)

        # plt.semilogy(f_eeg_raw_avg, Pxx_eeg_raw_avg, label='raw')
        # plt.semilogy(f_eeg_cleaned_avg, Pxx_eeg_cleaned_avg, label='BCGNet')
        # plt.semilogy(f_eeg_fmrib_avg, Pxx_eeg_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # plt.ylim(0.01, 1e2)
        # plt.legend()
        # plt.show()
        #
        # plt.figure()
        # plt.semilogy(f_fz_raw_avg, Pxx_fz_raw_avg, label='raw')
        # plt.semilogy(f_fz_cleaned_avg, Pxx_fz_cleaned_avg, label='BCGNet')
        # plt.semilogy(f_fz_fmrib_avg, Pxx_fz_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # plt.ylim(0.01, 1e2)
        # plt.legend()
        #
        # plt.figure()
        # plt.semilogy(f_pz_raw_avg, Pxx_pz_raw_avg, label='raw')
        # plt.semilogy(f_pz_cleaned_avg, Pxx_pz_cleaned_avg, label='BCGNet')
        # plt.semilogy(f_pz_fmrib_avg, Pxx_pz_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # plt.ylim(0.01, 1e2)
        # plt.legend()
        # plt.show()
        # print('test')

    # Variables corresponding to all EEG channels
    f_eeg_raw_run = np.array(f_eeg_raw_run)
    f_eeg_cleaned_run = np.array(f_eeg_cleaned_run)
    f_eeg_fmrib_run = np.array(f_eeg_fmrib_run)

    Pxx_eeg_raw_run = np.array(Pxx_eeg_raw_run)
    Pxx_eeg_cleaned_run = np.array(Pxx_eeg_cleaned_run)
    Pxx_eeg_fmrib_run = np.array(Pxx_eeg_fmrib_run)

    f_eeg_raw_run_avg = np.mean(f_eeg_raw_run, axis=0)
    f_eeg_cleaned_run_avg = np.mean(f_eeg_cleaned_run, axis=0)
    f_eeg_fmrib_run_avg = np.mean(f_eeg_fmrib_run, axis=0)

    Pxx_eeg_raw_run_avg = np.mean(Pxx_eeg_raw_run, axis=0)
    Pxx_eeg_cleaned_run_avg = np.mean(Pxx_eeg_cleaned_run, axis=0)
    Pxx_eeg_fmrib_run_avg = np.mean(Pxx_eeg_fmrib_run, axis=0)

    f_eeg_raw_sub.append(f_eeg_raw_run_avg)
    f_eeg_cleaned_sub.append(f_eeg_cleaned_run_avg)
    f_eeg_fmrib_sub.append(f_eeg_fmrib_run_avg)

    Pxx_eeg_raw_sub.append(Pxx_eeg_raw_run_avg)
    Pxx_eeg_cleaned_sub.append(Pxx_eeg_cleaned_run_avg)
    Pxx_eeg_fmrib_sub.append(Pxx_eeg_fmrib_run_avg)

    # Variables corresponding to channel Fz
    f_fz_raw_run = np.array(f_fz_raw_run)
    f_fz_cleaned_run = np.array(f_fz_cleaned_run)
    f_fz_fmrib_run = np.array(f_fz_fmrib_run)

    Pxx_fz_raw_run = np.array(Pxx_fz_raw_run)
    Pxx_fz_cleaned_run = np.array(Pxx_fz_cleaned_run)
    Pxx_fz_fmrib_run = np.array(Pxx_fz_fmrib_run)

    f_fz_raw_run_avg = np.mean(f_fz_raw_run, axis=0)
    f_fz_cleaned_run_avg = np.mean(f_fz_cleaned_run, axis=0)
    f_fz_fmrib_run_avg = np.mean(f_fz_fmrib_run, axis=0)

    Pxx_fz_raw_run_avg = np.mean(Pxx_fz_raw_run, axis=0)
    Pxx_fz_cleaned_run_avg = np.mean(Pxx_fz_cleaned_run, axis=0)
    Pxx_fz_fmrib_run_avg = np.mean(Pxx_fz_fmrib_run, axis=0)

    f_fz_raw_sub.append(f_fz_raw_run_avg)
    f_fz_cleaned_sub.append(f_fz_cleaned_run_avg)
    f_fz_fmrib_sub.append(f_fz_fmrib_run_avg)

    Pxx_fz_raw_sub.append(Pxx_fz_raw_run_avg)
    Pxx_fz_cleaned_sub.append(Pxx_fz_cleaned_run_avg)
    Pxx_fz_fmrib_sub.append(Pxx_fz_fmrib_run_avg)

    # Variablse corresponding to channel Pz
    f_pz_raw_run = np.array(f_pz_raw_run)
    f_pz_cleaned_run = np.array(f_pz_cleaned_run)
    f_pz_fmrib_run = np.array(f_pz_fmrib_run)

    Pxx_pz_raw_run = np.array(Pxx_pz_raw_run)
    Pxx_pz_cleaned_run = np.array(Pxx_pz_cleaned_run)
    Pxx_pz_fmrib_run = np.array(Pxx_pz_fmrib_run)

    f_pz_raw_run_avg = np.mean(f_pz_raw_run, axis=0)
    f_pz_cleaned_run_avg = np.mean(f_pz_cleaned_run, axis=0)
    f_pz_fmrib_run_avg = np.mean(f_pz_fmrib_run, axis=0)

    Pxx_pz_raw_run_avg = np.mean(Pxx_pz_raw_run, axis=0)
    Pxx_pz_cleaned_run_avg = np.mean(Pxx_pz_cleaned_run, axis=0)
    Pxx_pz_fmrib_run_avg = np.mean(Pxx_pz_fmrib_run, axis=0)

    f_pz_raw_sub.append(f_pz_raw_run_avg)
    f_pz_cleaned_sub.append(f_pz_cleaned_run_avg)
    f_pz_fmrib_sub.append(f_pz_fmrib_run_avg)

    Pxx_pz_raw_sub.append(Pxx_pz_raw_run_avg)
    Pxx_pz_cleaned_sub.append(Pxx_pz_cleaned_run_avg)
    Pxx_pz_fmrib_sub.append(Pxx_pz_fmrib_run_avg)

    # plt.semilogy(f_eeg_raw_run_avg, Pxx_eeg_raw_run_avg, label='raw')
    # plt.semilogy(f_eeg_cleaned_run_avg, Pxx_eeg_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_eeg_fmrib_run_avg, Pxx_eeg_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.ylim(0.01, 1e2)
    # plt.legend()
    # plt.show()
    # print('test')
    # print('nothing')

    # plt.figure()
    # plt.semilogy(f_fz_raw_run_avg, Pxx_fz_raw_run_avg, label='raw')
    # plt.semilogy(f_fz_cleaned_run_avg, Pxx_fz_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_fz_fmrib_run_avg, Pxx_fz_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.ylim(0.01, 1e2)
    # plt.legend()
    #
    # plt.figure()
    # plt.semilogy(f_pz_raw_run_avg, Pxx_pz_raw_run_avg, label='raw')
    # plt.semilogy(f_pz_cleaned_run_avg, Pxx_pz_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_pz_fmrib_run_avg, Pxx_pz_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.ylim(0.01, 1e2)
    # plt.legend()
    # plt.show()
    # print('test')

# Variables corresponding to all channels
f_eeg_raw_sub = np.array(f_eeg_raw_sub)
f_eeg_cleaned_sub = np.array(f_eeg_cleaned_sub)
f_eeg_fmrib_sub = np.array(f_eeg_fmrib_sub)

Pxx_eeg_raw_sub = np.array(Pxx_eeg_raw_sub)
Pxx_eeg_cleaned_sub = np.array(Pxx_eeg_cleaned_sub)
Pxx_eeg_fmrib_sub = np.array(Pxx_eeg_fmrib_sub)

f_eeg_raw_sub_avg = np.mean(f_eeg_raw_sub, axis=0)
f_eeg_cleaned_sub_avg = np.mean(f_eeg_cleaned_sub, axis=0)
f_eeg_fmrib_sub_avg = np.mean(f_eeg_fmrib_sub, axis=0)

Pxx_eeg_raw_sub_avg = np.mean(Pxx_eeg_raw_sub, axis=0)
Pxx_eeg_cleaned_sub_avg = np.mean(Pxx_eeg_cleaned_sub, axis=0)
Pxx_eeg_fmrib_sub_avg = np.mean(Pxx_eeg_fmrib_sub, axis=0)

# Variables corresponding to channel Fz
f_fz_raw_sub = np.array(f_fz_raw_sub)
f_fz_cleaned_sub = np.array(f_fz_cleaned_sub)
f_fz_fmrib_sub = np.array(f_fz_fmrib_sub)

Pxx_fz_raw_sub = np.array(Pxx_fz_raw_sub)
Pxx_fz_cleaned_sub = np.array(Pxx_fz_cleaned_sub)
Pxx_fz_fmrib_sub = np.array(Pxx_fz_fmrib_sub)

f_fz_raw_sub_avg = np.mean(f_fz_raw_sub, axis=0)
f_fz_cleaned_sub_avg = np.mean(f_fz_cleaned_sub, axis=0)
f_fz_fmrib_sub_avg = np.mean(f_fz_fmrib_sub, axis=0)

Pxx_fz_raw_sub_avg = np.mean(Pxx_fz_raw_sub, axis=0)
Pxx_fz_cleaned_sub_avg = np.mean(Pxx_fz_cleaned_sub, axis=0)
Pxx_fz_fmrib_sub_avg = np.mean(Pxx_fz_fmrib_sub, axis=0)

# Variables corresponding to channel Pz
f_pz_raw_sub = np.array(f_pz_raw_sub)
f_pz_cleaned_sub = np.array(f_pz_cleaned_sub)
f_pz_fmrib_sub = np.array(f_pz_fmrib_sub)

Pxx_pz_raw_sub = np.array(Pxx_pz_raw_sub)
Pxx_pz_cleaned_sub = np.array(Pxx_pz_cleaned_sub)
Pxx_pz_fmrib_sub = np.array(Pxx_pz_fmrib_sub)

f_pz_raw_sub_avg = np.mean(f_pz_raw_sub, axis=0)
f_pz_cleaned_sub_avg = np.mean(f_pz_cleaned_sub, axis=0)
f_pz_fmrib_sub_avg = np.mean(f_pz_fmrib_sub, axis=0)

Pxx_pz_raw_sub_avg = np.mean(Pxx_pz_raw_sub, axis=0)
Pxx_pz_cleaned_sub_avg = np.mean(Pxx_pz_cleaned_sub, axis=0)
Pxx_pz_fmrib_sub_avg = np.mean(Pxx_pz_fmrib_sub, axis=0)


# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='#5BC23E')
    plt.setp(bp['caps'][0], color='#5BC23E')
    plt.setp(bp['caps'][1], color='#5BC23E')
    plt.setp(bp['whiskers'][0], color='#5BC23E')
    plt.setp(bp['whiskers'][1], color='#5BC23E')
    plt.setp(bp['fliers'][0], color='#5BC23E')
    # plt.setp(bp['fliers'][1], color='#5BC23E')
    plt.setp(bp['medians'][0], color='#5BC23E')

    plt.setp(bp['boxes'][1], color='#447846')
    plt.setp(bp['caps'][2], color='#447846')
    plt.setp(bp['caps'][3], color='#447846')
    plt.setp(bp['whiskers'][2], color='#447846')
    plt.setp(bp['whiskers'][3], color='#447846')
    plt.setp(bp['fliers'][1], color='#447846')
    # plt.setp(bp['fliers'][3], color='#447846')
    plt.setp(bp['medians'][1], color='#447846')

# Generating Figure 5a
fig5a = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_fz_raw_sub.shape[1])
delta_num = freq_num[np.where(np.logical_and(f_fz_raw_sub_avg >= 0.5, f_fz_raw_sub_avg <= 4))]
fz_delta_fmrib = np.sum(Pxx_fz_fmrib_sub[:, delta_num], axis=1)
fz_delta_cleaned = np.sum(Pxx_fz_cleaned_sub[:, delta_num], axis=1)
fz_delta = [fz_delta_fmrib, fz_delta_cleaned]

theta_num = freq_num[np.where(np.logical_and(f_fz_raw_sub_avg >= 4, f_fz_raw_sub_avg <= 8))]
fz_theta_fmrib = np.sum(Pxx_fz_fmrib_sub[:, theta_num], axis=1)
fz_theta_cleaned = np.sum(Pxx_fz_cleaned_sub[:, theta_num], axis=1)
fz_theta = [fz_theta_fmrib, fz_theta_cleaned]

alpha_num = freq_num[np.where(np.logical_and(f_fz_raw_sub_avg >= 8, f_fz_raw_sub_avg <= 13))]
fz_alpha_fmrib = np.sum(Pxx_fz_fmrib_sub[:, alpha_num], axis=1)
fz_alpha_cleaned = np.sum(Pxx_fz_cleaned_sub[:, alpha_num], axis=1)
fz_alpha = [fz_alpha_fmrib, fz_alpha_cleaned]

bp_delta = plt.boxplot(fz_delta, positions=[1, 2], widths=0.6)
setBoxColors(bp_delta)

bp_theta = plt.boxplot(fz_theta, positions=[4, 5], widths=0.6)
setBoxColors(bp_theta)

bp_alpha = plt.boxplot(fz_alpha, positions=[7, 8], widths=0.6)
setBoxColors(bp_alpha)

plt.xlim(0, 9)
plt.ylim(1, 1e3)
plt.ylabel('Power ($\mu V^2$/Hz)')

ax = plt.axes()
ax.set_yscale('log')
ax.set_xticklabels(['Delta (0.5-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-13Hz)'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(fz_delta_fmrib)):
    plt.plot([1, 2], [fz_delta_fmrib[i], fz_delta_cleaned[i]], color='#B1B1B0')

for i in range(len(fz_theta_fmrib)):
    plt.plot([4, 5], [fz_theta_fmrib[i], fz_theta_cleaned[i]], color='#B1B1B0')

for i in range(len(fz_alpha_fmrib)):
    plt.plot([7, 8], [fz_alpha_fmrib[i], fz_alpha_cleaned[i]], color='#B1B1B0')

hB, = plt.plot([1, 1], color='#5BC23E', ls='solid')
hR, = plt.plot([1, 1], color='#447846', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'))
hB.set_visible(False)
hR.set_visible(False)
plt.title('Fz')
plt.tight_layout()

f_figure = 'fig05a.svg'
fig5a.savefig(p_figure / f_figure, format='svg')

# Generating Figure 5b
fig5b = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_pz_raw_sub.shape[1])
delta_num = freq_num[np.where(np.logical_and(f_pz_raw_sub_avg >= 0.5, f_pz_raw_sub_avg <= 4))]
pz_delta_fmrib = np.sum(Pxx_pz_fmrib_sub[:, delta_num], axis=1)
pz_delta_cleaned = np.sum(Pxx_pz_cleaned_sub[:, delta_num], axis=1)
pz_delta = [pz_delta_fmrib, pz_delta_cleaned]

theta_num = freq_num[np.where(np.logical_and(f_pz_raw_sub_avg >= 4, f_pz_raw_sub_avg <= 8))]
pz_theta_fmrib = np.sum(Pxx_pz_fmrib_sub[:, theta_num], axis=1)
pz_theta_cleaned = np.sum(Pxx_pz_cleaned_sub[:, theta_num], axis=1)
pz_theta = [pz_theta_fmrib, pz_theta_cleaned]

alpha_num = freq_num[np.where(np.logical_and(f_pz_raw_sub_avg >= 8, f_pz_raw_sub_avg <= 13))]
pz_alpha_fmrib = np.sum(Pxx_pz_fmrib_sub[:, alpha_num], axis=1)
pz_alpha_cleaned = np.sum(Pxx_pz_cleaned_sub[:, alpha_num], axis=1)
pz_alpha = [pz_alpha_fmrib, pz_alpha_cleaned]

bp_delta = plt.boxplot(pz_delta, positions=[1, 2], widths=0.6)
setBoxColors(bp_delta)

bp_theta = plt.boxplot(pz_theta, positions=[4, 5], widths=0.6)
setBoxColors(bp_theta)

bp_alpha = plt.boxplot(pz_alpha, positions=[7, 8], widths=0.6)
setBoxColors(bp_alpha)

plt.xlim(0, 9)
plt.ylim(1, 1e3)
plt.ylabel('Power ($\mu V^2$/Hz)')

ax = plt.axes()
ax.set_yscale('log')
ax.set_xticklabels(['Delta (0.5-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-13Hz)'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(pz_delta_fmrib)):
    plt.plot([1, 2], [pz_delta_fmrib[i], pz_delta_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_theta_fmrib)):
    plt.plot([4, 5], [pz_theta_fmrib[i], pz_theta_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_alpha_fmrib)):
    plt.plot([7, 8], [pz_alpha_fmrib[i], pz_alpha_cleaned[i]], color='#B1B1B0')

hB, = plt.plot([1, 1], color='#5BC23E', ls='solid')
hR, = plt.plot([1, 1], color='#447846', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'))
hB.set_visible(False)
hR.set_visible(False)
plt.title('Pz')
plt.tight_layout()

f_figure = 'fig05b.svg'
fig5b.savefig(p_figure / f_figure, format='svg')

# Generating Figure 5c
fig5c = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_eeg_raw_sub.shape[1])
delta_num = freq_num[np.where(np.logical_and(f_eeg_raw_sub_avg >= 0.5, f_eeg_raw_sub_avg <= 4))]
eeg_delta_fmrib = np.sum(Pxx_eeg_fmrib_sub[:, delta_num], axis=1)
eeg_delta_cleaned = np.sum(Pxx_eeg_cleaned_sub[:, delta_num], axis=1)
eeg_delta = [eeg_delta_fmrib, eeg_delta_cleaned]

theta_num = freq_num[np.where(np.logical_and(f_eeg_raw_sub_avg >= 4, f_eeg_raw_sub_avg <= 8))]
eeg_theta_fmrib = np.sum(Pxx_eeg_fmrib_sub[:, theta_num], axis=1)
eeg_theta_cleaned = np.sum(Pxx_eeg_cleaned_sub[:, theta_num], axis=1)
eeg_theta = [eeg_theta_fmrib, eeg_theta_cleaned]

alpha_num = freq_num[np.where(np.logical_and(f_eeg_raw_sub_avg >= 8, f_eeg_raw_sub_avg <= 13))]
eeg_alpha_fmrib = np.sum(Pxx_eeg_fmrib_sub[:, alpha_num], axis=1)
eeg_alpha_cleaned = np.sum(Pxx_eeg_cleaned_sub[:, alpha_num], axis=1)
eeg_alpha = [eeg_alpha_fmrib, eeg_alpha_cleaned]

bp_delta = plt.boxplot(eeg_delta, positions=[1, 2], widths=0.6)
setBoxColors(bp_delta)

bp_theta = plt.boxplot(eeg_theta, positions=[4, 5], widths=0.6)
setBoxColors(bp_theta)

bp_alpha = plt.boxplot(eeg_alpha, positions=[7, 8], widths=0.6)
setBoxColors(bp_alpha)

plt.xlim(0, 9)
plt.ylim(1, 1e3)
plt.ylabel('Power ($\mu V^2$/Hz)')

ax = plt.axes()
ax.set_yscale('log')
ax.set_xticklabels(['Delta (0.5-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-13Hz)'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(eeg_delta_fmrib)):
    plt.plot([1, 2], [eeg_delta_fmrib[i], eeg_delta_cleaned[i]], color='#B1B1B0')

for i in range(len(eeg_theta_fmrib)):
    plt.plot([4, 5], [eeg_theta_fmrib[i], eeg_theta_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_alpha_fmrib)):
    plt.plot([7, 8], [eeg_alpha_fmrib[i], eeg_alpha_cleaned[i]], color='#B1B1B0')

hB, = plt.plot([1, 1], color='#5BC23E', ls='solid')
hR, = plt.plot([1, 1], color='#447846', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'))
hB.set_visible(False)
hR.set_visible(False)
plt.tight_layout()

f_figure = 'fig05c.svg'
fig5c.savefig(p_figure / f_figure, format='svg')

# Generating Figure 5e
fig5e = plt.figure(figsize=(6, 6))
plt.semilogy(f_fz_raw_sub_avg, Pxx_fz_raw_sub_avg, label='Corrupted EEG', color='#B1B1B0')
plt.semilogy(f_fz_fmrib_sub_avg, Pxx_fz_fmrib_sub_avg, label='OBS', color='#5BC23E')
plt.semilogy(f_fz_cleaned_sub_avg, Pxx_fz_cleaned_sub_avg, label='BCGNet', color='#447846')

ax = plt.axes()
Pxx_fz_raw_sub_avg_se2 = stats.sem(Pxx_fz_raw_sub, axis=0) * 2
Pxx_fz_raw_sub_avg_fill_low = Pxx_fz_raw_sub_avg - Pxx_fz_raw_sub_avg_se2
Pxx_fz_raw_sub_avg_fill_high = Pxx_fz_raw_sub_avg + Pxx_fz_raw_sub_avg_se2
ax.fill_between(f_fz_raw_sub_avg, Pxx_fz_raw_sub_avg_fill_low, Pxx_fz_raw_sub_avg_fill_high, facecolor='#B1B1B0',
                interpolate=True, alpha=0.5)

Pxx_fz_fmrib_sub_avg_se2 = stats.sem(Pxx_fz_fmrib_sub, axis=0) * 2
Pxx_fz_fmrib_sub_avg_fill_low = Pxx_fz_fmrib_sub_avg - Pxx_fz_fmrib_sub_avg_se2
Pxx_fz_fmrib_sub_avg_fill_high = Pxx_fz_fmrib_sub_avg + Pxx_fz_fmrib_sub_avg_se2
ax.fill_between(f_fz_fmrib_sub_avg, Pxx_fz_fmrib_sub_avg_fill_low, Pxx_fz_fmrib_sub_avg_fill_high, facecolor='#447846',
                interpolate=True, alpha=0.5)

Pxx_fz_cleaned_sub_avg_se2 = stats.sem(Pxx_fz_cleaned_sub, axis=0) * 2
Pxx_fz_cleaned_sub_avg_fill_low = Pxx_fz_cleaned_sub_avg - Pxx_fz_cleaned_sub_avg_se2
Pxx_fz_cleaned_sub_avg_fill_high = Pxx_fz_cleaned_sub_avg + Pxx_fz_cleaned_sub_avg_se2
ax.fill_between(f_fz_cleaned_sub_avg, Pxx_fz_cleaned_sub_avg_fill_low, Pxx_fz_cleaned_sub_avg_fill_high, facecolor='#5BC23E',
                interpolate=True, alpha=0.5)

plt.xlim(0, 30)
plt.ylim(0.001, 1e3)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power ($\mu V^2$/Hz)')
plt.legend()
plt.title('Fz')
plt.tight_layout()

f_figure = 'fig05e.svg'
fig5e.savefig(p_figure / f_figure, format='svg')

# Generating Figure 5f
fig5f = plt.figure(figsize=(6, 6))
plt.semilogy(f_pz_raw_sub_avg, Pxx_pz_raw_sub_avg, label='Corrupted EEG', color='#B1B1B0')
plt.semilogy(f_pz_fmrib_sub_avg, Pxx_pz_fmrib_sub_avg, label='OBS', color='#5BC23E')
plt.semilogy(f_pz_cleaned_sub_avg, Pxx_pz_cleaned_sub_avg, label='BCGNet', color='#447846')

ax = plt.axes()
Pxx_pz_raw_sub_avg_se2 = stats.sem(Pxx_pz_raw_sub, axis=0) * 2
Pxx_pz_raw_sub_avg_fill_low = Pxx_pz_raw_sub_avg - Pxx_pz_raw_sub_avg_se2
Pxx_pz_raw_sub_avg_fill_high = Pxx_pz_raw_sub_avg + Pxx_pz_raw_sub_avg_se2
ax.fill_between(f_pz_raw_sub_avg, Pxx_pz_raw_sub_avg_fill_low, Pxx_pz_raw_sub_avg_fill_high, facecolor='#B1B1B0',
                interpolate=True, alpha=0.5)

Pxx_pz_fmrib_sub_avg_se2 = stats.sem(Pxx_pz_fmrib_sub, axis=0) * 2
Pxx_pz_fmrib_sub_avg_fill_low = Pxx_pz_fmrib_sub_avg - Pxx_pz_fmrib_sub_avg_se2
Pxx_pz_fmrib_sub_avg_fill_high = Pxx_pz_fmrib_sub_avg + Pxx_pz_fmrib_sub_avg_se2
ax.fill_between(f_pz_fmrib_sub_avg, Pxx_pz_fmrib_sub_avg_fill_low, Pxx_pz_fmrib_sub_avg_fill_high, facecolor='#447846',
                interpolate=True, alpha=0.5)

Pxx_pz_cleaned_sub_avg_se2 = stats.sem(Pxx_pz_cleaned_sub, axis=0) * 2
Pxx_pz_cleaned_sub_avg_fill_low = Pxx_pz_cleaned_sub_avg - Pxx_pz_cleaned_sub_avg_se2
Pxx_pz_cleaned_sub_avg_fill_high = Pxx_pz_cleaned_sub_avg + Pxx_pz_cleaned_sub_avg_se2
ax.fill_between(f_pz_cleaned_sub_avg, Pxx_pz_cleaned_sub_avg_fill_low, Pxx_pz_cleaned_sub_avg_fill_high, facecolor='#5BC23E',
                interpolate=True, alpha=0.5)

plt.xlim(0, 30)
plt.ylim(0.001, 1e3)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power ($\mu V^2$/Hz)')
plt.legend()
plt.title('Pz')
plt.tight_layout()

f_figure = 'fig05f.svg'
fig5f.savefig(p_figure / f_figure, format='svg')

# Generating Figure 5g
fig5g = plt.figure(figsize=(6, 6))
plt.semilogy(f_eeg_raw_sub_avg, Pxx_eeg_raw_sub_avg, label='Corrupted EEG', color='#B1B1B0')
plt.semilogy(f_eeg_fmrib_sub_avg, Pxx_eeg_fmrib_sub_avg, label='OBS', color='#5BC23E')
plt.semilogy(f_eeg_cleaned_sub_avg, Pxx_eeg_cleaned_sub_avg, label='BCGNet', color='#447846')

ax = plt.axes()
Pxx_eeg_raw_sub_avg_se2 = stats.sem(Pxx_eeg_raw_sub, axis=0) * 2
Pxx_eeg_raw_sub_avg_fill_low = Pxx_eeg_raw_sub_avg - Pxx_eeg_raw_sub_avg_se2
Pxx_eeg_raw_sub_avg_fill_high = Pxx_eeg_raw_sub_avg + Pxx_eeg_raw_sub_avg_se2
ax.fill_between(f_eeg_raw_sub_avg, Pxx_eeg_raw_sub_avg_fill_low, Pxx_eeg_raw_sub_avg_fill_high, facecolor='#B1B1B0',
                interpolate=True, alpha=0.5)

Pxx_eeg_fmrib_sub_avg_se2 = stats.sem(Pxx_eeg_fmrib_sub, axis=0) * 2
Pxx_eeg_fmrib_sub_avg_fill_low = Pxx_eeg_fmrib_sub_avg - Pxx_eeg_fmrib_sub_avg_se2
Pxx_eeg_fmrib_sub_avg_fill_high = Pxx_eeg_fmrib_sub_avg + Pxx_eeg_fmrib_sub_avg_se2
ax.fill_between(f_eeg_fmrib_sub_avg, Pxx_eeg_fmrib_sub_avg_fill_low, Pxx_eeg_fmrib_sub_avg_fill_high, facecolor='#447846',
                interpolate=True, alpha=0.5)

Pxx_eeg_cleaned_sub_avg_se2 = stats.sem(Pxx_eeg_cleaned_sub, axis=0) * 2
Pxx_eeg_cleaned_sub_avg_fill_low = Pxx_eeg_cleaned_sub_avg - Pxx_eeg_cleaned_sub_avg_se2
Pxx_eeg_cleaned_sub_avg_fill_high = Pxx_eeg_cleaned_sub_avg + Pxx_eeg_cleaned_sub_avg_se2
ax.fill_between(f_eeg_cleaned_sub_avg, Pxx_eeg_cleaned_sub_avg_fill_low, Pxx_eeg_cleaned_sub_avg_fill_high, facecolor='#5BC23E',
                interpolate=True, alpha=0.5)

plt.xlim(0, 30)
plt.ylim(0.001, 1e3)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power ($\mu V^2$/Hz)')
plt.legend()
plt.tight_layout()

f_figure = 'fig05g.svg'
fig5g.savefig(p_figure / f_figure, format='svg')

print('Finished')
