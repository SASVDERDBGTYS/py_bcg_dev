from pathlib import Path
import os.path
import sys
import importlib

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path: sys.path.append(module_path)
import py_bcg
import numpy as np

importlib.reload(py_bcg)
import py_bcg.paradigm as paradigm
import pandas as pd


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns

gl = py_bcg.data_specific.git_label()
print(str(gl.label))
print(str(gl.tag))

datadir = Path('/home/mcintosh/Cloud/DataPort/')
# datadir = Path('C:/Users/jmcin/Cloud/DataPort/')
dataset = Path('170421_EEG_linbi')

arch = []
ix_fig = 6

if ix_fig == 5:
    arch.append('net_sgd_009')
elif ix_fig == 6:
    arch.append('net_sgd_012')

opt_train, opt_test = paradigm.opt_init(arch, overwrite_dat=False)
sub_list = paradigm.data_loader_jm.sublist(datadir, dataset)

print(sub_list, opt_train, opt_test)
# Generate FOOOF results (may crash at end on first run)
gen_fooof = True
sub_result = paradigm.main_multiple_subject(datadir, dataset, sub_list, arch, opt_train, opt_test, gen_fooof=gen_fooof)

opt_train, opt_test = paradigm.opt_init(arch, overwrite_dat=False, overwrite_res=False)

# Generate FOOOF results (may crash at end on first run)
sub_result = paradigm.main_multiple_subject(datadir, dataset, sub_list, arch, opt_train, opt_test, append_model=True)


#
sns.set()
plt.close('all')

s = 16
params = {
    'axes.labelsize': s,
    'axes.titlesize': s,
    'legend.fontsize': s,
    'xtick.labelsize': s,
    'ytick.labelsize': s,
    'text.usetex': False,
}
rcParams.update(params)
cmap = sns.color_palette("colorblind", 10)

arch_select = ['' + arch_ for arch_ in arch]

arch_nice = {'test_fir_gen_000': 'Alpha-band C-FIR',
             'test_fir_opt_000': 'Opt. Symmetrical C-FIR',
             'test_fir_mph_000': 'Minimum-phase C-FIR',
             'test_net_sgd_000': 'Opt. Filter',
             'test_net_sgd_006': 'Opt. NL-Filter',
             'test_net_gru_000': 'GRU',
             'test_fir_sgd_000': 'Opt. Filter',
             'test_fir_gru_000': 'GRU',
             'test_net_sgd_002': 'net_sgd_002', 'test_net_sgd_003': 'net_sgd_003',
             'test_net_sgd_004': 'net_sgd_004',
             'test_fir_alp_000': 'fir_alp_000',
             'as_fir_gen_000': 'Alpha-band C-FIR',
             'as_net_sgd_000': 'Opt. Filter',
             'as_net_gru_000': 'GRU',
             'as_net_sgd_006': 'Opt. NL-Filter',
             'test_net_sgd_009': 'GRU REP SIMPLE',
             'test_net_sgd_010': 'GRU REP NL',
             }

for ix_arch, arch_ in enumerate(arch_select):
    y = list()
    for sub in sub_list:
        vec_run = list(sub_result[sub].keys())
        # x = np.array([])
        x = list()
        for ix_run in vec_run:
            if sub_result[sub][ix_run] != {}:
                arch__ = arch_
                x_ = sub_result[sub][ix_run][arch__]['model_info']['model'].get_weights()
                x.append(x_)
        y.append(x)




def get_trace(y, ix_channel, sub_list, ix_layer=0):
    f = np.array([])

    for ix_sub in range(len(sub_list)):
        filt_run = np.array([y[ix_sub][ix_run][ix_layer][:, ix_channel] for ix_run in range(np.shape(y[ix_sub])[0])])
        filt_mea = np.mean(filt_run, axis=0)
        # filt_mea = filt_mea / np.max(filt_mea)
        filt_mea = filt_mea.reshape(1, -1)
        if ix_sub == 0:
            f = filt_mea
        else:
            f = np.concatenate((f, filt_mea), axis=0)
    return f.transpose()

from py_bcg import data_loader_jm, sp, summary, paradigm, met

meta = list()
f_alpha = np.array([])
for ix_sub in range(len(sub_list)):
    for ix_run in vec_run:
            sub = sub_list[ix_sub]
            _, meta_ = data_loader_jm.get_alpha(datadir, dataset, sub, ix_run)
            if meta_ is not None:
                # print(sub, meta_['f_alpha'])
                meta.append(meta_)
                f_alpha = np.append(f_alpha, meta_['f_alpha'])
                break

fs = 512

def plot_trace(y, ix, sub_list, fs):
    f1 = get_trace(y, ix, sub_list)
    f1_std = np.nanstd(f1, axis=1)
    f1_sem = f1_std/np.sqrt(np.shape(f1)[1])
    f1_mea = np.nanmean(f1, axis=1)
    t_cut = np.linspace(-np.shape(f1)[0]/fs, 0, np.shape(f1)[0])
    plt.fill_between(t_cut, f1_mea-f1_std, f1_mea+f1_std, alpha=0.95)

    # f0_ = f0.copy()
    # ix_peak2 = np.nanargmax(f0_, axis=0)
    # ix_trough2 = np.nanargmin(f0_, axis=0)
    # f0_[int(np.mean(ix_peak2) - 40):, :] = np.nan
    # ix_peak1 = np.nanargmax(f0_, axis=0)
    # ix_trough1 = np.nanargmin(f0_, axis=0)
    #
    # d = np.diff(np.array([ix_peak1, ix_peak2]), axis=0)
    # f_x = 1/ ((np.mean(d, axis=0)) / fs)


f_height = 6
dpi = 150
fig = plt.figure(figsize=(f_height / 1.5, f_height/2), dpi=dpi)
ax2 = fig.add_subplot(1, 1, 1)
plot_trace(y, 0, sub_list, fs)
plot_trace(y, 1, sub_list, fs)
plt.xlabel('Time (s)')
plt.ylabel('Gain')
# plt.show()
f_name = 'ieee_2019_poster_fig_{}{}.svg'.format(ix_fig, 'a')
plt.savefig(f_name, format='svg')

fig = plt.figure(figsize=(f_height / 1.5, f_height/2), dpi=dpi)
ax1 = fig.add_subplot(1, 1, 1)
plt.hist(f_alpha)
plt.xlabel('IAF (Hz)')
plt.ylabel('N. subjects')

f_name = 'ieee_2019_poster_fig_{}{}.svg'.format(ix_fig, 'b')
plt.savefig(f_name, format='svg')
