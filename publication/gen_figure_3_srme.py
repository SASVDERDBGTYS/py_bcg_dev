import mne
from py_bcg import set_env
from py_bcg import bcg_net
from matplotlib import rcParams
import numpy as np
from pathlib import Path
import pickle
from scipy import signal

params = {
   'axes.labelsize': 10,
   'legend.title_fontsize': 10,
   'legend.fontsize': 10,
   'xtick.labelsize': 10,
   'ytick.labelsize': 10,
   'text.usetex': False,
   }
rcParams.update(params)

rcParams['font.family'] = 'sans-serif'
rcParams['mathtext.default'] = 'regular'
rcParams['font.sans-serif'] = ['Verdana']

import matplotlib.pyplot as plt

vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]

str_sub = 'sub34'
run_id = 3
arch = 'gru_arch_general3'
trial_type = 'multi_run'

p_cleaned, f_cleaned = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)
p_rs, f_rs = set_env.rs_path(str_sub, run_id)

data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
data_dir_bcg = str(p_bcg.joinpath(f_bcg))
data_dir_rs = str(p_rs.joinpath(f_rs))

rs_added_raw = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)
cleaned_raw = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)
srate = cleaned_raw.info['sfreq']

rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

epoched_data_cleaned, good_ix = bcg_net.dataset_epoch(dataset=cleaned_raw, duration=3, epoch_rejection=True,
                                      threshold=5, raw_dataset=rs_added_raw_copy)

epoched_data_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False, good_ix=good_ix)

epoched_data_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)

epoched_data_cleaned = epoched_data_cleaned.get_data()


vec_epoched_data_raw_iter = []
vec_good_ix_iter = []
for i in range(len(vec_run_ix)):
    p_rs, f_rs = set_env.rs_path(str_sub, vec_run_ix[i])
    data_dir = str(p_rs.joinpath(f_rs))

    raw = mne.io.read_raw_eeglab(data_dir, preload=True, stim_channel=False)
    raw_copy = mne.io.RawArray(raw.get_data(), raw.info)
    raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    epoched_data_raw_iter, good_ix_iter = bcg_net.dataset_epoch(dataset=raw, duration=3, epoch_rejection=True,
                                                          threshold=5, raw_dataset=raw_copy)
    vec_epoched_data_raw_iter.append(epoched_data_raw_iter)
    vec_good_ix_iter.append(good_ix_iter)

epoched_data_combined = None
for i in range(len(vec_epoched_data_raw_iter)):
    if i == 0:
        epoched_data_combined = vec_epoched_data_raw_iter[i]
    else:
        epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data_raw_iter[i]])

normalizedData = epoched_data_combined.get_data()
ecg_ch = epoched_data_combined.info['ch_names'].index('ECG')

num_epochs = normalizedData.shape[0]
batch_size = normalizedData.shape[2]
evaluation = 0.85

with bcg_net.temp_seed(1997):
    s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(normalizedData, evaluation)

vec_n_events = []
for i in range(len(vec_epoched_data_raw_iter)):
    n_events = len(vec_epoched_data_raw_iter[i].get_data())
    vec_n_events.append(n_events)

vec_n_events_cum = np.cumsum(vec_n_events)

ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 2] <= vec_ix_slice_test, vec_ix_slice_test < vec_n_events_cum[run_id - 1]))]
ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 2])

epoch_num = 64
ecg_ch = 31
pz_ch = 18
fz_ch = 16
unit = 1e6

# Generating Figure 4a
fig4a = plt.figure(figsize=(8, 2))
time_stamps = np.linspace(0, 3, epoched_data_raw.shape[2])
plt.plot(time_stamps, epoched_data_raw[epoch_num, ecg_ch, :] * unit, color='#A2142F')
plt.xlabel('Time (s)', family='Verdana')
plt.ylabel('Amplitude ($\mu V$)', family='Verdana')
plt.tight_layout()
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-750, 750)

p_figure = set_env.figure_path(4)
p_figure.mkdir(parents=True, exist_ok=True)
f_figure = 'fig04a.svg'

fig4a.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4b
fig4b = plt.figure(figsize=(8, 4))
time_stamps = np.linspace(0, 3, epoched_data_raw.shape[2])
plt.subplot(211)
plt.plot(time_stamps, epoched_data_raw[epoch_num, fz_ch, :] * unit, color='#B1B1B0', label='Corrupted EEG')
plt.plot(time_stamps, (epoched_data_raw[epoch_num, fz_ch, :] - epoched_data_cleaned[epoch_num, fz_ch, :]) * unit, color='#4945A4',
         label='BCG Predicted by BCGNet')
plt.legend()
plt.xlabel('Time (s)', family='Verdana')
plt.ylabel('Amplitude ($\mu V$)', family='Verdana')
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-60, 60)
plt.title('Fz')

plt.subplot(212)
plt.plot(time_stamps, epoched_data_fmrib[epoch_num, fz_ch, :] * unit, color='#5BC23E', label='OBS')
plt.plot(time_stamps, epoched_data_cleaned[epoch_num, fz_ch, :] * unit, color='#447846', label='BCGNet')
plt.legend()
plt.xlabel('Time (s)')
plt.ylabel('Amplitude ($\mu V$)', family='Verdana')
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-60, 60)
plt.tight_layout()


f_figure = 'fig04b.svg'
fig4b.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4c
fig4c = plt.figure(figsize=(8, 4))
time_stamps = np.linspace(0, 3, epoched_data_raw.shape[2])
plt.subplot(211)
plt.plot(time_stamps, epoched_data_raw[epoch_num, pz_ch, :] * unit, color='#B1B1B0', label='Corrupted EEG')
plt.plot(time_stamps, (epoched_data_raw[epoch_num, pz_ch, :] - epoched_data_cleaned[epoch_num, pz_ch, :]) * unit, color='#4945A4',
         label='BCG Predicted by BCGNet')
plt.legend()
plt.xlabel('Time (s)')
plt.ylabel('Amplitude ($\mu V$)')
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-60, 60)
plt.title('Pz')

plt.subplot(212)
plt.plot(time_stamps, epoched_data_fmrib[epoch_num, pz_ch, :] * unit, color='#5BC23E', label='OBS')
plt.plot(time_stamps, epoched_data_cleaned[epoch_num, pz_ch, :] * unit, color='#447846', label='BCGNet')
plt.legend()
plt.xlabel('Time (s)')
plt.ylabel('Amplitude ($\mu V$)')
plt.tight_layout()
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-60, 60)

f_figure = 'fig04c.svg'
fig4c.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4d
p_rmse = Path('/home/jyao/Local/working_eegbcg/proc/proc_net/sub34/gru_arch/r012345/net_gru_arch_general3_20190722194142/TEp114/')
f_rmse = 'history_rmse'

with open(p_rmse / f_rmse, 'rb') as handle:
    m = pickle.load(handle)

fig4d = plt.figure(figsize=(4, 2))
loss = m['loss']
val_loss = m['val_loss']
vec_epochs = range(1, len(loss) + 1)

plt.plot(vec_epochs, loss, color='#447846', label='Training loss')
plt.plot(vec_epochs, val_loss, color='y', label='Validation loss')
plt.legend()
plt.xlabel('Training Epochs')
plt.ylabel('Loss (A.U.)')
plt.xlim(vec_epochs[0], vec_epochs[-1])
plt.ylim(0, 2)
plt.tight_layout()

f_figure = 'fig04d.svg'
fig4d.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4e
fz_eeg_raw_test = epoched_data_raw[ix_slice_test, fz_ch, :] * unit
fz_eeg_cleaned_test = epoched_data_cleaned[ix_slice_test, fz_ch, :] * unit
fz_eeg_fmrib_test = epoched_data_fmrib[ix_slice_test, fz_ch, :] * unit

f_fz_raw = []
Pxx_fz_raw = []
for i in range(fz_eeg_raw_test.shape[0]):
    f_fz_raw_i, Pxx_fz_raw_i = signal.welch(fz_eeg_raw_test[i, :], srate, nperseg=srate * 5)
    f_fz_raw.append(f_fz_raw_i)
    Pxx_fz_raw.append(Pxx_fz_raw_i)

f_fz_cleaned = []
Pxx_fz_cleaned = []
for i in range(fz_eeg_cleaned_test.shape[0]):
    f_fz_cleaned_i, Pxx_fz_cleaned_i = signal.welch(fz_eeg_cleaned_test[i, :], srate, nperseg=srate * 5)
    f_fz_cleaned.append(f_fz_cleaned_i)
    Pxx_fz_cleaned.append(Pxx_fz_cleaned_i)

f_fz_fmrib = []
Pxx_fz_fmrib = []
for i in range(fz_eeg_cleaned_test.shape[0]):
    f_fz_fmrib_i, Pxx_fz_fmrib_i = signal.welch(fz_eeg_fmrib_test[i, :], srate, nperseg=srate * 5)
    f_fz_fmrib.append(f_fz_fmrib_i)
    Pxx_fz_fmrib.append(Pxx_fz_fmrib_i)

Pxx_fz_raw = np.array(Pxx_fz_raw)
Pxx_fz_cleaned = np.array(Pxx_fz_cleaned)
Pxx_fz_fmrib = np.array(Pxx_fz_fmrib)

Pxx_fz_raw_avg = np.mean(Pxx_fz_raw, axis=0)
Pxx_fz_cleaned_avg = np.mean(Pxx_fz_cleaned, axis=0)
Pxx_fz_fmrib_avg = np.mean(Pxx_fz_fmrib, axis=0)

fig4e = plt.figure(figsize=(4, 4))
plt.title('Fz')
plt.semilogy(f_fz_raw[0], Pxx_fz_raw_avg, color='#B1B1B0', label='Corrupted EEG')
plt.semilogy(f_fz_raw[0], Pxx_fz_fmrib_avg, color='#5BC23E', label='OBS')
plt.semilogy(f_fz_raw[0], Pxx_fz_cleaned_avg, color='#447846', label='BCGNet')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power ($\mu V^2$/Hz)')
plt.xlim(0, 30)
plt.ylim(0.01, 1e2)
plt.legend()
plt.tight_layout()

f_figure = 'fig04e.svg'
fig4e.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4f
pz_eeg_raw_test = epoched_data_raw[ix_slice_test, pz_ch, :] * unit
pz_eeg_cleaned_test = epoched_data_cleaned[ix_slice_test, pz_ch, :] * unit
pz_eeg_fmrib_test = epoched_data_fmrib[ix_slice_test, pz_ch, :] * unit

f_pz_raw = []
Pxx_pz_raw = []
for i in range(fz_eeg_raw_test.shape[0]):
    f_pz_raw_i, Pxx_pz_raw_i = signal.welch(pz_eeg_raw_test[i, :], srate, nperseg=srate * 5)
    f_pz_raw.append(f_pz_raw_i)
    Pxx_pz_raw.append(Pxx_pz_raw_i)

f_pz_cleaned = []
Pxx_pz_cleaned = []
for i in range(fz_eeg_cleaned_test.shape[0]):
    f_pz_cleaned_i, Pxx_pz_cleaned_i = signal.welch(pz_eeg_cleaned_test[i, :], srate, nperseg=srate * 5)
    f_pz_cleaned.append(f_pz_cleaned_i)
    Pxx_pz_cleaned.append(Pxx_pz_cleaned_i)

f_pz_fmrib = []
Pxx_pz_fmrib = []
for i in range(fz_eeg_cleaned_test.shape[0]):
    f_pz_fmrib_i, Pxx_pz_fmrib_i = signal.welch(pz_eeg_fmrib_test[i, :], srate, nperseg=srate * 5)
    f_pz_fmrib.append(f_pz_fmrib_i)
    Pxx_pz_fmrib.append(Pxx_pz_fmrib_i)

Pxx_pz_raw = np.array(Pxx_pz_raw)
Pxx_pz_cleaned = np.array(Pxx_pz_cleaned)
Pxx_pz_fmrib = np.array(Pxx_pz_fmrib)

Pxx_pz_raw_avg = np.mean(Pxx_pz_raw, axis=0)
Pxx_pz_cleaned_avg = np.mean(Pxx_pz_cleaned, axis=0)
Pxx_pz_fmrib_avg = np.mean(Pxx_pz_fmrib, axis=0)

fig4f = plt.figure(figsize=(4, 4))
plt.title('Pz')
plt.semilogy(f_pz_raw[0], Pxx_pz_raw_avg, color='#B1B1B0', label='Corrupted EEG')
plt.semilogy(f_pz_raw[0], Pxx_pz_fmrib_avg, color='#5BC23E', label='OBS')
plt.semilogy(f_pz_raw[0], Pxx_pz_cleaned_avg, color='#447846', label='BCGNet')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power ($\mu V^2$/Hz)')
plt.xlim(0, 30)
plt.ylim(0.01, 1e2)
plt.legend()
plt.tight_layout()

f_figure = 'fig04f.svg'
fig4f.savefig(p_figure / f_figure, format='svg')

print('Finished')