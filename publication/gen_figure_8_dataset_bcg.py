import mne
from pathlib import Path
import numpy as np
import pickle
from py_bcg import set_env
from py_bcg import bcg_net
from scipy.io import savemat
import glob
import re

vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]
# arch = 'gru_arch_general4'
arch = 'gru_arch_bcginput01'

trial_type = 'multi_sub'
p_arch = set_env.arch_path(None, None)
str_sub_joined = ''.join(re.findall(r'\d+', x)[0] + '_' for x in vec_str_sub)
str_sub_joined = str_sub_joined[:-1]
str_sub_local = 'sub{}'.format(str_sub_joined)

# vec_f_arch = ['net_gru_arch_general4_20190813182604',
#               'net_gru_arch_general4_20190814014942',
#               'net_gru_arch_general4_20190814083905',
#               'net_gru_arch_general4_20190814135247',
#               'net_gru_arch_general4_20190814233820',
#               'net_gru_arch_general4_20190815055912',
#               'net_gru_arch_general4_20190815132623',
#               'net_gru_arch_general4_20190815194624',
#               'net_gru_arch_general4_20190816032045',
#               'net_gru_arch_general4_20190816105154',
#               'net_gru_arch_general4_20190816194105']

vec_f_arch = ['net_gru_arch_bcginput01_20190903155646',
              'net_gru_arch_bcginput01_20190904145908',
              'net_gru_arch_bcginput01_20190904214713',
              'net_gru_arch_bcginput01_20190905025959',
              'net_gru_arch_bcginput01_20190905080751',
              'net_gru_arch_bcginput01_20190905125325',
              'net_gru_arch_bcginput01_20190905182744',
              'net_gru_arch_bcginput01_20190905235015',
              'net_gru_arch_bcginput01_20190906041829',
              'net_gru_arch_bcginput01_20190906090814',
              'net_gru_arch_bcginput01_20190906144657']

vec_f_arch_across_run = ['net_gru_arch_general4_20190723000226',
                         'net_gru_arch_general4_20190723003254',
                         'net_gru_arch_general4_20190723012215',
                         'net_gru_arch_general4_20190723015724',
                         'net_gru_arch_general4_20190723024456',
                         'net_gru_arch_general4_20190723031615',
                         'net_gru_arch_general4_20190723041906',
                         'net_gru_arch_general4_20190723054238',
                         'net_gru_arch_general4_20190723064225',
                         'net_gru_arch_general4_20190723071546',
                         'net_gru_arch_general4_20190723080149']

evaluation = 0.85

for str_sub in vec_str_sub:
    vec_epoch_raw = []
    vec_epoch_pretrained = []
    vec_epoch_trained = []
    vec_epoch_fmrib = []

    for run_id in vec_run_ix:
        p_rs, f_rs = set_env.rs_path(str_sub, run_id)
        p_pretrained, f_pretrained = set_env.as_dataset_pretrained_path(str_sub, run_id, arch, trial_type)
        p_trained, f_trained = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
        p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)

        data_dir_raw = str(p_rs.joinpath(f_rs))
        data_dir_pretrained = str(p_pretrained.joinpath(f_pretrained))
        data_dir_trained = str(p_trained.joinpath(f_trained))
        data_dir_fmrib = str(p_bcg.joinpath(f_bcg))

        rs_added_raw = mne.io.read_raw_eeglab(data_dir_raw, preload=True, stim_channel=False)
        pretrained_raw = mne.io.read_raw_eeglab(data_dir_pretrained, preload=True, stim_channel=False)
        trained_raw = mne.io.read_raw_eeglab(data_dir_trained, preload=True, stim_channel=False)
        srate = trained_raw.info['sfreq']

        rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
        rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        fmrib_raw = mne.io.read_raw_eeglab(data_dir_fmrib, preload=True, stim_channel=False)
        fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        ch_names = fmrib_raw.ch_names

        epoched_data_trained, good_ix = bcg_net.dataset_epoch(dataset=trained_raw, duration=3, epoch_rejection=True,
                                                              threshold=5, raw_dataset=rs_added_raw_copy)

        epoched_d_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False,
                                              good_ix=good_ix)
        raw_info = rs_added_raw.info
        epoched_data_raw = mne.EpochsArray(epoched_d_raw, raw_info)

        epoched_d_pretrained = bcg_net.dataset_epoch(dataset=pretrained_raw, duration=3, epoch_rejection=False,
                                              good_ix=good_ix)
        pretrained_info = pretrained_raw.info
        epoched_data_pretrained = mne.EpochsArray(epoched_d_pretrained, pretrained_info)

        epoched_d_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False,
                                                good_ix=good_ix)
        fmrib_info = fmrib_raw.info
        epoched_data_fmrib = mne.EpochsArray(epoched_d_fmrib, fmrib_info)

        vec_epoch_raw.append(epoched_data_raw)
        vec_epoch_pretrained.append(epoched_data_pretrained)
        vec_epoch_trained.append(epoched_data_trained)
        vec_epoch_fmrib.append(epoched_data_fmrib)

    # Code for combining epochs based on subjects starts here
    epoch_combined_raw = None
    for i in range(len(vec_epoch_raw)):
        if i == 0:
            epoch_combined_raw = vec_epoch_raw[i]
        else:
            epoch_combined_raw = mne.concatenate_epochs([epoch_combined_raw, vec_epoch_raw[i]])

    epoch_data_combined_raw = epoch_combined_raw.get_data()
    num_epochs = epoch_data_combined_raw.shape[0]
    batch_size = epoch_data_combined_raw.shape[2]
    with bcg_net.temp_seed(1997):
        s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(epoch_data_combined_raw, evaluation)

    epoch_combined_pretrained = None
    for i in range(len(vec_epoch_pretrained)):
        if i == 0:
            epoch_combined_pretrained = vec_epoch_pretrained[i]
        else:
            epoch_combined_pretrained = mne.concatenate_epochs([epoch_combined_pretrained, vec_epoch_pretrained[i]])
    epoch_data_combined_pretrained = epoch_combined_pretrained.get_data()

    epoch_combined_trained = None
    for i in range(len(vec_epoch_trained)):
        if i == 0:
            epoch_combined_trained = vec_epoch_trained[i]
        else:
            epoch_combined_trained = mne.concatenate_epochs([epoch_combined_trained, vec_epoch_trained[i]])
    epoch_data_combined_trained = epoch_combined_trained.get_data()

    epoch_combined_fmrib = None
    for i in range(len(vec_epoch_fmrib)):
        if i == 0:
            epoch_combined_fmrib = vec_epoch_fmrib[i]
        else:
            epoch_combined_fmrib = mne.concatenate_epochs([epoch_combined_fmrib, vec_epoch_fmrib[i]])
    epoch_data_combined_fmrib = epoch_combined_fmrib.get_data()

    vec_n_events = []
    for i in range(len(vec_epoch_raw)):
        n_events = vec_epoch_raw[i].get_data().shape[0]
        vec_n_events.append(n_events)

    vec_n_events_cum = np.cumsum(vec_n_events)

    i = vec_str_sub.index(str_sub)
    p_history = p_arch.joinpath(str_sub_local).joinpath('gru_arch').joinpath(arch).joinpath('r012345').joinpath(str_sub).joinpath(vec_f_arch[i])
    local_history = glob.glob(str(p_history) + '/**/history_rmse', recursive=True)
    if len(local_history) > 1:
        assert Exception('More than one case of training')
    if local_history == []:
        assert Exception('No history found')
    with open(local_history[0], 'rb') as handle:
        h = pickle.load(handle)
    pretrained_epoch = len(h['loss'])
    heldout_epoch = len(h['history_held_out_sub']['loss'])

    p_history_across_run = p_arch.joinpath(vec_str_sub[i]).joinpath('gru_arch').joinpath('r012345').joinpath(vec_f_arch_across_run[i])
    local_history_across_run = glob.glob(str(p_history_across_run) + '/**/history_rmse', recursive=True)
    if len(local_history_across_run) > 1:
        raise Exception('More than one case of training')
    if len(local_history_across_run) == []:
        raise Exception('No history found')
    with open(local_history_across_run[0], 'rb') as handle:
        h_across_run = pickle.load(handle)

    across_run_epoch = len(h_across_run['loss'])

    for run_id in vec_run_ix:
        if run_id == 1:
            ix_slice_test = vec_ix_slice_test[np.where(vec_ix_slice_test < vec_n_events_cum[run_id - 1])]
            ix_slice_test = np.sort(ix_slice_test)
        else:
            ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 2] <= vec_ix_slice_test,
                                                                      vec_ix_slice_test < vec_n_events_cum[
                                                                          run_id - 1]))]
            ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 2])

        epoched_raw = vec_epoch_raw[run_id - 1]
        epoched_pretrained = vec_epoch_pretrained[run_id - 1]
        epoched_trained = vec_epoch_trained[run_id - 1]
        epoched_fmrib = vec_epoch_fmrib[run_id - 1]
        srate = epoched_raw.info['sfreq']

        epoched_data_raw = epoched_raw.get_data()
        epoched_data_pretrained = epoched_pretrained.get_data()
        epoched_data_trained = epoched_trained.get_data()
        epoched_data_fmrib = epoched_fmrib.get_data()

        dir_out = set_env.figure_path(0).parents[0].parents[0] / 'proc_test_epochs' / trial_type/ arch / str_sub
        dir_out.mkdir(parents=True, exist_ok=True)

        savemat(dir_out / f'{str_sub}_r0{run_id}_test_epochs.mat',
            mdict={'met_obs': epoched_data_fmrib,
                   'met_net_pretrained': epoched_data_pretrained,
                   'met_net_trained': epoched_data_trained,
                   'met_gar': epoched_data_raw,
                   'ix_slice_test_py': ix_slice_test,
                   'pretrained_epoch': pretrained_epoch,
                   'heldout_epoch': heldout_epoch,
                   'across_run_epoch': across_run_epoch,
                   'srate': srate,
                   'ch_names': ch_names})