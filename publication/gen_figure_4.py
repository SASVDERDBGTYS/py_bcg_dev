import mne
from py_bcg import set_env
from py_bcg import bcg_net
from matplotlib import rcParams
import numpy as np
from pathlib import Path
import pickle
from scipy import signal
from scipy import stats
from py_bcg import set_env
import glob

p_arch = set_env.arch_path(None, None)

params = {
   'axes.labelsize': 10,
   'legend.title_fontsize': 10,
   'legend.fontsize': 10,
   'xtick.labelsize': 10,
   'ytick.labelsize': 10,
   'text.usetex': False,
   }
rcParams.update(params)

rcParams['font.family'] = 'sans-serif'
rcParams['mathtext.default'] = 'regular'
rcParams['font.sans-serif'] = ['Verdana']
rcParams["legend.loc"] = 'upper right'

import matplotlib.pyplot as plt


vec_str_sub = ['sub11', 'sub12', 'sub14',
               'sub15', 'sub17', 'sub18',
               'sub19', 'sub20', 'sub21',
               'sub22', 'sub23', 'sub24',
               'sub25', 'sub29', 'sub30',
               'sub32', 'sub33', 'sub34',
               'sub35']

# vec_str_sub = ['sub11', 'sub12', 'sub14', 'sub20', 'sub21', 'sub22', 'sub29', 'sub30', 'sub32', 'sub33', 'sub34']
vec_run_ix = [1, 2, 3, 4, 5]
vec_f_arch = ['net_gru_arch_general4_20191003204148',
              'net_gru_arch_general4_20191003213700',
              'net_gru_arch_general4_20191003223056',
              'net_gru_arch_general4_20191004142120',
              'net_gru_arch_general4_20191004143315',
              'net_gru_arch_general4_20191003230531',
              'net_gru_arch_general4_20191004145308',
              'net_gru_arch_general4_20191004152425',
              'net_gru_arch_general4_20191004160452',
              'net_gru_arch_general4_20191004163749',
              'net_gru_arch_general4_20191004171132',
              'net_gru_arch_general4_20191003233715',
              'net_gru_arch_general4_20191004001710',
              'net_gru_arch_general4_20191004172156',
              'net_gru_arch_general4_20191005141220',
              'net_gru_arch_general4_20191005150059',
              'net_gru_arch_general4_20191005153113',
              'net_gru_arch_general4_20191005161118',
              'net_gru_arch_general4_20191004004526']

arch = 'gru_arch_general4'
trial_type = 'multi_run'
do_print = False

vec_raw_sub = []
vec_cleaned_raw_sub = []
vec_fmrib_raw_sub = []
ecg_ch = 31
pz_ch = 18
fz_ch = 16
unit = 1e6
evaluation = 0.85
p_figure = set_env.figure_path(4)
p_figure.mkdir(parents=True, exist_ok=True)

# Variables corresponding to all EEG channels
f_eeg_diff_cleaned_sub = []
f_eeg_diff_fmrib_sub = []

Pxx_eeg_diff_cleaned_sub = []
Pxx_eeg_diff_fmrib_sub = []

# Variables corresponding to channel Fz
f_fz_diff_cleaned_sub = []
f_fz_diff_fmrib_sub = []

Pxx_fz_diff_cleaned_sub = []
Pxx_fz_diff_fmrib_sub = []

# Variables corresponding to channel Pz
f_pz_diff_cleaned_sub = []
f_pz_diff_fmrib_sub = []

Pxx_pz_diff_cleaned_sub = []
Pxx_pz_diff_fmrib_sub = []

for str_sub in vec_str_sub:
    vec_epoch_raw = []
    vec_epoch_cleaned = []
    vec_epoch_fmrib = []

    run_id_actual = []

    for run_id in vec_run_ix:
        p_rs, f_rs = set_env.rs_path(str_sub, run_id)
        p_cleaned, f_cleaned = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
        p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)

        # Exception handling for subjects with less than 5 runs
        data_dir_rs = p_rs.joinpath(f_rs)
        data_dir_bcg = p_bcg.joinpath(f_bcg)
        data_dir_cleaned = p_cleaned.joinpath(f_cleaned)

        if not data_dir_rs.exists() or not data_dir_bcg.exists() or not data_dir_cleaned.exists():
            continue

        if data_dir_rs.stat().st_size == 0 or data_dir_bcg.stat().st_size == 0 or data_dir_cleaned.stat().st_size == 0:
            continue

        try:
            data_dir_rs = str(p_rs.joinpath(f_rs))
            rs_added_raw_err = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)

            data_dir_bcg = str(p_bcg.joinpath(f_bcg))
            fmrib_raw_err = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)

            data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
            cleaned_raw_err = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)

        except RuntimeWarning:
            continue

        data_dir_raw = str(p_rs.joinpath(f_rs))
        data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
        data_dir_fmrib = str(p_bcg.joinpath(f_bcg))

        rs_added_raw = mne.io.read_raw_eeglab(data_dir_raw, preload=True, stim_channel=False)
        cleaned_raw = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)
        srate = cleaned_raw.info['sfreq']

        rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
        rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

        fmrib_raw = mne.io.read_raw_eeglab(data_dir_fmrib, preload=True, stim_channel=False)
        fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])
        ch_names = fmrib_raw.ch_names

        epoched_data_cleaned, good_ix = bcg_net.dataset_epoch(dataset=cleaned_raw, duration=3, epoch_rejection=True,
                                                              threshold=5, raw_dataset=rs_added_raw_copy)

        epoched_d_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False,
                                              good_ix=good_ix)
        raw_info = rs_added_raw.info
        epoched_data_raw = mne.EpochsArray(epoched_d_raw, raw_info)

        epoched_d_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False,
                                                good_ix=good_ix)
        fmrib_info = fmrib_raw.info
        epoched_data_fmrib = mne.EpochsArray(epoched_d_fmrib, fmrib_info)

        vec_epoch_raw.append(epoched_data_raw)
        vec_epoch_cleaned.append(epoched_data_cleaned)
        vec_epoch_fmrib.append(epoched_data_fmrib)

        run_id_actual.append(run_id)

    # if str_sub == 'sub14':
    #     print('Subject 14 and run 2 for testing')
    #     r2_raw = vec_epoch_raw[1]
    #     r2_cleaned = vec_epoch_cleaned[1]
    #     r2_fmrib = vec_epoch_fmrib[1]
    #
    #     epoch_num = 68
    #     ecg_ch = 31
    #     ch_num = 7
    #     unit = 1e6
    #
    #     plt.subplot(211)
    #     plt.plot(r2_raw.get_data()[epoch_num, ecg_ch, :] * unit)
    #
    #     plt.subplot(212)
    #     plt.plot(r2_raw.get_data()[epoch_num, ch_num, :] * unit, label='raw')
    #     plt.plot(r2_cleaned.get_data()[epoch_num, ch_num, :] * unit, label='BCGNet')
    #     plt.plot(r2_fmrib.get_data()[epoch_num, ch_num, :] * unit, label='OBS')
    #     plt.legend()
    #     plt.show()
    #     print('nothing')

    # Code for combining epochs based on subjects starts here
    epoch_combined_raw = None
    for i in range(len(vec_epoch_raw)):
        if i == 0:
            epoch_combined_raw = vec_epoch_raw[i]
        else:
            epoch_combined_raw = mne.concatenate_epochs([epoch_combined_raw, vec_epoch_raw[i]])

    epoch_data_combined_raw = epoch_combined_raw.get_data()
    num_epochs = epoch_data_combined_raw.shape[0]
    batch_size = epoch_data_combined_raw.shape[2]
    with bcg_net.temp_seed(1997):
        s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(epoch_data_combined_raw, evaluation)

    epoch_combined_cleaned = None
    for i in range(len(vec_epoch_cleaned)):
        if i == 0:
            epoch_combined_cleaned = vec_epoch_cleaned[i]
        else:
            epoch_combined_cleaned = mne.concatenate_epochs([epoch_combined_cleaned, vec_epoch_cleaned[i]])
    epoch_data_combined_cleaned = epoch_combined_cleaned.get_data()

    epoch_combined_fmrib = None
    for i in range(len(vec_epoch_fmrib)):
        if i == 0:
            epoch_combined_fmrib = vec_epoch_fmrib[i]
        else:
            epoch_combined_fmrib = mne.concatenate_epochs([epoch_combined_fmrib, vec_epoch_fmrib[i]])
    epoch_combined_fmrib = epoch_combined_fmrib.get_data()

    vec_n_events = []
    for i in range(len(vec_epoch_raw)):
        n_events = vec_epoch_raw[i].get_data().shape[0]
        vec_n_events.append(n_events)

    vec_n_events_cum = np.cumsum(vec_n_events)

    # Variables corresponding to all EEG channels
    f_eeg_diff_cleaned_run = []
    f_eeg_diff_fmrib_run = []

    Pxx_eeg_diff_cleaned_run = []
    Pxx_eeg_diff_fmrib_run = []

    # Variables corresponding to channel Fz
    f_fz_diff_cleaned_run = []
    f_fz_diff_fmrib_run = []

    Pxx_fz_diff_cleaned_run = []
    Pxx_fz_diff_fmrib_run = []

    # Variables corresponding to channel Pz
    f_pz_diff_cleaned_run = []
    f_pz_diff_fmrib_run = []

    Pxx_pz_diff_cleaned_run = []
    Pxx_pz_diff_fmrib_run = []

    for run_id in run_id_actual:
        if len(run_id_actual) == 5:
            if run_id == 1:
                ix_slice_test = vec_ix_slice_test[np.where(vec_ix_slice_test < vec_n_events_cum[run_id - 1])]
                ix_slice_test = np.sort(ix_slice_test)
            else:
                ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 2] <= vec_ix_slice_test,
                                                                          vec_ix_slice_test < vec_n_events_cum[
                                                                              run_id - 1]))]
                ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 2])

        else:
            relative_run_id = run_id_actual.index(run_id) + 1
            if run_id_actual.index(run_id) == 0:
                ix_slice_test = vec_ix_slice_test[np.where(vec_ix_slice_test < vec_n_events_cum[run_id - 1])]
                ix_slice_test = np.sort(ix_slice_test)
            else:
                ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[relative_run_id - 2] <= vec_ix_slice_test,
                                                                          vec_ix_slice_test < vec_n_events_cum[
                                                                              relative_run_id - 1]))]
                ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[relative_run_id - 2])

            run_id_real = run_id
            run_id = relative_run_id

        epoched_raw = vec_epoch_raw[run_id - 1]
        epoched_cleaned = vec_epoch_cleaned[run_id - 1]
        epoched_fmrib = vec_epoch_fmrib[run_id - 1]
        srate = epoched_raw.info['sfreq']

        epoched_data_raw = epoched_raw.get_data()
        epoched_data_cleaned = epoched_cleaned.get_data()
        epoched_data_fmrib = epoched_fmrib.get_data()

        # Obtaining all EEG data
        target_ch = np.delete(np.arange(0, 64, 1), ecg_ch)

        eeg_raw_all = epoched_data_raw[:, target_ch, :] * unit
        eeg_raw_test = eeg_raw_all[ix_slice_test, :, :]

        eeg_cleaned_all = epoched_data_cleaned[:, target_ch, :] * unit
        eeg_cleaned_test = eeg_cleaned_all[ix_slice_test, :, :]

        eeg_fmrib_all = epoched_data_fmrib[:, target_ch, :] * unit
        eeg_fmrib_test = eeg_fmrib_all[ix_slice_test, :, :]

        # Obtaining data for channel Fz
        fz_raw_test = epoched_data_raw[ix_slice_test, fz_ch, :] * unit
        fz_cleaned_test = epoched_data_cleaned[ix_slice_test, fz_ch, :] * unit
        fz_fmrib_test = epoched_data_fmrib[ix_slice_test, fz_ch, :] * unit

        # Obtaining data for channel Pz
        pz_raw_test = epoched_data_raw[ix_slice_test, pz_ch, :] * unit
        pz_cleaned_test = epoched_data_cleaned[ix_slice_test, pz_ch, :] * unit
        pz_fmrib_test = epoched_data_fmrib[ix_slice_test, pz_ch, :] * unit

        from scipy.io import savemat as save

        if len(run_id_actual) == 5:
            dir_out = set_env.figure_path(0).parents[0].parents[0] / 'proc_test_epochs' / trial_type / arch / str_sub
            dir_out.mkdir(parents=True, exist_ok=True)
            save(dir_out / f'{str_sub}_r0{run_id}_test_epochs.mat',
                mdict={'met_obs': epoched_data_fmrib, 'met_net': epoched_data_cleaned,
                   'met_gar': epoched_data_raw, 'ix_slice_test_py': ix_slice_test,
                   'srate': srate, 'ch_names': ch_names})

        else:
            dir_out = set_env.figure_path(0).parents[0].parents[0] / 'proc_test_epochs' / trial_type / arch / str_sub
            dir_out.mkdir(parents=True, exist_ok=True)
            save(dir_out / f'{str_sub}_r0{run_id_real}_test_epochs.mat',
                mdict={'met_obs': epoched_data_fmrib, 'met_net': epoched_data_cleaned,
                   'met_gar': epoched_data_raw, 'ix_slice_test_py': ix_slice_test,
                   'srate': srate, 'ch_names': ch_names})

        # Variables corresponding to all EEG channels
        f_eeg_diff_cleaned = []
        f_eeg_diff_fmrib = []

        Pxx_eeg_diff_cleaned = []
        Pxx_eeg_diff_fmrib = []

        # Variables corresponding to channel Fz
        f_fz_diff_cleaned = []
        f_fz_diff_fmrib = []

        Pxx_fz_diff_cleaned = []
        Pxx_fz_diff_fmrib = []

        # Variables corresponding to channel Pz
        f_pz_diff_cleaned = []
        f_pz_diff_fmrib = []

        Pxx_pz_diff_cleaned = []
        Pxx_pz_diff_fmrib = []

        for ix_epoch in range(eeg_raw_test.shape[0]):
            # Generating the PSD for all channels

            f_eeg_diff_fmrib_chs = []
            f_eeg_diff_cleaned_chs = []

            Pxx_eeg_diff_fmrib_chs = []
            Pxx_eeg_diff_cleaned_chs = []

            for ix_ch in range(eeg_raw_test.shape[1]):
                eeg_raw_data = eeg_raw_test[ix_epoch, ix_ch, :]
                eeg_cleaned_data = eeg_cleaned_test[ix_epoch, ix_ch, :]
                eeg_fmrib_data = eeg_fmrib_test[ix_epoch, ix_ch, :]

                f_eeg_raw_i, Pxx_eeg_raw_i = signal.welch(eeg_raw_data, srate, nperseg=eeg_raw_data.shape[0])
                f_eeg_cleaned_i, Pxx_eeg_cleaned_i = signal.welch(eeg_cleaned_data, srate, nperseg=eeg_cleaned_data.shape[0])
                f_eeg_fmrib_i, Pxx_eeg_fmrib_i = signal.welch(eeg_fmrib_data, srate, nperseg=eeg_fmrib_data.shape[0])

                f_eeg_diff_cleaned_chs.append(f_eeg_cleaned_i)
                Pxx_eeg_diff_cleaned_chs.append(Pxx_eeg_raw_i - Pxx_eeg_cleaned_i)

                f_eeg_diff_fmrib_chs.append(f_eeg_fmrib_i)
                Pxx_eeg_diff_fmrib_chs.append(Pxx_eeg_raw_i - Pxx_eeg_fmrib_i)

            f_eeg_diff_cleaned_chs = np.array(f_eeg_diff_cleaned_chs)
            f_eeg_diff_fmrib_chs = np.array(f_eeg_diff_fmrib_chs)

            Pxx_eeg_diff_cleaned_chs = np.array(Pxx_eeg_diff_cleaned_chs)
            Pxx_eeg_diff_fmrib_chs = np.array(Pxx_eeg_diff_fmrib_chs)

            f_eeg_diff_cleaned_chs_avg = np.mean(f_eeg_diff_cleaned_chs, axis=0)
            f_eeg_diff_fmrib_chs_avg = np.mean(f_eeg_diff_fmrib_chs, axis=0)

            Pxx_eeg_diff_cleaned_chs_avg = np.mean(Pxx_eeg_diff_cleaned_chs, axis=0)
            Pxx_eeg_diff_fmrib_chs_avg = np.mean(Pxx_eeg_diff_fmrib_chs, axis=0)

            f_eeg_diff_cleaned.append(f_eeg_diff_cleaned_chs_avg)
            f_eeg_diff_fmrib.append(f_eeg_diff_fmrib_chs_avg)

            Pxx_eeg_diff_cleaned.append(Pxx_eeg_diff_cleaned_chs_avg)
            Pxx_eeg_diff_fmrib.append(Pxx_eeg_diff_fmrib_chs_avg)

            # Generating the PSD for channel Fz
            fz_raw_data = fz_raw_test[ix_epoch, :]
            fz_cleaned_data = fz_cleaned_test[ix_epoch, :]
            fz_fmrib_data = fz_fmrib_test[ix_epoch, :]

            f_fz_raw_i, Pxx_fz_raw_i = signal.welch(fz_raw_data, srate, nperseg=fz_raw_data.shape[0])
            f_fz_cleaned_i, Pxx_fz_cleaned_i = signal.welch(fz_cleaned_data, srate, nperseg=fz_cleaned_data.shape[0])
            f_fz_fmrib_i, Pxx_fz_fmrib_i = signal.welch(fz_fmrib_data, srate, nperseg=fz_fmrib_data.shape[0])

            f_fz_diff_cleaned.append(f_fz_cleaned_i)
            f_fz_diff_fmrib.append(f_fz_fmrib_i)

            Pxx_fz_diff_cleaned.append(Pxx_fz_raw_i - Pxx_fz_cleaned_i)
            Pxx_fz_diff_fmrib.append(Pxx_fz_raw_i - Pxx_fz_fmrib_i)

            # Generating the PSD for channel Pz
            pz_raw_data = pz_raw_test[ix_epoch, :]
            pz_cleaned_data = pz_cleaned_test[ix_epoch, :]
            pz_fmrib_data = pz_fmrib_test[ix_epoch, :]

            f_pz_raw_i, Pxx_pz_raw_i = signal.welch(pz_raw_data, srate, nperseg=pz_raw_data.shape[0])
            f_pz_cleaned_i, Pxx_pz_cleaned_i = signal.welch(pz_cleaned_data, srate, nperseg=pz_cleaned_data.shape[0])
            f_pz_fmrib_i, Pxx_pz_fmrib_i = signal.welch(pz_fmrib_data, srate, nperseg=pz_fmrib_data.shape[0])

            f_pz_diff_cleaned.append(f_pz_cleaned_i)
            f_pz_diff_fmrib.append(f_pz_fmrib_i)

            Pxx_pz_diff_cleaned.append(Pxx_pz_raw_i - Pxx_pz_cleaned_i)
            Pxx_pz_diff_fmrib.append(Pxx_pz_raw_i - Pxx_pz_fmrib_i)

        # Variables corresponding to all EEG channels
        f_eeg_diff_cleaned = np.array(f_eeg_diff_cleaned)
        f_eeg_diff_fmrib = np.array(f_eeg_diff_fmrib)

        Pxx_eeg_diff_cleaned = np.array(Pxx_eeg_diff_cleaned)
        Pxx_eeg_diff_fmrib = np.array(Pxx_eeg_diff_fmrib)

        f_eeg_diff_cleaned_avg = np.mean(f_eeg_diff_cleaned, axis=0)
        f_eeg_diff_fmrib_avg = np.mean(f_eeg_diff_fmrib, axis=0)

        Pxx_eeg_diff_cleaned_avg = np.mean(Pxx_eeg_diff_cleaned, axis=0)
        Pxx_eeg_diff_fmrib_avg = np.mean(Pxx_eeg_diff_fmrib, axis=0)

        f_eeg_diff_cleaned_run.append(f_eeg_diff_cleaned_avg)
        f_eeg_diff_fmrib_run.append(f_eeg_diff_fmrib_avg)

        Pxx_eeg_diff_cleaned_run.append(Pxx_eeg_diff_cleaned_avg)
        Pxx_eeg_diff_fmrib_run.append(Pxx_eeg_diff_fmrib_avg)

        # Variables corresponding to Channel Fz
        f_fz_diff_cleaned = np.array(f_fz_diff_cleaned)
        f_fz_diff_fmrib = np.array(f_fz_diff_fmrib)

        Pxx_fz_diff_cleaned = np.array(Pxx_fz_diff_cleaned)
        Pxx_fz_diff_fmrib = np.array(Pxx_fz_diff_fmrib)

        f_fz_diff_cleaned_avg = np.mean(f_fz_diff_cleaned, axis=0)
        f_fz_diff_fmrib_avg = np.mean(f_fz_diff_fmrib, axis=0)

        Pxx_fz_diff_cleaned_avg = np.mean(Pxx_fz_diff_cleaned, axis=0)
        Pxx_fz_diff_fmrib_avg = np.mean(Pxx_fz_diff_fmrib, axis=0)

        f_fz_diff_cleaned_run.append(f_fz_diff_cleaned_avg)
        f_fz_diff_fmrib_run.append(f_fz_diff_fmrib_avg)

        Pxx_fz_diff_cleaned_run.append(Pxx_fz_diff_cleaned_avg)
        Pxx_fz_diff_fmrib_run.append(Pxx_fz_diff_fmrib_avg)

        # Variables corresponding to Channel Pz
        f_pz_diff_cleaned = np.array(f_pz_diff_cleaned)
        f_pz_diff_fmrib = np.array(f_pz_diff_fmrib)

        Pxx_pz_diff_cleaned = np.array(Pxx_pz_diff_cleaned)
        Pxx_pz_diff_fmrib = np.array(Pxx_pz_diff_fmrib)

        f_pz_diff_cleaned_avg = np.mean(f_pz_diff_cleaned, axis=0)
        f_pz_diff_fmrib_avg = np.mean(f_pz_diff_fmrib, axis=0)

        Pxx_pz_diff_cleaned_avg = np.mean(Pxx_pz_diff_cleaned, axis=0)
        Pxx_pz_diff_fmrib_avg = np.mean(Pxx_pz_diff_fmrib, axis=0)

        f_pz_diff_cleaned_run.append(f_pz_diff_cleaned_avg)
        f_pz_diff_fmrib_run.append(f_pz_diff_fmrib_avg)

        Pxx_pz_diff_cleaned_run.append(Pxx_pz_diff_cleaned_avg)
        Pxx_pz_diff_fmrib_run.append(Pxx_pz_diff_fmrib_avg)

        # plt.plot(f_eeg_diff_cleaned_avg, Pxx_eeg_diff_cleaned_avg, label='BCGNet')
        # plt.plot(f_eeg_diff_fmrib_avg, Pxx_eeg_diff_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # # plt.ylim(0.00001, 1e3)
        # plt.legend()
        #
        # plt.figure()
        # plt.plot(f_fz_diff_cleaned_avg, Pxx_fz_diff_cleaned_avg, label='BCGNet')
        # plt.plot(f_fz_diff_fmrib_avg, Pxx_fz_diff_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # # plt.ylim(0.00001, 1e3)
        # plt.legend()
        #
        # plt.figure()
        # plt.plot(f_pz_diff_cleaned_avg, Pxx_pz_diff_cleaned_avg, label='BCGNet')
        # plt.plot(f_pz_diff_fmrib_avg, Pxx_pz_diff_fmrib_avg, label='OBS')
        #
        # plt.xlim(0, 30)
        # # plt.ylim(0.00001, 1e3)
        # plt.legend()
        # plt.show()
        # print('test')

    # Variables corresponding to all EEG channels
    f_eeg_diff_cleaned_run = np.array(f_eeg_diff_cleaned_run)
    f_eeg_diff_fmrib_run = np.array(f_eeg_diff_fmrib_run)

    Pxx_eeg_diff_cleaned_run = np.array(Pxx_eeg_diff_cleaned_run)
    Pxx_eeg_diff_fmrib_run = np.array(Pxx_eeg_diff_fmrib_run)

    f_eeg_diff_cleaned_run_avg = np.mean(f_eeg_diff_cleaned_run, axis=0)
    f_eeg_diff_fmrib_run_avg = np.mean(f_eeg_diff_fmrib_run, axis=0)

    Pxx_eeg_diff_cleaned_run_avg = np.mean(Pxx_eeg_diff_cleaned_run, axis=0)
    Pxx_eeg_diff_fmrib_run_avg = np.mean(Pxx_eeg_diff_fmrib_run, axis=0)

    f_eeg_diff_cleaned_sub.append(f_eeg_diff_cleaned_run_avg)
    f_eeg_diff_fmrib_sub.append(f_eeg_diff_fmrib_run_avg)

    Pxx_eeg_diff_cleaned_sub.append(Pxx_eeg_diff_cleaned_run_avg)
    Pxx_eeg_diff_fmrib_sub.append(Pxx_eeg_diff_fmrib_run_avg)

    # Variables corresponding to channel Fz
    f_fz_diff_cleaned_run = np.array(f_fz_diff_cleaned_run)
    f_fz_diff_fmrib_run = np.array(f_fz_diff_fmrib_run)

    Pxx_fz_diff_cleaned_run = np.array(Pxx_fz_diff_cleaned_run)
    Pxx_fz_diff_fmrib_run = np.array(Pxx_fz_diff_fmrib_run)

    f_fz_diff_cleaned_run_avg = np.mean(f_fz_diff_cleaned_run, axis=0)
    f_fz_diff_fmrib_run_avg = np.mean(f_fz_diff_fmrib_run, axis=0)

    Pxx_fz_diff_cleaned_run_avg = np.mean(Pxx_fz_diff_cleaned_run, axis=0)
    Pxx_fz_diff_fmrib_run_avg = np.mean(Pxx_fz_diff_fmrib_run, axis=0)

    f_fz_diff_cleaned_sub.append(f_fz_diff_cleaned_run_avg)
    f_fz_diff_fmrib_sub.append(f_fz_diff_fmrib_run_avg)

    Pxx_fz_diff_cleaned_sub.append(Pxx_fz_diff_cleaned_run_avg)
    Pxx_fz_diff_fmrib_sub.append(Pxx_fz_diff_fmrib_run_avg)

    # Variablse corresponding to channel Pz
    f_pz_diff_cleaned_run = np.array(f_pz_diff_cleaned_run)
    f_pz_diff_fmrib_run = np.array(f_pz_diff_fmrib_run)

    Pxx_pz_diff_cleaned_run = np.array(Pxx_pz_diff_cleaned_run)
    Pxx_pz_diff_fmrib_run = np.array(Pxx_pz_diff_fmrib_run)

    f_pz_diff_cleaned_run_avg = np.mean(f_pz_diff_cleaned_run, axis=0)
    f_pz_diff_fmrib_run_avg = np.mean(f_pz_diff_fmrib_run, axis=0)

    Pxx_pz_diff_cleaned_run_avg = np.mean(Pxx_pz_diff_cleaned_run, axis=0)
    Pxx_pz_diff_fmrib_run_avg = np.mean(Pxx_pz_diff_fmrib_run, axis=0)

    f_pz_diff_cleaned_sub.append(f_pz_diff_cleaned_run_avg)
    f_pz_diff_fmrib_sub.append(f_pz_diff_fmrib_run_avg)

    Pxx_pz_diff_cleaned_sub.append(Pxx_pz_diff_cleaned_run_avg)
    Pxx_pz_diff_fmrib_sub.append(Pxx_pz_diff_fmrib_run_avg)

    # # Plotting the averaged PSD for every single subject
    #
    # p_figure_log = p_figure.joinpath('log_scale')
    # p_figure_log.mkdir(parents=True, exist_ok=True)
    #
    # p_figure_num = p_figure.joinpath('num_scale')
    # p_figure_num.mkdir(parents=True, exist_ok=True)
    #
    # # Figure 4e, log scale
    # fig4e_log = plt.figure(figsize=(6, 6))
    # plt.semilogy(f_eeg_diff_cleaned_run_avg, Pxx_eeg_diff_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_eeg_diff_fmrib_run_avg, Pxx_eeg_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4e_log = str_sub + '_4e_log.svg'
    # fig4e_log.savefig(p_figure_log / f_figure4e_log, format='svg')
    #
    # # Figure 4a, numeric scale
    # fig4e_num = plt.figure(figsize=(6, 6))
    # plt.plot(f_eeg_diff_cleaned_run_avg, Pxx_eeg_diff_cleaned_run_avg, label='BCGNet')
    # plt.plot(f_eeg_diff_fmrib_run_avg, Pxx_eeg_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4e_num = str_sub + '_4e_num.svg'
    # fig4e_num.savefig(p_figure_num / f_figure4e_num, format='svg')
    #
    # # Figure 4f, log scale
    # fig4f_log = plt.figure(figsize=(6, 6))
    # plt.semilogy(f_fz_diff_cleaned_run_avg, Pxx_fz_diff_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_fz_diff_fmrib_run_avg, Pxx_fz_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4f_log = str_sub + '_4f_log.svg'
    # fig4f_log.savefig(p_figure_log / f_figure4f_log, format='svg')
    #
    # # Figure 4f, numeric scale
    # fig4f_num = plt.figure(figsize=(6, 6))
    # plt.plot(f_fz_diff_cleaned_run_avg, Pxx_fz_diff_cleaned_run_avg, label='BCGNet')
    # plt.plot(f_fz_diff_fmrib_run_avg, Pxx_fz_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4f_num = str_sub + '_4f_num.svg'
    # fig4f_num.savefig(p_figure_num / f_figure4f_num, format='svg')
    #
    # # Figure 4g, log scale
    # fig4g_log = plt.figure(figsize=(6, 6))
    # plt.semilogy(f_pz_diff_cleaned_run_avg, Pxx_pz_diff_cleaned_run_avg, label='BCGNet')
    # plt.semilogy(f_pz_diff_fmrib_run_avg, Pxx_pz_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4g_log = str_sub + '_4g_log.svg'
    # fig4g_log.savefig(p_figure_log / f_figure4g_log, format='svg')
    #
    # # Figure 4g, numeric scale
    # fig4g_num = plt.figure(figsize=(6, 6))
    # plt.plot(f_pz_diff_cleaned_run_avg, Pxx_pz_diff_cleaned_run_avg, label='BCGNet')
    # plt.plot(f_pz_diff_fmrib_run_avg, Pxx_pz_diff_fmrib_run_avg, label='OBS')
    #
    # plt.xlim(0, 30)
    # plt.legend()
    # f_figure4g_num = str_sub + '_4g_num.svg'
    # fig4g_num.savefig(p_figure_num / f_figure4g_num, format='svg')


# Variables corresponding to all channels
f_eeg_diff_cleaned_sub = np.array(f_eeg_diff_cleaned_sub)
f_eeg_diff_fmrib_sub = np.array(f_eeg_diff_fmrib_sub)

Pxx_eeg_diff_cleaned_sub = np.array(Pxx_eeg_diff_cleaned_sub)
Pxx_eeg_diff_fmrib_sub = np.array(Pxx_eeg_diff_fmrib_sub)

f_eeg_diff_cleaned_sub_avg = np.mean(f_eeg_diff_cleaned_sub, axis=0)
f_eeg_diff_fmrib_sub_avg = np.mean(f_eeg_diff_fmrib_sub, axis=0)

Pxx_eeg_diff_cleaned_sub_avg = np.mean(Pxx_eeg_diff_cleaned_sub, axis=0)
Pxx_eeg_diff_fmrib_sub_avg = np.mean(Pxx_eeg_diff_fmrib_sub, axis=0)

# Variables corresponding to channel Fz
f_fz_diff_cleaned_sub = np.array(f_fz_diff_cleaned_sub)
f_fz_diff_fmrib_sub = np.array(f_fz_diff_fmrib_sub)

Pxx_fz_diff_cleaned_sub = np.array(Pxx_fz_diff_cleaned_sub)
Pxx_fz_diff_fmrib_sub = np.array(Pxx_fz_diff_fmrib_sub)

f_fz_diff_cleaned_sub_avg = np.mean(f_fz_diff_cleaned_sub, axis=0)
f_fz_diff_fmrib_sub_avg = np.mean(f_fz_diff_fmrib_sub, axis=0)

Pxx_fz_diff_cleaned_sub_avg = np.mean(Pxx_fz_diff_cleaned_sub, axis=0)
Pxx_fz_diff_fmrib_sub_avg = np.mean(Pxx_fz_diff_fmrib_sub, axis=0)

# Variables corresponding to channel Pz
f_pz_diff_cleaned_sub = np.array(f_pz_diff_cleaned_sub)
f_pz_diff_fmrib_sub = np.array(f_pz_diff_fmrib_sub)

Pxx_pz_diff_cleaned_sub = np.array(Pxx_pz_diff_cleaned_sub)
Pxx_pz_diff_fmrib_sub = np.array(Pxx_pz_diff_fmrib_sub)

f_pz_diff_cleaned_sub_avg = np.mean(f_pz_diff_cleaned_sub, axis=0)
f_pz_diff_fmrib_sub_avg = np.mean(f_pz_diff_fmrib_sub, axis=0)

Pxx_pz_diff_cleaned_sub_avg = np.mean(Pxx_pz_diff_cleaned_sub, axis=0)
Pxx_pz_diff_fmrib_sub_avg = np.mean(Pxx_pz_diff_fmrib_sub, axis=0)


# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='#447846')
    plt.setp(bp['caps'][0], color='#447846')
    plt.setp(bp['caps'][1], color='#447846')
    plt.setp(bp['whiskers'][0], color='#447846')
    plt.setp(bp['whiskers'][1], color='#447846')
    plt.setp(bp['fliers'][0], color='#447846')
    # plt.setp(bp['fliers'][1], color='#447846')
    plt.setp(bp['medians'][0], color='#447846')

    plt.setp(bp['boxes'][1], color='#5BC23E')
    plt.setp(bp['caps'][2], color='#5BC23E')
    plt.setp(bp['caps'][3], color='#5BC23E')
    plt.setp(bp['whiskers'][2], color='#5BC23E')
    plt.setp(bp['whiskers'][3], color='#5BC23E')
    plt.setp(bp['fliers'][1], color='#5BC23E')
    # plt.setp(bp['fliers'][3], color='#5BC23E')
    plt.setp(bp['medians'][1], color='#5BC23E')

# Generating Figure 4a
fig4a = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_fz_diff_cleaned_sub.shape[1])
delta_num = freq_num[np.where(np.logical_and(f_fz_diff_cleaned_sub_avg >= 0.5, f_fz_diff_cleaned_sub_avg <= 4))]
fz_delta_fmrib = np.sum(Pxx_fz_diff_fmrib_sub[:, delta_num], axis=1)
fz_delta_cleaned = np.sum(Pxx_fz_diff_cleaned_sub[:, delta_num], axis=1)
fz_delta = [fz_delta_fmrib, fz_delta_cleaned]

pz_delta_fmrib = np.sum(Pxx_pz_diff_fmrib_sub[:, delta_num], axis=1)
pz_delta_cleaned = np.sum(Pxx_pz_diff_cleaned_sub[:, delta_num], axis=1)
pz_delta = [pz_delta_fmrib, pz_delta_cleaned]

eeg_delta_fmrib = np.sum(Pxx_eeg_diff_fmrib_sub[:, delta_num], axis=1)
eeg_delta_cleaned = np.sum(Pxx_eeg_diff_cleaned_sub[:, delta_num], axis=1)
eeg_delta = [eeg_delta_fmrib, eeg_delta_cleaned]

bp_fz_delta = plt.boxplot(fz_delta, positions=[1, 2], widths=0.6)
setBoxColors(bp_fz_delta)

bp_pz_delta = plt.boxplot(pz_delta, positions=[4, 5], widths=0.6)
setBoxColors(bp_pz_delta)

bp_eeg_delta = plt.boxplot(eeg_delta, positions=[7, 8], widths=0.6)
setBoxColors(bp_eeg_delta)

plt.xlim(0, 9)
# plt.ylim(1, 1e3)
plt.ylabel('PSD ($\mu V^2$/Hz)')

ax = plt.axes()
# ax.set_yscale('log')
ax.set_xticklabels(['Fz', 'Pz', 'All EEG'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(fz_delta_fmrib)):
    plt.plot([1, 2], [fz_delta_fmrib[i], fz_delta_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_delta_fmrib)):
    plt.plot([4, 5], [pz_delta_fmrib[i], pz_delta_cleaned[i]], color='#B1B1B0')

for i in range(len(eeg_delta_fmrib)):
    plt.plot([7, 8], [eeg_delta_fmrib[i], eeg_delta_cleaned[i]], color='#B1B1B0')


hB, = plt.plot([1, 1], color='#447846', ls='solid')
hR, = plt.plot([1, 1], color='#5BC23E', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'), frameon=False)
hB.set_visible(False)
hR.set_visible(False)
plt.title('Delta (0.5-4Hz)', fontweight='bold')
plt.tight_layout()

f_figure = 'fig04a.svg'
if do_print:
    fig4a.savefig(p_figure / f_figure, format='svg')


# Generating Figure 4b
fig4b = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_fz_diff_cleaned_sub.shape[1])

theta_num = freq_num[np.where(np.logical_and(f_fz_diff_cleaned_sub_avg >= 4, f_fz_diff_cleaned_sub_avg <= 8))]
fz_theta_fmrib = np.sum(Pxx_fz_diff_fmrib_sub[:, theta_num], axis=1)
fz_theta_cleaned = np.sum(Pxx_fz_diff_cleaned_sub[:, theta_num], axis=1)
fz_theta = [fz_theta_fmrib, fz_theta_cleaned]

pz_theta_fmrib = np.sum(Pxx_pz_diff_fmrib_sub[:, theta_num], axis=1)
pz_theta_cleaned = np.sum(Pxx_pz_diff_cleaned_sub[:, theta_num], axis=1)
pz_theta = [pz_theta_fmrib, pz_theta_cleaned]

eeg_theta_fmrib = np.sum(Pxx_eeg_diff_fmrib_sub[:, theta_num], axis=1)
eeg_theta_cleaned = np.sum(Pxx_eeg_diff_cleaned_sub[:, theta_num], axis=1)
eeg_theta = [eeg_theta_fmrib, eeg_theta_cleaned]

bp_fz_theta = plt.boxplot(fz_theta, positions=[1, 2], widths=0.6)
setBoxColors(bp_fz_theta)

bp_pz_theta = plt.boxplot(pz_theta, positions=[4, 5], widths=0.6)
setBoxColors(bp_pz_theta)

bp_eeg_theta = plt.boxplot(eeg_theta, positions=[7, 8], widths=0.6)
setBoxColors(bp_eeg_theta)

plt.xlim(0, 9)
# plt.ylim(1, 1e3)
plt.ylabel('PSD ($\mu V^2$/Hz)')


ax = plt.axes()
# ax.set_yscale('log')
ax.set_xticklabels(['Fz', 'Pz', 'All EEG'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(fz_theta_fmrib)):
    plt.plot([1, 2], [fz_theta_fmrib[i], fz_theta_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_theta_fmrib)):
    plt.plot([4, 5], [pz_theta_fmrib[i], pz_theta_cleaned[i]], color='#B1B1B0')

for i in range(len(eeg_theta_fmrib)):
    plt.plot([7, 8], [eeg_theta_fmrib[i], eeg_theta_cleaned[i]], color='#B1B1B0')

hB, = plt.plot([1, 1], color='#447846', ls='solid')
hR, = plt.plot([1, 1], color='#5BC23E', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'), frameon=False)
hB.set_visible(False)
hR.set_visible(False)
plt.title('Theta (4-8Hz)', fontweight='bold')
plt.tight_layout()

f_figure = 'fig04b.svg'
if do_print:
    fig4b.savefig(p_figure / f_figure, format='svg')

# Generating Figure 4c
fig4c = plt.figure(figsize=(6, 6))

freq_num = np.arange(f_eeg_diff_cleaned_sub.shape[1])
alpha_num = freq_num[np.where(np.logical_and(f_eeg_diff_cleaned_sub_avg >= 8, f_eeg_diff_cleaned_sub_avg <= 13))]
fz_alpha_fmrib = np.sum(Pxx_fz_diff_fmrib_sub[:, alpha_num], axis=1)
fz_alpha_cleaned = np.sum(Pxx_fz_diff_cleaned_sub[:, alpha_num], axis=1)
fz_alpha = [fz_alpha_fmrib, fz_alpha_cleaned]

pz_alpha_fmrib = np.sum(Pxx_pz_diff_fmrib_sub[:, alpha_num], axis=1)
pz_alpha_cleaned = np.sum(Pxx_pz_diff_cleaned_sub[:, alpha_num], axis=1)
pz_alpha = [pz_alpha_fmrib, pz_alpha_cleaned]

eeg_alpha_fmrib = np.sum(Pxx_eeg_diff_fmrib_sub[:, alpha_num], axis=1)
eeg_alpha_cleaned = np.sum(Pxx_eeg_diff_cleaned_sub[:, alpha_num], axis=1)
eeg_alpha = [eeg_alpha_fmrib, eeg_alpha_cleaned]

bp_fz_alpha = plt.boxplot(fz_alpha, positions=[1, 2], widths=0.6)
setBoxColors(bp_fz_alpha)

bp_pz_alpha = plt.boxplot(pz_alpha, positions=[4, 5], widths=0.6)
setBoxColors(bp_pz_alpha)

bp_eeg_alpha = plt.boxplot(eeg_alpha, positions=[7, 8], widths=0.6)
setBoxColors(bp_eeg_alpha)

plt.xlim(0, 9)
# plt.ylim(1, 1e3)
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.title('Alpha (8-13Hz)', fontweight='bold')

ax = plt.axes()
# ax.set_yscale('log')
ax.set_xticklabels(['Fz', 'Pz', 'All EEG'])
ax.set_xticks([1.5, 4.5, 7.5])

for i in range(len(fz_alpha_fmrib)):
    plt.plot([1, 2], [fz_alpha_fmrib[i], fz_alpha_cleaned[i]], color='#B1B1B0')

for i in range(len(pz_alpha_fmrib)):
    plt.plot([4, 5], [pz_alpha_fmrib[i], pz_alpha_cleaned[i]], color='#B1B1B0')

for i in range(len(eeg_alpha_fmrib)):
    plt.plot([7, 8], [eeg_alpha_fmrib[i], eeg_alpha_cleaned[i]], color='#B1B1B0')

hB, = plt.plot([1, 1], color='#447846', ls='solid')
hR, = plt.plot([1, 1], color='#5BC23E', ls='solid')
plt.legend((hB, hR), ('OBS', 'BCGNet'), frameon=False)
hB.set_visible(False)
hR.set_visible(False)
plt.tight_layout()

f_figure = 'fig04c.svg'
if do_print:
    fig4c.savefig(p_figure / f_figure, format='svg')


# Generating Figure 4d

vec_loss = []
vec_val_loss = []

for i in range(len(vec_str_sub)):
    p_history = p_arch.joinpath(vec_str_sub[i]).joinpath('gru_arch').joinpath('r012345').joinpath(vec_f_arch[i])
    local_history = glob.glob(str(p_history) + '/**/history_rmse', recursive=True)
    if len(local_history) > 1:
        assert Exception('More than one case of training')
    if len(local_history) == []:
        assert Exception('No history found')
    with open(local_history[0], 'rb') as handle:
        h = pickle.load(handle)
    vec_loss.append(h['loss'])
    vec_val_loss.append(h['val_loss'])

max_length = len(max(vec_loss, key=lambda i: len(i)))
arr_loss = np.ma.empty((len(vec_str_sub), max_length))
arr_loss.mask = True

arr_val_loss = np.ma.empty((len(vec_str_sub), max_length))
arr_val_loss.mask = True

for i in range(len(vec_loss)):
    arr_loss[i, :len(vec_loss[i])] = vec_loss[i]
    arr_val_loss[i, :len(vec_val_loss[i])] = vec_val_loss[i]

vec_epochs = np.arange(1, max_length + 1)
fig4d = plt.figure(figsize=(6, 6))
plt.plot(vec_epochs, arr_loss.mean(axis=0), color='#447846', label='Training loss')

ax = plt.axes()
arr_loss_se2 = 2 * stats.mstats.sem(arr_loss, axis=0)
arr_loss_fill_low = arr_loss.mean(axis=0) - arr_loss_se2
arr_loss_fill_high = arr_loss.mean(axis=0) + arr_loss_se2
ax.fill_between(vec_epochs, arr_loss_fill_low, arr_loss_fill_high,
                facecolor='#447846', interpolate=True, alpha=0.5)

plt.plot(vec_epochs, arr_val_loss.mean(axis=0), color='y', label='Validation loss')
arr_val_loss_se2 = 2 * stats.mstats.sem(arr_val_loss, axis=0)
arr_val_loss_fill_low = arr_val_loss.mean(axis=0) - arr_val_loss_se2
arr_val_loss_fill_high = arr_val_loss.mean(axis=0) + arr_val_loss_se2
ax.fill_between(vec_epochs, arr_val_loss_fill_low, arr_val_loss_fill_high,
                facecolor='y', interpolate=True, alpha=0.5)

plt.xlabel('Training Epochs')
plt.ylabel('Loss (A.U.)')
plt.tight_layout()
plt.legend(frameon=False)
plt.xlim(0, 120)
plt.ylim(0, 2)

f_figure = 'fig04d.png'
if do_print:
    fig4d.savefig(p_figure / f_figure)


# Generating Figure 4e
# # Log version of the plot
# fig4e_log = plt.figure(figsize=(6, 6))
# plt.semilogy(f_fz_diff_fmrib_sub_avg, Pxx_fz_diff_fmrib_sub_avg, label='OBS', color='#447846')
# plt.semilogy(f_fz_diff_cleaned_sub_avg, Pxx_fz_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')
#
# plt.xlim(0, 30)
# plt.ylim(0.001, 1e3)
# plt.xlabel('Frequency (Hz)')
# plt.ylabel('PSD ($\mu V^2$/Hz)')
# plt.legend()
# plt.title('Fz')
# plt.tight_layout()
#
# f_figure_log = 'fig04e_log.svg'
# if do_print:
    # fig4e_log.savefig(p_figure / f_figure_log, format='svg')

# Numeric version of the plot
fig4e_num = plt.figure(figsize=(6, 6))
plt.plot(f_fz_diff_fmrib_sub_avg, Pxx_fz_diff_fmrib_sub_avg, label='OBS', color='#447846')

ax = plt.axes()
Pxx_fz_diff_fmrib_sub_avg_se2 = 2 * stats.sem(Pxx_fz_diff_fmrib_sub, axis=0)
Pxx_fz_diff_fmrib_sub_avg_fill_low = Pxx_fz_diff_fmrib_sub_avg - Pxx_fz_diff_fmrib_sub_avg_se2
Pxx_fz_diff_fmrib_sub_avg_fill_high = Pxx_fz_diff_fmrib_sub_avg + Pxx_fz_diff_fmrib_sub_avg_se2
ax.fill_between(f_fz_diff_fmrib_sub_avg, Pxx_fz_diff_fmrib_sub_avg_fill_low, Pxx_fz_diff_fmrib_sub_avg_fill_high,
                facecolor='#447846', interpolate=True, alpha=0.5)

plt.plot(f_fz_diff_cleaned_sub_avg, Pxx_fz_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')
Pxx_fz_diff_cleaned_sub_avg_se2 = 2 * stats.sem(Pxx_fz_diff_cleaned_sub, axis=0)
Pxx_fz_diff_cleaned_sub_avg_fill_low = Pxx_fz_diff_cleaned_sub_avg - Pxx_fz_diff_cleaned_sub_avg_se2
Pxx_fz_diff_cleaned_sub_avg_fill_high = Pxx_fz_diff_cleaned_sub_avg + Pxx_fz_diff_cleaned_sub_avg_se2
ax.fill_between(f_fz_diff_cleaned_sub_avg, Pxx_fz_diff_cleaned_sub_avg_fill_low, Pxx_fz_diff_cleaned_sub_avg_fill_high,
                facecolor='#5BC23E', interpolate=True, alpha=0.5)

plt.xlim(0, 30)
plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.legend(frameon=False)
plt.title('Fz', fontweight='bold')
plt.tight_layout()

f_figure_num = 'fig04e.svg'
if do_print:
    fig4e_num.savefig(p_figure / f_figure_num, format='svg')

fig4e_signrank_cleaned = plt.figure(figsize=(6, 6))
vec_fig4e_w = []
vec_fig4e_p = []
for freq in np.arange(f_fz_diff_fmrib_sub_avg.shape[0]):
    Pxx_fz_diff_fmrib_sub_freq = Pxx_fz_diff_fmrib_sub[:, freq]
    Pxx_fz_diff_cleaned_sub_freq = Pxx_fz_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_fz_diff_cleaned_sub_freq, Pxx_fz_diff_fmrib_sub_freq, alternative='greater')

    vec_fig4e_w.append(w)
    vec_fig4e_p.append(p)

plt.plot(f_fz_diff_fmrib_sub_avg, vec_fig4e_p, label='p-value')
plt.plot(f_fz_diff_fmrib_sub_avg, np.repeat(0.05, f_fz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_fz_diff_fmrib_sub_avg, np.repeat(0.01, f_fz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('Fz', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_cleaned = 'fig04e_signrank_cleaned.png'
if do_print:
    fig4e_signrank_cleaned.savefig(p_figure / f_figure_signrank_cleaned)

fig4e_signrank_obs = plt.figure(figsize=(6, 6))
vec_fig4e_w = []
vec_fig4e_p = []
for freq in np.arange(f_fz_diff_fmrib_sub_avg.shape[0]):
    Pxx_fz_diff_fmrib_sub_freq = Pxx_fz_diff_fmrib_sub[:, freq]
    Pxx_fz_diff_cleaned_sub_freq = Pxx_fz_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_fz_diff_fmrib_sub_freq, Pxx_fz_diff_cleaned_sub_freq, alternative='greater')

    vec_fig4e_w.append(w)
    vec_fig4e_p.append(p)

plt.plot(f_fz_diff_fmrib_sub_avg, vec_fig4e_p, label='p-value')
plt.plot(f_fz_diff_fmrib_sub_avg, np.repeat(0.05, f_fz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_fz_diff_fmrib_sub_avg, np.repeat(0.01, f_fz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('Fz', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_obs = 'fig04e_signrank_obs.png'
if do_print:
    fig4e_signrank_obs.savefig(p_figure / f_figure_signrank_obs)

# Generating Figure 4f
# # Log version of the plot
# fig4f_log = plt.figure(figsize=(6, 6))
# plt.semilogy(f_pz_diff_fmrib_sub_avg, Pxx_pz_diff_fmrib_sub_avg, label='OBS', color='#447846')
# plt.semilogy(f_pz_diff_cleaned_sub_avg, Pxx_pz_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')
#
# plt.xlim(0, 30)
# plt.ylim(0.001, 1e3)
# plt.xlabel('Frequency (Hz)')
# plt.ylabel('PSD ($\mu V^2$/Hz)')
# plt.legend()
# plt.title('Pz')
# plt.tight_layout()
#
# f_figure_log = 'fig04f_log.svg'
#if do_print:
# fig4f_log.savefig(p_figure / f_figure_log, format='svg')

# Numeric version of the plot
fig4f_num = plt.figure(figsize=(6, 6))
plt.plot(f_pz_diff_fmrib_sub_avg, Pxx_pz_diff_fmrib_sub_avg, label='OBS', color='#447846')

ax = plt.axes()
Pxx_pz_diff_fmrib_sub_avg_se2 = 2 * stats.sem(Pxx_pz_diff_fmrib_sub, axis=0)
Pxx_pz_diff_fmrib_sub_avg_fill_low = Pxx_pz_diff_fmrib_sub_avg - Pxx_pz_diff_fmrib_sub_avg_se2
Pxx_pz_diff_fmrib_sub_avg_fill_high = Pxx_pz_diff_fmrib_sub_avg + Pxx_pz_diff_fmrib_sub_avg_se2
ax.fill_between(f_pz_diff_fmrib_sub_avg, Pxx_pz_diff_fmrib_sub_avg_fill_low, Pxx_pz_diff_fmrib_sub_avg_fill_high,
                facecolor='#447846', interpolate=True, alpha=0.5)

plt.plot(f_pz_diff_cleaned_sub_avg, Pxx_pz_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')

Pxx_pz_diff_cleaned_sub_avg_se2 = 2 * stats.sem(Pxx_pz_diff_cleaned_sub, axis=0)
Pxx_pz_diff_cleaned_sub_avg_fill_low = Pxx_pz_diff_cleaned_sub_avg - Pxx_pz_diff_cleaned_sub_avg_se2
Pxx_pz_diff_cleaned_sub_avg_fill_high = Pxx_pz_diff_cleaned_sub_avg + Pxx_pz_diff_cleaned_sub_avg_se2
ax.fill_between(f_pz_diff_cleaned_sub_avg, Pxx_pz_diff_cleaned_sub_avg_fill_low, Pxx_pz_diff_cleaned_sub_avg_fill_high,
                facecolor='#5BC23E', interpolate=True, alpha=0.5)


plt.xlim(0, 30)
plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.legend(frameon=False)
plt.title('Pz', fontweight='bold')
plt.tight_layout()

f_figure_num = 'fig04f.svg'
if do_print:
    fig4f_num.savefig(p_figure / f_figure_num, format='svg')

fig4f_signrank_cleaned = plt.figure(figsize=(6, 6))
vec_fig4f_w = []
vec_fig4f_p = []
for freq in np.arange(f_pz_diff_fmrib_sub_avg.shape[0]):
    Pxx_pz_diff_fmrib_sub_freq = Pxx_pz_diff_fmrib_sub[:, freq]
    Pxx_pz_diff_cleaned_sub_freq = Pxx_pz_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_pz_diff_cleaned_sub_freq, Pxx_pz_diff_fmrib_sub_freq, alternative='greater')

    vec_fig4f_w.append(w)
    vec_fig4f_p.append(p)

plt.plot(f_pz_diff_fmrib_sub_avg, vec_fig4f_p, label='p-value')
plt.plot(f_pz_diff_fmrib_sub_avg, np.repeat(0.05, f_pz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_pz_diff_fmrib_sub_avg, np.repeat(0.01, f_pz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('Pz', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_cleaned = 'fig04f_signrank_cleaned.png'
if do_print:
    fig4f_signrank_cleaned.savefig(p_figure / f_figure_signrank_cleaned)

fig4f_signrank_obs = plt.figure(figsize=(6, 6))
vec_fig4f_w = []
vec_fig4f_p = []
for freq in np.arange(f_pz_diff_fmrib_sub_avg.shape[0]):
    Pxx_pz_diff_fmrib_sub_freq = Pxx_pz_diff_fmrib_sub[:, freq]
    Pxx_pz_diff_cleaned_sub_freq = Pxx_pz_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_pz_diff_fmrib_sub_freq, Pxx_pz_diff_cleaned_sub_freq, alternative='greater')

    vec_fig4f_w.append(w)
    vec_fig4f_p.append(p)

plt.plot(f_pz_diff_fmrib_sub_avg, vec_fig4f_p, label='p-value')
plt.plot(f_pz_diff_fmrib_sub_avg, np.repeat(0.05, f_pz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_pz_diff_fmrib_sub_avg, np.repeat(0.01, f_pz_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('Pz', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_obs = 'fig04f_signrank_obs.png'
if do_print:
    fig4f_signrank_obs.savefig(p_figure / f_figure_signrank_obs)

# Generating Figure 4g
# # Log version of the plot
# fig4g_log = plt.figure(figsize=(6, 6))
# plt.semilogy(f_eeg_diff_fmrib_sub_avg, Pxx_eeg_diff_fmrib_sub_avg, label='OBS', color='#447846')
# plt.semilogy(f_eeg_diff_cleaned_sub_avg, Pxx_eeg_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')
#
# plt.xlim(0, 30)
# plt.ylim(0.001, 1e3)
# plt.xlabel('Frequency (Hz)')
# plt.ylabel('PSD ($\mu V^2$/Hz)')
# plt.legend()
# plt.tight_layout()
#
# f_figure_log = 'fig04g_log.svg'
# if do_print:
# fig4g_log.savefig(p_figure / f_figure_log, format='svg')

# Numeric version of the plot
fig4g_num = plt.figure(figsize=(6, 6))
plt.plot(f_eeg_diff_fmrib_sub_avg, Pxx_eeg_diff_fmrib_sub_avg, label='OBS', color='#447846')

ax = plt.axes()
Pxx_eeg_diff_fmrib_sub_avg_se2 = 2 * stats.sem(Pxx_eeg_diff_fmrib_sub, axis=0)
Pxx_eeg_diff_fmrib_sub_avg_fill_low = Pxx_eeg_diff_fmrib_sub_avg - Pxx_eeg_diff_fmrib_sub_avg_se2
Pxx_eeg_diff_fmrib_sub_avg_fill_high = Pxx_eeg_diff_fmrib_sub_avg + Pxx_eeg_diff_fmrib_sub_avg_se2
ax.fill_between(f_eeg_diff_fmrib_sub_avg, Pxx_eeg_diff_fmrib_sub_avg_fill_low, Pxx_eeg_diff_fmrib_sub_avg_fill_high,
                facecolor='#447846', interpolate=True, alpha=0.5)

plt.plot(f_eeg_diff_cleaned_sub_avg, Pxx_eeg_diff_cleaned_sub_avg, label='BCGNet', color='#5BC23E')

Pxx_eeg_diff_cleaned_sub_avg_se2 = 2 * stats.sem(Pxx_eeg_diff_cleaned_sub, axis=0)
Pxx_eeg_diff_cleaned_sub_avg_fill_low = Pxx_eeg_diff_cleaned_sub_avg - Pxx_eeg_diff_cleaned_sub_avg_se2
Pxx_eeg_diff_cleaned_sub_avg_fill_high = Pxx_eeg_diff_cleaned_sub_avg + Pxx_eeg_diff_cleaned_sub_avg_se2
ax.fill_between(f_eeg_diff_cleaned_sub_avg, Pxx_eeg_diff_cleaned_sub_avg_fill_low, Pxx_eeg_diff_cleaned_sub_avg_fill_high,
                facecolor='#5BC23E', interpolate=True, alpha=0.5)


plt.xlim(0, 30)
plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.title('All EEG', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_num = 'fig04g.svg'
if do_print:
    fig4g_num.savefig(p_figure / f_figure_num, format='svg')

fig4g_signrank_cleaned = plt.figure(figsize=(6, 6))
vec_fig4g_w = []
vec_fig4g_p = []
for freq in np.arange(f_eeg_diff_fmrib_sub_avg.shape[0]):
    Pxx_eeg_diff_fmrib_sub_freq = Pxx_eeg_diff_fmrib_sub[:, freq]
    Pxx_eeg_diff_cleaned_sub_freq = Pxx_eeg_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_eeg_diff_cleaned_sub_freq, Pxx_eeg_diff_fmrib_sub_freq, alternative='greater')

    vec_fig4g_w.append(w)
    vec_fig4g_p.append(p)

plt.plot(f_eeg_diff_fmrib_sub_avg, vec_fig4g_p, label='p-value')
plt.plot(f_eeg_diff_fmrib_sub_avg, np.repeat(0.05, f_eeg_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_eeg_diff_fmrib_sub_avg, np.repeat(0.01, f_eeg_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('All EEG', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_cleaned = 'fig04g_signrank_cleaned.png'
if do_print:
    fig4g_signrank_cleaned.savefig(p_figure / f_figure_signrank_cleaned)

fig4g_signrank_obs = plt.figure(figsize=(6, 6))
vec_fig4g_w = []
vec_fig4g_p = []
for freq in np.arange(f_eeg_diff_fmrib_sub_avg.shape[0]):
    Pxx_eeg_diff_fmrib_sub_freq = Pxx_eeg_diff_fmrib_sub[:, freq]
    Pxx_eeg_diff_cleaned_sub_freq = Pxx_eeg_diff_cleaned_sub[:, freq]
    w, p = stats.wilcoxon(Pxx_eeg_diff_fmrib_sub_freq, Pxx_eeg_diff_cleaned_sub_freq, alternative='greater')

    vec_fig4g_w.append(w)
    vec_fig4g_p.append(p)

plt.plot(f_eeg_diff_fmrib_sub_avg, vec_fig4g_p, label='p-value')
plt.plot(f_eeg_diff_fmrib_sub_avg, np.repeat(0.05, f_eeg_diff_fmrib_sub_avg.shape[0]), label='alpha=0.05')
plt.plot(f_eeg_diff_fmrib_sub_avg, np.repeat(0.01, f_eeg_diff_fmrib_sub_avg.shape[0]), label='alpha=0.01')
plt.xlim(0, 30)

plt.xlabel('Frequency (Hz)')
plt.ylabel('p-value')
plt.title('All EEG', fontweight='bold')
plt.legend(frameon=False)
plt.tight_layout()

f_figure_signrank_obs = 'fig04g_signrank_obs.png'
if do_print:
    fig4g_signrank_obs.savefig(p_figure / f_figure_signrank_obs)

print('Finished')
