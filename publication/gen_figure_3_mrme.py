import mne
from py_bcg import set_env
from py_bcg import bcg_net
from matplotlib import rcParams
import numpy as np
from pathlib import Path
import pickle
from scipy import signal
from scipy import stats

"""
Parameter definition
"""
# matplotlib parameters
params = {
    'axes.labelsize': 8,
    'legend.title_fontsize': 7,
    'legend.fontsize': 7,
    'xtick.labelsize': 7,
    'ytick.labelsize': 7,
    'text.usetex': False,
   }
rcParams.update(params)
rcParams['font.family'] = 'sans-serif'
rcParams['mathtext.default'] = 'regular'
rcParams['font.sans-serif'] = ['Verdana']
rcParams["legend.loc"] = 'upper right'

import matplotlib.pyplot as plt

# Define the parameters for analysis (subject 34, run 3)
str_sub = 'sub34'
run_id = 3
arch = 'gru_arch_general4'
trial_type = 'multi_run'
test_data_only = True
vec_run_ix = [1, 2, 3, 4, 5]
p_cleaned, f_cleaned = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)
p_rs, f_rs = set_env.rs_path(str_sub, run_id)

# Hard coding the channel to be shown and the epoch to select the data from
epoch_num = 64
ecg_ch = 31
pz_ch = 18
t8_ch = 13
unit = 1e6

cm_to_inch = 1/2.54
fformat = 'svg'

"""
Loading the data
"""
data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
data_dir_bcg = str(p_bcg.joinpath(f_bcg))
data_dir_rs = str(p_rs.joinpath(f_rs))

rs_added_raw = mne.io.read_raw_eeglab(data_dir_rs, preload=True, stim_channel=False)
cleaned_raw = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)
srate = cleaned_raw.info['sfreq']

rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

# Performs epoch rejection
# Despite the confusing way it's set up, the function performs MAD-based rejection on the rs data, and returns data
# from the cleaned dataset with epochs that pass the test
epoched_data_cleaned, good_ix = bcg_net.dataset_epoch(dataset=cleaned_raw, duration=3, epoch_rejection=True,
                                      threshold=5, raw_dataset=rs_added_raw_copy)

# Split the other datasets in the same way
epoched_data_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False, good_ix=good_ix)

epoched_data_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False, good_ix=good_ix)

epoched_data_cleaned = epoched_data_cleaned.get_data()


"""
Obtain the test set epochs
"""

vec_epoched_data_raw_iter = []
vec_good_ix_iter = []
for i in range(len(vec_run_ix)):
    p_rs, f_rs = set_env.rs_path(str_sub, vec_run_ix[i])
    data_dir = str(p_rs.joinpath(f_rs))

    raw = mne.io.read_raw_eeglab(data_dir, preload=True, stim_channel=False)
    raw_copy = mne.io.RawArray(raw.get_data(), raw.info)
    raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    epoched_data_raw_iter, good_ix_iter = bcg_net.dataset_epoch(dataset=raw, duration=3, epoch_rejection=True,
                                                          threshold=5, raw_dataset=raw_copy)
    vec_epoched_data_raw_iter.append(epoched_data_raw_iter)
    vec_good_ix_iter.append(good_ix_iter)

# Combining all the epochs to obtain index corresponding to the test set
epoched_data_combined = None
for i in range(len(vec_epoched_data_raw_iter)):
    if i == 0:
        epoched_data_combined = vec_epoched_data_raw_iter[i]
    else:
        epoched_data_combined = mne.concatenate_epochs([epoched_data_combined, vec_epoched_data_raw_iter[i]])

normalizedData = epoched_data_combined.get_data()
ecg_ch = epoched_data_combined.info['ch_names'].index('ECG')

num_epochs = normalizedData.shape[0]
batch_size = normalizedData.shape[2]
evaluation = 0.85

# obtain the index of epochs in the test set
with bcg_net.temp_seed(1997):
    s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(normalizedData, evaluation)

# obtain the cumulative number of epochs in each run
vec_n_events = []
for i in range(len(vec_epoched_data_raw_iter)):
    n_events = len(vec_epoched_data_raw_iter[i].get_data())
    vec_n_events.append(n_events)

vec_n_events_cum = np.cumsum(vec_n_events)
vec_n_events_cum = np.insert(vec_n_events_cum, 0, 0)

# obtain the test set from the desired run by testing against the cumulative number of runs
ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 1] <= vec_ix_slice_test, vec_ix_slice_test < vec_n_events_cum[run_id]))]
ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 1])


"""
Generating Figure 3a: Plotting the ECG trace for a single epoch
"""

# Plotting the figure
fig3a = plt.figure(figsize=(12 * cm_to_inch, 2.5 * cm_to_inch))
time_stamps = np.linspace(0, 3, epoched_data_raw.shape[2])
ax = plt.subplot(111)
plt.plot(time_stamps, epoched_data_raw[epoch_num, ecg_ch, :] * unit, color='#A2142F')

# Setting axes configuration
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

# Adding labels
plt.xlabel('Time (s)', family='Verdana')
plt.ylabel('Amp. ($\mu V$)', family='Verdana')

# Setting axis limit
plt.xlim(time_stamps[0], time_stamps[-1])
plt.ylim(-750, 750)

p_figure = set_env.figure_path(3)
p_figure.mkdir(parents=True, exist_ok=True)
f_figure = 'fig03a.svg'

plt.subplots_adjust(top=0.95, bottom=0.35, left=0.2, right=0.95, wspace=0, hspace=0)
fig3a.savefig(p_figure / f_figure, format=fformat)

"""
Generating Figure 3b and 3c: Plotting the corresponding BCE and predicted BCG traces for channels T8 and Pz 
"""
for ix_fig in range(2):
    if ix_fig == 0:
        str_ch = 'T8'
        use_ch = t8_ch
    elif ix_fig == 1:
        str_ch = 'Pz'
        use_ch = pz_ch
    fig3b = plt.figure(figsize=(12 * cm_to_inch, 5 * cm_to_inch))
    time_stamps = np.linspace(0, 3, epoched_data_raw.shape[2])
    ax = plt.subplot(211)
    plt.plot(time_stamps, epoched_data_raw[epoch_num, use_ch, :] * unit, color='#B1B1B0', label='Corrupted EEG')
    plt.plot(time_stamps, (epoched_data_raw[epoch_num, use_ch, :] - epoched_data_cleaned[epoch_num, use_ch, :]) * unit, color='#4945A4',
             label='BCGNet Predicted BCG')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    # plt.legend(frameon=False)
    # plt.xlabel('Time (s)', family='Verdana')
    # plt.ylabel('Amp. ($\mu V$)', family='Verdana')
    plt.xlim(time_stamps[0], time_stamps[-1])
    plt.ylim(-120, 120)
    # plt.title('t8', fontweight='bold')
    plt.tick_params(axis='x', which='both', labelbottom=False)

    ax = plt.subplot(212)
    plt.plot(time_stamps, epoched_data_fmrib[epoch_num, use_ch, :] * unit, color='#447846', label='OBS')
    plt.plot(time_stamps, epoched_data_cleaned[epoch_num, use_ch, :] * unit, color='#5BC23E', label='BCGNet')
    # plt.legend(frameon=False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    plt.xlabel('Time (s)', family='Verdana')
    plt.ylabel('Amp. ($\mu V$)', family='Verdana')
    plt.xlim(time_stamps[0], time_stamps[-1])
    plt.ylim(-120, 120)

    f_figure = f'fig03_{str_ch}.svg'
    plt.subplots_adjust(top=0.95, bottom=0.2, left=0.2, right=0.95, wspace=0, hspace=0.2)
    fig3b.savefig(p_figure / f_figure, format=fformat)

"""
Loading data from all runs
"""
# Reading in all runs from the subject (subject 34) and obtaining the correct test epochs
vec_epoch_raw = []
vec_epoch_cleaned = []
vec_epoch_fmrib = []

for run_id in vec_run_ix:
    p_cleaned, f_cleaned = set_env.cleaned_dataset_path(str_sub, run_id, arch, trial_type)
    p_bcg, f_bcg = set_env.bcg_path(str_sub, run_id)
    p_rs, f_rs = set_env.rs_path(str_sub, run_id)

    data_dir_cleaned = str(p_cleaned.joinpath(f_cleaned))
    data_dir_bcg = str(p_bcg.joinpath(f_bcg))
    data_dir_raw = str(p_rs.joinpath(f_rs))

    rs_added_raw = mne.io.read_raw_eeglab(data_dir_raw, preload=True, stim_channel=False)
    cleaned_raw = mne.io.read_raw_eeglab(data_dir_cleaned, preload=True, stim_channel=False)
    srate = cleaned_raw.info['sfreq']

    rs_added_raw_copy = mne.io.RawArray(rs_added_raw.get_data(), rs_added_raw.info)
    rs_added_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    fmrib_raw = mne.io.read_raw_eeglab(data_dir_bcg, preload=True, stim_channel=False)
    fmrib_raw.drop_channels(['t0', 't1', 't2', 'r0', 'r1', 'r2'])

    epoched_data_cleaned, good_ix = bcg_net.dataset_epoch(dataset=cleaned_raw, duration=3, epoch_rejection=True,
                                                          threshold=5, raw_dataset=rs_added_raw_copy)

    epoched_d_raw = bcg_net.dataset_epoch(dataset=rs_added_raw, duration=3, epoch_rejection=False,
                                          good_ix=good_ix)
    raw_info = rs_added_raw.info
    epoched_data_raw = mne.EpochsArray(epoched_d_raw, raw_info)

    epoched_d_fmrib = bcg_net.dataset_epoch(dataset=fmrib_raw, duration=3, epoch_rejection=False,
                                            good_ix=good_ix)
    fmrib_info = fmrib_raw.info
    epoched_data_fmrib = mne.EpochsArray(epoched_d_fmrib, fmrib_info)

    vec_epoch_raw.append(epoched_data_raw)
    vec_epoch_cleaned.append(epoched_data_cleaned)
    vec_epoch_fmrib.append(epoched_data_fmrib)

epoch_combined_raw = None
for i in range(len(vec_epoch_raw)):
    if i == 0:
        epoch_combined_raw = vec_epoch_raw[i]
    else:
        epoch_combined_raw = mne.concatenate_epochs([epoch_combined_raw, vec_epoch_raw[i]])

if test_data_only:
    vec_n_events = []
    for i in range(len(vec_epoch_raw)):
        n_events = vec_epoch_raw[i].get_data().shape[0]
        vec_n_events.append(n_events)

    vec_n_events_cum = np.cumsum(vec_n_events)

    epoch_data_combined_raw = epoch_combined_raw.get_data()
    num_epochs = epoch_data_combined_raw.shape[0]
    batch_size = epoch_data_combined_raw.shape[2]
    with bcg_net.temp_seed(1997):
        s_ev, s_test, vec_ix_slice_test = bcg_net.split_evaluation_test(epoch_data_combined_raw, evaluation)

    # Variables corresponding to channel t8
    f_t8_raw_run = []
    f_t8_cleaned_run = []
    f_t8_fmrib_run = []

    Pxx_t8_raw_run = []
    Pxx_t8_cleaned_run = []
    Pxx_t8_fmrib_run = []

    # Variables corresponding to channel Pz
    f_pz_raw_run = []
    f_pz_cleaned_run = []
    f_pz_fmrib_run = []

    Pxx_pz_raw_run = []
    Pxx_pz_cleaned_run = []
    Pxx_pz_fmrib_run = []

    t8_raw_test_run = None
    t8_cleaned_test_run = None
    t8_fmrib_test_run = None

    pz_raw_test_run = None
    pz_cleaned_test_run = None
    pz_fmrib_test_run = None

    for run_id in vec_run_ix:
        if run_id == 1:
            ix_slice_test = vec_ix_slice_test[np.where(vec_ix_slice_test < vec_n_events_cum[run_id - 1])]
            ix_slice_test = np.sort(ix_slice_test)
        else:
            ix_slice_test = vec_ix_slice_test[np.where(np.logical_and(vec_n_events_cum[run_id - 2] <= vec_ix_slice_test,
                                                                      vec_ix_slice_test < vec_n_events_cum[
                                                                          run_id - 1]))]
            ix_slice_test = np.sort(ix_slice_test - vec_n_events_cum[run_id - 2])

        epoched_raw = vec_epoch_raw[run_id - 1]
        epoched_cleaned = vec_epoch_cleaned[run_id - 1]
        epoched_fmrib = vec_epoch_fmrib[run_id - 1]
        srate = epoched_raw.info['sfreq']

        epoched_data_raw = epoched_raw.get_data()
        epoched_data_cleaned = epoched_cleaned.get_data()
        epoched_data_fmrib = epoched_fmrib.get_data()

        # Obtaining data for channel t8
        t8_raw_test = epoched_data_raw[ix_slice_test, t8_ch, :] * unit
        t8_cleaned_test = epoched_data_cleaned[ix_slice_test, t8_ch, :] * unit
        t8_fmrib_test = epoched_data_fmrib[ix_slice_test, t8_ch, :] * unit

        # Obtaining data for channel Pz
        pz_raw_test = epoched_data_raw[ix_slice_test, pz_ch, :] * unit
        pz_cleaned_test = epoched_data_cleaned[ix_slice_test, pz_ch, :] * unit
        pz_fmrib_test = epoched_data_fmrib[ix_slice_test, pz_ch, :] * unit

        if run_id == 1:
            t8_raw_test_run = t8_raw_test
            t8_cleaned_test_run = t8_cleaned_test
            t8_fmrib_test_run = t8_fmrib_test

            pz_raw_test_run = pz_raw_test
            pz_cleaned_test_run = pz_cleaned_test
            pz_fmrib_test_run = pz_fmrib_test

        else:
            t8_raw_test_run = np.append(t8_raw_test_run, t8_raw_test, axis=0)
            t8_cleaned_test_run = np.append(t8_cleaned_test_run, t8_cleaned_test, axis=0)
            t8_fmrib_test_run = np.append(t8_fmrib_test_run, t8_fmrib_test, axis=0)

            pz_raw_test_run = np.append(pz_raw_test_run, pz_raw_test, axis=0)
            pz_cleaned_test_run = np.append(pz_cleaned_test_run, pz_cleaned_test, axis=0)
            pz_fmrib_test_run = np.append(pz_fmrib_test_run, pz_fmrib_test, axis=0)

    for ix_epoch in range(t8_raw_test_run.shape[0]):
        t8_raw_data = t8_raw_test_run[ix_epoch, :]
        t8_cleaned_data = t8_cleaned_test_run[ix_epoch, :]
        t8_fmrib_data = t8_fmrib_test_run[ix_epoch, :]

        pz_raw_data = pz_raw_test_run[ix_epoch, :]
        pz_cleaned_data = pz_cleaned_test_run[ix_epoch, :]
        pz_fmrib_data = pz_fmrib_test_run[ix_epoch, :]

        f_t8_raw_i, Pxx_t8_raw_i = signal.welch(t8_raw_data, srate, nperseg=srate * 5)
        f_t8_cleaned_i, Pxx_t8_cleaned_i = signal.welch(t8_cleaned_data, srate, nperseg=srate * 5)
        f_t8_fmrib_i, Pxx_t8_fmrib_i = signal.welch(t8_fmrib_data, srate, nperseg=srate * 5)

        f_t8_raw_run.append(f_t8_raw_i)
        f_t8_cleaned_run.append(f_t8_cleaned_i)
        f_t8_fmrib_run.append(f_t8_fmrib_i)

        Pxx_t8_raw_run.append(Pxx_t8_raw_i)
        Pxx_t8_cleaned_run.append(Pxx_t8_cleaned_i)
        Pxx_t8_fmrib_run.append(Pxx_t8_fmrib_i)

        f_pz_raw_i, Pxx_pz_raw_i = signal.welch(pz_raw_data, srate, nperseg=srate * 5)
        f_pz_cleaned_i, Pxx_pz_cleaned_i = signal.welch(pz_cleaned_data, srate, nperseg=srate * 5)
        f_pz_fmrib_i, Pxx_pz_fmrib_i = signal.welch(pz_fmrib_data, srate, nperseg=srate * 5)

        f_pz_raw_run.append(f_pz_raw_i)
        f_pz_cleaned_run.append(f_pz_cleaned_i)
        f_pz_fmrib_run.append(f_pz_fmrib_i)

        Pxx_pz_raw_run.append(Pxx_pz_raw_i)
        Pxx_pz_cleaned_run.append(Pxx_pz_cleaned_i)
        Pxx_pz_fmrib_run.append(Pxx_pz_fmrib_i)

    # Variables corresponding to channel t8
    f_t8_raw_run = np.array(f_t8_raw_run)
    f_t8_cleaned_run = np.array(f_t8_cleaned_run)
    f_t8_fmrib_run = np.array(f_t8_fmrib_run)

    Pxx_t8_raw_run = np.array(Pxx_t8_raw_run)
    Pxx_t8_cleaned_run = np.array(Pxx_t8_cleaned_run)
    Pxx_t8_fmrib_run = np.array(Pxx_t8_fmrib_run)

    f_t8_raw_run_avg = np.mean(f_t8_raw_run, axis=0)
    f_t8_cleaned_run_avg = np.mean(f_t8_cleaned_run, axis=0)
    f_t8_fmrib_run_avg = np.mean(f_t8_fmrib_run, axis=0)

    Pxx_t8_raw_run_avg = np.mean(Pxx_t8_raw_run, axis=0)
    Pxx_t8_cleaned_run_avg = np.mean(Pxx_t8_cleaned_run, axis=0)
    Pxx_t8_fmrib_run_avg = np.mean(Pxx_t8_fmrib_run, axis=0)

"""
Generating Figure 3e
"""
fig3e = plt.figure(figsize=(6 * cm_to_inch, 5 * cm_to_inch))
ax = plt.subplot(111)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
# plt.title('t8', fontweight='bold')
plt.semilogy(f_t8_raw_run_avg, Pxx_t8_raw_run_avg, color='#B1B1B0', label='Corrupted EEG')
plt.semilogy(f_t8_fmrib_run_avg, Pxx_t8_fmrib_run_avg, color='#447846', label='OBS')
plt.semilogy(f_t8_cleaned_run_avg, Pxx_t8_cleaned_run_avg, color='#5BC23E', label='BCGNet')

ax = plt.axes()
Pxx_t8_raw_run_avg_se2 = stats.sem(Pxx_t8_raw_run, axis=0) * 2
Pxx_t8_raw_run_avg_fill_low = Pxx_t8_raw_run_avg - Pxx_t8_raw_run_avg_se2
Pxx_t8_raw_run_avg_fill_high = Pxx_t8_raw_run_avg + Pxx_t8_raw_run_avg_se2
ax.fill_between(f_t8_raw_run_avg, Pxx_t8_raw_run_avg_fill_low, Pxx_t8_raw_run_avg_fill_high, facecolor='#B1B1B0',
                interpolate=True, alpha=0.5)

Pxx_t8_fmrib_run_avg_se2 = stats.sem(Pxx_t8_fmrib_run, axis=0) * 2
Pxx_t8_fmrib_run_avg_fill_low = Pxx_t8_fmrib_run_avg - Pxx_t8_fmrib_run_avg_se2
Pxx_t8_fmrib_run_avg_fill_high = Pxx_t8_fmrib_run_avg + Pxx_t8_fmrib_run_avg_se2
ax.fill_between(f_t8_fmrib_run_avg, Pxx_t8_fmrib_run_avg_fill_low, Pxx_t8_fmrib_run_avg_fill_high, facecolor='#447846',
                interpolate=True, alpha=0.5)

Pxx_t8_cleaned_run_avg_se2 = stats.sem(Pxx_t8_cleaned_run, axis=0) * 2
Pxx_t8_cleaned_run_avg_fill_low = Pxx_t8_cleaned_run_avg - Pxx_t8_cleaned_run_avg_se2
Pxx_t8_cleaned_run_avg_fill_high = Pxx_t8_cleaned_run_avg + Pxx_t8_cleaned_run_avg_se2
ax.fill_between(f_t8_cleaned_run_avg, Pxx_t8_cleaned_run_avg_fill_low, Pxx_t8_cleaned_run_avg_fill_high, facecolor='#5BC23E',
                interpolate=True, alpha=0.5)

plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.xlim(0, 30)
plt.ylim(1e-3, 1e3)
# plt.legend(frameon=False)
plt.tight_layout()

f_figure = 'fig03_psd_t8.svg'
plt.subplots_adjust(top=0.95, bottom=0.2, left=0.3, right=0.95, wspace=0, hspace=0.2)
fig3e.savefig(p_figure / f_figure, format=fformat)

# Variablse corresponding to channel Pz
f_pz_raw_run = np.array(f_pz_raw_run)
f_pz_cleaned_run = np.array(f_pz_cleaned_run)
f_pz_fmrib_run = np.array(f_pz_fmrib_run)

Pxx_pz_raw_run = np.array(Pxx_pz_raw_run)
Pxx_pz_cleaned_run = np.array(Pxx_pz_cleaned_run)
Pxx_pz_fmrib_run = np.array(Pxx_pz_fmrib_run)

f_pz_raw_run_avg = np.mean(f_pz_raw_run, axis=0)
f_pz_cleaned_run_avg = np.mean(f_pz_cleaned_run, axis=0)
f_pz_fmrib_run_avg = np.mean(f_pz_fmrib_run, axis=0)

Pxx_pz_raw_run_avg = np.mean(Pxx_pz_raw_run, axis=0)
Pxx_pz_cleaned_run_avg = np.mean(Pxx_pz_cleaned_run, axis=0)
Pxx_pz_fmrib_run_avg = np.mean(Pxx_pz_fmrib_run, axis=0)


"""
Generating Figure 3f
"""
fig3f = plt.figure(figsize=(6 * cm_to_inch, 5 * cm_to_inch))
ax = plt.subplot(111)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
# plt.title('Pz', fontweight='bold')
plt.semilogy(f_pz_raw_run_avg, Pxx_pz_raw_run_avg, color='#B1B1B0', label='Corrupted EEG')
plt.semilogy(f_pz_fmrib_run_avg, Pxx_pz_fmrib_run_avg, color='#447846', label='OBS')
plt.semilogy(f_pz_cleaned_run_avg, Pxx_pz_cleaned_run_avg, color='#5BC23E', label='BCGNet')

ax = plt.axes()
Pxx_pz_raw_run_avg_se2 = stats.sem(Pxx_pz_raw_run, axis=0) * 2
Pxx_pz_raw_run_avg_fill_low = Pxx_pz_raw_run_avg - Pxx_pz_raw_run_avg_se2
Pxx_pz_raw_run_avg_fill_high = Pxx_pz_raw_run_avg + Pxx_pz_raw_run_avg_se2
ax.fill_between(f_pz_raw_run_avg, Pxx_pz_raw_run_avg_fill_low, Pxx_pz_raw_run_avg_fill_high, facecolor='#B1B1B0',
                interpolate=True, alpha=0.5)

Pxx_pz_fmrib_run_avg_se2 = stats.sem(Pxx_pz_fmrib_run, axis=0) * 2
Pxx_pz_fmrib_run_avg_fill_low = Pxx_pz_fmrib_run_avg - Pxx_pz_fmrib_run_avg_se2
Pxx_pz_fmrib_run_avg_fill_high = Pxx_pz_fmrib_run_avg + Pxx_pz_fmrib_run_avg_se2
ax.fill_between(f_pz_fmrib_run_avg, Pxx_pz_fmrib_run_avg_fill_low, Pxx_pz_fmrib_run_avg_fill_high, facecolor='#447846',
                interpolate=True, alpha=0.5)

Pxx_pz_cleaned_run_avg_se2 = stats.sem(Pxx_pz_cleaned_run, axis=0) * 2
Pxx_pz_cleaned_run_avg_fill_low = Pxx_pz_cleaned_run_avg - Pxx_pz_cleaned_run_avg_se2
Pxx_pz_cleaned_run_avg_fill_high = Pxx_pz_cleaned_run_avg + Pxx_pz_cleaned_run_avg_se2
ax.fill_between(f_pz_cleaned_run_avg, Pxx_pz_cleaned_run_avg_fill_low, Pxx_pz_cleaned_run_avg_fill_high, facecolor='#5BC23E',
                interpolate=True, alpha=0.5)

plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD ($\mu V^2$/Hz)')
plt.xlim(0, 30)
plt.ylim(1e-3, 1e3)
# plt.legend(frameon=False)
plt.tight_layout()

f_figure = 'fig03_psd_pz.svg'
plt.subplots_adjust(top=0.95, bottom=0.2, left=0.3, right=0.95, wspace=0, hspace=0.2)
fig3f.savefig(p_figure / f_figure, format=fformat)

print('Finished')