clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_simulate');
d_project = 'working_eegbcg';

%%
proc_net = fullfile('proc_bcgnet_mat', 'multi_run', 'gru_arch_general4');
d_simu = 'proc_sim';
d_proc_rs = 'proc_rs';  % directory of corrupted EEG

v.overwrite = false;
v.str_ecg = 'ECG';

v.include_respiration = false;
v.sim_type = 'sim';
% or to include respiration:
% v.include_respiration = true;
% v.sim_type = 'sim_ir';

sp_simulate_bcg(d_project, d_simu, proc_net, d_proc_rs, ...
    'include_respiration', v.include_respiration, ...
    'overwrite', v.overwrite);
sp_simulate_rmbcg(d_project, d_simu, proc_net, 'overwrite', v.overwrite, 'sim_type', v.sim_type);
T = sp_simulate_psd(d_project, d_simu, proc_net, 'overwrite', v.overwrite);
T_sub = rowfun(@(x) unique(x), T, 'InputVariables', {'str_sub'}, 'GroupingVariable', 'str_sub', 'OutputVariableName', {'ignore'});

%%
c = get_cmap;
v.figsave = true;
v.figformat = 'svg';
% p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures', 'figxx');
% print_local = @(h, dim) printForPub(h, sprintf('f_qrs_%s', h.Name), 'doPrint', v.figsave,...
%     'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig);

%%
f = T.Properties.UserData.f;
case_eeg = not(strcmpi(T.Properties.UserData.chlabels, v.str_ecg));
eeg_band = 'delta';
% eeg_band = 'theta';
% eeg_band = 'delta';

f_band = get_eeg_band(eeg_band);
case_f = and(T.Properties.UserData.f > f_band(1), T.Properties.UserData.f < f_band(2));
op = sprintf('avg_gt_minus_obs_%s', eeg_band);
summarise_dpsd = @(x, y) median(x{1}(case_f, case_eeg) - y{1}(case_f, case_eeg), [1, 2]);
B = rowfun(@(x, y) summarise_dpsd(x, y), T, 'InputVariables', {'p_gt', 'p_obs'}, 'OutputVariableName', op);
T = [T, B];

%% group by subjects (i.e. average across runs)
voi = {op};
for ix_voi = 1:length(voi)
    T_sub_ = rowfun(@(x) mean(x), T, 'InputVariables', voi{ix_voi}, 'GroupingVariable', 'str_sub', 'OutputVariableName', voi{ix_voi});
    assert(all(strcmpi(T_sub.str_sub, T_sub_.str_sub)), '?')
    T_sub.(voi{ix_voi}) = T_sub_.(voi{ix_voi});
end

xlim_ = (max(ceil(abs(T_sub.(op)))) + 0.1) * [-1, 1];
x_edges = xlim_(1):1:xlim_(end);
histogram(T_sub.(op), x_edges)
signrank(T_sub.(op))

%%
voi = {'p_gt', 'p_obs'};
for ix_voi = 1:length(voi)
    T_sub_ = rowfun(@(x) median_across_runs(x, case_eeg), T, 'InputVariables', voi{ix_voi}, 'GroupingVariable', 'str_sub', 'OutputVariableName', voi{ix_voi});
    assert(all(strcmpi(T_sub.str_sub, T_sub_.str_sub)), '?')
    T_sub.(voi{ix_voi}) = T_sub_.(voi{ix_voi});
end
clf;
X = nan(height(T_sub), length(f));
for ix_T_sub = 1:height(T_sub)
    X(ix_T_sub, :) = T_sub.p_obs{ix_T_sub}./T_sub.p_gt{ix_T_sub};
    %     hold on;
    %     plot(f, T_sub.p_gt{ix_T_sub} -  T_sub.p_obs{ix_T_sub}, 'k')
    % %     plot(f, T_sub.p_gt{ix_T_sub}, 'r')
    
end

p_vec = nan(1, length(f));
for ix_p_vec = 1:length(p_vec)
    [~, p_vec(ix_p_vec)] = ttest(X(:, ix_p_vec));
    [p_vec(ix_p_vec)] = signrank(X(:, ix_p_vec));
    
end
ulim = 20;
p_vec(f<ulim) = bonf_holm(p_vec(f<ulim));
p_vec(f>=ulim) = nan;
p_vec(p_vec>0.05)= nan;
p_vec(p_vec<=0.05) = 1;

X_mea = nanmean(X, 1);
X_sem = 2 * nanstd(X, 0, 1)./sqrt(sum(isfinite(X), 1));
clf;hold on;
plot(f, X_mea, 'k-');
ciplot(X_mea - X_sem, X_mea + X_sem, f, c.neutral, 0.5);
xlim([0, ulim])
grid on;
xlabel('Frequency (Hz)');
ylabel('Power ratio')
plot(f, p_vec*2, 'k', 'lineWidth', 2)

%%
clf;
X = nan(height(T_sub), length(f));
for ix_T_sub = 1:height(T_sub)
    subplot(3, 4, ix_T_sub)
        hold on;
        title(T_sub.str_sub{ix_T_sub})
        plot(f, T_sub.p_obs{ix_T_sub}./T_sub.p_gt{ix_T_sub}, 'k')
%         plot(f, , 'r')
    xlim([0, 2.5])
    ylim([0.8, 1.2])
    grid on;
    xlabel('Frequency (Hz)');
    ylabel('Power ratio');

end

%%
function z = median_across_runs(x, case_eeg)
z = cell2mat(reshape(x, [1, 1, length(x)]));
z = z(:, case_eeg, :);
z = median(z, 3);

z = median(z, 2);

z = {z};
end

