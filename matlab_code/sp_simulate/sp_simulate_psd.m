function T = sp_simulate_psd(d_project, d_simu, proc_net, varargin)
%% sp_simulate_psd: 
% param d_project:
% param d_simu:
% param proc_net:
% param varargin: typical varargin paired structure (see d structure)
% return: table with PSDs of simulated EEG + BCG and other combinations
d.overwrite = true;
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.t_welch_window = 4;
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v.source_files = [proc_net]; % so that they get included in the hash
v_hash = generate_ip_hash(v);
%%
d_proc = fullfile(getenv_check_empty('D_DATA'), d_project, v.proc);
%%
T = get_file_table(fullfile(d_proc, d_simu), 'set', 'sim_obs');
%%
f_psd = fullfile(getenv('D_OUT'), d_project, v.proc, d_simu, 'psd');
do_psd = generate_check_mat(f_psd, v.overwrite, v_hash);
if do_psd
    cell_sub = unique(T.str_sub);
    for ix_cell_sub = 1:length(cell_sub)
        str_sub = cell_sub{ix_cell_sub};
        case_sub = strcmpi(T.str_sub, str_sub);
        vec_run = sort(unique(T.ix_run(case_sub)));
        for ix_vec_run = 1:length(vec_run)
            % ix_run = vec_run(ix_vec_run);
            
            case_obs_run = T.ix_run == vec_run(ix_vec_run);
            case_obs_sub = strcmpi(T.str_sub, str_sub);
            
            EEG_obs = pop_loadset(T.file{case_obs_sub & case_obs_run});
            
            split_len = EEG_obs.srate * v.t_welch_window;
            
            % can be removed - 
            ix_ecg = find(strcmpi({EEG_obs.chanlocs.labels}, 'ECG'));
            EEG_obs.etc.data_noise(ix_ecg, :) = 0;
            EEG_obs.etc.data_bcg(ix_ecg, :) = 0;
            EEG_obs.etc.data_bcg_and_noise(ix_ecg, :) = 0;
            EEG_obs.data(ix_ecg, :) = 0;
            
            [p_obs, fw] = pwelch(EEG_obs.data', split_len, [], [], EEG_obs.srate);
            [p_bcg_and_noise, ~] = pwelch(EEG_obs.etc.data_bcg_and_noise', split_len, [], [], EEG_obs.srate);
            [p_bcg, ~] = pwelch(EEG_obs.etc.data_bcg', split_len, [], [], EEG_obs.srate);
            [p_gt, ~] = pwelch(EEG_obs.etc.data_noise', split_len, [], [], EEG_obs.srate);
                        
            T.p_obs{case_obs_sub & case_obs_run} = p_obs;
            T.p_bcg_and_noise{case_obs_sub & case_obs_run} = p_bcg_and_noise;
            T.p_bcg{case_obs_sub & case_obs_run} = p_bcg;
            T.p_gt{case_obs_sub & case_obs_run} = p_gt;
            if and(ix_vec_run == 1, ix_cell_sub == 1)
                v.srate = EEG_obs.srate;
                v.chanlocs = EEG_obs.chanlocs;
                v.chlabels = {EEG_obs.chanlocs.labels};
                v.f = fw;
            end
        end
    end
    T.Properties.UserData = v;
    save(f_psd, 'T', 'v_hash');
else
    T = load(f_psd);
    T = T.T;
end
end

