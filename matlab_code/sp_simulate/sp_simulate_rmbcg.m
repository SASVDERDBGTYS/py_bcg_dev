function sp_simulate_rmbcg(d_project, d_simu, proc_net, varargin)
%% sp_simulate_rmbcg: Do BCG removal on simulated BCG
% param d_project: root data directory of project
% param d_simu: simulation data directory relative to 'project/proc/' e.g. 'simu'
% param varargin: typical varargin paired structure (see d structure)
% type: d_project: string
% type: d_simu: string
% return: nothing - writes files back to d_simu directory
d.proc = getenv_check_empty('R_PROC');
d.str_ecg = 'ECG';
d.hp_passband = 2.5;
d.fmrib_pas_method = 'obs';
d.met_str = 'qrs0';
d.skip_hash_check = false;
d.sim_type = 'sim';
d.overwrite = false;
d.d_overwrite = struct;
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v.source_files = [proc_net]; % so that they get included in the hash
v_hash = generate_ip_hash(v);
%%
d_simulated = fullfile(getenv_check_empty('D_OUT'), d_project, v.proc, d_simu);
d_proc = fullfile(getenv_check_empty('D_DATA'), d_project, v.proc);
get_f_simulated_obs = @(d, str_sub, ix_run) fullfile(d, ...
    sprintf('%s', str_sub), ...
    sprintf('%s_r%02d_sim_obs', str_sub, ix_run));
%%
T = get_file_table(fullfile(d_proc, d_simu), 'set', v.sim_type);
%%

cell_sub = unique(T.str_sub);
for ix_cell_sub = 1:length(cell_sub)
    str_sub = cell_sub{ix_cell_sub};
    case_sub = strcmpi(T.str_sub, str_sub);
    vec_run = sort(unique(T.ix_run(case_sub)));
    for ix_vec_run = 1:length(vec_run)
        ix_run = vec_run(ix_vec_run);
        case_run = vec_run(ix_vec_run) == T.ix_run;
        f_simulate_obs = get_f_simulated_obs(d_simulated, str_sub, ix_run);
        
        do_obs = generate_check_eeg(f_simulate_obs, v.overwrite, v_hash, v.skip_hash_check);
        if do_obs
            EEG = pop_loadset(T.file{case_sub & case_run});
            
            EEG.etc.data_bcg_and_noise = EEG.data;  % store the simulated bcg + noise
            EEG = sp_simulate_rmbcg_core(EEG, ...
                v.fmrib_pas_method, v.hp_passband, v.met_str, v.str_ecg);
            
            EEG.etc.hashes.sp_simulate_rmbcg = v_hash;
            EEG.etc.sp_simulate_rmbcg = v;
            EEG.subject = str_sub;
            %
            EEG = eeg_checkset( EEG );
            [~, setn] = fileparts(f_simulate_obs);
            EEG.setname = setn;
            
            pop_saveset(EEG, 'filename', f_simulate_obs);
        end
    end
end
end

function EEG_nobcg = sp_simulate_rmbcg_core(EEG, fmrib_pas_method, hp_passband, met_str, str_ecg, varargin)
%% sp_simulate_rmbcg_core: QRS detect (with high-pass) and fMRIB BCG removal
% param EEG: EEGLAB based data structure
% param fmrib_pas_method: BCG removal method for fmrib_pas (e.g. 'obs')
% param hp_passband: high-pass filter cutoff, e.g. 2.5Hz
% param met_str: label for qrs markers e.g. 'qrs'
% param str_ecg: the label in the EEGLAB structure of the ECG channel
% param varargin: typical varargin paired structure (not used here)
% return: EEGLAB structure with BCG signal removed
%%
d.d_overwrite = struct;
% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
v.fmrib_pas_method = fmrib_pas_method;
v.hp_passband = hp_passband;
v.met_str = met_str;
v.str_ecg = str_ecg;
%%
EEG_qrs = pop_eegfiltnew(EEG, hp_passband, [], [], 0, [], 1);

%
%detect the QRS (take a look at the markers relative to the BCG/ECG to see if it worked
% if it didn't work, adjust the filter above
ch_eeg = find(strcmpi({EEG_qrs.chanlocs.labels}, v.str_ecg));
EEG_qrs = pop_fmrib_qrsdetect(EEG_qrs, ch_eeg, v.met_str, 'no');
choice_qrs = [EEG_qrs.event.latency];
choice_qrs = choice_qrs(strcmpi({EEG_qrs.event.type}, v.met_str));

% force 0 duration of QRS events to match with no bcg structure
for ix_event = 1:length(EEG_qrs.event)
    EEG_qrs.event(ix_event).duration = 0;
end

% Run the BCG removal script
EEG_nobcg = fmrib_pas(EEG, choice_qrs, v.fmrib_pas_method, []);
%
EEG.etc.sp_simulate_rmbcg_core = v;
end