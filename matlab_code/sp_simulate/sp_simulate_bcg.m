function [get_f_simulated, d_simulated] = sp_simulate_bcg(d_project, d_output, proc_net, proc_rs, varargin)
%% simulate_bcg: Simulate corrupted BCG by fitting pinknoise and ground-truth BCG to corrupted EEG spectrum
% param d_project:
% param d_output:
% param data_source_net:
% param varargin: typical varargin paired structure (see d structure)
% return: {1} ... and {2} ...
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.hp_passband = 0.25;  % high-pass cut-off to apply to simulated signal (should match what was applied to the corrupted EEG)
d.t_welch_window = 4;  % welch window with which to computed PSDs used for cost function of fit
% frequency bounds used to fit pinknoise and BCG signal to corrupted EEG:
d.f_bounds = [[4, 8];[12, 18]]; % 4 to 8 is to avoid drift, and avoid alpha. 12 to 18 is to avoid alpha then avoid GA
d.fig_save = false;  %
d.fig_gen = false;  %
d.include_respiration = false;
d.respiration_amplitude = 10;
d.es = '';
d.skip_hash_check = false;  % whether to skip hash check (to decide whether to re-process file if d has changed)
d.overwrite = false;  % overwrite main results of function
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v.source_files = [proc_net]; % so that they get included in the hash
v_hash = generate_ip_hash(v);
%%
d_proc = fullfile(getenv_check_empty('D_DATA'), d_project, v.proc);
%%
T = get_file_table(fullfile(d_proc, proc_net), 'mat', 'bcgnet_purebcg');
%%
es = v.es;
if v.include_respiration
    es = [es, '_ir'];
end
%%
d_simulated = fullfile(getenv_check_empty('D_OUT'), d_project, v.proc, d_output);
d_rs = fullfile(getenv_check_empty('D_OUT'), d_project, v.proc, proc_rs);
get_f_simulated = @(d, str_sub, ix_run) fullfile(d, ...
    sprintf('%s', str_sub), ...
    sprintf('%s_r%02d_sim%s', str_sub, ix_run, es));

get_f_rs = @(d, str_sub, ix_run) fullfile(d, ...
    sprintf('%s', str_sub), ...
    sprintf('%s_r%02d_rs', str_sub, ix_run));

cell_sub = unique(T.str_sub);
for ix_cell_sub = 1:length(cell_sub)
    str_sub = cell_sub{ix_cell_sub};
    case_sub = strcmpi(T.str_sub, str_sub);
    vec_run = sort(unique(T.ix_run(case_sub)));
    v.subject_seed = string2hash(str_sub);
    rng(v.subject_seed);
    f_respiration = 12/60 + rand * (20/60-12/60);
    for ix_vec_run = 1:length(vec_run)
        ix_run = vec_run(ix_vec_run);
        case_run = vec_run(ix_vec_run) == T.ix_run;
        f_simulate = get_f_simulated(d_simulated, str_sub, ix_run);
        do_simulate = generate_check_eeg(f_simulate, v.overwrite, v_hash, v.skip_hash_check);
        if do_simulate
            fprintf('Loading %s\n', T.file{case_run & case_sub});
            
            % load in the corrupted rs
            EEG = pop_loadset([get_f_rs(d_rs, str_sub,  ix_run), '.set']);
            
            case_ecg = strcmpi({EEG.chanlocs.labels}, 'ECG');
            
            EEG = remove_motion_data(EEG);
            
            % now load in the simulated bcg
            bcg = load(T.file{case_run & case_sub});
            EEG_bcg = EEG;
            EEG_bcg.data = bcg.data * 1e6;
            
            % initiate
            EEG_noise = EEG;
            EEG_noise.data = EEG_noise.data * 0;
            
            v.vec_A_noise = zeros(EEG.nbchan, 1);
            v.vec_A_noise(1) = 100;  % gets overwritten just to init
            split_len = EEG.srate * v.t_welch_window;
            
            v.seed = nan(EEG.nbchan, 1);
            
            for ix_ch = 1:EEG.nbchan
                fprintf('Fitting noise amplitude channel\t%2d\n', ix_ch);
                if case_ecg(ix_ch)
                    % noise here is left at 0
                    fprintf('Skipping ECG channel\n'); % EEG.data(ix_ch, :) is already the ECG
                else
                    %%
                    eeg_corrupted = EEG.data(ix_ch, :);
                    %% fit pink-noise scaling
                    v.seed(ix_ch) = string2hash(sprintf('%s_ch%02d', f_simulate, ix_ch));
                    if v.include_respiration
                        h_noise_function = @(x, y, z) get_noise_with_respiration(x, y, z, f_respiration, v.respiration_amplitude, 1/EEG_bcg.srate);
                    else
                        h_noise_function = @(x, y, z) get_noise(x, y, z);
                    end
                    h_get_fit_cost = @(A) get_fit_cost(A, eeg_corrupted, EEG_bcg.data(ix_ch, :), EEG.srate, v.seed(ix_ch), split_len, v.f_bounds, h_noise_function);
                    
                    A_init = nanmean(v.vec_A_noise); % initiate with average of previous amplitudes
                    A = fminsearch(h_get_fit_cost, A_init);
                    % A = patternsearch(h_get_fit_cost, A);
                    EEG_noise.data(ix_ch, :) = h_noise_function(A, length(eeg_corrupted), v.seed(ix_ch));
                    
                    v.vec_A_noise(ix_ch, 1) = A;
                    %%
                end
            end
            %% high-pass the noise and high-pass the bcg to match rs highpassing
            [EEG_noise, ~, b] = pop_eegfiltnew(EEG_noise, v.hp_passband, [], [], 0, [], 1);
            v.hp_order = length(b) - 1;
            EEG_bcg = pop_eegfiltnew(EEG_bcg, v.hp_passband, [], v.hp_order, 0, [], 1);
            %%
            EEG.etc.data_bcg = EEG_bcg.data;
            EEG.etc.data_noise = EEG_noise.data;
            EEG.etc.data_corrupted = EEG.data;  % store input
            EEG.data = EEG_bcg.data + EEG_noise.data;
            %%
            EEG.etc.hashes.sp_simulate_bcg = v_hash;
            EEG.etc.sp_simulate_bcg = v;
            EEG.subject = str_sub;
            %
            EEG = eeg_checkset( EEG );
            [~, setn] = fileparts(f_simulate);
            EEG.setname = setn;
            pop_saveset(EEG, 'filename', f_simulate);
            if v.fig_gen
                fprintf('Re-run to generate figures\n');
            end
        else
            if v.fig_gen
                EEG = pop_loadset([f_simulate, '.set']);
                split_len = EEG.srate * v.t_welch_window;
                for ix_ch = 1:EEG.nbchan
                    %%
                    eeg_corrupted = EEG.etc.data_corrupted(ix_ch, :);
                    eeg_sim = EEG.data(ix_ch, :);
                    [p_eeg, fw] = pwelch(eeg_corrupted, split_len, [], [], EEG.srate);
                    [p_sim, ~] = pwelch(eeg_sim, split_len, [], [], EEG.srate);
                    
                    clf;hold on;
                    plot(fw, p_eeg, 'k');
                    plot(fw, p_sim, 'r');
                    xlim([0, 20]);
                    %%
                    if v.fig_save
                        error('Have not imeplemented figure saving');
                    end
                end
            else
                % do nothing
            end
        end
    end
end
end
function noise = get_noise(A, n, seed)
rng(seed);
noise = A * pinknoise(n);
end
function noise = get_noise_with_respiration(A, n, seed, f_respiration, A_respiration, dt)
rng(seed);
t = [0:dt:(n-1)*dt];
noise = A * pinknoise(n) + A_respiration * sin(2*pi*f_respiration * t);
end

function cost = get_fit_cost(A, eeg_corrupted, bcg_pure, fs, seed, split_len, f_bounds, h_noise_function)
noise = h_noise_function(A, length(eeg_corrupted), seed);
[p_eeg, fw] = pwelch(eeg_corrupted, split_len, [], [], fs);
[p_sim, ~] = pwelch(bcg_pure + noise, split_len, [], [], fs);

f_case = false(size(fw));
for ix_f_bounds = 1:size(f_bounds)
    f_case = f_case | and(fw>f_bounds(ix_f_bounds, 1), fw<f_bounds(ix_f_bounds, 2));
end

cost = sqrt(mean((p_eeg(f_case) - p_sim(f_case))).^2);
end