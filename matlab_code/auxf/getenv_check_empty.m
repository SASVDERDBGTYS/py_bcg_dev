function op = getenv_check_empty(ip)
%% run getenv, but throw an error if it returns empty
op = getenv(ip);
assert(not(isempty(op)), sprintf('Environment variable %s not found.', ip))
end