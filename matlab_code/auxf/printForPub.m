function fileLoc = printForPub(fig,fName,varargin)
%%
% printForPub(gcf,[figuredir_png filesep extendStyle_s extraString '_' sub],...
%     'fformat','png','physicalSizeCM',[17.5 17.5]);
%%
% v1.0
%%
p = inputParser;
p.addParameter('physicalSizeCM',[10 10]);
% p.addParameter('saveDir',fullfile(getuserdir,'Drive','Research','reports','170100_tRNS_RT','figures'));
p.addParameter('fontname','Verdana');
p.addParameter('fontsizeText',9);
p.addParameter('fontsizeAxes',8);
p.addParameter('doPrint',true);
p.addParameter('fformat','svg');
p.addParameter('proj','');%used to be tRNS_RT
p.addParameter('autopos',false);
p.addParameter('saveDir',fullfile(pwd));
% p.addParameter('savedir','');

% if not(isempty(p.Results.savedir))
%     p.Results.saveDir = p.Results.savedir;
% end

p.parse(varargin{:});
saveDir = p.Results.saveDir;
set(fig,'Units','centimeters');%size of figure on screen will be in cm
physicalSizeCM = [0,0,p.Results.physicalSizeCM];
if p.Results.autopos
    %     a = get(0, 'MonitorPositions');
    physicalSizeCM = [-25,-5,fig.Position(3),fig.Position(4)];
end
proj = p.Results.proj;
% saveDir = p.Results.saveDir;%takes precedence
if strcmpi(proj,'tRNS_RT')
    saveDir = fullfile(getuserdir,'Drive','Research','reports','170100_tRNS_RT','figures');
end
doPrint = p.Results.doPrint;
fformat = p.Results.fformat;
% [~,hostname] = system('hostname');% if strcmpi(strtrim(hostname),'OJFEL');end
fontname = p.Results.fontname;
fontsizeText = p.Results.fontsizeText;
fontsizeAxes = p.Results.fontsizeAxes;
fileLoc = '';
try
    %should probably change the next line
    set(0,'defaulttextinterpreter','tex');
    %     set(0,'defaultaxesfontname',fontname);set(0,'defaulttextfontname',fontname);
    %     set(0,'defaultaxesfontsize',fs);set(0,'defaulttextfontsize',fs-1);
    
    set(fig,'Position',physicalSizeCM + [2.5 2.5 0 0]);%This line sets on-screen figure size (and position)
    set(fig,'PaperPositionMode','manual');%This has to be manual or the distinction between position and paperposition does not work
    
    set(fig,'PaperUnits','centimeters');%Paper units
    set(fig,'PaperPosition',[0 0 physicalSizeCM(3) physicalSizeCM(4)]);%Think paper pos might be for non vector graphics saving
    set(fig,'PaperSize',[physicalSizeCM(3) physicalSizeCM(4)]);%Size for vector graphics saving
    
    fs_properties = findall(fig,'-property','FontSize');
    fs_properties_types = arrayfun(@(x) x.Type, fs_properties,'UniformOutput',0);
    set(fs_properties(strcmpi(fs_properties_types,'Text')),'FontSize',fontsizeText);
    set(fs_properties(strcmpi(fs_properties_types,'axes')),'FontSize',fontsizeAxes);
    set(findall(fig,'-property','FontName'),'FontName',fontname);
    
    
    if doPrint
        if strcmpi(fformat,'meta')
            print(fig,['-d' fformat]);
        else
            if strcmpi(fformat,'epsc'),ext = 'eps';
            elseif strcmpi(fformat,'epsc2'),ext = 'eps';
            else
                ext = fformat;
            end
            fileLoc = [saveDir filesep fName '.' ext];
            if strcmpi(fformat,'png')
                print(fig,fileLoc,['-d' fformat],'-r300');
            elseif strcmpi(fformat,'fig')
                1;
                saveas(fig,fileLoc);
            else
                %                 axis tight;
                print(fig,fileLoc,['-d' fformat], '-painters');
            end
        end
    end
catch
    warning('Save figure failed');
end
end