addpath('/Users/jyao/Documents/eeglab2019_0');
eeglab;
hdr_name = @(run) strcat('0607_', string(run),'_GA_Remov_noF_21vols.vhdr');
output_name = @(run) strcat('0607_', string(run),'_GA_Remov_noF_21vols');

for ix = 1:5
    % load
    [EEG, com] = pop_loadbv('/Users/jyao/Documents/EEG_data/180607_Sub11', hdr_name(ix));

    % save
    %name = string(output_name(ix));
    %pop_saveset(EEG, 'filename', name, 'filepath', '/Users/jyao/Documents/EEG_data/180607_Sub11/f_out');
    
end
