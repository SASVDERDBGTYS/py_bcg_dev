clear;
close all;
addpath('..')
set_env;
addpath(genpath('../auxf'));
addpath(getenv('D_EEGLAB'));
addpath('../sp_preproc');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
d_project = 'working_eegbcg';

mat_to_eeglab(d_project);
