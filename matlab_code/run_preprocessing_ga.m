clear;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');

d_project = 'working_eegbcg';
%%
sp_count_markers(d_project);
sp_rmga(d_project);
sp_raw_resample(d_project);
