clearvars;
close all;
set_env;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%
d_project = 'working_eegbcg';
reref = 'laplace';
% spectral_band = 'stft_ersp'; t_low = -1.5; t_hig = 3.5;  % if you add ersp it should work here...
spectral_band = 'stft-pls_ersp'; t_low = -1.5; t_hig = 3.5;  % if you add ersp it should work here...
lowpass = false;
str_spectral_band = spectral_band;
proc = getenv_check_empty('R_PROC');
t_baseline = [-0.75, -0.25];es = ''; % default
% t_baseline = 'log';es = '_log'; 

%%
data_source_gar = fullfile('proc_rs');
d_proc_bcg_gar = fullfile(getenv('D_OUT'), d_project, proc, data_source_gar);
[str_source_gar, str_type_gar] = gen_source(data_source_gar);

data_source_obs = fullfile('proc_bcgobs');
d_proc_bcg_obs = fullfile(getenv('D_OUT'), d_project, proc, data_source_obs);
[str_source_obs, str_type_obs] = gen_source(data_source_obs);

data_source_net = fullfile('proc_bcgnet', 'multi_run', 'gru_arch_general4'); % use this format to classify on networks!
d_proc_bcg_net = fullfile(getenv('D_OUT'), d_project, proc, data_source_net);
[str_sourc_net, str_type_net] = gen_source(data_source_net);

%%
[d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar] = an_epoch(d_project, d_proc_bcg_gar, ...
    'reref', reref, 'str_source', str_source_gar, 'str_merged', str_type_gar, 'spectral_band', spectral_band, ...
    't_low',t_low, 't_hig',t_hig, 'lowpass', lowpass);

[d_proc_epoch_obs, get_f_epoched_obs, vec_sub_obs] = an_epoch(d_project, d_proc_bcg_obs, ...
    'reref', reref, 'str_source', str_source_obs, 'str_merged', str_type_obs, 'spectral_band', spectral_band, ...
    't_low',t_low, 't_hig',t_hig, 'lowpass', lowpass);

[d_proc_epoch_net, get_f_epoched_net, vec_sub_net] = an_epoch(d_project, d_proc_bcg_net, ...
    'reref', reref, 'str_source', str_sourc_net, 'str_merged', str_type_net, 'spectral_band', spectral_band, ...
    't_low',t_low, 't_hig',t_hig, 'lowpass', lowpass);

%%

[std_standard_gar, std_target_gar, mea_standard_gar, mea_target_gar, med_standard_gar, med_target_gar, ...
    se2_standard_gar, se2_target_gar, t1, f, e1, topo_locs] = an_plot_evoked_stft(d_project, ...
    d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar, 'str_source', str_source_gar, 't_baseline', t_baseline);

[std_standard_obs, std_target_obs, mea_standard_obs, mea_target_obs, med_standard_obs, med_target_obs, ......
    se2_standard_obs, se2_target_obs] = an_plot_evoked_stft(d_project, ...
    d_proc_epoch_obs, get_f_epoched_obs, vec_sub_obs, 'str_source', str_source_obs, 't_baseline', t_baseline);

[std_standard_net, std_target_net, mea_standard_net, mea_target_net, med_standard_net, med_target_net, ...
    se2_standard_net, se2_target_net, t, ~, eeg_labels] = an_plot_evoked_stft(d_project, ...
    d_proc_epoch_net, get_f_epoched_net, vec_sub_net, 'str_source', str_sourc_net, 't_baseline', t_baseline);

%%
vec_sub = vec_sub_net;
%%
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = sprintf('%s%s', es, '_5ronly');
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = sprintf('%s%s', es, '');
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';

%%

coi = {'T8', 'Pz'};
coi_full = { ...
    'Fz','F1','F2','F3','F4', ...
    'FC1','FC2','FC3','FC4', ...
    'FT7', 'FT8', ...
    'Cz','C1','C2','C3','C4', ...
    'T7', 'T8', ...
    'CPz','CP1','CP2','CP3','CP4', ...
    'TP7', 'TP8', ...
    'Pz','P1','P2','P3','P4', ...
    'Oz','O1','O2', ...
    };

coi_full = intersect(coi_full, eeg_labels);
if strcmpi(reref, 'laplace')
    a_units = 'µV/m²';
elseif strcmpi(reref, 'hjort')
    a_units = '?';
    coi = {'C3', 'C4'};
else
    a_units = 'µV';
end
%%
for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    if ix_vec_sub == 1
        nb_chan = size(std_standard_gar.(str_sub), 3);
        nb_methods = 3; %obs, net_based
        nb_stimuli = 2; % standard, target
        nb_freqs = size(med_standard_gar.(str_sub), 1);
        mat_med = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        mat_std = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        mat_se2 = nan(nb_freqs, length(t), nb_chan, length(vec_sub), nb_methods, nb_stimuli);
        
    end
    mat_med(:, :, :, ix_vec_sub, 1, 1) = med_standard_gar.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 1, 2) = med_target_gar.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 2, 1) = med_standard_obs.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 2, 2) = med_target_obs.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 3, 1) = med_standard_net.(str_sub);
    mat_med(:, :, :, ix_vec_sub, 3, 2) = med_target_net.(str_sub);
    
    mat_std(:, :, :, ix_vec_sub, 1, 1) = std_standard_gar.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 1, 2) = std_target_gar.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 2, 1) = std_standard_obs.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 2, 2) = std_target_obs.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 3, 1) = std_standard_net.(str_sub);
    mat_std(:, :, :, ix_vec_sub, 3, 2) = std_target_net.(str_sub);
    
    mat_se2(:, :, :, ix_vec_sub, 1, 1) = se2_standard_gar.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 1, 2) = se2_target_gar.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 2, 1) = se2_standard_obs.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 2, 2) = se2_target_obs.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 3, 1) = se2_standard_net.(str_sub);
    mat_se2(:, :, :, ix_vec_sub, 3, 2) = se2_target_net.(str_sub);
end

%%

% p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'proc_epoch', 'figures', ...
%     sprintf('c%s_vs%s%s', gen_source(data_source_obs), gen_source(data_source_net), str_spectral_band));
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig06', ...
    sprintf('c_%s%s_vs%s%s', reref, gen_source(data_source_obs), gen_source(data_source_net), str_spectral_band));
% p_fig = '~/Desktop/temp/';

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'png';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
print_local = @(h, dim) printForPub(h, sprintf('f_evo_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

cmap = [c.obs_st; c.net_st; c.obs_ta; c.net_ta; c.neutral];
% save('delete_me.mat');
%%
% load('delete_me.mat')
%%
ns = numSubplots(length(coi_full)+1);
fprintf('---------\n');
clims = [-1, 3.5];
cell_met = {'GAR', 'OBS', 'BCGNet'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('%s', str_met));clf;
    for ix_coi = 1:length(coi_full)+1
        if ix_coi==length(coi_full)+1
            ax = subplot('Position',[ ...
                0.8, 0.1, ...
                topo_locs.topoW*2, ...
                topo_locs.topoH, ...
                ]);hold on;
            title('bits');
            ax_im = imagesc(ax, 'XData', t,'YData', f,'CData', x, clims);
            colorbar;
            xlabel('Time (s)');
            ylabel('Frequency (Hz)')
            xlim([0, 3])
            ylim([1 30])
        else
            case_ch = strcmpi(eeg_labels, coi_full{ix_coi});
            title_ = sprintf('%s', coi_full{ix_coi});
            %         if strcmpi(coi{ix_coi}, 'Fz'), d = 10;else d = 5;end
            
            %         ax = subplot(ns(1), ns(2), ix_coi);hold on;
            ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);hold on;
            
            % needs to be checked!
            
            if strcmpi(str_met, 'GAR')
                mea_standard_gar = mean(mat_med(:, :, case_ch, :, 1, 1), 4);
                mea_target_gar = mean(mat_med(:, :, case_ch, :, 1, 2), 4);
                med_target_gar = median(mat_med(:, :, case_ch, :, 1, 2), 4);
                se2_standard_gar = std_error(mat_med(:, :, case_ch, :, 1, 1), 0, 4);
                se2_target_gar = std_error(mat_med(:, :, case_ch, :, 1, 2), 0, 4);
                x = squeeze(med_target_gar);
            elseif strcmpi(str_met, 'OBS')
                mea_standard_obs = mean(mat_med(:, :, case_ch, :, 2, 1), 4);
                mea_target_obs = mean(mat_med(:, :, case_ch, :, 2, 2), 4);
                med_target_obs = median(mat_med(:, :, case_ch, :, 2, 2), 4);
                se2_standard_obs = std_error(mat_med(:, :, case_ch, :, 2, 1), 0, 4);
                se2_target_obs = std_error(mat_med(:, :, case_ch, :, 2, 2), 0, 4);
                x = squeeze(med_target_obs);
            elseif strcmpi(str_met, 'BCGNet')
                mea_standard_net = mean(mat_med(:, :, case_ch, :, 3, 1), 4);
                mea_target_net = mean(mat_med(:, :, case_ch, :, 3, 2), 4);
                med_target_net = median(mat_med(:, :, case_ch, :, 3, 2), 4);
                se2_standard_net = std_error(mat_med(:, :, case_ch, :, 3, 1), 0, 4);
                se2_target_net = std_error(mat_med(:, :, case_ch, :, 3, 2), 0, 4);
                x = squeeze(med_target_net);
            else
                error('!');
            end
            x = smoothn(x, 0.15);
            
            [min(x(:)), max(x(:))]
            ax_im = imagesc(ax, 'XData', t,'YData', f,'CData', x, clims);
            
            title(title_);
            %
            axis tight;
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            xlim([0, 3])
            ylim([1 30])
        end
    end
    s_f = [18.1, 22];
    
    fprintf('---------\n');
    print_local(h_f, [18.1, 22]);
end

%%

%%
ns = numSubplots(length(coi_full)+1);
fprintf('---------\n');
clims = [-1.5, 4.5];
cell_met = {'GAR'};
for ix_vec_sub = 1:length(vec_sub)
ix_sub = vec_sub(ix_vec_sub);
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('%s_sub%02d', str_met, ix_sub));clf;
    for ix_coi = 1:length(coi_full)+1
        if ix_coi==length(coi_full)+1
            ax = subplot('Position',[ ...
                0.8, 0.1, ...
                topo_locs.topoW*2, ...
                topo_locs.topoH, ...
                ]);hold on;
            title('bits');
            ax_im = imagesc(ax, 'XData', t,'YData', f,'CData', x, clims);
            colorbar;
            xlabel('Time (s)');
            ylabel('Frequency (Hz)')
            xlim([0, 3])
            ylim([1 30])
        else
            case_ch = strcmpi(eeg_labels, coi_full{ix_coi});
            title_ = sprintf('%s', coi_full{ix_coi});
            %         if strcmpi(coi{ix_coi}, 'Fz'), d = 10;else d = 5;end
            
            %         ax = subplot(ns(1), ns(2), ix_coi);hold on;
            ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);hold on;
            
            % needs to be checked!
            
            if strcmpi(str_met, 'GAR')
                mea_standard_gar = mat_med(:, :, case_ch, ix_vec_sub, 1, 1);
                mea_target_gar = mat_med(:, :, case_ch, ix_vec_sub, 1, 2);
                x = squeeze(mea_target_gar);
            elseif strcmpi(str_met, 'OBS')
                mea_standard_obs = mat_med(:, :, case_ch, ix_vec_sub, 2, 1);
                mea_target_obs = mat_med(:, :, case_ch, ix_vec_sub, 2, 2);
                x = squeeze(mea_target_obs);
            elseif strcmpi(str_met, 'BCGNet')
                mea_standard_net = mat_med(:, :, case_ch, ix_vec_sub, 3, 1);
                mea_target_net = mat_med(:, :, case_ch, ix_vec_sub, 3, 2);
                x = squeeze(mea_target_net);
            else
                error('!');
            end
            x = smoothn(x, 0.15);
            
            [min(x(:)), max(x(:))]
            ax_im = imagesc(ax, 'XData', t,'YData', f,'CData', x, clims);
            
            title(title_);
            %
            axis tight;
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            xlim([-1, 2.5])
            ylim([1 30])
        end
    end
    s_f = [18.1, 22];
    
    fprintf('---------\n');
    print_local(h_f, [18.1, 22]);
end
end