function T = an_psd_test_epochs(d_project, proc_net, varargin)
%% an_psd_test_epochs: calculated the PSD and PSD differences for OBS, RS (GAR, corrupted data after GA removal), BCGNet
% param d_project: the root directory of the project e.g. working_eegbcg
% param proc_bcgobs: a string with the directory relevant to d_project/proc/
% param proc_rs: a string with the directory relevant to d_project/proc/
% param proc_net: a string with the directory relevant to d_project/proc/
% param varargin:
% return: table with power and power differences
d.mne_scaling = 1e6;
d.str_ecg = 'ECG';
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.proc_psd = 'proc_psd'; % a string with the output directory relevant to d_project/proc/
d.overwrite = false;  % overwrite main results of function
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
d.es = '';
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v.source_files = [proc_net]; % so that they get included in the hash
v_hash = generate_ip_hash(v);
%%
cell_met_psd = {'met_obs', 'met_net', 'met_gar', 'met_net_trained', 'met_net_pretrained'};
cell_met_other = {'pretrained_epoch', 'heldout_epoch', 'across_run_epoch'};
%%
if contains(proc_net, 'proc_cv')
    fprintf('In CV mode!\n');
    str_in_file_name = 'cv_epoch';
else
    str_in_file_name = 'test_epochs';
end
%%
[~, arch, ~] = fileparts(proc_net);
f_psd_epoch = fullfile(getenv('D_OUT'), d_project, v.proc, v.proc_psd, ['psd_', str_in_file_name, '_', arch, v.es]);
do_psd_epoch = generate_check_mat(f_psd_epoch, v.overwrite, v_hash);
force_do_psd_epoch = false; % takes longer to read it from disk over network than to make it...
if do_psd_epoch||force_do_psd_epoch
    d_proc_net = fullfile(getenv('D_OUT'), d_project, v.proc, sprintf('%s', proc_net));
    T = get_file_table(d_proc_net, 'mat', str_in_file_name);
    T.ch = cell(height(T), 1);
    cell_sub = unique(T.str_sub);
    for ix_cell_sub = 1:length(cell_sub)
        str_sub = cell_sub{ix_cell_sub};
        case_sub = strcmpi(T.str_sub, str_sub);
        vec_run = sort(unique(T.ix_run(case_sub)));
        for ix_vec_run = 1:length(vec_run)
            ix_run = vec_run(ix_vec_run);
            case_run = vec_run(ix_vec_run) == T.ix_run;
            case_file = case_run & case_sub;
            X = load(T.file{case_file});
            ch_names = X.ch_names;
            srate = X.srate;
            
            ix_slice_test = X.ix_slice_test_py + 1;
            for ix_met_psd = 1:length(cell_met_psd)
                met_psd = cell_met_psd{ix_met_psd};
                if isfield(X, met_psd)
                    X_met = double(X.(met_psd)(ix_slice_test, :, :));
                    X_met = X_met * v.mne_scaling;
                    for ix_chan = 1:size(ch_names, 1)
                        Y = squeeze(X_met(:, ix_chan, :))';
                        [psd, f] = welch_custom(Y, srate);
                        if ix_chan == 1
                            psd_mat = nan(length(X.ch_names), length(f), length(ix_slice_test));
                        end
                        psd_mat(ix_chan, :, :) = psd;
                    end
                    T.(strrep(met_psd, 'met_', 'p_')){case_file} = psd_mat;
                end
            end
            
            for ix_met_other = 1:length(cell_met_other)
                met_other = cell_met_other{ix_met_other};
                if isfield(X, met_other)
                    T.(met_other)(case_file) = double(X.(met_other));
                end
            end
            
            ch = cell(1, size(ch_names, 1));
            for ix_ch = 1:length(ch)
                ch{ix_ch} = strtrim(ch_names(ix_ch, :));
            end
            T.ch{case_file} = ch;
            
            if case_file(1)
                v.srate = srate;
                v.f = f;
                v.chlabels = ch;
                T.Properties.UserData = v;
            end
        end
    end
    if all(cellfun(@(x) isequal(x, T.ch{1}), T.ch))
        % all channels for all runs are the same, great.
    else
        % if not all channels are the same... find the interesection of all
        % channels and use that (required for Clayden data).
        ch_intersect = T.ch{1};
        for ix_ch = 2:length(T.ch)
            ch_intersect = intersect(ch_intersect, T.ch{ix_ch});
        end
        T.Properties.UserData.chlabels = ch_intersect;
        for ix_file = 1:height(T)
            ch_local = T.ch{ix_file};
            %             ix_ch_interesct = ismember(ch, ch_intersect);
            for ix_met_psd = 1:length(cell_met_psd)
                met_psd = cell_met_psd{ix_met_psd};
                new_met_psd = strrep(met_psd, 'met_', 'p_');
                if any(strcmpi(new_met_psd, T.Properties.VariableNames))
                    old_p = T.(new_met_psd){ix_file};
                    new_p = nan([length(ch_intersect), size(old_p, 2), size(old_p, 3)]);
                    for ix_ch = 1:length(ch_intersect)
                        new_p(ix_ch, :, :) = ...
                            old_p(strcmpi(ch_intersect{ix_ch}, ch_local), :, :);
                    end
                    T.(new_met_psd){ix_file} = new_p;
                end
            end
            T.ch{ix_file} = ch_intersect;
        end
        
    end
    if do_psd_epoch
        fprintf('Saving...\n');
        save(f_psd_epoch, 'T', 'v_hash');
    end
else
    T = load(f_psd_epoch);
    T = T.T;
end
end