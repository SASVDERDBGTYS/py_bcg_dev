function [d_proc_stft, get_f_filtered, vec_sub] = an_proc_filter(d_project, d_proc_bcg, file_filter, varargin)
%%
d.band = 'none';
d.reref = 'laplace';
d.lowpass = true;
d.highpass = true;
d.d_output = 'proc_filter';
d.proc = getenv_check_empty('R_PROC');
d.overwrite = false;
d.d_overwrite = struct;
d.concatenate_runs = true;
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v_hash = generate_ip_hash(v);
%%
assert(v.highpass, 'HP must be true due to concat');
es = sprintf('_%s_hp%d_lp%d_band_%s_cr%d', v.reref, v.highpass, v.lowpass, v.band, v.concatenate_runs);

%%
d_proc_stft = fullfile(getenv('D_OUT'), d_project, v.proc, v.d_output);
get_f_filtered = @(d, ix_sub) fullfile(d, ...
    sprintf('sub%02d', ix_sub), ...
    sprintf('sub%02d_ep_%s%s', ix_sub, file_filter, es));

%% get a table of all the files
T = get_file_table(d_proc_bcg, 'set', file_filter);

%% merge the datasets
vec_sub = unique(T.ix_sub);
% vec_sub = vec_sub(1:3);
for ix_vec_sub = 1:length(vec_sub)
    EEG = [];
    f_stft = get_f_filtered(d_proc_stft, vec_sub(ix_vec_sub));
    do_filter = generate_check_eeg(f_stft, v.overwrite, v_hash);
    if do_filter
        %%
        %     case_sub_obs = T_obs.ix_sub == vec_sub(ix_vec_sub);
        case_sub = T.ix_sub == vec_sub(ix_vec_sub);
        
        cell_f = T.file(case_sub);
        for ix_cell_f = 1:length(cell_f)
            f_net = cell_f{ix_cell_f};
            %     f_obs = T_obs.file{case_sub_obs};
            
            % special cases - ugly - but things that failed OBS need to be excluded
            if strcmpi(getenv('R_PROC'), 'proc_full')
                if contains(f_net, 'sub15_r02')
                    continue;
                elseif contains(f_net, 'sub17_r02')
                    continue;
                end
            end
            EEG_run = pop_loadset(f_net);
            
            EEG_run = pop_rmbase(EEG_run, [], [1, EEG_run.pnts]);
            
            % HP and LP
            if v.highpass
                EEG_run = pop_eegfiltnew(EEG_run, [], 0.5, 3300, 1, [], 0);
            end
            if v.lowpass
                EEG_run = pop_eegfiltnew(EEG_run, [], 50, 132, 0, [], 0);
            end
            % need to be careful about this... should be the same for all methods
            % EEG = pop_rejchan(EEG, 'elec',[1:EEG.nbchan] ,'threshold',5,'norm','on','measure','kurt');
            
            % probably should do epoch rejection... but that also should be the
            % same for both.
            
            type_noteeg = false(size({EEG_run.chanlocs.labels}));
            noteeg = {'ECG', 't0', 't1', 't2', 'r0', 'r1', 'r2', 'O9', 'O10'};
            for ix_noteeg = 1:length(noteeg)
                type_noteeg = or(type_noteeg, strcmpi({EEG_run.chanlocs.labels}, noteeg{ix_noteeg}));
            end
            type_eeg = not(type_noteeg);
            
            EEG_run.etc.noteeg.data = EEG_run.data(not(type_eeg), :);
            EEG_run.etc.noteeg.chanlocs = EEG_run.chanlocs(not(type_eeg));
            
            EEG_run = pop_select( EEG_run, 'channel', {EEG_run.chanlocs(type_eeg).labels});
            
            
            if strcmpi(v.reref, 'car')
                EEG_run = pop_reref( EEG_run, []);
            elseif strcmpi(v.reref, 'laplace')
                EEG_run = sp_reference_laplace(EEG_run);
            end
            
            if strcmpi(v.band, 'none')
                % do nothing
            elseif strcmpi(v.band, 'spect')
                foi = [0.5:0.2:30];
                fs_ds = 10;
                t_w = 5;
                save_type = 'psd';
                EEG_run = generate_stft(EEG_run, foi, t_w, fs_ds, save_type);
            else
                f_range = get_eeg_band(v.band);
                EEG_run = pop_eegfiltnew(EEG_run, f_range(1), f_range(2));
            end
            
            EEG_run.etc.an_epoch = v;
            EEG_run.etc.hashes.an_epoch = v_hash;
            EEG_run.subject = vec_sub(ix_vec_sub);
            
            EEG_run = eeg_checkset( EEG_run );
            [~, setn] = fileparts(f_stft);
            EEG_run.setname = sprintf('%s_f%02d', setn, ix_cell_f);
            
            [EEG, ~, ~] = eeg_store( EEG, EEG_run, 0 );
            
        end
        
        if v.concatenate_runs
            if strcmpi(v.band, 'spect') && length(EEG)>1, error('not sure if this works');end
            EEG = pop_mergeset( EEG,1:length(EEG), 0);
        end
        [~, setn] = fileparts(f_stft);
        EEG_run.setname = setn;
        pop_saveset(EEG, 'filename', f_stft);
        
    end
end
end