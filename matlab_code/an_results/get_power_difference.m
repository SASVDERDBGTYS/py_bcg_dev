function [T, T_sub] = get_power_difference(T, cell_band, cell_coi,...
    met_comparison, str_ecg, str_sub, op_additional)
if nargin<7
    op_additional = [];
end
D = table;
op = {};
for ix_coi = 1:length(cell_coi)
    coi = cell_coi{ix_coi};
    
    for ix_cell_band = 1:length(cell_band)
        eeg_band = cell_band{ix_cell_band};
        met_oi = {'p_obs', 'p_net', 'p_net_pretrained', 'p_net_trained'};
        for ix_met_oi = 1:length(met_oi)
            met = met_oi{ix_met_oi};
            if any(strcmpi(T.Properties.VariableNames, met))
                [D1, op1] = get_power_difference_core(T, eeg_band, 'p_gar', met, coi, met_comparison, str_ecg);
                D = [D, D1];
                op = [op, op1];
            end
        end
        
    end
end
T = [T, D];
%% group by subjects (i.e. average across runs)
T_sub = rowfun(@(x) unique(x), T, 'InputVariables', {'str_sub'}, ...
    'GroupingVariable', 'str_sub', 'OutputVariableName', {'ignore'});
T_sub.ignore = [];

% T_sub = rowfun(@(x) unique(x), T, 'InputVariables', {'ix_sub'}, ...
%     'GroupingVariable', 'ix_sub', 'OutputVariableName', {'ignore'});
% T_sub.ignore = [];
% str_sub = rowfun(@(x) sprintf('sub%02d', x), T_sub, ...
%     'InputVariables', 'ix_sub', 'OutputVariableName', 'str_sub');
% T_sub = [T_sub, str_sub];

op = [op, op_additional, 'ix_sub'];
for ix_op = 1:length(op)
    op_ = op{ix_op};
    T_sub_ = rowfun(@(x) mean(x), T, 'InputVariables', op_, 'GroupingVariable', str_sub, 'OutputVariableName', op_);
    assert(all(strcmpi(T_sub.(str_sub), T_sub_.(str_sub))), '?')
    T_sub.(op_) = T_sub_.(op_);
end
end
function [B, op] = get_power_difference_core(T, eeg_band, met1, met2, coi, met_comparison, str_ecg)
f_band = get_eeg_band(eeg_band);
case_f = and(T.Properties.UserData.f > f_band(1), T.Properties.UserData.f < f_band(2));

if strcmpi(coi, 'AVG')
    case_eeg = true(size(T.Properties.UserData.chlabels));
else
    case_eeg = strcmpi(coi, T.Properties.UserData.chlabels);
end
case_eeg(strcmpi(T.Properties.UserData.chlabels, str_ecg)) = false;
assert(sum(case_eeg)>0, 'case_eeg has no channels selected for coi: %s', coi);

    function o = summarise_dpsd(x, y)
        x = x{1}(case_eeg, case_f, :);
        y = y{1}(case_eeg, case_f, :);
        
        if strcmpi(met_comparison, 'diff')
            % o = median(x - y, [1, 2, 3]);
            o = median(x - y, [2]); % across f otherwise dominated by crazy ratios
            o = median(o, [3]); % across epochs
            o = median(o, [1]); % across channels
            o = squeeze(o);
            
            
        elseif strcmpi(met_comparison, 'div')
            %             o = median(y ./ x, [1, 2, 3]);
            o = median(y ./ x, [2]); % across f otherwise dominated by crazy ratios
            o = median(o, [3]); % across epochs
            o = median(o, [1]); % across channels
            o = squeeze(o);
            
            
        else
            error('');
        end
    end

if strcmpi(met_comparison, 'diff')
    op = sprintf('%s_%s_%s_minus_%s', coi, eeg_band, met1, met2);
elseif strcmpi(met_comparison, 'div')
    op = sprintf('%s_%s_%s_over_%s', coi, eeg_band, met2, met1);
else
    error('');
end


B = rowfun(@(x, y) summarise_dpsd(x, y), T, 'InputVariables', {met1, met2}, 'OutputVariableName', op);
end