function T = an_psd_time_series(d_project, proc_bcgobs, proc_rs, proc_net, varargin)
%% an_psd: calculated the PSD and PSD differences for OBS, RS (corrupted data after GA removal), BCGNet
% param d_project: the root directory of the project e.g. working_eegbcg
% param proc_bcgobs: a string with the directory relevant to d_project/proc/ 
% param proc_rs: a string with the directory relevant to d_project/proc/ 
% param proc_net: a string with the directory relevant to d_project/proc/
% param varargin: 
% return: table with power and power differences
d.t_welch_window = 4;
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.proc_psd = 'proc_psd'; % a string with the output directory relevant to d_project/proc/
d.overwrite = false;  % overwrite main results of function
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v.source_files = [proc_bcgobs, proc_rs, proc_net]; % so that they get included in the hash
v_hash = generate_ip_hash(v);

%%
[~, arch, ~] = fileparts(proc_net);

f_psd = fullfile(getenv('D_OUT'), d_project, v.proc, v.proc_psd, ['psd_' arch]);
do_psd = generate_check_mat(f_psd, v.overwrite, v_hash);
if do_psd
    d_proc_bcgobs = fullfile(getenv('D_OUT'), d_project, v.proc, proc_bcgobs);
    d_proc_rs = fullfile(getenv('D_OUT'), d_project, v.proc, proc_rs);
    d_proc_net = fullfile(getenv('D_OUT'), d_project, v.proc, proc_net);
    
    T_obs = get_file_table(d_proc_bcgobs, 'set');
    T_rs = get_file_table(d_proc_rs, 'set');
    T_net = get_file_table(d_proc_net, 'set');
    
    T_reference = T_net; % the one that all the others get matched to
    
    T = struct;
    
    cell_sub = unique(T_reference.str_sub);
    ix_file = 0;
    for ix_cell_sub = 1:length(cell_sub)
        str_sub = cell_sub{ix_cell_sub};
        case_sub = strcmpi(T_reference.str_sub, str_sub);
        vec_run = sort(unique(T_reference.ix_run(case_sub)));
        for ix_vec_run = 1:length(vec_run)
            
            ix_file = ix_file + 1;
            
            ix_run = vec_run(ix_vec_run);
            
            case_net_run = T_net.ix_run == vec_run(ix_vec_run);
            case_net_sub = strcmpi(T_net.str_sub, str_sub);
            
            case_rs_run = T_rs.ix_run == vec_run(ix_vec_run);
            case_rs_sub = strcmpi(T_rs.str_sub, str_sub);
            
            case_obs_run = T_obs.ix_run == vec_run(ix_vec_run);
            case_obs_sub = strcmpi(T_obs.str_sub, str_sub);
            
            file_net = T_net.file{case_net_sub & case_net_run};
            file_rs = T_rs.file{case_rs_sub & case_rs_run};
            file_obs = T_obs.file{case_obs_sub & case_obs_run};
            
            EEG_net = pop_loadset(file_net);
            EEG_rs = pop_loadset(file_rs);
            EEG_obs = pop_loadset(file_obs);
            
            EEG_obs = remove_motion_data(EEG_obs);
            EEG_rs = remove_motion_data(EEG_rs);
            
            split_len = EEG_net.srate * v.t_welch_window;
            
            [p_net, fw] = pwelch(EEG_net.data', split_len, [], [], EEG_net.srate);
            [p_gar, ~] = pwelch(EEG_rs.data', split_len, [], [], EEG_rs.srate);
            [p_obs, ~] = pwelch(EEG_obs.data', split_len, [], [], EEG_obs.srate);
            
            T(ix_file).file_rs = file_rs;
            T(ix_file).file_obs = file_obs;
            T(ix_file).file_net = file_net;
            T(ix_file).str_sub = str_sub;
            T(ix_file).ix_run = ix_run;
            T(ix_file).p_net = p_net;
            T(ix_file).p_gar = p_gar;
            T(ix_file).p_obs = p_obs;
%             
%             T(ix_file).p_gar_minus_net = p_gar - p_net;
%             T(ix_file).p_gar_minus_obs = p_gar - p_obs;
%             
%             T(ix_file).p_net_fracof_gar = p_net./p_gar;
%             T(ix_file).p_obs_fracof_gar = p_obs./p_gar;
            
            if ix_file==1
                v.srate = EEG_net.srate;
                v.chanlocs = EEG_net.chanlocs;
                v.chlabels = {EEG_net.chanlocs.labels};
                v.f = fw;
            end
        end
    end
    T = struct2table(T);
    T.Properties.UserData = v;
    save(f_psd, 'T', 'v_hash');
else
    T = load(f_psd);
    T = T.T;
end
end
