function T = an_qrs_statistics(d_project, d_source, d_target, varargin)
%%
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.overwrite = false;  % overwrite main results of function
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
d.met_qrs = 'qrs0';
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_proc_bcgobs = fullfile(getenv('D_OUT'), d_project, v.proc, d_source);
f_qrs = fullfile(getenv('D_OUT'), d_project, v.proc, d_target, 'qrs');
%%
do_qrs = generate_check_mat(f_qrs, v.overwrite);
if do_qrs
    clear T_net;
    T = get_file_table(d_proc_bcgobs, 'set');
    for ix_file = 1:height(T)
        EEG = pop_loadset('filename', T.file{ix_file});
        lat = [EEG.event.latency];
        T.qrs_lat{ix_file} = lat(strcmpi({EEG.event.type}, v.met_qrs));
        T.qrs_delta{ix_file} = diff(T.qrs_lat{ix_file})/EEG.srate;
        T.qrs_std(ix_file) = std(T.qrs_delta{ix_file});
        m_dqrs = median(T.qrs_delta{ix_file});
        T.qrs_outlier(ix_file) = sum((T.qrs_delta{ix_file} < m_dqrs * 0.5) + (T.qrs_delta{ix_file} > m_dqrs * 1.75))/length(T.qrs_delta);
        T.Properties.UserData.an_qrs_statistics.fs = EEG.srate;
        clear EEG;
    end
    save(f_qrs, 'T');
else
    T = load(f_qrs, 'T');
    T = T.T;
end
end