function [t, auc, d_proc_classified, v] = an_classify(d_project, ...
    d_proc_epoch, get_f_epoched, vec_sub, varargin)
% stuff

%% define defaults
d.alg = 'ridgeglmnet';  % other opt: 'stepwiseglm', 'lassoglm', 'TreeBagger'
d.n_folds_inner = 6; % only used if lambda = nan (then it uses these folds to search lambda)
d.alpha = 0.1; % only used if alg = elasticglmnet
d.max_iter = 1000;
d.kfold = 6;
d.lambda = nan;
d.shuffle_labels = false;  % sanity check
d.tau = 0.06;
d.ix_step = 10;
d.str_source = '';
d.figsave = true;
d.proc = getenv_check_empty('R_PROC');
d.d_output = 'proc_classified';
d.overwrite = false;
d.d_overwrite = struct;

%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%%
d_proc_classified = fullfile(getenv('D_OUT'), d_project, v.proc, v.d_output);
p_out_figure = fullfile(d_proc_classified, 'figures');
if ~(exist(p_out_figure, 'dir') == 7), mkdir(p_out_figure); end
%%
for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    ix_sub = vec_sub(ix_vec_sub);
    f_epoched = get_f_epoched(d_proc_epoch, ix_sub);
    EEG = pop_loadset('filename', sprintf('%s.set', f_epoched));
    EEG.data = double(EEG.data);
    
    if ix_vec_sub == 1
        fn = fieldnames(EEG.etc.an_epoch);
        for ix_fn = 1:length(fn)
            if not(isfield(v, fn{ix_fn}))
                v.(fn{ix_fn}) = EEG.etc.an_epoch.(fn{ix_fn});
            end
        end
        
        
        
        v_hash = generate_ip_hash(v);
        
        delta = EEG.srate * v.tau;
        if mod(delta, 2) == 0
            delta = delta + 1;
        end
        vec_tau = [1:size(EEG.data, 2)]';
        vec_delta = -floor(delta/2):+floor(delta/2);
        mat_delta = vec_tau + vec_delta;
        
        auc = struct;
        t = linspace(v.t_low, v.t_hig, length(vec_tau));
        
        if contains(v.alg, 'elastic')
            str_alpha = strrep(sprintf('%0.2f', v.alpha), '.', 'p');
        else
            str_alpha = '';
        end
        if isnan(v.lambda)
            str_lambda = '_Licv';
        elseif v.lambda == 1e-4
            %backwards compat
            str_lambda = '';
        else
            str_lambda = strrep(strrep(sprintf('_L%0.1e', v.lambda), '.', 'p'), '-', 'm');
        end
        if isempty(v.spectral_band)
            str_spectral_band = v.spectral_band;
        else
            str_spectral_band = sprintf('_%s', v.spectral_band);
        end
        % this line should be the same as in an_epoch.m
        es = sprintf('_%s_hp%d_lp%d_rb%d%s%s', v.reref, v.highpass, v.lowpass, v.rmbase, v.str_source, str_spectral_band);
        % this one extends that line for classification:
        es = sprintf('%s_%s%s%s', es, v.alg, str_alpha, str_lambda);
        get_f_classify = @(d, ix_sub) fullfile(d, ...
            sprintf('sub%02d', ix_sub), ...
            sprintf('sub%02d_cl%s', ix_sub, es));
    end
    
    local_rng = true;
    if local_rng
        for ix_tau = 1:v.ix_step:size(mat_delta, 1)
            for ix_cv = 1:v.kfold
                v.seed(ix_tau, ix_cv) = string2hash(...
                    sprintf('%s_%04d_%02d', str_sub, ix_tau, ix_cv));
            end
        end
    else
        v.seed = string2hash(str_sub);
        rng(v.seed);
    end
    
    f_classify = get_f_classify(d_proc_classified, ix_sub);
    if not(exist(fileparts(f_classify), 'dir')==7), mkdir(fileparts(f_classify));end
    do_classify = generate_check_mat(f_classify, v.overwrite, v_hash);
    
    if do_classify
        % replaced this:
        %     y_o = arrayfun(@(ix) any(contains(EEG.epoch(ix).eventtype, v.l_targets)),...
        %         [1:length(EEG.epoch)], 'UniformOutput', true);
        % with this, which is more robust for long epochs (although returns identical results
        % for all analysis done up to now 20-02-08)
        y_o = extract_standards_targets_from_epochs(EEG, v.l_standards, v.l_targets);
        
        y = y_o;
        
        CVO = cvpartition(y, 'KFold', v.kfold);
        auc_sub = nan(v.kfold, size(mat_delta, 1));
        
        % sanity check
        if v.shuffle_labels
            y = y(randperm(length(y)));
        end
        
        for ix_tau = 1:v.ix_step:size(mat_delta, 1)
            win = mat_delta(ix_tau, :);
            
            if any(win<min(vec_tau)) || any(win>max(vec_tau))
                continue
            end
            
            fprintf('%0.2f of %0.2fs for %s\n', t(ix_tau), t(end), str_sub);
            
            X = EEG.data(:, win, :);
            
            assert(v.kfold == CVO.NumTestSets, 'kfold does not much CVO folds?');
            
            for ix_cv = 1:v.kfold
                if local_rng
                    rng(v.seed(ix_tau, ix_cv));
                end
                trIdx_bool = CVO.training(ix_cv);
                teIdx = find(CVO.test(ix_cv));
                trIdx = find(trIdx_bool);
                
                % you have to do the CV slicing before you reshape
                % to avoid autocorrelated times being in different CV slices!
                x_train = X(:, :, trIdx);
                y_train = y(trIdx);
                [x_train, y_train] = reshape_custom(x_train, y_train);
                
                x_test = X(:, :, teIdx);
                y_test = y(teIdx);
                [x_test, y_test] = reshape_custom(x_test, y_test);
                
                if strcmpi(v.alg, 'glmfit')
                    opts = statset(v.alg);
                    opts.MaxIter = v.max_iter;
                    mdl = fitglm(x_train, y_train, 'Distribution', 'binomial', ...
                        'link', 'logit', 'options', opts);
                    y_test_cv_prob = mdl.predict(x_test);
                elseif strcmpi(v.alg, 'stepwiseglm')
                    % this is way too slow to do anything time resolved
                    mdl = stepwiseglm(x_train, y_train, 'constant', 'Distribution', ...
                        'binomial', 'link', 'logit');
                    y_test_cv_prob = mdl.predict(x_test);
                elseif strcmpi(v.alg, 'treebagger')
                    NumTrees = 250;
                    opts = statset(v.alg);
                    mdl = TreeBagger(NumTrees, x_train, y_train, 'Method', 'classification', ...
                        'options', opts);
                    [~, y_test_cv_prob] = predict(mdl, x_test);
                    y_test_cv_prob = y_test_cv_prob(:,2);
                elseif strcmpi(v.alg, 'lassoglm')||strcmpi(v.alg, 'ridgeglm')||strcmpi(v.alg, 'elasticglm')
                    opts = statset('lassoglm');
                    if strcmpi(v.alg, 'lassoglm')
                        alpha_val = 1;
                    elseif strcmpi(v.alg, 'ridgeglm')
                        alpha_val = 0;
                    elseif strcmpi(v.alg, 'elasticglm')
                        opts.alpha = v.alpha;
                    end
                    
                    opts.MaxIter = v.max_iter;
                    
                    if isnan(v.lambda)
                        [B, FitInfo] = lassoglm(x_train, y_train, 'binomial', ...
                            'CV', v.n_folds_inner, 'Link','logit', 'alpha', alpha_val, 'options', opts);
                        ix_lambda = FitInfo.Index1SE;
                    else
                        [B, FitInfo] = lassoglm(x_train, y_train, 'binomial', ...
                            'Link','logit', 'alpha', alpha_val, 'Lambda', v.lambda, ...
                            'options', opts);
                        ix_lambda = 1;
                    end
                    B0 = FitInfo.Intercept(ix_lambda);
                    mdl.coef = [B0; B(:,ix_lambda)];
                    
                    y_test_cv_prob = glmval(mdl.coef, x_test, 'logit');
                elseif strcmpi(v.alg, 'lassoglmnet')||strcmpi(v.alg, 'ridgeglmnet')||strcmpi(v.alg, 'elasticglmnet')
                    opts = glmnetSet;
                    if strcmpi(v.alg, 'lassoglmnet')
                        opts.alpha = 1;
                    elseif strcmpi(v.alg, 'ridgeglmnet')
                        opts.alpha = 0;
                    elseif strcmpi(v.alg, 'elasticglm')
                        opts.alpha = v.alpha;
                    end
                    % I think glmnet is faster than lassoglm, but if you
                    % don't want to use regularization, then might as well
                    % use glmfit.
                    if isnan(v.lambda)
                        gcp_settings = gcp('nocreate');
                        if isempty(gcp_settings)
                            parpool(v.n_folds_inner);
                        end
                        mdl = cvglmnet(x_train, y_train, 'binomial', opts, [], ...
                            v.n_folds_inner, [], true);
                        y_test_cv_prob = glmnetPredict(mdl.glmnet_fit, x_test, [], 'response');
                        [~, ix_lambda] = min(abs(mdl.lambda - mdl.lambda_1se));
                        y_test_cv_prob = y_test_cv_prob(:, ix_lambda);
                    else
                        opts.lambda = v.lambda;
                        mdl = glmnet(x_train, y_train, 'binomial', opts);
                        y_test_cv_prob = glmnetPredict(mdl, x_test, [], 'response');
                    end
                else
                    error('Alg not defined');
                end
                
                [~, ~, ~, auc_val] = perfcurve(y_test, y_test_cv_prob, true);
                
                if not(isempty(y(teIdx)))
                    auc_sub(ix_cv, ix_tau) = auc_val;
                end
            end
            
            % this is just to look at the progress -
            plot(t, nanmean(auc_sub(:, :), 1), '.');
            title(sprintf('%s %s', v.alg, str_sub));ylim([0.45 1.05]);xlim([t(1), t(end)])
            xlabel('Time (s)');
            ylabel('Avg. CV AUC');
            grid on;
            drawnow;
        end
        save(f_classify, 't', 'auc_sub', 'v' ,'v_hash', 'mdl');
        
        f_out_figure = sprintf('%s%s', str_sub, es);
        printForPub(gcf, sprintf('%s', f_out_figure), 'doPrint', v.figsave,...
            'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_figure);
    else
        mat_load = load(f_classify, 't', 'auc_sub', 'v' ,'v_hash');
        t = mat_load.t;
        auc_sub = mat_load.auc_sub;
    end
    auc.(str_sub) = auc_sub;
end

end



function [X, y] = reshape_custom(X, y)
n_channels = size(X, 1);
delta = size(X, 2);
X = reshape(X, n_channels, [])';
y = reshape(y, 1, 1, []);
y = repmat(y, [1, delta, 1]);
y = reshape(y, 1, [])';
end

