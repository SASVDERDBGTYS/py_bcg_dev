function [std_standard, std_target, mea_standard, mea_target, med_standard, med_target, ...
    se2_standard, se2_target, t, eeg_labels, topo_locs] = ...
    an_plot_evoked(d_project, d_proc_epoch, get_f_epoched, vec_sub, varargin)
% stuff

%% define defaults
% d.alg = 'glmfit';  % other opt: 'stepwiseglm', 'lassoglm', 'TreeBagger'
% d.max_iter = 1000;
% d.kfold = 10;
% d.lambda = nan;
% d.shuffle_labels = false;  % sanity check
d.t_baseline = nan;
d.str_source = '';
d.overwrite = false;
d.d_overwrite = struct;
d.proc = getenv_check_empty('R_PROC');
d.figgen = true;
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v_hash = generate_ip_hash(v);

%%
% d_proc_classified = fullfile(getenv('D_OUT'), d_project, v.proc, 'proc_classified');

%%
% vec_sub = vec_sub(1);  % TEMPORARY!!!!

for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    f_epoched = get_f_epoched(d_proc_epoch, vec_sub(ix_vec_sub));
    EEG = pop_loadset('filename', sprintf('%s.set', f_epoched));
    EEG = pop_chanedit(EEG, 'lookup', ...
        fullfile(getenv('D_EEGLAB'), 'plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc'));
    
    theta = -pi/2;
    topo = [[EEG.chanlocs.X]',[EEG.chanlocs.Y]']*[cos(theta),-sin(theta);sin(theta),cos(theta)]*1.0;
    topoX = topo(:,1);
    topoY = topo(:,2);
    topoW = 0.08;
    topoH = 0.08;
    %prototypical subject
    EEG.etc.topo.topoX = topoX;
    EEG.etc.topo.topoY = topoY;
    EEG.etc.topo.topoW = topoW;
    EEG.etc.topo.topoH = topoH;
    
    topo_locs = EEG.etc.topo;
    
    if ix_vec_sub == 1
        v.l_targets = EEG.etc.an_epoch.l_targets;
        v.l_standards = EEG.etc.an_epoch.l_standards;
        v.t_low = EEG.etc.an_epoch.t_low;
        v.t_hig = EEG.etc.an_epoch.t_hig;
        
        vec_tau = [1:size(EEG.data, 2)]';
        t = linspace(v.t_low, v.t_hig, length(vec_tau));
        case_baseline = and(t>v.t_baseline(1), t<v.t_baseline(end));
        
        eeg_labels = {EEG.chanlocs.labels};
        
        mea_standard = struct;
        mea_target = struct;
        std_standard = struct;
        std_target = struct;
        se2_standard = struct;
        se2_target = struct;
    end
    
    % recover some parameters used to generate epochs
    
    % replaced this:
    %     y_o = arrayfun(@(ix) any(contains(EEG.epoch(ix).eventtype, v.l_targets)),...
    %         [1:length(EEG.epoch)], 'UniformOutput', true);
    % with this, which is more robust for long epochs (although returns identical results
    % for all analysis done up to now 20-02-08)
    y_o = extract_standards_targets_from_epochs(EEG, v.l_standards, v.l_targets);
    
    if all(isinf(v.t_baseline))
        %BASELINE DIVISISION
        data_baseline = nanmedian(EEG.data(:, :, :), [2, 3]);
        EEG.data = (EEG.data - data_baseline);
        EEG.data = EEG.data./data_baseline;
    elseif not(all(isnan(v.t_baseline)))
        %BASELINE DIVISISION
        data_baseline = nanmedian(EEG.data(:, case_baseline, :), 2);
        EEG.data = (EEG.data - data_baseline);
        EEG.data = EEG.data./data_baseline;
    end
    
    eeg_standard = EEG.data(: , :, not(y_o));
    mea_standard.(str_sub) = nanmean(eeg_standard, 3);
    med_standard.(str_sub) = nanmedian(eeg_standard, 3);
    std_standard.(str_sub) = nanstd(eeg_standard, 0, 3);
    se2_standard.(str_sub) = 2 * std_standard.(str_sub) ./ sqrt(sum(isfinite(eeg_standard), 3));
    
    eeg_target = EEG.data(: , :, y_o);
    mea_target.(str_sub) = nanmean(eeg_target, 3);
    med_target.(str_sub) = nanmedian(eeg_target, 3);
    std_target.(str_sub) = nanstd(eeg_target, 0, 3);
    se2_target.(str_sub) = 2 * std_target.(str_sub) ./ sqrt(sum(isfinite(eeg_target), 3));
    
    
end
end