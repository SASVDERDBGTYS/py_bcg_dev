function [d_proc_epoch, get_f_epoched, vec_sub, get_f_merged] = an_epoch(d_project, d_proc_bcg, varargin)
%%
d.l_standards = 'S  8';
d.l_targets = 'S 32';
d.t_low = -0.5;
d.t_hig = 1.5;
d.reref = 'laplace';
d.highpass = true;
d.lowpass = true;
d.rmbase = false;
d.str_source = '';
d.str_merged = '';
d.spectral_band = ''; % e.g. d.spectral_band = 'alpha_beta1_beta2'; or 'alpha_square'. Order matters!
d.d_output = 'proc_epoch';
d.proc = getenv_check_empty('R_PROC');
d.overwrite = false;
d.d_overwrite = struct;
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
v_hash = generate_ip_hash(v);

%%
if isempty(v.spectral_band)
    str_spectral_band = v.spectral_band;
else
    str_spectral_band = sprintf('_%s', v.spectral_band);
    cell_spectral_band = strsplit(v.spectral_band, '_');
end
%%
assert(v.t_low<0,'t_low must be < 0');
assert(not(and(v.highpass, v.rmbase)), 'You either need to higpass or remove baseline, or both.')
% this line should be identical in an_classify
es = sprintf('_%s_hp%d_lp%d_rb%d%s%s', v.reref, v.highpass, v.lowpass, v.rmbase, v.str_source, str_spectral_band);

%%

d_proc_epoch = fullfile(getenv('D_OUT'), d_project, v.proc, v.d_output);
get_f_merged = @(d, ix_sub) fullfile(d, ...
    sprintf('sub%02d', ix_sub), ...
    sprintf('sub%02d_all%s', ix_sub, v.str_merged));
get_f_epoched = @(d, ix_sub) fullfile(d, ...
    sprintf('sub%02d', ix_sub), ...
    sprintf('sub%02d_ep%s', ix_sub, es));
%% get a table of all the files
T = sp_get_set_files(d_proc_bcg);

%% merge the datasets
vec_sub = unique(T.ix_sub);
for ix_vec_sub = 1:length(vec_sub)
    overwrite_merge = v.overwrite;
    if v.overwrite
        warning('OVERWRITE DOES NOT OVERWRITE THE MERGE!!! - if you want to do that, do it manually!');
        overwrite_merge = false;
    end
    f_merged = get_f_merged(d_proc_bcg, vec_sub(ix_vec_sub));
    do_merge = generate_check_eeg(f_merged, overwrite_merge);
    
    if do_merge
        case_sub = T.ix_sub == vec_sub(ix_vec_sub);
        T_sub = T(case_sub, :);
        vec_run = unique(T_sub.ix_run);
        clear EEG_merge;
        [EEG_merge] = eeglab;close;
        avg_end = [];
        for ix_vec_run = 1:length(vec_run)
            case_run = T.ix_run == vec_run(ix_vec_run);
            case_merged = case_run & case_sub;
            f = T.file{case_merged};
            
            EEG = pop_loadset('filename', f);
            avg_start = mean(EEG.data(:, 1:round(EEG.srate * 0.15)), 2);
            if ix_vec_run > 1
                EEG.data = EEG.data - avg_start + avg_end;
            end
            avg_end = mean(EEG.data(:, EEG.pnts - round(EEG.srate * 0.15):EEG.pnts), 2);
            
            n_event = length(EEG.event);
            EEG.event(n_event+1).type = sprintf('startrun_%d', vec_run(ix_vec_run));
            EEG.event(n_event+1).latency = 1;
            n_event = length(EEG.event);
            EEG.event(n_event+1).type = sprintf('endrun_%d', vec_run(ix_vec_run));
            EEG.event(n_event+1).latency = EEG.pnts;
            [EEG_merge, ~, ~] = eeg_store( EEG_merge, EEG, 0 );
        end
        EEG_merge = pop_mergeset( EEG_merge, 1:length(vec_run), 0);
        pop_saveset(EEG_merge, 'filename', f_merged);
    end
end
%%
% L = [EEG_merge.event(or(strcmpi({EEG.event.type}, 'S  8'), strcmpi({EEG_merge.event.type}, 'S 32'))).latency];
% D = diff(EEG_merge.times(round(L)))/1000;
% D(D>4) = [];
% histogram(D);
%%
vec_sub = unique(T.ix_sub);
%%
for ix_vec_sub = 1:length(vec_sub)
    f_epoched = get_f_epoched(d_proc_epoch, vec_sub(ix_vec_sub));
    
    if not(exist(fileparts(f_epoched), 'dir')==7), mkdir(fileparts(f_epoched));end
    do_epoch = generate_check_eeg(f_epoched, v.overwrite, v_hash);
    
    if do_epoch
        f_merged = get_f_merged(d_proc_bcg, vec_sub(ix_vec_sub));
        EEG = pop_loadset('filename', sprintf('%s.set', f_merged));
        
        % remove motion data
        EEG = pop_select(EEG, 'nochannel', {'r0', 'r1', 'r2', 't0', 't1', 't2', 'ECG'});
        
        EEG = pop_rmbase(EEG, [], [1, EEG.pnts]);
        
        % HP and LP
        if v.highpass
            EEG = pop_eegfiltnew(EEG, [], 0.5, 3300, 1, [], 0);
        end
        if v.lowpass
            EEG = pop_eegfiltnew(EEG, [], 50, 132, 0, [], 0);
        end
        % need to be careful about this... should be the same for all methods
        % EEG = pop_rejchan(EEG, 'elec',[1:EEG.nbchan] ,'threshold',5,'norm','on','measure','kurt');
        
        % probably should do epoch rejection... but that also should be the
        % same for both.
        
        if strcmpi(v.reref, 'car')
            EEG = pop_reref( EEG, []);
        elseif strcmpi(v.reref, 'laplace')
            EEG = sp_reference_laplace(EEG);
        elseif strcmpi(v.reref, 'hjorth')
            ch_C3 = strcmpi({EEG.chanlocs.labels}, 'C3');
            ch_C3_ref = strcmpi({EEG.chanlocs.labels}, 'P3')|...
                strcmpi({EEG.chanlocs.labels}, 'F3')|...
                strcmpi({EEG.chanlocs.labels}, 'T7')|...
                strcmpi({EEG.chanlocs.labels}, 'Cz');
            assert(sum(ch_C3_ref)==4, 'not all ch in ref')
            EEG.data(ch_C3, :) = EEG.data(ch_C3, :) - mean(EEG.data(ch_C3_ref, :), 1);
            ch_C4 = strcmpi({EEG.chanlocs.labels}, 'C4');
            ch_C4_ref = strcmpi({EEG.chanlocs.labels}, 'P4')|...
                strcmpi({EEG.chanlocs.labels}, 'F4')|...
                strcmpi({EEG.chanlocs.labels}, 'T8')|...
                strcmpi({EEG.chanlocs.labels}, 'Cz');
            assert(sum(ch_C4_ref)==4, 'not all ch in ref')
            EEG.data(ch_C4, :) = EEG.data(ch_C4, :) - mean(EEG.data(ch_C4_ref, :), 1);
            EEG = pop_select( EEG, 'channel', {'C3', 'C4'});
            EEG = pop_select( EEG, 'channel', {'C3', 'C4'});
        elseif strcmpi(v.reref, 'none')
            % whatever the original is
        else
            EEG = pop_reref(EEG, v.reref);
        end
        
        if contains(v.spectral_band, 'stft') && contains(v.spectral_band, 'ersp')
            assert(not(v.rmbase), 'rmbase should be off for stft');
            
            % if we are doing stft epoch first... otherwise you need to
            % keep fs high -> massive datasets
            EEG = pop_epoch( EEG, {v.l_standards, v.l_targets},...
                [v.t_low, v.t_hig]);
            
            if contains(v.spectral_band, 'pls')
                % phase locked subtraction
                cell_spectral_band(strcmpi(cell_spectral_band, 'pls')) = [];
                y_o = extract_standards_targets_from_epochs(EEG, v.l_standards, v.l_targets);
                EEG.data(:, :, y_o) = EEG.data(:, :, y_o) - median(EEG.data(:, :, y_o), 3);
                EEG.data(:, :, not(y_o)) = EEG.data(:, :, not(y_o)) - median(EEG.data(:, :, not(y_o)), 3);
            end
            
            % theses should all be in v of course...
            foi = [1:30];
            fs_ds = 50;
            t_w = 0.5;
            save_type = 'psd';
            EEG = generate_stft(EEG, foi, t_w, fs_ds, save_type);
        elseif not(isempty(v.spectral_band))
            % btw, this can take e.g.
            % 'alpha' or 'alpha_ersp' or 'alpha_beta' etc.
            assert(not(v.rmbase), 'rmbase should be off for bands');
            
            if contains(v.spectral_band, 'notch')
                if strcmpi(getenv('R_PROC'), 'proc_full')
                    EEG = pop_eegfiltnew(EEG, 19, 21, 1650, 1, [], 0);
                    cell_spectral_band(strcmpi(cell_spectral_band, 'notch')) = [];
                else
                    error('Only written for proc full');
                end
            end
            
            t_epoch_extend = 3; % extension so that we can epoch then pls then filter
            ix_epoch_extend = t_epoch_extend*EEG.srate;
            EEG = pop_epoch( EEG, {v.l_standards, v.l_targets},...
                [v.t_low - t_epoch_extend, v.t_hig + t_epoch_extend]);
            
            if contains(v.spectral_band, 'pls')
                % phase locked subtraction
                cell_spectral_band(strcmpi(cell_spectral_band, 'pls')) = [];
                y_o = extract_standards_targets_from_epochs(EEG, v.l_standards, v.l_targets);
                EEG.data(:, :, y_o) = EEG.data(:, :, y_o) - median(EEG.data(:, :, y_o), 3);
                EEG.data(:, :, not(y_o)) = EEG.data(:, :, not(y_o)) - median(EEG.data(:, :, not(y_o)), 3);
            end
            
            EEG = generate_spectral_bands(EEG, cell_spectral_band);
            
            %reverse extension
            EEG.data = EEG.data(:, ix_epoch_extend + 1:end - ix_epoch_extend, :);
            EEG = eeg_checkset( EEG );
            
        elseif false % old version - filtered first so could not pls
            % btw, this can take e.g.
            % 'alpha' or 'alpha_ersp' or 'alpha_beta' etc.
            assert(not(v.rmbase), 'rmbase should be off for bands');
            
            if contains(v.spectral_band, 'notch')
                if strcmpi(getenv('R_PROC'), 'proc_full')
                    EEG = pop_eegfiltnew(EEG, 19, 21, 1650, 1, [], 0);
                    cell_spectral_band(strcmpi(cell_spectral_band, 'notch')) = [];
                else
                    error('Only written for proc full');
                end
            end
            if contains(v.spectral_band, 'pls')
                % phase locked subtraction
                error('Not implemented because I epoch after the spectral band generation...');
            end
            
            EEG = generate_spectral_bands(EEG, cell_spectral_band);
            EEG = pop_epoch( EEG, {v.l_standards, v.l_targets},...
                [v.t_low, v.t_hig]);
        else
            EEG = pop_epoch( EEG, {v.l_standards, v.l_targets},...
                [v.t_low, v.t_hig]);
        end
        
        if v.rmbase
            EEG = pop_rmbase( EEG, [], [1, -v.t_low*EEG.srate]);
        end
        
        EEG.etc.an_epoch = v;
        EEG.etc.hashes.an_epoch = v_hash;
        EEG.subject = vec_sub(ix_vec_sub);
        
        EEG = eeg_checkset( EEG );
        [~, setn] = fileparts(f_epoched);
        EEG.setname = setn;
        pop_saveset(EEG, 'filename', f_epoched);
    end
end
end


function EEG_spect = generate_spectral_bands(EEG, cell_spectral_band)
%% takes in normal EEG and returns band power concatenated in channel dimension
EEG_spect = EEG;
EEG_spect.data = [];
EEG_spect.chanlocs = [];

%these are ugly ways to overload spectral band info, but I don't want to
%modify the opt to the main function because everything else builds on this
if contains('square', cell_spectral_band)
    do_square = true;
    cell_spectral_band(strcmpi(cell_spectral_band, 'square')) = [];
else
    do_square = false;
end
if contains('ersp', cell_spectral_band)
    do_ersp = true;
    cell_spectral_band(strcmpi(cell_spectral_band, 'ersp')) = [];
else
    do_ersp = false;
end
if contains('test', cell_spectral_band)
    cell_spectral_band(strcmpi(cell_spectral_band, 'test')) = [];
    error('why is this happening?');
end
for ix_band = 1:length(cell_spectral_band)
    f_range = get_eeg_band(cell_spectral_band{ix_band});
    EEG_band = pop_eegfiltnew(EEG, f_range(1), f_range(2));
    % "If xr is a matrix, then hilbert finds the analytic signal
    % corresponding to each column."
    for ix_epoch = 1:size(EEG_band.data, 3) % it's ok if this is == 1
        EEG_band.data(:, :, ix_epoch) = transpose(hilbert(EEG_band.data(:, :, ix_epoch).'));
    end
    if do_square
        EEG_band.data = EEG_band.data.^2;
    else
        EEG_band.data = abs(EEG_band.data);
    end
    
    if do_ersp
        % nothing
        assert(length(cell_spectral_band) == 1, 'for ersp band must be single');
    else
        % this is for classification... then you want to be
        % able to stack features, hence we rename the channels...
        EEG_band.data = EEG_band.data - mean(EEG_band.data, 2);
        EEG_band.data = EEG_band.data./std(EEG_band.data, 0, 2);
        for ix_ch = 1:length(EEG_band.chanlocs)
            EEG_band.chanlocs(ix_ch).labels = sprintf('%s_%s', ...
                EEG_band.chanlocs(ix_ch).labels, cell_spectral_band{ix_band});
            EEG_band.chanlocs(ix_ch).type = cell_spectral_band{ix_band};
        end
    end
    
    EEG_spect.data = cat(1, EEG_spect.data, EEG_band.data);
    EEG_spect.chanlocs = cat(2, EEG_spect.chanlocs, EEG_band.chanlocs);
end
EEG_spect.nbchan = size(EEG_spect.data, 1);
end
