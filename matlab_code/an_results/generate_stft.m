function EEG = generate_stft(EEG, foi, t_w, fs_ds, save_type)
%% takes in normal EEG and returns band power concatenated in channel dimension
w = (EEG.srate * t_w);
win = hamming(w);
ov = w - round(EEG.srate/fs_ds);
ch = {EEG.chanlocs.labels};
for ix_ch = 1:length(ch)
    fprintf('%s ', ch{ix_ch});
    if mod(ix_ch, 15) == 0
        fprintf('\n');
    end
    for ix_trial = 1:EEG.trials
        x = EEG.data(ix_ch, :, ix_trial);
        [s_, f_, t_, p_] = spectrogram(x, win, ov, foi, EEG.srate);
        if not(all(isfinite(x)))
            % otherwise things get very slow
            p_ = nan(size(p_));
            s_ = nan(size(s_));
        end
        if and(ix_ch == 1, ix_trial == 1)
            EEG.spect_data = nan([size(s_), length(ch), EEG.trials]);
            ix_t = round(interp1((EEG.times - EEG.times(1))/1000, 1:EEG.pnts, t_));
            EEG.spect_times = EEG.times(ix_t);
            EEG.spect_frequencies = f_;
            EEG.spect_srate = fs_ds;
            EEG.etc.spect.t_w = t_w;
        end
        if strcmpi(save_type, 'psd')
            EEG.spect_data(:, :, ix_ch, ix_trial) = p_;
        else
            EEG.spect_data(:, :, ix_ch, ix_trial) = s_;
        end
    end
end
end