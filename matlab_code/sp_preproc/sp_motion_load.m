function mdat = sp_motion_load(p_motion, f_motion)
% assert(exist(mcf, 'file')==2, 'mcf file does not exist?');
% with >2019a there is a readmatrix function that could be used instead...
delimiter = ' ';
formatSpec = '%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(fullfile(p_motion, f_motion),'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string',  'ReturnOnError', false);
fclose(fileID);
mdat = table(dataArray{1:end-1}, 'VariableNames', {'t0','t1','t2','r0','r1','r2'});
end