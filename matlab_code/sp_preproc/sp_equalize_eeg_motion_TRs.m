function ev = sp_equalize_eeg_motion_TRs(mdat, ev)
% what is this used for?
if size(mdat, 1) ~= size(ev, 1)
    for ix = 1:size(ev,1)
        d = diff(ev);
        id = find(d < median(d)/2);
        if ~isempty(id)
            ev(id + 1) = [];
        end
    end
    
    assert(size(mdat, 1) == size(ev, 1), 'Motion TRs and EEG TRs cant be made to match!');
end
        
        