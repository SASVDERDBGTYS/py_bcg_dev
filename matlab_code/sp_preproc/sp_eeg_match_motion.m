function M = sp_eeg_match_motion(mdat, EEG, ev)
ev = sp_equalize_eeg_motion_TRs(mdat, ev);
assert(size(mdat, 1) == size(ev, 1), 'Motion TRs and EEG TRs do not match!');

M = nan(size(mdat, 2), EEG.pnts);
for ix_M = 1:size(M, 1)
    x = EEG.times(ev);
    y = table2array(mdat(:, ix_M));
    extrap_value = nan;
    M(ix_M, :) = interp1(x(:)', y(:)', EEG.times(:)', ...
        'makima', extrap_value);
    
    %deal with the edges
    M(ix_M, 1:ev(1)) = M(ix_M, ev(1));
    M(ix_M, ev(end):end) = M(ix_M, ev(end));
end
end
