function sp_count_markers(d_project, varargin)
%%
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%%
d.overwrite = false;
d.d_overwrite = struct;
d.testing_mode = false;
d.proc = getenv_check_empty('R_PROC');
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'log', 'rmga_failure_log.txt');
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_raw = fullfile(getenv('D_DATA'), d_project, 'raw');
%%

% Multiple subjects
% subs = string(extractBefore(extractAfter(glob(fullfile(d_raw ,'/*_*')), fullfile(d_raw, '/')), '/'));
% for ix = 1:size(subs)
%    str_sub = subs(ix)

file_list = glob(fullfile(d_raw, '**_[0-9].vhdr'));
file_list = file_list(not(contains(file_list, 'data_renaming')));

for ix_file = 1:length(file_list)
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    [~, f_for_rng] = fileparts(p_raw_eeg);
    rng(string2hash([f_for_rng, f_raw_eeg]));
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
    %%
    [~,ddd, ~] = fileparts(p_raw_eeg);
    m = unique({EEG.event.type});
    diary on;
    fprintf('%s/%s\n', ddd, f_raw_eeg);
    fprintf('This run is %0.1fs\n', EEG.times(end)/1000)
    for ix_m = 1:length(m)
        n = sum(strcmpi({EEG.event.type}, m{ix_m}));
        fprintf('%s: %d\t', m{ix_m}, n)
    end
    fprintf('\n\n');
    diary off;
    %%
end
