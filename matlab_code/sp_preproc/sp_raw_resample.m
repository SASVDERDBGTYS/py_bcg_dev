function sp_raw_resample(d_project, varargin)
%% Idea was to train and train a network directly on this raw data
% maybe with a diff operation somewhere. Probably need to wait for version
% two to avoid minefields though...
%%
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%%
d.str_grad = 'S128';
d.str_ecg = 'ECG';
d.figsave = true;
d.overwrite = false;
d.d_overwrite = struct;
d.testing_mode = false;
d.fs_rs = 500;
d.slice_boundary = 0.2;  % Add periods of specified time around the slice
d.proc = getenv_check_empty('R_PROC');
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'proc_ga', 'failure_log.txt');
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_raw = fullfile(getenv('D_DATA'), d_project, 'raw');
d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);
%%
file_list = glob(fullfile(d_raw, '**_[0-9].vhdr'));
file_list = file_list(not(contains(file_list, 'data_renaming')));
for ix_sub = 1:length(file_list)
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_sub});
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    [~,folder_raw_eeg,~] = fileparts(p_raw_eeg);
    ix_run = extractBetween(f_raw_eeg, '_', '.');
    ix_run = str2double(ix_run{end});
    str_sub = lower(extractAfter(folder_raw_eeg,'_'));
    
    % SPECIAL CASE FOR SUBJECT 35!!!
    % v. ugly to have it here - but oh well
    if strcmpi(str_sub, 'sub35')
        ix_run = ix_run - 1;
    end
        
    f_out_rawrs =  sprintf('%s_r%02d_rawrs', str_sub, ix_run);
    d_out_rawrs = fullfile(d_proc, 'proc_rawrs', str_sub, f_out_rawrs);
    do_rawrs = generate_check_eeg(d_out_rawrs, v.overwrite);
    if do_rawrs
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, f_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);

        EEG = pop_resample(EEG, v.fs_rs);
        
        % now slice the data
        % Obtain the data between the first and the last TR markers
        EEG.etc.orig_event = EEG.event;
        grad_lat = [EEG.event(strcmpi({EEG.event.type}, v.str_grad)).latency];
        ix_lat = grad_lat([1, end]);
        
        slice_from = floor(ix_lat(1)-EEG.srate*v.slice_boundary);
        slice_to = ceil(ix_lat(end)+EEG.srate*v.slice_boundary);
        EEG = pop_select( EEG, 'point', [slice_from, slice_to]);
        %
        
        EEG.etc.sp_raw_resample = v; %store the processing conditions
        EEG.setname = f_out_rawrs;
        EEG = eeg_checkset(EEG);
        pop_saveset(EEG, 'filename', d_out_rawrs);
    end
end
end