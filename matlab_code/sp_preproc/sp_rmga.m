function sp_rmga(d_project, varargin)
%%
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%%
d.str_grad = 'S128';
d.str_ecg = 'ECG';
d.n_expected_TR = 150;
d.figsave = true;
d.overwrite = false;
d.d_overwrite = struct;
d.testing_mode = false;
d.proc = getenv_check_empty('R_PROC');
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'log', 'rmga_failure_log.txt');
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_raw = fullfile(getenv('D_DATA'), d_project, 'raw');
d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);
%%

% Multiple subjects
% subs = string(extractBefore(extractAfter(glob(fullfile(d_raw ,'/*_*')), fullfile(d_raw, '/')), '/'));
% for ix = 1:size(subs)
%    str_sub = subs(ix)

file_list = glob(fullfile(d_raw, '**_[0-9].vhdr'));
file_list = file_list(not(contains(file_list, 'data_renaming')));

for ix_file = 1:length(file_list)
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    [~, f_for_rng] = fileparts(p_raw_eeg);
    rng(string2hash([f_for_rng, f_raw_eeg]));
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    [~,folder_raw_eeg,~] = fileparts(p_raw_eeg);
    ix_run = extractBetween(f_raw_eeg, '_', '.');
    ix_run = str2double(ix_run{end});
    str_sub = lower(extractAfter(folder_raw_eeg,'_'));
    
    % SPECIAL CASE FOR SUBJECT 35!!!
    % v. ugly to have it here - but oh well
    if strcmpi(str_sub, 'sub35')
        ix_run = ix_run - 1;
    end
    
    f_out_ga_figure_root = sprintf('%s_r%02d_rmga', str_sub, ix_run);
    p_out_ga_figure = fullfile(d_proc, 'proc_ga', str_sub, 'figures');
    if ~(exist(p_out_ga_figure, 'dir') == 7), mkdir(p_out_ga_figure); end
    
    f_out_ga =  sprintf('%s_r%02d_rmga', str_sub, ix_run);
    d_out_ga = fullfile(d_proc, 'proc_ga', str_sub, f_out_ga);
    do_rmga = generate_check_eeg(d_out_ga, v.overwrite);
    if do_rmga
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, f_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        EEG.setname = f_out_ga; % this should actually be placed just before the save, since eeglab modifies it
        
        n_markers_pre = sum(strcmpi({EEG.event.type}, v.str_grad));
        h_f = figure(11);clf;
        figure(h_f);h_s1 = subplot(2, 1, 1);cla;hold on;
        sp_eeg_plot_markers(EEG, 1);
        title(sprintf('Pre GA removal, %s:%d', v.str_ecg, n_markers_pre));
        printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
            'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
        
        if v.testing_mode
            warning('In testing mode, keeping only two channels for speed!');
            EEG = pop_select( EEG, 'channel', {EEG.chanlocs(1).labels});
        end
        
        ch_ecg = find(strcmpi({EEG.chanlocs.labels}, v.str_ecg));
        ch_non_eeg = ch_ecg;
        
        % Fix the TR markers and GA remove
        [EEG_ga, v.settings] = sp_eeg_fix(EEG, v.str_grad, ch_non_eeg, v.f_failure_log(v.proc));
        
        if isnan(EEG_ga.data)
            str_failure = sprintf(...
                'There was a failure in sp_eeg_fix for %s, run %s\n', str_sub, ix_run);
            sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), sprintf('%s.set',d_out_ga));
            continue;
        end
        
        n_markers = sum(strcmpi({EEG_ga.event.type}, v.str_grad));
        
        figure(h_f);h_s2 = subplot(2, 1, 2);cla;hold on;
        sp_eeg_plot_markers(EEG_ga, 1);
        title(sprintf('Post GA removal, %s:%d', v.str_ecg, n_markers));
        linkaxes([h_s1, h_s2], 'x');
        printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
            'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
        
        if not(v.n_expected_TR == n_markers), ...
                str_failure = sprintf(...
                'Unexpected number of gradient markers?!\n For %s, run %d\n', str_sub, ix_run);
            sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), sprintf('%s.set',d_out_ga));
        else
            EEG_ga.etc.sp_rmga = v; %store the processing conditions
            EEG_ga = eeg_checkset(EEG_ga);
            pop_saveset(EEG_ga, 'filename', d_out_ga);
        end
    end
end
end
