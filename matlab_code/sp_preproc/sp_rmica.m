function sp_rmica(d_project, varargin)
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%%
d.str_grad = 'S128';
d.str_ecg = 'ECG';
d.nbchan = 64; % this should be loaded from data (see sp_rmga)
d.ch_rem = 42; % and manually setting these two numbers make me uncomfortable...
warning('Channel info is hard coded! - this should be fixed in version 2');
d.ch_rem_mc = 2;
d.addmc = true;
d.slice_boundary = 0.2;  % Add periods of specified time around the slice
d.figgen = false;
d.overwrite = false;
d.d_overwrite = struct;
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'log', 'rmbcg_failure_log.txt');
d.met_str = 'qrs0'; % just a label for the markers
d.fmrib_pas_method = 'obs'; %or median
d.proc = getenv_check_empty('R_PROC');
d.causal_method = 'none';
d.ica_maxsteps = 2000;
d.ica_stop = 1e-7;
d.overwrite_ica = false;
%% Parse input
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_proc_in = fullfile(getenv('D_DATA'), d_project, v.proc);
d_proc_out = fullfile(getenv('D_OUT'), d_project, v.proc);
%% With Motion Data
if v.addmc
    sp_resample(d_project, 'f_failure_log', v.f_failure_log, 'proc', v.proc, 'causal_method', v.causal_method);
    if not(strcmpi(v.causal_method, 'none'))
        warning('Causal method requested so not continuing to do OBS');
        return
    end
    file_list = glob(fullfile(d_proc_in, 'proc_rs', '**_r0[0-9]_rs.set'));
    if isempty(file_list)
        assert( ~isempty(file_list), 'Add MC script fails!');
    end
    
    for ix_file = 1:length(file_list)
        
        [p_rs, f_rs, ext_rs] = fileparts(file_list{ix_file});
        rng(string2hash(f_rs));
        
        f_rs_orig = f_rs;
        f_rs = sprintf('%s%s', f_rs, ext_rs);
        [~,folder_rs,~] = fileparts(p_rs);
        ix_run = extractBetween(f_rs, '_r0', '_rs.');
        ix_run = str2double(ix_run{end});
        str_sub = lower(folder_rs);
        
        %         if or(strcmp(str_sub, 'sub17'), strcmp(str_sub, 'sub25'))
        %             continue
        %         end
        
        p_rmbcg = fullfile(d_proc_out, 'proc_bcgica', str_sub);
        f_rmbcg = sprintf('%s_r%02d_ica', str_sub, ix_run);
        if ~(exist(p_rmbcg, 'dir') == 7), mkdir(p_rmbcg); end
        
        pfe_out_ica = fullfile(p_rmbcg, [f_rmbcg, '.set']);
        
        
        
        if or(not(exist(pfe_out_ica, 'file') == 2), v.overwrite)
            
            % I think the extension here should be removed - but not this
            % far yet.
            if not(exist(fullfile(p_rs, [f_rs_orig, '.fdt']), 'file') == 2)
                str_failure = sprintf(...
                    'Missing rs.fdt file for %s, run %s\n', str_sub, ix_run);
                sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), pfe_out_ica);
                continue;
            end
            
            fprintf('Loading:\n%s\n', f_rs);
            EEG_mc = pop_loadset(f_rs, p_rs);
            EEG_mc.times = double(EEG_mc.times);
            EEG_mc.data = double(EEG_mc.data);
            EEG_mc.setname = f_rmbcg;
            %% BCG Removal
            EEG_nobcg = EEG_mc;
            
            % Remove the motion data temporarily
            motion_data = EEG_nobcg.data(v.nbchan + 1:EEG_nobcg.nbchan, :);
            EEG_nobcg.data = EEG_nobcg.data(1:v.nbchan, :);
            
            motion_nbchan = EEG_nobcg.nbchan;
            EEG_nobcg.nbchan = v.nbchan;
            
            motion_chanlocs = EEG_nobcg.chanlocs(1, v.nbchan + 1: size(EEG_nobcg.chanlocs, 2));
            EEG_nobcg.chanlocs = EEG_nobcg.chanlocs(1, 1:v.nbchan);
            
            % DO ICA REMOVAL HERE
            %% 0. make sure pre-ICA stuff is OK. e.g. filtering.
            %% 1. take out ECG
            EEG_nobcg_noecg = pop_select( EEG_nobcg, 'nochannel', {v.str_ecg});
            EEG_ecg = pop_select( EEG_nobcg, 'channel', {v.str_ecg});
            %% 2. do ICA
            get_f_ica_w = @(d, str_sub_) fullfile(d, ...
                sprintf('%s_r%02d_ica_w', str_sub_, ix_run));
            f_ica_w = get_f_ica_w(p_rmbcg, str_sub);
            do_ica_w = generate_check_mat(f_ica_w, v.overwrite_ica);
            
            if do_ica_w
                EEG_ica = pop_runica(EEG_nobcg_noecg,'icatype','binica', 'extended',1,'maxsteps',v.ica_maxsteps,'stop',v.ica_stop);
                EEG_ica = eeg_checkset( EEG_ica );
                
                ica_s.W = EEG_ica.icaweights;
                ica_s.WS = EEG_ica.icasphere;
                ica_s.WI = EEG_ica.icawinv;
                save(f_ica_w, 'ica_s', 'v');
                clear EEG_ica;
            else
                tempX = load(f_ica_w);
                ica_s = tempX.ica_s;
                assert(v.ica_maxsteps == tempX.v.ica_maxsteps, 'loaded ICA W has different config to set');
                assert(v.ica_stop == tempX.v.ica_stop, 'loaded ICA W has different config to set');
                clear tempX;
            end
            %% 3. Find ICs that are correlated with ECG (>0.2) - see Debener 2007
            X = EEG_nobcg_noecg.data;
            Y = ica_s.W*ica_s.WS*X;
            
            r = nan(size(Y, 1), 1);
            for ix_r = 1:length(r)
                r(ix_r) = corr(Y(ix_r, :).', EEG_ecg.data.');
            end
            v.ica_rm = abs(r)>0.2;
            a = 1:size(Y,1);
            a(ismember(a, v.ica_rm)) = [];
            Z = ica_s.WI(:,a)*Y(a,:);
            
            %% Write back in, taking care of ECG
            ch_ecg = strcmpi({EEG_nobcg.chanlocs.labels}, v.str_ecg);
            ch_no_ecg = not(ch_ecg);
            EEG_nobcg.data(ch_no_ecg, :) = Z;
            %%
            % Add the motion data again
            nbchan = EEG_nobcg.nbchan;
            for ix_M = 1:size(motion_data, 1)
                EEG_nobcg.data(nbchan + ix_M, :) = motion_data(ix_M, :);
                if ~isempty(EEG_nobcg.chanlocs)
                    EEG_nobcg.chanlocs(nbchan + ix_M).labels = motion_chanlocs(ix_M).labels;
                end
            end
            EEG_nobcg.nbchan = motion_nbchan;
            
            %%
            % Plotting the raw data pre and post removal
            figure
            subplot(2,1,1)
            plot(EEG_mc.data(v.ch_rem, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
            hold on
            plot(EEG_nobcg.data(v.ch_rem, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
            hold off
            title('EEG data pre and post BCG removal')
            legend('Pre BCG removal', 'Post BCG removal')
            
            subplot(2,1,2)
            plot(EEG_mc.data(v.ch_rem_mc + v.nbchan, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
            hold on
            plot(EEG_nobcg.data(v.ch_rem_mc + v.nbchan, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
            hold off
            title('Motion data pre and post BCG removal')
            legend('Pre BCG removal', 'Post BCG removal')
            
            plot_psd(v, EEG_mc, EEG_nobcg)
            
%             EEG_nobcg.event = [EEG_nobcg.event, EEG_qrs.event];
            EEG_nobcg = eeg_checkset(EEG_nobcg);
            
            EEG_nobcg.etc.sp_rmbcg = v;
            %             (has to be version 6 for MNE)
            pop_saveset(EEG_nobcg, 'filename', f_rmbcg, 'filepath', p_rmbcg, 'version', '6');
        end
    end
else
    %     the duplication of the resampling code with its own sampling rate and
    %     filter settings makes me super uncofmrtable - if we ever need this
    %     bit of code, we can talk about how to modify the sp_resample code
    error('motion_mc flag must be on');
    %     file_list = glob(fullfile(d_proc_in, 'proc_ga', '**_r0[0-9]_rmga.set'));
    %     for ix_sub = 1:length(file_list)
    %
    %         [p_rmga, f_rmga, ext_rmga] = fileparts(file_list{ix_sub});
    %         f_rmga = sprintf('%s%s', f_rmga, ext_rmga);
    %         [~,folder_rmga,~] = fileparts(p_rmga);
    %         ix_run = extractBetween(f_rmga, '_r0', '_rmga.');
    %         ix_run = str2double(ix_run{end});
    %         str_sub = lower(folder_rmga);
    %
    %         p_rmbcg = fullfile(d_proc_out, 'proc_bcgobs_nomc', str_sub);
    %         f_rmbcg = sprintf('%s_r%02d_rmbcg', str_sub, ix_run);
    %         if ~(exist(p_rmbcg, 'dir') == 7), mkdir(p_rmbcg); end
    %
    %         pfe_out_bcg = fullfile(p_rmbcg, [f_rmbcg, '.set']);
    %
    %         if or(not(exist(pfe_out_bcg, 'file') == 2), v.overwrite)
    %             fprintf('Loading:\n%s\n', f_rmga);
    %             EEG_ga = pop_loadset(f_rmga, p_rmga);
    %             EEG_ga.times = double(EEG_ga.times);
    %             EEG_ga.data = double(EEG_ga.data);
    %             EEG_ga.setname = f_rmbcg;
    %
    %             % Downsampling the data and applying a HPF
    %             v.fs_rs = 500;
    %             EEG_ga = pop_resample(EEG_ga, v.fs_rs);
    %             EEG_ga = pop_eegfiltnew(EEG_ga, 0.01, [], [], 0, [], 1);
    %             EEG_ga.times = double(EEG_ga.times);
    %             EEG_ga.data = double(EEG_ga.data);
    %
    %             % Obtain the data between the first and the last TR markers
    %             EEG_ga.etc.orig_event = EEG_ga.event;
    %             grad_lat = [EEG_ga.event(strcmpi({EEG_ga.event.type}, v.str_grad)).latency];
    %             ix_lat = grad_lat([1, end]);
    %
    %             slice_from = floor(ix_lat(1)-EEG_ga.srate*v.slice_boundary);
    %             slice_to = ceil(ix_lat(end)+EEG_ga.srate*v.slice_boundary);
    %             EEG_mc = pop_select( EEG_mc, 'point', [slice_from, slice_to]);
    %
    %             %             slice_vec = slice_from:slice_to;
    %             %             EEG_ga.times = EEG_ga.times(1,  slice_vec);
    %             %             EEG_ga.data = EEG_ga.data(:, slice_vec);
    %             %             EEG_ga.pnts = size(EEG_ga.data, 2);
    %             %             EEG_ga.event = EEG_ga.event([EEG_ga.event.latency]>=ix_lat(1)- EEG_ga.srate*v.slice_boundary);
    %             %
    %             %             lat_list = round([EEG_ga.event.latency] - (ix_lat(1)- EEG_ga.srate*v.slice_boundary));
    %             %             lat_list = num2cell(lat_list);
    %             %             [EEG_ga.event.latency] = lat_list{:};
    %             %
    %             %             urevent_list = num2cell(1:size(EEG_ga.event,2));
    %             %             [EEG_ga.event.urevent] = urevent_list{:};
    %
    %             EEG_ga = eeg_checkset(EEG_ga);
    %
    %             %% QRS Detection
    %             % QRS detection probably won't work well unless there is a highpass filter
    %             % no idea really what is best... probably somewhere between 0.1 and 5Hz...
    %             % you might need to do some testing
    %
    %             EEG_qrs = pop_eegfiltnew(EEG_ga, 2.5, [], [], 0, [], 1);
    %
    %             %detect the QRS (take a look at the markers relative to the BCG/EKG to see if it worked
    %             % if it didn't work, adjust the filter above
    %             EEG_qrs = pop_fmrib_qrsdetect(EEG_qrs, v.ch_ecg, v.met_str, 'no');
    %
    %             choice_qrs = [EEG_qrs.event.latency];
    %             choice_qrs = choice_qrs(strcmpi({EEG_qrs.event.type}, v.met_str));
    %
    %             %%
    %             % Draw the plot with the QRS markers
    %             figure;
    %             str_title = 'first five QRS markers';
    %             choice_qrs_slice = choice_qrs(1:5);
    %             plot_qrs(EEG, choice_qrs_slice, str_title, v.ch_ecg, v.ch_rem)
    %
    %             figure;
    %             str_title = 'middle five QRS markers';
    %             choice_qrs_slice = choice_qrs(round(size(choice_qrs, 2)/2)-2:round(size(choice_qrs, 2)/2)+2);
    %             plot_qrs(EEG, choice_qrs_slice, str_title, v.ch_ecg, v.ch_rem)
    %
    %             figure;
    %             str_title = 'last five QRS markers';
    %             choice_qrs_slice = choice_qrs(end-4:end);
    %             plot_qrs(EEG, choice_qrs_slice, str_title, v.ch_ecg, v.ch_rem)
    %             %% BCG Removal
    %             % Run the BCG removal script
    %             EEG_nobcg = fmrib_pas(EEG_ga, choice_qrs, v.fmrib_pas_method, []);
    %             EEG_nobcg.etc.sp_rmbcg = v;
    %
    %             % Plotting the raw data pre and post removal
    %             figure
    %             plot(EEG_ga.data(v.ch_rem, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
    %             hold on
    %             plot(EEG_nobcg.data(v.ch_rem, size(EEG_nobcg.data,2)/2 - 1000:size(EEG_nobcg.data,2)/2 + 1000))
    %             hold off
    %             legend('Pre BCG removal', 'Post BCG removal')
    %
    %             plot_psd(v, EEG_ga, EEG_nobcg)
    %
    %             pop_saveset(EEG_nobcg, 'filename', f_rmbcg, 'filepath', p_rmbcg);
    %         end
    %     end
end
end
%% Visualizing the Data through PSD
function plot_psd(v, EEG, EEG_nobcg)
if v.figgen
    fs = EEG_nobcg.srate;
    ECG_data = EEG.data(v.ch_ecg,:);
    original_EEG = EEG.data(v.ch_rem, :);
    nobcg_EEG = EEG_nobcg.data(v.ch_rem,:);
    
    L = 1500;
    nfft = 1500;
    N = 1200;
    
    [pxx1, f1] = pwelch(ECG_data, L, N, nfft, fs);
    [pxx2, folder_raw_eeg] = pwelch(original_EEG, L, N, nfft, fs);
    [pxx3, f3] = pwelch(nobcg_EEG, L, N, nfft, fs);
    
    figure
    semilogy(f1,pxx1)
    xlim([0 50])
    hold on
    semilogy(folder_raw_eeg,pxx2)
    xlim([0 50])
    hold on
    semilogy(f3,pxx3)
    xlim([0 50])
    hold on
    
    legend('ECG data','original EEG','nobcg EEG')
end
end


function plot_qrs(EEG, choice_qrs_slice, str_title, ch_ecg, ch_rem)
ix_back = 300;
ix_forward = 300;
subplot(2,1,1)
plot(EEG.times(choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), ...
    EEG.data(ch_ecg, choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), 'k');
hold on;
for ix = 1:length(choice_qrs_slice)
    y = get(gca, 'ylim');
    plot(ones(1, 2) * EEG.times(choice_qrs_slice(ix)), y, 'Color', 'm');
end
s1 = sprintf('ECG with %s', str_title);
title(s1)
hold off;

subplot(2,1,2)
plot(EEG.times(choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), ...
    EEG.data(ch_rem, choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), 'k');
hold on;
for ix = 1:length(choice_qrs_slice)
    y = get(gca, 'ylim');
    plot(ones(1, 2) * EEG.times(choice_qrs_slice(ix)), y, 'Color', 'm');
end
s2 = sprintf('EEG with %s', str_title);
title(s2)
hold off;
end