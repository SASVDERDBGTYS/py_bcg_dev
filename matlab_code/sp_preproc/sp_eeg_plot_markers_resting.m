function sp_eeg_plot_markers_resting(EEG, ix_channel)
    plot(EEG.times / 1000, EEG.data(ix_channel, :), 'k');
    ev_u = unique({EEG.event.type});
    cmap = lines(length(ev_u));
    for ix = 1:length(EEG.event)
        c = cmap(strcmpi(EEG.event(ix).type, ev_u), :);
        y = get(gca, 'ylim');
        plot(ones(1, 2) * EEG.times(EEG.event(ix).latency) / 1000, y, 'Color', c);
    end
    xlabel('Time (s)');
end