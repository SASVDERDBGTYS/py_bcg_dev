% Function that works with the resting state data (different file
% organization compared to the previous dataset)

function sp_rmga_resting(d_project, varargin)
%% condition checking
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');

%%

d.str_grad = 'S128';
d.str_ecg = 'ECG';
d.n_expected_TR = 150;
d.figsave = true;
d.overwrite = false;
d.d_overwrite = struct;
d.testing_mode = false;
d.proc = 'proc_resting';
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'log', 'rmga_failure_log.txt');
%% Parse inputs

[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Path setting

% Setting the input and output path
d_raw = fullfile(getenv('D_DATA'), d_project, 'proc_resting', 'raw');
d_out = fullfile(getenv('D_OUT'), d_project, v.proc);

%% File parsing

% obtain the list of all header files
file_list = glob(fullfile(d_raw, '**[0-9].vhdr'));

for ix_file = 1:length(file_list)
    % Split the fullpath into path, file name and extension
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    fe_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    
    % obtain the index of the current subject
    num_sub = strsplit(p_raw_eeg, filesep);
    num_sub = cell2mat(num_sub(end - 1));
    
    % Setting the subject string in the form sub**
    str_sub = sprintf('sub%s', num_sub);
    
    % Obtain the output filename and directory and check if file exists
    f_rmga = sprintf('%s_rmga', str_sub);
    pf_rmga = fullfile(d_out, 'proc_ga', str_sub, f_rmga);
    do_rmga = generate_check_eeg(pf_rmga, v.overwrite);
    
    % Set the path for generating the figures
    % If folder doesn't exist, make folder
    f_ga_figure_root = sprintf('%s_rmga', str_sub);
    p_ga_figure = fullfile(d_out, 'proc_ga', str_sub, 'figures');
    if ~(exist(p_ga_figure, 'dir') == 7), mkdir(p_ga_figure); end
    
    % If the file doesn't exist, perform gradient removal
    if do_rmga
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, fe_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, fe_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        EEG.setname = f_rmga; % this should actually be placed just before the save, since eeglab modifies it
        
        h_f = figure(11);clf;
        figure(h_f);h_s1 = subplot(2, 1, 1);cla;hold on;
        sp_eeg_plot_markers_resting(EEG, 1);
        
        disp('nothing');
    end
end