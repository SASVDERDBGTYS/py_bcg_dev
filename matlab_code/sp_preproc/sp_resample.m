function sp_resample(d_project, varargin)
if nargin<1
    d_project = '190327_eeg_fmri_linbi';
end
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%%
d.str_grad = 'S128';
d.str_ecg = 'ECG';
d.slice_boundary = 0.2; % Add periods of specified time around the slice
d.overwrite = false;
d.d_overwrite = struct;
d.fs_rs = 500;
d.proc = getenv_check_empty('R_PROC');
d.causal_method = 'none';
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'proc_ga', 'failure_log.txt');
%% Parse inputs
[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);
%%
d_proc = fullfile(getenv('D_DATA'), d_project, v.proc);
d_proc_mc = fullfile(getenv('D_OUT'), d_project, v.proc);
%%
file_list = glob(fullfile(d_proc, 'proc_ga', '**_r0[0-9]_rmga.set'));
for ix_sub = 1:length(file_list)
    
    [p_rmga, f_rmga, ext_rmga] = fileparts(file_list{ix_sub});
    rng(string2hash(f_rmga));
    f_rmga = sprintf('%s%s', f_rmga, ext_rmga);
    [~,folder_rmga,~] = fileparts(p_rmga);
    ix_run = extractBetween(f_rmga, '_r0', '_rmga');
    ix_run = str2double(ix_run{end});
    str_sub = lower(folder_rmga);
    
    f_out_mc = sprintf('%s_r%02d_rs', str_sub, ix_run);
    if strcmpi(v.causal_method, 'none')
        p_out_mc = fullfile(d_proc_mc, 'proc_rs', str_sub);
    elseif strcmpi(v.causal_method, 'ecg_diff')
        p_out_mc = fullfile(d_proc_mc, 'proc_rs_ecg_diff', str_sub);
    elseif strcmpi(v.causal_method, 'ecg_subsmooth')
        p_out_mc = fullfile(d_proc_mc, 'proc_rs_ecg_subsmooth', str_sub);
    else
        error('Bad causal method specified');
    end
    if ~(exist(p_out_mc, 'dir') == 7), mkdir(p_out_mc); end
    
    pfe_out_mc = fullfile(p_out_mc, [f_out_mc, '.set']);
    
    f_motion = sprintf('%s_r0%d_prefiltered_func_data_mcf.par', str_sub, ix_run);
    p_motion = fullfile(d_proc, 'proc_mcf', str_sub);
    
    if not(exist(fullfile(p_motion, f_motion), 'file') == 2)
        fprintf('The motion file doesnt exist for subject %s, run %d, skipping...\n', str_sub(end-1:end), ix_run)
    elseif or(not(exist(pfe_out_mc, 'file') == 2), v.overwrite)
        if not(exist(fullfile(p_rmga, f_rmga), 'file') == 2)
            str_failure = sprintf(...
                'Missing rmga.fdt file for %s, run %s\n', str_sub, ix_run);
            sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), pfe_out_mc);
            continue;
        else
            f_prop = dir(fullfile(p_rmga, f_rmga));
            if f_prop.bytes==0
                str_failure = sprintf(...
                    '0 byte rmga file for %s, run %d\n', str_sub, ix_run);
                sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), pfe_out_mc);
                continue;
            end
        end
        fprintf('Loading:\n%s\n', f_rmga);
        EEG_ga = pop_loadset(f_rmga, p_rmga);
        EEG_ga.times = double(EEG_ga.times);
        EEG_ga.data = double(EEG_ga.data);
        EEG_ga.setname = f_out_mc; % this should actually be placed just before the save, since eeglab modifies it
        %%
        EEG_ga = pop_resample(EEG_ga, v.fs_rs);
        %%
        y_ecg = EEG_ga.data(strcmpi({EEG_ga.chanlocs.labels}, v.str_ecg), :);
        EEG_ga = pop_eegfiltnew(EEG_ga, 0.25, [], [], 0, [], 1);
        
        if strcmpi(v.causal_method, 'none')
            % the default - nothing to do be done
        elseif strcmpi(v.causal_method, 'ecg_diff')
            % write back causally differenced ECG (that has not been
            % high-passed)
            y_ecg = diff(y_ecg, 1, 2);
            y_ecg = [y_ecg, y_ecg(end)];
            EEG_ga.data(strcmpi({EEG_ga.chanlocs.labels}, v.str_ecg), :) = y_ecg;
        elseif strcmpi(v.causal_method, 'ecg_subsmooth')
            % write back causally subsmoothed ECG (that has not been
            % high-passed)
            w_filter = ones(1, 5 * EEG_ga.srate);
            w_filter = w_filter/sum(w_filter);
            y_ecg_smooth = filter(w_filter, 1, y_ecg')';
            y_ecg = y_ecg - y_ecg_smooth;
            EEG_ga.data(strcmpi({EEG_ga.chanlocs.labels}, v.str_ecg), :) = y_ecg;
        else
            error('Bad causal method specified');
        end
        %%
        % EEG_ga = pop_eegfiltnew(EEG_ga, 19, 21, [], 1, [], 0);
        % EEG_ga = pop_eegfiltnew(EEG_ga, 39, 41, [], 1, [], 0);
        % EEG_ga = pop_eegfiltnew(EEG_ga, 59, 61, [], 1, [], 0);
        % EEG_ga = pop_eegfiltnew(EEG_ga, 79, 81, [], 1, [], 0);
        %% now add motion data
        EEG_mc= EEG_ga;
        mdat = sp_motion_load(p_motion, f_motion);
        ev_type = {EEG_mc.event.type};
        ev_lat = [EEG_mc.event.latency];
        
        % Interpolate the motion data relative to the EEG time series
        ev_grad = ev_lat(strcmpi(ev_type, v.str_grad))';
        ev_grad_midpoint = round(ev_grad - median(diff(ev_grad))/2);
        
        M = sp_eeg_match_motion(mdat, EEG_mc, ev_grad_midpoint);
        
        % Write the motion data back into the EEG structure
        fprintf('Adding motion data for subject %s, run %d\n', str_sub(end-1:end), ix_run)
        nbchan = EEG_mc.nbchan;
        for ix_M = 1:size(M, 1)
            EEG_mc.data(nbchan + ix_M, :) = M(ix_M, :);
            if ~isempty(EEG_mc.chanlocs)
                EEG_mc.chanlocs(nbchan + ix_M).labels = mdat.Properties.VariableNames{ix_M};
            end
        end
        EEG_mc.nbchan = size(EEG_mc.data,1);
        EEG_mc.etc.sp_addmc = v;
        
        %% now slice the data
        % Obtain the data between the first and the last TR markers
        EEG_mc.etc.orig_event = EEG_mc.event;
        grad_lat = [EEG_mc.event(strcmpi({EEG_mc.event.type}, v.str_grad)).latency];
        ix_lat = grad_lat([1, end]);
        
        slice_from = floor(ix_lat(1)-EEG_mc.srate*v.slice_boundary);
        slice_to = ceil(ix_lat(end)+EEG_mc.srate*v.slice_boundary);
        EEG_mc = pop_select( EEG_mc, 'point', [slice_from, slice_to]);
        
        % there is an EEGLAB function to do all this! ( see above)
        %         slice_vec = slice_from:slice_to;
        %         EEG_ga.times = EEG_ga.times(1,  slice_vec);
        %         EEG_ga.data = EEG_ga.data(:, slice_vec);
        %         EEG_mc.pnts = size(EEG_mc.data, 2);
        %         EEG_mc.event = EEG_mc.event([EEG_mc.event.latency]>=ix_lat(1)- EEG_mc.srate*v.slice_boundary);
        %
        %         lat_list = round([EEG_mc.event.latency] - (ix_lat(1)- EEG_mc.srate*v.slice_boundary));
        %         lat_list = num2cell(lat_list);
        %         [EEG_mc.event.latency] = lat_list{:};
        %
        %         urevent_list = num2cell(1:size(EEG_mc.event,2));
        %         [EEG_mc.event.urevent] = urevent_list{:};
        
        %%
        EEG_mc = eeg_checkset(EEG_mc);
        % Saving the sliced data set (has to be version 6 for MNE)
        pop_saveset(EEG_mc, 'filename', f_out_mc, 'filepath', p_out_mc, 'version', '6');
    end
end
end