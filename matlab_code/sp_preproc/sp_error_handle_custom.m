function sp_error_handle_custom(str_failure, failure_log, pfe_out_ga)
diary(failure_log)
fprintf(str_failure);
fprintf('Touch output so that we do not keep trying to generated failures\n');
touch(pfe_out_ga);
diary off;
end
function touch(f)
fid = fopen(f,'w');
fclose(fid);
end