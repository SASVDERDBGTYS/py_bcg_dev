function T = sp_get_set_files(d_proc_bcg)
% don't use this anymore!!!
    T = get_file_table(d_proc_bcg, 'set', '**');

% % note that I have a very similar function called get_file_table that
% % should probably be merged...
% file_list = glob(fullfile(d_proc_bcg, '**_r[0-9][0-9]_**.set'));
% T = cell2table(file_list);
% T.rm_file = false(height(T), 1);
% for ix_file = 1:height(T)
%     run_info = regexp(T.file_list{ix_file}, '.+sub([0-9][0-9])_r([0-9][0-9]).+', 'tokens');
%     T.ix_sub(ix_file) = str2double(run_info{1}{1});
%     T.ix_run(ix_file) = str2double(run_info{1}{2});
%     s = dir(T.file_list{ix_file});
%     T.rm_file(ix_file) = s.bytes==0;
% end
% % remove empty files
% T(T.rm_file, :) = [];
end