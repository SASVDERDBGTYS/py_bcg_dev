function [EEG, v] = sp_eeg_fix(EEG, str_grad, ch_non_eeg, f_failure_log)
% 1. Remove all TR markers that surround a separation not equal to median sep.
% 2. Fill in the removed markers
% 3. Add in extra markers at the start if we seem to have early gradients
% 4. Run FMRIB FASTR code for GA removal
%% remove some stuff that nobody will ever need
EEG = rmfield(EEG, 'urevent');
EEG.event = rmfield(EEG.event, 'bvtime');
EEG.event = rmfield(EEG.event, 'code');
EEG.event = rmfield(EEG.event, 'channel');
EEG.urevent = rmfield(EEG.event, 'urevent');
EEG = eeg_checkset( EEG );
%% remove all TR markers that surround a separation not equal to median sep.
grad_samples = median(diff([EEG.event(strcmpi({EEG.event.type}, str_grad)).latency]));
ev_grad = find(strcmpi({EEG.event.type}, str_grad));
ev_correct = [diff([EEG.event(ev_grad).latency]) == grad_samples, true];
ev_correct([false, diff(ev_correct)==1]) = false; % spreads forward by one marker
ev_incorrect = not(ev_correct);
EEG.event(ev_grad(ev_incorrect)) = [];
%% Add an event in until all events are the exact same distance apart
ev_grad = find(strcmpi({EEG.event.type}, str_grad));
d = diff([EEG.event(ev_grad).latency]);
ix_count = 0;
while not(all(d == grad_samples))
    ix_count = ix_count + 1;
    if ix_count > 20, error('Failed to fix gradients!');end
    
    ix_pre_missing = ev_grad(find(not(d == grad_samples), 1, 'first'));
    
    EEG.event = [EEG.event(1:ix_pre_missing), EEG.event(ix_pre_missing:end)];
    EEG.event(ix_pre_missing + 1).latency = ...
        EEG.event(ix_pre_missing).latency + grad_samples;
    
    ev_grad = find(strcmpi({EEG.event.type}, str_grad));
    d = diff([EEG.event(ev_grad).latency]);
end

EEG.urevent = rmfield(EEG.event, 'urevent');
EEG = eeg_checkset( EEG );
intermediate_event = EEG.event;
%%
% ev_lat_init = [EEG.event.latency];
%
% ev_grad_init = ev_lat_init(strcmpi({EEG.event.type}, str_grad))';
%
% for ix = 1:size(ev_grad_init,1)
%     ev_type = {EEG.event.type};
%     ev_lat = [EEG.event.latency];
%
%     ev_grad = ev_lat(strcmpi(ev_type, str_grad))';
%
%     d = diff(ev_grad);
%
%     id = find(d < median(d)/2);
%     if ~isempty(id)
%         rmv = ev_grad(id + 1);
%         id2 = find(ev_lat == rmv);
%         EEG.event(:, id2) = [];
%         EEG.urevent(:, id2) = [];
%         for ix2 = 1:size(EEG.event, 2)
%             EEG.event(ix2).bvmknum = ix2;
%             EEG.event(ix2).urevent = ix2;
%
%             EEG.urevent(ix2).bvmknum = ix2;
%         end
%     end
% end
%
% assert(size(EEG.event, 2)~=0, 'No TRs left');
% EEG.urevent = rmfield(EEG.event, 'urevent');
% EEG = eeg_checkset( EEG );
% intermediate_event = EEG.event;
%% now figure out if we need to add some TRs at the start
% do this all based on a single channel for simplicity
EEG_ch1 = pop_select( EEG, 'channel', {EEG.chanlocs(1).labels});
EEG_ch1 = pop_eegfiltnew(EEG_ch1, [],1,16500,1,[],0); % high pass

ix_marker_grad_first = find(strcmpi({EEG.event.type}, str_grad), 1, 'first');
lat_marker_grad_first = EEG.event(ix_marker_grad_first).latency;
grad_samples = median(diff([EEG.event(strcmpi({EEG.event.type}, str_grad)).latency]));

% set the data up to 5 gradients before the first recorded marker to 0
EEG_ch1.data(1, 1:(lat_marker_grad_first - grad_samples * 5)) = 0;
th_gradient = 2000;
lat_grad_first = find(EEG_ch1.data > th_gradient, 1, 'first');
n_extra_grad = round((lat_marker_grad_first - lat_grad_first)/grad_samples);

% this cannot happen anymore
% if n_extra_grad<=5
%     diary(f_failure_log);
%     fprintf('%s\n', EEG.setname);
%     warning('GAs start too early for this file???');
%     diary off;
% end
for ix = 1:n_extra_grad
    EEG = add_initial_grad_marker(EEG, str_grad, grad_samples);
end
EEG.urevent = rmfield(EEG.event, 'urevent');
EEG = eeg_checkset( EEG );
%% Do the actual GA removal
v.is_slice = 1; % this is not right... but it works (and does not work = 0)
v.L = round(25000/EEG.srate); % upsampling factor
v.anc_chk = 1; %adaptive noise cancelling
v.trig_correct = 0; v.Volumes = []; v.NPC = 'auto';
v.lpf = 70; % important to remember there is a 70Hz low pass here
v.Win = 30; % just the default
% try
    EEG = pop_fmrib_fastr(EEG, v.lpf, v.L, v.Win, str_grad, ...
        v.is_slice, v.anc_chk, v.trig_correct, v.Volumes, [], [], ...
        ch_non_eeg, v.NPC);
    EEG = eeg_checkset( EEG );
% catch
%     diary(f_failure_log);
%     fprintf('%s\n', EEG.setname);
%     warning('GAs start too early for this file???');
%     diary off;
%     EEG.data = nan;
% end
%%
% this is super ugly - but we only actually have 150 volumes.
% Presumably these correspond to the 150 volumes that were marked in the
% EEG to start with (after correction for duplicates).
% So we restored the events after the duplicate fix, and before introducing
% extra ones at the start of the sequence.
EEG.event = intermediate_event;
end

function EEG = add_initial_grad_marker(EEG, ev, grad_samples)
ix_x = find(strcmpi({EEG.event.type}, ev), 1, 'first');
EEG.event = [EEG.event(1:ix_x), EEG.event(ix_x:end)];
EEG.event(ix_x).latency = EEG.event(ix_x).latency - grad_samples;
EEG = eeg_checkset(EEG);
end