
if not(exist('skip_settings','var') == 1)
    skip_settings = false;
end
if not(skip_settings)
    clearvars;
    close all;
    set_env;
    addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
    addpath(getenv('D_EEGLAB'));eeglab;close;
    addpath('sp_preproc');
    addpath('an_results');
end
%%

d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';
%%
arch = 'gru_arch_general4';
if strcmpi(getenv('R_PROC'), 'proc_clayden')
    proc_net = fullfile('proc_bcgnet', 'single_sub', arch);
else
    proc_net = fullfile('proc_bcgnet', 'multi_run', arch);
end
proc_obs = 'proc_bcgobs';
proc_rs = 'proc_rs';
%%
d_proc_net = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_net);file_filter_net = 'bcgnet';
d_proc_obs = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_obs);file_filter_bcg = 'rmbcg';
d_proc_gar = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_rs);file_filter_rs = 'rs';

%%
str_band = 'none';

% str_band = 'theta';

% coi = 'T8';str_band = 'alpha';
% coi = 'Pz';str_band = 'alpha';
% coi = 'Fz';str_band = 'alpha';
coi = 'Oz';str_band = 'alpha';
% coi = 'C3';str_band = 'mu';

% str_band = 'none';
[d_proc_filtered, get_f_filtered_net, vec_sub_net] = an_proc_filter(d_project, d_proc_net, file_filter_net, 'band', str_band);
[~, get_f_filtered_obs, ~] = an_proc_filter(d_project, d_proc_obs, file_filter_bcg, 'band', str_band);
[~, get_f_filtered_gar, ~] = an_proc_filter(d_project, d_proc_gar, file_filter_rs, 'band', str_band);
%%
vec_sub = vec_sub_net;
if strcmpi(getenv('R_PROC'), 'proc_full')
    assert(length(vec_sub)==19, 'Missing subjects?');
    only_use_five_runs = false;
    if only_use_five_runs
        es = '_5ronly';
        vec_sub(vec_sub==15) = [];
        vec_sub(vec_sub==17) = [];
        vec_sub(vec_sub==19) = [];
        vec_sub(vec_sub==23) = [];
    else
        es = '';
    end
    cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
end
%%
if strcmpi(getenv('R_PROC'), 'proc_full')
    fig_folder = 'figures_response1';
elseif strcmpi(getenv('R_PROC'), 'proc_clayden')
    fig_folder = 'figures_clayden';
end

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig05', ...
    sprintf('c%s_vs%s_%s', gen_source(proc_net), gen_source(proc_obs), str_band));
% p_fig = '~/Desktop/temp/';

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'svg';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
es = '';
print_local = @(h, dim) printForPub(h, sprintf('f_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

%%
% smooth_op = @ smooth;
smooth_op = @ medfilt1;
srate_assumed = 500; % !!!
t_sm = 45;
n_sm = t_sm * srate_assumed;
if strcmpi(str_band, 'none')
    h_f = figure('Name', sprintf('fft_%s', coi));clf;
    for ix_vec_sub = 1:length(vec_sub)
        
        f_net = get_f_filtered_net(d_proc_filtered, vec_sub(ix_vec_sub));
        f_obs = get_f_filtered_obs(d_proc_filtered, vec_sub(ix_vec_sub));
        EEG_net = pop_loadset(sprintf('%s%s', f_net, '.set'));
        EEG_obs = pop_loadset(sprintf('%s%s', f_obs, '.set'));
        
        fs = EEG_net.srate;
        t = EEG_net.times/1000;
        
        if ix_vec_sub == 1
            t_max = t(end);
        end
        
        assert(isequal({EEG_net.chanlocs}, {EEG_obs.chanlocs}), '?');
        
        y_coi_net = extract_coi(EEG_net, coi);
        y_coi_obs = extract_coi(EEG_obs, coi);
        
        figure(h_f);
        split_len = 4 * fs;
        subplot(ns(1), ns(2), ix_vec_sub);hold on;
        
        [pw_coi_net, ~] = pwelch(y_coi_net, split_len, [],[],fs);
        [pw_coi_obs, fw] = pwelch(y_coi_obs, split_len, [],[],fs);
        
        plot(fw, pw_coi_obs, 'Color', c.obs_st);
        plot(fw, pw_coi_net, 'Color', c.net_st);
        xlim([1 20]);
        xlabel('Frquency (Hz)');
        ylabel('Amp. (\muV/m^2)^2/Hz');
        
    end
    print_local(h_f, [18.8, 16]);
else
    v.str_qrs = 'qrs0';
    h_f1 = figure('Name', sprintf('%s_amplitude_%s_qrs_locked', str_band, coi));clf;
    h_f2 = figure('Name', sprintf('%s_amplitude_%s_time', str_band, coi));clf;
    ns = numSubplots(length(vec_sub));
    if strcmpi(getenv('R_PROC'), 'proc_full')
        t_i = 0:1/500:1500;
    else
        t_i = 0:1/500:600;
    end
    
    X_coi_yyy = zeros(length(vec_sub), length(t_i));
    A_coi_net = X_coi_yyy;
    A_coi_obs = X_coi_yyy;
    A_coi_gar = X_coi_yyy;
    F_coi_net = X_coi_yyy;
    F_coi_obs = X_coi_yyy;
    F_coi_gar = X_coi_yyy;
    clear X_coi_yyy;
    for ix_vec_sub = 1:length(vec_sub)
        
        f_net = get_f_filtered_net(d_proc_filtered, vec_sub(ix_vec_sub));
        f_obs = get_f_filtered_obs(d_proc_filtered, vec_sub(ix_vec_sub));
        f_gar = get_f_filtered_gar(d_proc_filtered, vec_sub(ix_vec_sub));
        
        EEG_net = pop_loadset(sprintf('%s%s', f_net, '.set'));
        EEG_obs = pop_loadset(sprintf('%s%s', f_obs, '.set'));
        EEG_gar = pop_loadset(sprintf('%s%s', f_gar, '.set'));
        
        fs = EEG_net.srate;
        t = EEG_net.times/1000;
        
        if ix_vec_sub == 1
            t_max = t(end);
        end
        
        assert(isequal({EEG_net.chanlocs}, {EEG_obs.chanlocs}), '?');
        
        [y_coi_net, a_coi_net, p_coi_net, h_coi_net] = extract_coi(EEG_net, coi);
        [y_coi_obs, a_coi_obs, p_coi_obs, h_coi_obs] = extract_coi(EEG_obs, coi);
        [y_coi_gar, a_coi_gar, p_coi_gar, h_coi_gar] = extract_coi(EEG_gar, coi);
        
        dt =  median(diff(t));
        tdt = (t(1:end-1) + t(2:end))/2;
        
        f_coi_net = diff(unwrap(p_coi_net))./(2*pi*dt);
        f_coi_obs = diff(unwrap(p_coi_obs))./(2*pi*dt);
        f_coi_gar = diff(unwrap(p_coi_gar))./(2*pi*dt);
        F_coi_net(ix_vec_sub, :) = interp1(tdt, f_coi_net, t_i);
        F_coi_obs(ix_vec_sub, :) = interp1(tdt, f_coi_obs, t_i);
        F_coi_gar(ix_vec_sub, :) = interp1(tdt, f_coi_gar, t_i);
        A_coi_net(ix_vec_sub, :) = interp1(t, a_coi_net, t_i);
        A_coi_obs(ix_vec_sub, :) = interp1(t, a_coi_obs, t_i);
        A_coi_gar(ix_vec_sub, :) = interp1(t, a_coi_gar, t_i);
        
        %
        ev_qrs = [EEG_obs.event(strcmpi({EEG_obs.event.type}, v.str_qrs)).latency]';
        ix_cut = -fs/2:fs/2;
        ev_qrs_mat = ev_qrs + ix_cut;
        
        ev_qrs_mat(1, :) = [];
        ev_qrs_mat(end, :) = [];
        %
        x_mea_net = mean(a_coi_net(ev_qrs_mat), 1);
        x_mea_obs = mean(a_coi_obs(ev_qrs_mat), 1);
        x_sem_net = std_error(a_coi_net(ev_qrs_mat), 0, 1);
        x_sem_obs = std_error(a_coi_obs(ev_qrs_mat), 0, 1);
        t_cut = ix_cut/fs;
        
        % clf;hold on;
        figure(h_f1);
        subplot(ns(1), ns(2), ix_vec_sub);hold on;
        ci_alpha = 0.15;
        plot(t_cut, x_mea_obs, 'Color', c.obs_st);
        ciplot(x_mea_obs - 2 * x_sem_obs, x_mea_obs + 2 * x_sem_obs, t_cut, c.obs_st, ci_alpha);
        plot(t_cut, x_mea_net, 'Color', c.net_st);
        ciplot(x_mea_net - 2 * x_sem_net, x_mea_net + 2 * x_sem_net, t_cut, c.net_st, ci_alpha);
        if ix_vec_sub == 1
            xlabel('QRS-locked time (s)');
            ylabel('Amplitude (\muV/m^2)');
        end
        title(sprintf('sub%d', vec_sub(ix_vec_sub)));
        plot([0, 0], get(gca, 'ylim'), 'k--')
        
        do_example_plot = false;
        do_example_plot = do_example_plot|and(strcmpi(getenv('R_PROC'), 'proc_full'), vec_sub(ix_vec_sub) == 34); % same as for figure 3
        do_example_plot = do_example_plot|and(strcmpi(getenv('R_PROC'), 'proc_clayden'), vec_sub(ix_vec_sub) == 43); % same as for figure 3
        if do_example_plot
            h_f3 = figure('Name', sprintf('%s_amplitude_%s_qrs_locked_example', str_band, coi));clf;
            
            figure(h_f3);cla;hold on;
            %             subplot(ns(1), ns(2), ix_vec_sub);hold on;
            ci_alpha = 0.15;
            plot(t_cut, x_mea_obs, 'Color', c.obs_st);
            ciplot(x_mea_obs - 2 * x_sem_obs, x_mea_obs + 2 * x_sem_obs, t_cut, c.obs_st, ci_alpha);
            plot(t_cut, x_mea_net, 'Color', c.net_st);
            ciplot(x_mea_net - 2 * x_sem_net, x_mea_net + 2 * x_sem_net, t_cut, c.net_st, ci_alpha);
            xlabel('QRS-locked time (s)');
            ylabel('Amplitude (\muV/m^2)');
            title(' ');
            plot([0, 0], get(gca, 'ylim'), 'k--')
            print_local(h_f3, [4.4, 4]);
        end
        
        
        %
        figure(h_f2);
        subplot(ns(1), ns(2), ix_vec_sub);hold on;
        plot(t, smooth_op(a_coi_obs, n_sm), 'Color', c.obs_st);
        plot(t, smooth_op(a_coi_net, n_sm), 'Color', c.net_st);
        plot(t, smooth_op(a_coi_obs, n_sm), 'Color', c.obs_st);
        plot(t, smooth_op(a_coi_net, n_sm), 'Color', c.net_st);
        xlim_ = [t_sm, t_max - t_sm];
        if t_max<400
            set(gca, 'xtick', [30:120:t_max]);
        else
            set(gca, 'xtick', [60:240:t_max]);
        end
        if ix_vec_sub == 1
            xlabel('Time (s)');
            ylabel('Amplitude (\muV/m^2)');
        end
        title(sprintf('sub%d', vec_sub(ix_vec_sub)));
        xlim(xlim_);
        %
        if ix_vec_sub == 1
            X_mea_obs = nan([length(vec_sub), size(x_mea_obs, 2)]);
            X_mea_net = nan([length(vec_sub), size(x_mea_net, 2)]);
        end
        X_mea_obs(ix_vec_sub, :) = x_mea_obs;
        X_mea_net(ix_vec_sub, :) = x_mea_net;
    end
    
    print_local(h_f1, [18.8, 16]);
    print_local(h_f2, [18.8, 16]);
    
    %%
    p_y = nan(size(t_cut));
    d_y = p_y;
    for ix_t = 1:length(t_cut)
        [p_y(ix_t), ~] = signrank(X_mea_obs(:, ix_t), X_mea_net(:, ix_t));
        dc = median([X_mea_obs(:, ix_t),X_mea_net(:, ix_t)], 1);
        d_y(ix_t) = double(dc(2) - dc(1));
    end
    [~, crit_p, ~, p_y_a] = fdr_bh(p_y);
    
    %%
    h_f = figure('Name', sprintf('%s_amplitude_%s_qrs_locked_stat_log', str_band, coi));clf;
    hold on;
    ll = floor((min(log(p_y_a)) - 0.5)*2)/2;
    plot([0, 0], [ll, 0], '--', 'Color', c.neutral);
    plot(t_cut([1, end]), log(0.05) * ones(1, 2), '--', 'Color', c.neutral);
    %         plot(t_cut([1, end]), [0, 0], 'k-')
    
    %     plot(t_cut([1,end]), ones(1, 2) * 0.05, 'k--');
    % plot(t_cut, p_y);
    lw = 1.25;
    plot(t_cut, log(p_y_a), 'Color', c.neutral_dark, 'lineWidth', lw)
    logical_sig = p_y_a > 0.05;
    logical_sig_d = double(logical_sig);
    logical_sig_d(logical_sig) = nan;
    plot(t_cut, logical_sig_d+ll+0.25, 'o', 'MarkerFaceColor', c.neutral, 'MarkerSize', 1.5, 'MarkerEdgeColor', 'none')
    %     plot(t_cut, d_y) % for debugging - just to check which way round things are
    ylim([ll 0])
    xlabel('Time (s)');
    ylabel('log(p)');
    title(' ');
    print_local(h_f, [4.4, 4]);
    %     ylim([0 0.05])
    %%
    sm_A_coi_net = A_coi_net;
    sm_A_coi_obs = A_coi_obs;
    sm_A_coi_gar = A_coi_gar;
    
    sm_F_coi_net = F_coi_net;
    sm_F_coi_obs = F_coi_obs;
    sm_F_coi_gar = F_coi_gar;
    
    % bad in general - ok for this one use case...
    sm_A_coi_net(isnan(sm_A_coi_net)) = 0;
    sm_A_coi_obs(isnan(sm_A_coi_obs)) = 0;
    sm_A_coi_gar(isnan(sm_A_coi_gar)) = 0;
    sm_F_coi_net(isnan(sm_F_coi_net)) = 0;
    sm_F_coi_obs(isnan(sm_F_coi_obs)) = 0;
    sm_F_coi_gar(isnan(sm_F_coi_gar)) = 0;
    
    for ix_vec_sub = 1:length(vec_sub)
        sm_A_coi_gar(ix_vec_sub, :) = smooth_op(sm_A_coi_gar(ix_vec_sub, :), n_sm)';
        sm_A_coi_obs(ix_vec_sub, :) = smooth_op(sm_A_coi_obs(ix_vec_sub, :), n_sm)';
        sm_A_coi_net(ix_vec_sub, :) = smooth_op(sm_A_coi_net(ix_vec_sub, :), n_sm)';
        sm_F_coi_gar(ix_vec_sub, :) = smooth_op(sm_F_coi_gar(ix_vec_sub, :), n_sm)';
        sm_F_coi_obs(ix_vec_sub, :) = smooth_op(sm_F_coi_obs(ix_vec_sub, :), n_sm)';
        sm_F_coi_net(ix_vec_sub, :) = smooth_op(sm_F_coi_net(ix_vec_sub, :), n_sm)';
    end
    %%
    h_f = figure('Name', sprintf('%s_amplitude_full_ts', str_band, coi));clf;
    %     hold on;
    subplot(3, 1, 1);hold on;
    y_avg_net = nanmedian(sm_A_coi_net - median(sm_A_coi_net, 2), 1);
    y_avg_obs = nanmedian(sm_A_coi_obs - median(sm_A_coi_obs, 2), 1);
    y_avg_gar = nanmedian(sm_A_coi_gar - median(sm_A_coi_gar, 2), 1);
    %     se_avg_net = std_error(A_coi_net, 0, 1);
    
    %     ciplot(y_avg_net - se_avg_net, y_avg_net + se_avg_net, t_i, [0 0 0], 0.2);
    
    h_gar = plot(t_i, y_avg_gar, 'Color', c.gar_st);
    h_obs = plot(t_i, y_avg_obs, 'Color', c.obs_st);
    h_net = plot(t_i, y_avg_net, 'Color', c.net_st);
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('Amplitude (\muV/m^2)');
    axis tight;
    xlim(xlim_);
    legend([h_gar, h_obs, h_net], {'BCE', 'OBS', 'BCGNet'}, 'Location', 'SouthEast', 'box', 'off')
    %     print_local(h_f, [9, 4]);
    %
    %     h_f = figure('Name', sprintf('%s_amplitude_ts_diff', str_band, coi));clf;
    subplot(3, 1, 2);hold on;
    k_mad = 1.4826;
    y_avg_net_gar = median(sm_A_coi_net - sm_A_coi_gar, 1);
    se_avg_net_gar = k_mad * mad(sm_A_coi_net - sm_A_coi_gar, 1, 1)/sqrt(size(sm_A_coi_net, 1));
    ciplot(y_avg_net_gar - 2 * se_avg_net_gar, y_avg_net_gar + 2 * se_avg_net_gar, t_i, c.gar_ta, 0.4);
    h_gar_net = plot(t_i, y_avg_net_gar, 'Color', c.gar_ta);
    legend([h_gar_net], {'BCGNet - BCE'}, 'Location', 'NorthWest', 'box', 'off')
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
        ylim([-2 8]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('\DeltaAmplitude (\muV/m^2)');
    plot([t_i(1), t_i(end)], 0 * ones(1, 2), 'k--');
    xlim(xlim_);
    ylim([-2.2 1.2]);
    
    subplot(3, 1, 3);hold on;
    y_avg_net_obs = median(sm_A_coi_net - sm_A_coi_obs, 1);
    se_avg_net_obs = k_mad * mad(sm_A_coi_net - sm_A_coi_obs, 1, 1)/sqrt(size(sm_A_coi_net, 1));
    ciplot(y_avg_net_obs - 2 * se_avg_net_obs, y_avg_net_obs + 2 * se_avg_net_obs, t_i, c.obs_ta, 0.25);
    h_obs_net = plot(t_i, y_avg_net_obs, 'Color', c.obs_ta);
    legend([h_obs_net], {'BCGNet - OBS'}, 'Location', 'NorthWest', 'box', 'off')
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
        ylim([-2 8]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('\DeltaAmplitude (\muV/m^2)');
    plot([t_i(1), t_i(end)], 0 * ones(1, 2), 'k--');
    xlim(xlim_);
    ylim([-2.2 1.2]);
    
    %     subplot(3, 1, 3);hold on;
    %     y_avg_gar_obs = median(sm_A_coi_gar - sm_A_coi_obs, 1);
    %     se_avg_gar_obs = std_error(sm_A_coi_gar - sm_A_coi_obs, 0, 1);
    %     ciplot(y_avg_gar_obs - 2 * se_avg_gar_obs, y_avg_gar_obs + 2 * se_avg_gar_obs, t_i, [0, 0, 0], 0.25);
    %     h_gar_obs = plot(t_i, y_avg_gar_obs, 'Color', 'k');
    %     legend([h_gar_obs], {'BCE - OBS'}, 'Location', 'NorthWest', 'box', 'off')
    %     xlim_ = [t_sm, t_i(end) - t_sm];
    %     if t_max<400
    %         set(gca, 'xtick', [30:120:t_max]);
    %         ylim([-2 8]);
    %     else
    %         set(gca, 'xtick', [60:240:t_max]);
    %     end
    %     xlabel('Time (s)');
    %     ylabel('\DeltaAmplitude (\muV/m^2)');
    %     plot([t_i(1), t_i(end)], 0 * ones(1, 2), 'k--');
    %     xlim(xlim_);
    
    print_local(h_f, [9, 10]);
    %%
    h_f = figure('Name', sprintf('%s_amplitude_ts', str_band, coi));clf;
    %     hold on;
    subplot(2, 1, 1);hold on;
    y_avg_net = nanmedian(sm_A_coi_net - median(sm_A_coi_net, 2), 1);
    y_avg_obs = nanmedian(sm_A_coi_obs - median(sm_A_coi_obs, 2), 1);
    %     y_avg_gar = nanmedian(sm_A_coi_gar - median(sm_A_coi_gar, 2), 1);
    %     se_avg_net = std_error(A_coi_net, 0, 1);
    
    %     ciplot(y_avg_net - se_avg_net, y_avg_net + se_avg_net, t_i, [0 0 0], 0.2);
    
    %     h_gar = plot(t_i, y_avg_gar, 'Color', c.gar_st);
    
    t_i_case = and(t_i>xlim_(1), t_i<xlim_(2));
    h_model_obs = fitlm(t_i(t_i_case),  y_avg_obs(t_i_case), 'linear');
    h_model_net = fitlm(t_i(t_i_case),  y_avg_net(t_i_case), 'linear'); % ,'RobustOpts','on'
    
    
    h_obs = plot(t_i, y_avg_obs, 'Color', c.obs_st);
    h_net = plot(t_i, y_avg_net, 'Color', c.net_st);
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('Amp. (\muV/m^2)');
    axis tight;
    xlim(xlim_);
    legend([h_obs, h_net], {'OBS', 'BCGNet'}, 'Location', 'SouthEast', 'box', 'off')
    
    subplot(2, 1, 2);hold on;
    y_avg_net_obs = median(sm_A_coi_net - sm_A_coi_obs, 1);
    se_avg_net_obs = k_mad * mad(sm_A_coi_net - sm_A_coi_obs, 1, 1)/sqrt(size(sm_A_coi_net, 1));
    ciplot(y_avg_net_obs - 2 * se_avg_net_obs, y_avg_net_obs + 2 * se_avg_net_obs, t_i, c.obs_ta, 0.25);
    h_obs_net = plot(t_i, y_avg_net_obs, 'Color', c.obs_ta);
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
        ylim([-2 8]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('\DeltaAmp. (\muV/m^2)');
    plot([t_i(1), t_i(end)], 0 * ones(1, 2), 'k--');
    xlim(xlim_);
    ylim([-2.2 1.2]);
    leg_ = legend([h_obs_net], {'BCGNet - OBS'}, 'Location', 'NorthEast', 'box', 'off');
    
    %     subplot(3, 1, 3);hold on;
    %     y_avg_gar_obs = median(sm_A_coi_gar - sm_A_coi_obs, 1);
    %     se_avg_gar_obs = std_error(sm_A_coi_gar - sm_A_coi_obs, 0, 1);
    %     ciplot(y_avg_gar_obs - 2 * se_avg_gar_obs, y_avg_gar_obs + 2 * se_avg_gar_obs, t_i, [0, 0, 0], 0.25);
    %     h_gar_obs = plot(t_i, y_avg_gar_obs, 'Color', 'k');
    %     legend([h_gar_obs], {'BCE - OBS'}, 'Location', 'NorthWest', 'box', 'off')
    %     xlim_ = [t_sm, t_i(end) - t_sm];
    %     if t_max<400
    %         set(gca, 'xtick', [30:120:t_max]);
    %         ylim([-2 8]);
    %     else
    %         set(gca, 'xtick', [60:240:t_max]);
    %     end
    %     xlabel('Time (s)');
    %     ylabel('\DeltaAmplitude (\muV/m^2)');
    %     plot([t_i(1), t_i(end)], 0 * ones(1, 2), 'k--');
    %     xlim(xlim_);
    
    print_local(h_f, [9, 6]);
    %%
    h_f = figure('Name', sprintf('%s_frequency_ts', str_band, coi));clf;
    hold on;
    y_avg_net = nanmedian(sm_F_coi_net - median(sm_F_coi_net, 2), 1);
    y_avg_obs = nanmedian(sm_F_coi_obs - median(sm_F_coi_obs, 2), 1);
    y_avg_gar = nanmedian(sm_F_coi_gar - median(sm_F_coi_gar, 2), 1);
    %     se_avg_net = std_error(A_coi_net, 0, 1);
    
    %     ciplot(y_avg_net - se_avg_net, y_avg_net + se_avg_net, t_i, [0 0 0], 0.2);
    
    h_gar = plot(t_i, y_avg_gar, 'Color', c.gar_st);
    h_obs = plot(t_i, y_avg_obs, 'Color', c.obs_st);
    h_net = plot(t_i, y_avg_net, 'Color', c.net_st);
    xlim_ = [t_sm, t_i(end) - t_sm];
    if t_max<400
        set(gca, 'xtick', [30:120:t_max]);
    else
        set(gca, 'xtick', [60:240:t_max]);
    end
    xlabel('Time (s)');
    ylabel('Amplitude (\muV/m^2)');
    axis tight;
    xlim(xlim_);
    legend([h_gar, h_obs, h_net], {'BCE', 'OBS', 'BCGNet'}, 'Location', 'SouthEast', 'box', 'off')
    print_local(h_f, [9, 4]);
    %% random rubbish A vs F
    
    % figure
    % for ix_vec_sub = 1:size(sm_A_coi_net)
    %     plot(sm_A_coi_net(ix_vec_sub, n_sm:end-n_sm), sm_F_coi_net(ix_vec_sub, n_sm:end-n_sm), 'o', 'MarkerSize', 2);
    %     pause(1);
    % end
    %
    % sm_A_coi_net_0 = sm_A_coi_net - median(sm_A_coi_net(:, n_sm:end-n_sm), 2);
    % sm_F_coi_net_0 = sm_F_coi_net - median(sm_F_coi_net(:, n_sm:end-n_sm), 2);
    %
    % sm_A_coi_net_0 = sm_A_coi_net_0(:,  n_sm:end-n_sm);
    % sm_F_coi_net_0 = sm_F_coi_net_0(:,  n_sm:end-n_sm);
    % plot(sm_A_coi_net_0(:), sm_F_coi_net_0(:), 'o', 'MarkerSize', 2);
    %
    % X = [sm_A_coi_net_0(:), sm_F_coi_net_0(:)];
    % gridx1 = -50:1:50;
    % gridx2 = -2:0.1:2;
    % [x1, x2] = meshgrid(gridx1,gridx2);
    % x1 = x1(:);
    % x2 = x2(:);
    % xi = [x1(:) x2(:)];
    %
    % tic
    % figure
    % ksdensity(X,xi);
    % toc
    %%
    
    %%
end
%%
% x_net = angle(mean(exp(1i * p_coi_net(ev_qrs_mat))));
% x_obs = angle(mean(exp(1i * p_coi_obs(ev_qrs_mat))));
% t = ix_cut/fs;
%
% clf;hold on;
% plot(t, x_obs, 'r');
% plot(t, x_net, 'k');
% xlabel('Time (s)');


%%
function [y_coi, a_coi, p_coi, h_coi] = extract_coi(EEG, coi)
y_coi = EEG.data(strcmpi({EEG.chanlocs.labels}, coi), :);
h_coi = hilbert(y_coi);
a_coi = abs(h_coi);
p_coi = wrapToPi(angle(h_coi));
end