if not(exist('skip_settings', 'var') == 1)
    skip_settings = false;
end
if not(skip_settings)
    clearvars;
    close all;
    set_env;
    addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
    addpath(getenv('D_EEGLAB'));eeglab;close;
    addpath('sp_preproc');
    addpath('an_results');
    use_cv = false;  % currently false for manuscript!! 2020-02-22
    % only gets set to true for info for reviewer 2 at TBME
end

%%
d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';

%%
arch = 'gru_arch_general4';
if strcmpi(getenv('R_PROC'), 'proc_full')
    
    if use_cv
        proc_net = fullfile('proc_cv_epochs', 'multi_run', arch);
    else
        proc_net = fullfile('proc_test_epochs', 'multi_run', arch);
    end
    coi_full = { ...
        'Fz', 'F1', 'F2', 'F3', 'F4', ...
        'FC1', 'FC2', 'FC3', 'FC4', ...
        'FT7', 'FT8', ...
        'Cz', 'C1', 'C2', 'C3', 'C4', ...
        'T7', 'T8', ...
        'CPz', 'CP1', 'CP2', 'CP3', 'CP4', ...
        'TP7', 'TP8', ...
        'Pz', 'P1', 'P2', 'P3', 'P4', ...
        'Oz', 'O1', 'O2', ...
        };
elseif strcmpi(getenv('R_PROC'), 'proc_clayden')
    if use_cv
        proc_net = fullfile('proc_cv_epochs', 'single_sub', arch);
    else
        proc_net = fullfile('proc_test_epochs', 'single_sub', arch);
    end
    coi_full = { ...
        'Fz', 'F3', 'F4', ...
        'FC1', 'FC2', 'FC3', 'FC4', ...
        'FT7', 'FT8', ...
        'Cz', 'C3', 'C4', ...
        'T7', 'T8', ...
        'CPz', 'CP1', 'CP2', 'CP3', 'CP4', ...
        'TP7', 'TP8', ...
        'Pz', 'P3', 'P4', ...
        'Oz', 'O1', 'O2', ...
        };
else
    error('!');
end
T = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg);
ch_full = T.Properties.UserData.chlabels;
ch_full(strcmpi(ch_full, 'ECG')) = [];
%%
cell_band = {'delta', 'theta', 'alpha'};
met_comparison = 'div';


[T, T_sub] = get_power_difference(T, cell_band, ch_full, met_comparison, ...
    v.str_ecg, 'str_sub');


%loading topo like this is a bit ugly (run_evoked.m, handles it better)
%ideally, it would be embedded in T.Properties.UserData
topo = load(fullfile(getenv('D_OUT'), d_project, proc, 'topo_locs.mat'));
topo_locs = topo.topo_locs;
topo_locs_labels = topo.labels;

%%
vec_sub = T_sub.ix_sub';
if strcmpi(getenv('R_PROC'), 'proc_clayden')
    %     proc_net = fullfile('proc_bcgnet', 'single_sub', arch);
    es = '';
else
    %     proc_net = fullfile('proc_bcgnet', 'multi_run', arch);
    
    assert(length(vec_sub)==19, 'Missing subjects?');
    only_use_five_runs = false;
    if only_use_five_runs
        es = '_5ronly';
        vec_sub(vec_sub==15) = [];
        vec_sub(vec_sub==17) = [];
        vec_sub(vec_sub==19) = [];
        vec_sub(vec_sub==23) = [];
    else
        es = '';
    end
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';

%% unlike other scripts, this one does direct operations on the whole of T/T_sub
% so just remove unused subjects if there are any
ix_keep_T = false(height(T), 1);
ix_keep_T_sub = false(height(T_sub), 1);
for ix_sub = 1:length(cell_sub)
    ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
    ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
end
T = T(ix_keep_T, :);
T_sub = T_sub(ix_keep_T_sub, :);

%%
v.figformat = 'svg';
v.figdir = pwd;
v.figsave = true;
% v.proc = 'proc';
v.fontsizeAxes = 7;
v.fontsizeText = 6;

if use_cv
    p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig04_supplemental_with_cv');
else
    p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig04_supplemental');
end
% p_fig = '~/Desktop/temp2/';
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end

v.figdir = p_fig;

print_local = @(h, dim) printForPub(h, sprintf('f_pow_%s_%s%s', arch, h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];
%%
p_mat = nan(length(ch_full), 3);
md_mat = p_mat;
h_f = figure('Name', sprintf('topo_epoch_power'));clf;

ns = [1, length(coi_full)];
fprintf('---------\n');
for ix_ch = 1:length(ch_full)
    ch = ch_full{ix_ch};
    
    
    if strcmpi(met_comparison, 'diff')
        met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', ch, band);
        met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', ch, band);
    elseif strcmpi(met_comparison, 'div')
        met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', ch, band);
        met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', ch, band);
    else
        error('');
    end
    
    case_ch = strcmpi(ch_full{ix_ch}, topo_locs_labels);
    
    
    %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    
    vec_obs_delta = T_sub.(met1('delta'));
    vec_net_delta = T_sub.(met2('delta'));
    vec_obs_theta = T_sub.(met1('theta'));
    vec_net_theta = T_sub.(met2('theta'));
    vec_obs_alpha = T_sub.(met1('alpha'));
    vec_net_alpha = T_sub.(met2('alpha'));
    X = [vec_obs_delta; vec_net_delta; vec_obs_theta; vec_net_theta; vec_obs_alpha; vec_net_alpha];
    
    [p_obs_net_delta, ~, stats] = signrank(vec_obs_delta, vec_net_delta);
    [p_obs_net_theta, ~, stats] = signrank(vec_obs_theta, vec_net_theta);
    [p_obs_net_alpha, ~, stats] = signrank(vec_obs_alpha, vec_net_alpha);
    
    [p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha] = ...
        deal_bonf_holm([p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha]);
    
    p_mat(ix_ch, 1) = p_obs_net_delta;
    p_mat(ix_ch, 2) = p_obs_net_theta;
    p_mat(ix_ch, 3) = p_obs_net_alpha;
    md_mat(ix_ch, 1) = median(vec_obs_delta - vec_net_delta);
    md_mat(ix_ch, 2) = median(vec_obs_theta - vec_net_theta);
    md_mat(ix_ch, 3) = median(vec_obs_alpha - vec_net_alpha);
    
    if any(strcmpi(ch_full{ix_ch}, coi_full))
        ax = get_ax_topo(case_ch, ch_full{ix_ch}, topo_locs);
        
        hold on;
        G = [1 * ones(size(vec_obs_delta)); 2 * ones(size(vec_net_delta)); 3 * ones(size(vec_obs_theta)); 4 * ones(size(vec_net_theta)); 5 * ones(size(vec_obs_alpha)); 6 * ones(size(vec_net_alpha))];
        h_box = boxplot(X, ...
            G, ...
            'ColorGroup', G, ...
            'Colors', repmat([cmap(2, :);cmap(3, :)], 3, 1), ...
            'labels',repmat({'OBS', 'BCGNet'}, 1, 3), 'Symbol', '');
        box off;
        %     axes('Color', 'none', 'XColor', 'none');
        set(findobj(h_box,'LineStyle','--'),'LineStyle','-')
        set(h_box, {'linew'}, {1.00});
        av = findobj(gca, 'tag', 'Lower Adjacent Value');
        for ix = 1:length(av)
            av(ix).XData = av(ix).XData + [-0.01, +0.01];
        end
        av = findobj(gca, 'tag', 'Upper Adjacent Value');
        for ix = 1:length(av)
            av(ix).XData = av(ix).XData + [-0.01, +0.01];
        end
        
        c_grey = 0.7*ones(1, 3);
        c_alpha = 0.4;
        MarkerSize = 3;
        MarkerEdgeColor = [1,1,1];
        % note that in inkscape you then need to add a 0.1mm border to each
        % point (find property circle)
        
        for ix_vec_sub = 1:length(cell_sub)
            %         h = plot([1, 2], [vec_obs_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %         h = plot([3, 4], [vec_obs_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %         h = plot([5, 6], [vec_obs_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            
            for ix = 1:6
                switch ix
                    case 1, vec_local = vec_obs_delta(ix_vec_sub);
                    case 2, vec_local = vec_net_delta(ix_vec_sub);
                    case 3, vec_local = vec_obs_theta(ix_vec_sub);
                    case 4, vec_local = vec_net_theta(ix_vec_sub);
                    case 5, vec_local = vec_obs_alpha(ix_vec_sub);
                    case 6, vec_local = vec_net_alpha(ix_vec_sub);
                end
                a = get(h_box(1, ix));
                a = a.YData(end);
                %             a = av(ix).YData(end)
                if vec_local > a
                    plot(ix, vec_local, ...
                        '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
                        'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
                end
            end
        end
        
        if strcmpi(ch, 'AVG')
            ylim([-0.05, 1.1   ])
            p_line_height = 1.;
        else
            ylim([-0.05, 1.6])
            p_line_height = 0.0;
        end
        
        y_range = [min(X(:)), max(X(:))];
        d = (diff(y_range)*0.075);
        tooth_length = -d * 0.25;
        line_height = y_range(2) + d;
        
        va = 'top';
        [p_obs_net_delta, ~, md_obs_net_delta] = plot_stars(vec_obs_delta, vec_net_delta, [1, 2], p_line_height, tooth_length, p_obs_net_delta, va);
        [p_obs_net_theta, ~, md_obs_net_theta] = plot_stars(vec_obs_theta, vec_net_theta, [3, 4], p_line_height, tooth_length, p_obs_net_theta, va);
        [p_obs_net_alpha, ~, md_obs_net_alpha] = plot_stars(vec_obs_alpha, vec_net_alpha, [5, 6], p_line_height, tooth_length, p_obs_net_alpha, va);
        
        
        
        %         xtickangle(45)
        
        title(ch);
        fprintf('---\n');
        set(gca, 'xtick',[])
        set(gca, 'xticklabel',[])
        set(gca, 'xtick',[1.5, 3.5, 5.5])
        set(gca, 'xticklabel', {'Delta', 'Theta', 'Alpha'})
        
        fprintf('%s:\n(', ch);
        fprintf('delta MD = %0.2f, p = %s; ', md_obs_net_delta, p_to_str(p_obs_net_delta));
        fprintf('theta MD = %0.2f, p = %s; ', md_obs_net_theta, p_to_str(p_obs_net_theta));
        fprintf('alpha MD = %0.2f, p = %s', md_obs_net_alpha, p_to_str(p_obs_net_alpha));
        fprintf(')\n');
        % ylabel('AUC')
        
        %     ylabel('Power ratio')
        %     axis off;
        ax.Color = 'none';
        ax.XColor = 'none';
        y_max = max(X(:)) + 0.01;
        y_max(y_max<1.0) = 1.11;
        y_max(y_max>1.5) = 1.51;
        ylim([0, y_max])
    end
    
end
print_local(h_f, [18.1, 22]);
%%
n_ch = length(ch_full);
fprintf('\n')
for ix_cell_band = 1:length(cell_band)
    md_sig = md_mat(p_mat(:, ix_cell_band)<0.05, ix_cell_band);
    sig_pc_obs_better = 100*sum(md_sig < 0)/n_ch;
    sig_pc_net_better = 100*sum(md_sig > 0)/n_ch;
    %     sig_frac = length()/size(p_mat, 1);
    fprintf('%s: OBS better:%0.1f%%, BCGNet better:%0.1f%% \n', cell_band{ix_cell_band}, sig_pc_obs_better, sig_pc_net_better)
end