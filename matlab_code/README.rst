************
Introduction
************

:Authors: J R McIntosh, J Yao

Usage
=======

run_preprocessing_ga
 | Runs sp_rmga.m, which by default performs FASTR template subtraction with OBS (ECG is treated sepearately, and only undergoes template subtraction). A template is constructed locally our of 30GAs, and the data is preprocessed by low-pass filtering at 70Hz. Data is saved (by default) to the proc_ga folder.

run_preprocessing_bcg.m
 |  Runs sp_rmbcg.m which by default has a mode of operation set with the addmc flag set to true.

 |  When this function is run, the first thing that happens is a call to sp_resample.m: all the GA removed data is resampled to 500Hz, high-pass filtered at 0.25Hz (so bw is now 0.25-70Hz), motion data from fMRI processing is loaded in (from proc_mcf), and sliced from the first to last TR [this should really be split...]. This output is stored in the proc_rs folder.

 | The output of prc_rs is then loaded, and sp_rmbcg.m m can continue. QRS detection occurs first: a copy of the data is made, high passed at 2.5Hz and fmrib_qrsdetect is run. Markers are then used in conjunction with the original data loaded from proc_rs to remove BCG using fmrib_pas (OBS is default). The output is saved to proc_bcgobs.

run_evoked.m
 | Epochs the data (written into proc_epoch), and then generates figures for stimulus evoked responses.
 | By default the motion data is removed, baseline is subtracted, 0.5Hz high-pass filter is implemented, scalp laplacian is applied, and data is cut from -0.5 to 1.5s. Data is not (by default), baseline subtracted (the highpass should be enough).

run_classifier.m
 | Uses the epoched data and (by default) a matlab implementation of glmnet (https://web.stanford.edu/~hastie/glmnet_matlab/) to classify between targets and standards. Figures generated are of AUC.
 | Defaults are: data is split with 6 folds, time slices are 60ms long, and 20ms apart (@500Hz), pure ridge regression is used with an internal 6 fold CV to estimate lambda. RNG is treated very carefully here to match classifications across methods as much as possible.

mat_to_eeglab.m
 | ?

BVtoEEGLAB
 | ? is this used?

run_introductory_figure.m
 | Generates figure 1 of the paper from a pregenerated slice of EEG/ECG

run_qrs_check.m
 | Generates some summary statistics about QRS detection, written into proc_qrs.

set_env.m
 | The most important function when running these functions on a new system. See the commented dnc_set_env.m in set_env.m for how to use.