clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%
d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';

%%
arch_as = 'gru_arch_realtime03';
arch = 'gru_arch_general4';

%%
op_additional = {};
proc_net = fullfile('proc_test_epochs', 'real_time', arch_as);
T = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg, 'es', '_rt', 'overwrite', v.overwrite);

%%
proc_net = fullfile('proc_test_epochs', 'multi_run', arch);
T_mr = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg, 'overwrite', false);

% net trained takes the role of realtime, while net takes the role of
% normal general04 (BCGNet eval)
%% merge the p_net from multi_run in
cell_sub = unique(T.str_sub);
for ix_cell_sub = 1:length(cell_sub)
    str_sub = cell_sub{ix_cell_sub};
    case_sub = strcmpi(T.str_sub, str_sub);
    vec_run = sort(unique(T.ix_run(case_sub)));
    for ix_vec_run = 1:length(vec_run)
        ix_run = vec_run(ix_vec_run);
        case_run = vec_run(ix_vec_run) == T.ix_run;
        case_file = case_run & case_sub;
        
        case_run_mr = vec_run(ix_vec_run) == T_mr.ix_run;
        case_sub_mr = strcmpi(T_mr.str_sub, str_sub);
        
        case_file_mr = case_run_mr & case_sub_mr;
        
        T.p_net{case_file} = T_mr.p_net{case_file_mr};
        
    end
end

%%
cell_band = {'delta', 'theta', 'alpha'};
met_comparison = 'div';
cell_coi = {'AVG'};
[T, T_sub] = get_power_difference(T, cell_band, cell_coi, met_comparison, ...
    v.str_ecg, 'str_sub', op_additional);

%%
vec_sub = T_sub.ix_sub';
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = '_5ronly';
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = '';
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
error('Check subject selection before use');
%% unlike other scripts, this one does direct operations on the whole of T/T_sub
% so just remove unused subjects if there are any
ix_keep_T = false(height(T), 1);
ix_keep_T_sub = false(height(T_sub), 1);
for ix_sub = 1:length(cell_sub)
ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
end
T = T(ix_keep_T, :);
T_sub = T_sub(ix_keep_T_sub, :);

%%
vobs.figformat = 'svg';
vobs.figdir = pwd;
vobs.figsave = true;
vobs.proc = 'proc';
v.fontsizeAxes = 7;
v.fontsizeText = 8;


p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures', 'fig_supp_rt');
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
%
% if not(vobs.figsave)
%     warning('Not saving figures!');
% end
print_local = @(h, dim) printForPub(h, sprintf('f_pow_rt_%s_%s%s', arch_as, h.Name, es), 'doPrint', vobs.figsave,...
    'fformat', vobs.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];

%%
h_f = figure('Name', sprintf('epoch_power'));clf;

ns = [1, length(cell_coi)];
fprintf('---------\n');
for ix_coi = 1:length(cell_coi)
    coi = cell_coi{ix_coi};
    
    
    if strcmpi(met_comparison, 'diff')
        met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', coi, band);
        met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', coi, band);
        met3 = @(band) sprintf('%s_%s_p_gar_minus_p_net_trained', coi, band);
    elseif strcmpi(met_comparison, 'div')
        met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', coi, band);
        met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', coi, band);
        met3 = @(band) sprintf('%s_%s_p_net_trained_over_p_gar', coi, band);
    else
        error('');
    end
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    %     vec_obs_delta = T_sub.(met1('delta'));
    vec_net_delta = T_sub.(met2('delta'));
    %     vec_obs_theta = T_sub.(met1('theta'));
    vec_net_theta = T_sub.(met2('theta'));
    %     vec_obs_alpha = T_sub.(met1('alpha'));
    vec_net_alpha = T_sub.(met2('alpha'));
    %     vec_net_pretrained_delta = T_sub.(met3('delta'));
    vec_net_trained_delta = T_sub.(met3('delta'));
    %     vec_net_pretrained_theta = T_sub.(met3('theta'));
    vec_net_trained_theta = T_sub.(met3('theta'));
    %     vec_net_pretrained_alpha = T_sub.(met3('alpha'));
    vec_net_trained_alpha = T_sub.(met3('alpha'));
    
    X = [vec_net_trained_delta; vec_net_delta;...
        vec_net_trained_theta; vec_net_theta;...
        vec_net_trained_alpha; vec_net_alpha];
    G = [1:6] + zeros(size(vec_net_trained_delta));
    G = G(:);
    
    hold on;
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', repmat([c.net_ta; c.net_st], 3, 1), ...
        'labels',repmat({'BCGNet RT', 'BCGNet'}, 1, 3), 'Symbol', '.');
    box off;
    set(h_box, {'linew'}, {1.75});
    av = findobj(gca, 'tag', 'Lower Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    av = findobj(gca, 'tag', 'Upper Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    
    c_grey = 0.7*ones(1, 3);
    for ix_vec_sub = 1:length(cell_sub)
        MarkerSize = 2.5;
        %         plot([1, 2], [vec_net_pretrained_delta(ix_vec_sub), vec_net_trained_delta(ix_vec_sub)], ...
        %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        %         plot([4, 5], [vec_net_pretrained_theta(ix_vec_sub), vec_net_trained_theta(ix_vec_sub)], ...
        %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        % %         plot([7, 8], [vec_net_pretrained_alpha(ix_vec_sub), vec_net_trained_alpha(ix_vec_sub)], ...
        %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        
        plot([1, 2], [vec_net_trained_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([3, 4], [vec_net_trained_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([5, 6], [vec_net_trained_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
    end
    
    y_range = [min(X(:)), max(X(:))];
    d = (diff(y_range)*0.175);
    tooth_length = d * 0.15;
    h = 1.4;
    line_height = y_range(2) + d;
    
    [p_net_trained_net_delta, ~, md_net_trained_net_delta] = ...
        plot_stars(vec_net_trained_delta, vec_net_delta, [1, 2], h, tooth_length);
    [p_net_trained_net_theta, ~, md_net_trained_net_theta] = ...
        plot_stars(vec_net_trained_theta, vec_net_theta, [3, 4], h, tooth_length);
    [p_net_trained_net_alpha, ~, md_net_trained_net_alpha] = ...
        plot_stars(vec_net_trained_alpha, vec_net_alpha, [5, 6], h, tooth_length);
    
    [p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha] = ...
        deal_bonf_holm(...
        [p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha]...
        );
    
    
    ylim([-0.05, y_range(2) + 1.5*d])
    ylim([-0.05, 1.55   ])
    %         xtickangle(45)
    
    %     title(coi);
    fprintf('---\n');
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'xtick',[1.5, 3.5, 5.5])
    set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})
    
    fprintf('%s:\n(', coi);
    fprintf('delta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_delta, p_net_trained_net_delta);
    fprintf('theta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_theta, p_net_trained_net_theta);
    fprintf('alpha MD_RE-WS = %0.2f, p = %0.3f', md_net_trained_net_alpha, p_net_trained_net_alpha);
    fprintf(')\n');
    % ylabel('AUC')
    
    ylabel('Power ratio')
    
    
end
print_local(h_f, [18.1/2, 5]);

%%


if strcmpi(met_comparison, 'diff')
    met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', coi, band);
    met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', coi, band);
    met3 = @(band) sprintf('%s_%s_p_gar_minus_p_net_trained', coi, band);
elseif strcmpi(met_comparison, 'div')
    met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', coi, band);
    met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', coi, band);
    met3 = @(band) sprintf('%s_%s_p_net_trained_over_p_gar', coi, band);
else
    error('');
end

for ix_cell_band = 1:length(cell_band)
%     subplot(1, length(cell_band), ix_cell_band);cla;hold on;
    
    vec_net = T.(met2(cell_band{ix_cell_band}));
    vec_net_trained = T.(met3(cell_band{ix_cell_band})); % this is rt

    mat_net = nan(length(cell_sub), max(T.ix_run));
    mat_net_trained = nan(length(cell_sub), max(T.ix_run));

    for ix_sub = 1:length(cell_sub)
        case_sub = strcmpi(cell_sub{ix_sub}, T.str_sub);
        for ix_run = 1:max(T.ix_run)
            case_run = T.ix_run == ix_run;
            mat_net(ix_sub, ix_run) =  vec_net(case_sub & case_run);
            mat_net_trained(ix_sub, ix_run) =  vec_net_trained(case_sub & case_run);
        end
        
    end
    figure(ix_cell_band);clf;
    hold on;plot(mat_net', 'k.-');plot(mat_net_trained', 'r.-');
    title(cell_band{ix_cell_band});
%     X = [vec_net_trained_delta; vec_net_delta;...
%         vec_net_trained_theta; vec_net_theta;...
%         vec_net_trained_alpha; vec_net_alpha];
%     G = [1:6] + zeros(size(vec_net_trained_delta));
%     G = G(:);
%     
%     hold on;
%     h_box = boxplot(X, ...
%         G, ...
%         'ColorGroup', G, ...
%         'Colors', repmat([c.net_ta; c.net_st], 3, 1), ...
%         'labels',repmat({'BCGNet RT', 'BCGNet'}, 1, 3), 'Symbol', '.');
%     box off;
%     set(h_box, {'linew'}, {1.75});
%     av = findobj(gca, 'tag', 'Lower Adjacent Value');
%     for ix = 1:length(av)
%         av(ix).XData = av(ix).XData + [-0.01, +0.01];
%     end
%     av = findobj(gca, 'tag', 'Upper Adjacent Value');
%     for ix = 1:length(av)
%         av(ix).XData = av(ix).XData + [-0.01, +0.01];
%     end
%     
%     c_grey = 0.7*ones(1, 3);
%     for ix_vec_sub = 1:length(cell_sub)
%         MarkerSize = 2.5;
%         %         plot([1, 2], [vec_net_pretrained_delta(ix_vec_sub), vec_net_trained_delta(ix_vec_sub)], ...
%         %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%         %         plot([4, 5], [vec_net_pretrained_theta(ix_vec_sub), vec_net_trained_theta(ix_vec_sub)], ...
%         %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%         % %         plot([7, 8], [vec_net_pretrained_alpha(ix_vec_sub), vec_net_trained_alpha(ix_vec_sub)], ...
%         %             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%         
%         plot([1, 2], [vec_net_trained_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
%             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%         plot([3, 4], [vec_net_trained_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
%             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%         plot([5, 6], [vec_net_trained_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
%             'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%     end
%     
%     y_range = [min(X(:)), max(X(:))];
%     d = (diff(y_range)*0.175);
%     tooth_length = d * 0.15;
%     h = 1.4;
%     line_height = y_range(2) + d;
%     
%     [p_net_trained_net_delta, ~, md_net_trained_net_delta] = ...
%         plot_stars(vec_net_trained_delta, vec_net_delta, [1, 2], h, tooth_length);
%     [p_net_trained_net_theta, ~, md_net_trained_net_theta] = ...
%         plot_stars(vec_net_trained_theta, vec_net_theta, [3, 4], h, tooth_length);
%     [p_net_trained_net_alpha, ~, md_net_trained_net_alpha] = ...
%         plot_stars(vec_net_trained_alpha, vec_net_alpha, [5, 6], h, tooth_length);
%     
%     [p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha] = ...
%         deal_bonf_holm(...
%         [p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha]...
%         );
%     
%     
%     ylim([-0.05, y_range(2) + 1.5*d])
%     ylim([-0.05, 1.55   ])
%     %         xtickangle(45)
%     
%     %     title(coi);
%     fprintf('---\n');
%     set(gca,'xtick',[])
%     set(gca,'xticklabel',[])
    set(gca,'xtick',1:max([T.ix_run]));
    xlabel('Run number');
    ylabel('Power ratio');
%     set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})
%     
%     fprintf('%s:\n(', coi);
%     fprintf('delta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_delta, p_net_trained_net_delta);
%     fprintf('theta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_theta, p_net_trained_net_theta);
%     fprintf('alpha MD_RE-WS = %0.2f, p = %0.3f', md_net_trained_net_alpha, p_net_trained_net_alpha);
%     fprintf(')\n');
%     % ylabel('AUC')
%     
%     ylabel('Power ratio')
    
    ylim([0, 2]);
    grid on;
end
% print_local(h_f, [18.1/2, 5]);