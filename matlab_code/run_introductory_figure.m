clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_explot');

%%
d_project = 'working_eegbcg';
reref = 'laplace';
proc = getenv_check_empty('R_PROC');
%%
overwrite = false;
%% Load an EEG dataset as example
ix_sub = 33;
d_proc_ga = fullfile(getenv('D_OUT'), d_project, proc, 'proc_ga');
f_proc_ga = fullfile(d_proc_ga, sprintf('sub%02d', ix_sub), sprintf('sub%02d_r01_rmga', ix_sub));
EEG = pop_loadset('filename', sprintf('%s.set', f_proc_ga));
EEG = pop_chanedit(EEG, 'lookup',fullfile(getenv('D_EEGLAB'),'plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc'));
for ix_chan = 1:length(EEG.chanlocs)
    EEG.chanlocs(ix_chan).type = 'EEG';
end
case_ecg = strcmpi({EEG.chanlocs.labels}, 'ECG');
EEG.chanlocs(case_ecg).type = 'ECG';
EEG = eeg_checkset( EEG );

%% this file is generated manually by Desmond in Python
M = load(fullfile(getenv('D_OUT'), d_project, proc, 'figures', 'fig01', sprintf('sub%02d_all_data.mat', ix_sub)));
M = M.data;
t = [0:1/EEG.srate:(size(M, 2)-1)/EEG.srate];
M = circshift(M, -27, 2); % just to make things line up a bit more nicely... 
%%
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig01', 'bits');
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
vobs.figformat = 'svg';
vobs.figdir = pwd;
vobs.figsave = true;
if not(vobs.figsave)
    warning('Not saving figures!');
end
print_local = @(f, dim) printForPub(f, sprintf('f_intro_%s', f.Name), 'doPrint', vobs.figsave,...
    'fformat', vobs.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig);
% cmap = [[1, 0, 0];[0, 0, 1]];
c_obs_st = [091, 194, 062]/255;
c_net_st = [068, 120, 070]/255;
c_obs_ta = [101, 136, 205]/255;
c_net_ta = [073, 069, 164]/255;
c_neutral= [177, 177, 176]/255;
c_ekg = [0.6350    0.0780    0.1840];
cmap = [c_ekg; c_neutral; c_neutral; c_neutral];

%%
coi = {'ECG', 'T8', 'Pz'};
t_s = 0.075;
t_d = 0.025;
t_slice = [t_s:t_d:t_d*4 + t_s];
ix_slice = nan(size(t_slice));
for ix_t_slice = 1:length(t_slice)
    [~, ix_slice(ix_t_slice)] = min(abs(t_slice(ix_t_slice) - t));
end
%%
h_f1 = figure('Name', 'introduction_ts');clf;
% cmap = [[0.8, 0.1, 0.1];[lines
n_p = 4;
for ix_subplot = 1:n_p-1
    subplot(n_p, 1, ix_subplot);cla;hold on;
    case_ch = strcmpi({EEG.chanlocs.labels}, coi{ix_subplot});
    plot([t(1), t(end)], [0 0],'--', 'Color', 0.9 * ones(1, 3));
    for ix_t_slice = 1:length(t_slice)
        plot(t_slice(ix_t_slice) * ones(1, 2), 1000 * [-1, 1], '--', 'Color', 0.25 * ones(1, 3));
    end
    plot(t, M(case_ch, :), 'Color', cmap(ix_subplot, :), ...
        'lineWidth', 2);
    ylabel('Amp. (µV)');
    box off;
    y_ = 80;
    if ix_subplot==1
        ylim([-1, 1]* 700)
        set(gca,'xtick',[]);
        set(gca,'xcolor',[1 1 1])
        text(0.0075, 450, coi{ix_subplot})
    elseif ix_subplot == 2
        ylim([-y_, y_]);
        set(gca,'xtick',[]);
        set(gca,'xcolor',[1 1 1])
        text(0.0075, 50, coi{ix_subplot})
    elseif ix_subplot == 3
        ylim([-y_, y_]);
        set(gca,'xtick',[]);
        set(gca,'xcolor',[1 1 1])
        text(0.0075, 50, coi{ix_subplot})
    else
        ylim([-105, 105]);
        text(0.0075, 90, coi{ix_subplot})
        
    end
end
xlabel('Time (s)');
set(h_f1, 'Renderer', 'painters');
print_local(h_f1, [8.8, 9]);

%%
h_f2 = figure('Name', 'introduction_topo');clf;
maplims = [-30, +30];
for ix_t_slice = 1:length(t_slice)
    subplot(1, length(t_slice), ix_t_slice);cla;
    topoplot(M(:, ix_slice(ix_t_slice)), EEG.chanlocs , ...
        'style', 'fill', 'electrodes', 'pts', 'conv', 'on', 'chantype', 'EEG', 'chaninfo', EEG.chaninfo, ...
        'maplimits', maplims, ...
        'plotrad', 0.55, 'colormap', parula, 'plotdisk', 'on');
    title(sprintf('%0.3fs', t_slice(ix_t_slice)))
end
% set(h_f2, 'Renderer', 'painters');
print_local(h_f2, [18.1, 10]);
%%
maplims = [-30, +30];
for ix_t_slice = 1:length(t_slice)
   h_f2_singles = figure('Name', sprintf('introduction_topo_%d_t_%0.3f', ix_t_slice, t_slice(ix_t_slice)));clf;
    topoplot(M(:, ix_slice(ix_t_slice)), EEG.chanlocs , ...
        'style', 'fill', 'electrodes', 'pts', 'conv', 'on', 'chantype', 'EEG', 'chaninfo', EEG.chaninfo, ...
        'maplimits', maplims, ...
        'plotrad', 0.55, 'colormap', parula, 'plotdisk', 'on');
%     set(h_f2_singles, 'Renderer', 'painters');
    print_local(h_f2_singles, [4, 4]);
end
%%
h_f_colorbar = figure('Name', 'introduction_colorbar_extract');clf;

v_maplims = maplims(1):maplims(end);
parula(length(v_maplims));
h = topoplot(M(:, ix_slice(1)), EEG.chanlocs , ...
    'style', 'fill', 'electrodes', 'pts', 'conv', 'on', 'chantype', 'EEG', 'chaninfo', EEG.chaninfo, ...
    'maplimits', maplims, ...
    'plotrad', 0.55, 'colormap', parula, 'plotdisk', 'on');
h.Visible = 'off';
colorbar;
set(h_f_colorbar, 'Renderer', 'painters');
print_local(h_f_colorbar, [10, 4]);
