clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_explot');

%%
d_project = 'working_eegbcg';
reref = 'laplace';
proc = getenv_check_empty('R_PROC');
%%
overwrite = false;
%% Load an EEG dataset as example
ix_sub = 33;
ix_run = 1;

d_proc_qrs = fullfile(getenv('D_OUT'), d_project, proc, 'proc_bcgobs');
f_proc_qrs = fullfile(d_proc_qrs, sprintf('sub%02d', ix_sub), sprintf('sub%02d_r%02d_rmbcg', ix_sub, ix_run));
EEG_qrs = pop_loadset('filename', sprintf('%s.set', f_proc_qrs));
vec_qrs = [EEG_qrs.event(strcmpi({EEG_qrs.event.type}, 'qrs0')).latency];

d_proc_rs = fullfile(getenv('D_OUT'), d_project, proc, 'proc_rs');
f_proc_rs = fullfile(d_proc_rs, sprintf('sub%02d', ix_sub), sprintf('sub%02d_r%02d_rs', ix_sub, ix_run));
EEG = pop_loadset('filename', sprintf('%s.set', f_proc_rs));
EEG = pop_chanedit(EEG, 'lookup',fullfile(getenv('D_EEGLAB'),'plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc'));
for ix_chan = 1:length(EEG.chanlocs)
    EEG.chanlocs(ix_chan).type = 'EEG';
end
case_ecg = strcmpi({EEG.chanlocs.labels}, 'ECG');
EEG.chanlocs(case_ecg).type = 'ECG';
EEG = eeg_checkset( EEG );
%%
v.figformat = 'svg';
v.figdir = pwd;
v.figsave = true;
% v.proc = 'proc';
v.fontsizeAxes = 7;
v.fontsizeText = 6;

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'reviewers_only');
% p_fig = '~/Desktop/temp2/';
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end

v.figdir = p_fig;
es = '';
print_local = @(h, dim) printForPub(h, sprintf('f_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

n = 100;
c_ecg = c.ecg;c_ecg(c_ecg>1) = 1;c_ecg(c_ecg<0) = 0;
cmap_r = linspace(c_ecg(1), 0.25, n);
cmap_g = linspace(c_ecg(2), 0.25, n);
cmap_b = linspace(c_ecg(3), 0.25, n);
cmap_odd = [cmap_r(:), cmap_g(:), cmap_b(:)];
%%
EEG_f = pop_eegfiltnew(EEG, 2.5, [], [], 0, [], 1);
% EEG_f = EEG;
%%
y = EEG_f.data(case_ecg, :);
t = EEG_f.times/1000;

h_f = figure('Name', sprintf('qrs_time_series'));clf;hold on;
plot(t, y);
for ix = 1:length(vec_qrs)
    plot(t(vec_qrs(ix)) * ones(1,2), get(gca, 'ylim'), 'k-');
end
plot(t, y, 'Color', c.ecg);
xlim([150, 160]);
ylim([-500, 500]);
ylabel('Amplitude (μV)');
xlabel('Time (s)');
print_local(h_f, [18.1, 4]);



ix_win = -round(EEG_f.srate/4):round(EEG_f.srate/4);
t_win = ix_win/EEG_f.srate;
mat_lat = vec_qrs'  + ix_win;
mat_lat([1, end], :) = [];
Y = y(mat_lat);

h_f = figure('Name', sprintf('qrs_lock_example'));clf;hold on;
rng(0);
for ix = 1:size(Y, 1)
    plot(t_win, Y(ix, :), 'Color', 0.75 * rand * [1, 1, 1]);
end
xlabel('QRS-locked time (s)');
ylabel('Amplitude (μV)');
axis tight;
plot(t_win, nanmean(Y, 1), 'lineWidth', 2, 'Color', c.ecg);
h0 = plot(ones(1, 2) * 0, get(gca, 'ylim'), 'k--');
uistack(h0, 'bottom');
print_local(h_f, [8.8, 4]);
