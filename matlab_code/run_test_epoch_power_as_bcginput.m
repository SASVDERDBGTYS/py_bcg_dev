clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%
d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';

%%
arch_as = 'gru_arch_bcginput01';
arch = 'gru_arch_general4';

%%
op_additional = {'pretrained_epoch', 'heldout_epoch', 'across_run_epoch'};
proc_net = fullfile('proc_test_epochs', 'multi_sub', arch_as);
T = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg, 'es', '_ms');

%%
proc_net = fullfile('proc_test_epochs', 'multi_run', arch);
T_mr = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg);

%% merge the p_net from multi_run in
cell_sub = unique(T.str_sub);
for ix_cell_sub = 1:length(cell_sub)
    str_sub = cell_sub{ix_cell_sub};
    case_sub = strcmpi(T.str_sub, str_sub);
    vec_run = sort(unique(T.ix_run(case_sub)));
    for ix_vec_run = 1:length(vec_run)
        ix_run = vec_run(ix_vec_run);
        case_run = vec_run(ix_vec_run) == T.ix_run;
        case_file = case_run & case_sub;
        
        case_run_mr = vec_run(ix_vec_run) == T_mr.ix_run;
        case_sub_mr = strcmpi(T_mr.str_sub, str_sub);
        
        case_file_mr = case_run_mr & case_sub_mr;
        
        T.p_net{case_file} = T_mr.p_net{case_file_mr};
        
    end
end

%%
cell_band = {'delta', 'theta', 'alpha'};
met_comparison = 'div';
cell_coi = {'AVG'};
[T, T_sub] = get_power_difference(T, cell_band, cell_coi, met_comparison, ...
    v.str_ecg, 'str_sub', op_additional);

%%
vec_sub = T_sub.ix_sub';
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = '_5ronly';
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = '';
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
error('Check subject selection before use');
%% unlike other scripts, this one does direct operations on the whole of T/T_sub
% so just remove unused subjects if there are any
ix_keep_T = false(height(T), 1);
ix_keep_T_sub = false(height(T_sub), 1);
for ix_sub = 1:length(cell_sub)
ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
end
T = T(ix_keep_T, :);
T_sub = T_sub(ix_keep_T_sub, :);

%%
vobs.figformat = 'svg';
vobs.figdir = pwd;
vobs.figsave = true;
vobs.proc = 'proc';
v.fontsizeAxes = 7;
v.fontsizeText = 8;


p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures', 'fig_supp_bcginput');
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
%
% if not(vobs.figsave)
%     warning('Not saving figures!');
% end
print_local = @(h, dim) printForPub(h, sprintf('f_pow_ms_%s_%s%s', arch_as, h.Name, es), 'doPrint', vobs.figsave,...
    'fformat', vobs.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];

%%
h_f = figure('Name', sprintf('epoch_power'));clf;

ns = [1, length(cell_coi)];
fprintf('---------\n');
for ix_coi = 1:length(cell_coi)
    coi = cell_coi{ix_coi};
    
    
    if strcmpi(met_comparison, 'diff')
        met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', coi, band);
        met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', coi, band);
        met3 = @(band) sprintf('%s_%s_p_gar_minus_p_net_pretrained', coi, band);
        met4 = @(band) sprintf('%s_%s_p_gar_minus_p_net_trained', coi, band);
    elseif strcmpi(met_comparison, 'div')
        met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', coi, band);
        met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', coi, band);
        met3 = @(band) sprintf('%s_%s_p_net_pretrained_over_p_gar', coi, band);
        met4 = @(band) sprintf('%s_%s_p_net_trained_over_p_gar', coi, band);
    else
        error('');
    end
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    vec_obs_delta = T_sub.(met1('delta'));
    vec_net_delta = T_sub.(met2('delta'));
    vec_obs_theta = T_sub.(met1('theta'));
    vec_net_theta = T_sub.(met2('theta'));
    vec_obs_alpha = T_sub.(met1('alpha'));
    vec_net_alpha = T_sub.(met2('alpha'));
    vec_net_pretrained_delta = T_sub.(met3('delta'));
    vec_net_trained_delta = T_sub.(met4('delta'));
    vec_net_pretrained_theta = T_sub.(met3('theta'));
    vec_net_trained_theta = T_sub.(met4('theta'));
    vec_net_pretrained_alpha = T_sub.(met3('alpha'));
    vec_net_trained_alpha = T_sub.(met4('alpha'));
    
    X = [vec_net_pretrained_delta; vec_net_trained_delta; vec_net_delta;...
        vec_net_pretrained_theta; vec_net_trained_theta; vec_net_theta;...
        vec_net_pretrained_alpha; vec_net_trained_alpha; vec_net_alpha];
    G = [1:9] + zeros(size(vec_net_trained_delta));
    G = G(:);
    
    hold on;
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', repmat([c.neutral_dark; c.net_ta; c.net_st], 3, 1), ...
        'labels',repmat({'BCGNet pre-trained', 'BCGNet re-trained', 'BCGNet'}, 1, 3), 'Symbol', '.');
    box off;
    set(h_box, {'linew'}, {1.75});
    av = findobj(gca, 'tag', 'Lower Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    av = findobj(gca, 'tag', 'Upper Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.01, +0.01];
    end
    
    c_grey = 0.7*ones(1, 3);
    for ix_vec_sub = 1:length(cell_sub)
        MarkerSize = 2.5;
        plot([1, 2], [vec_net_pretrained_delta(ix_vec_sub), vec_net_trained_delta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([4, 5], [vec_net_pretrained_theta(ix_vec_sub), vec_net_trained_theta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([7, 8], [vec_net_pretrained_alpha(ix_vec_sub), vec_net_trained_alpha(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        
        plot([2, 3], [vec_net_trained_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([5, 6], [vec_net_trained_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
        plot([8, 9], [vec_net_trained_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
    end
    
    y_range = [min(X(:)), max(X(:))];
    d = (diff(y_range)*0.175);
    tooth_length = d * 0.15;
    h = 1.4;
    line_height = y_range(2) + d;
    
    [p_net_pretrained_net_trained_delta, ~, md_net_pretrained_net_trained_delta] = ...
        plot_stars(vec_net_pretrained_delta, vec_net_trained_delta, [1, 2], h, tooth_length);
    [p_net_pretrained_net_trained_theta, ~, md_net_pretrained_net_trained_theta] = ...
        plot_stars(vec_net_pretrained_theta, vec_net_trained_theta, [4, 5], h, tooth_length);
    [p_net_pretrained_net_trained_alpha, ~, md_net_pretrained_net_trained_alpha] = ...
        plot_stars(vec_net_pretrained_alpha, vec_net_trained_alpha, [7, 8], h, tooth_length);
    
    [p_net_trained_net_delta, ~, md_net_trained_net_delta] = ...
        plot_stars(vec_net_trained_delta, vec_net_delta, [2, 3], h, tooth_length);
    [p_net_trained_net_theta, ~, md_net_trained_net_theta] = ...
        plot_stars(vec_net_trained_theta, vec_net_theta, [5, 6], h, tooth_length);
    [p_net_trained_net_alpha, ~, md_net_trained_net_alpha] = ...
        plot_stars(vec_net_trained_alpha, vec_net_alpha, [8, 9], h, tooth_length);
    
    [p_net_pretrained_net_trained_delta, p_net_pretrained_net_trained_theta, p_net_pretrained_net_trained_alpha,...
        p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha] = ...
        deal_bonf_holm(...
        [p_net_pretrained_net_trained_delta, p_net_pretrained_net_trained_theta, p_net_pretrained_net_trained_alpha,...
        p_net_trained_net_delta, p_net_trained_net_theta, p_net_trained_net_alpha]...
        );
    
    %     p_net_pretrained_net_delta = plot_stars(vec_net_pretrained_delta, vec_net_delta, [1, 3], h + d, tooth_length);
    %     p_net_pretrained_net_theta = plot_stars(vec_net_pretrained_theta, vec_net_theta, [4, 6], h + d, tooth_length);
    %     p_net_pretrained_net_alpha = plot_stars(vec_net_pretrained_alpha, vec_net_alpha, [7, 9], h + d, tooth_length);
    %
    ylim([-0.05, y_range(2) + 1.5*d])
    ylim([-0.05, 1.55   ])
    %         xtickangle(45)
    
    %     title(coi);
    fprintf('---\n');
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'xtick',[2, 5, 8])
    set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})
    
    fprintf('%s:\n(', coi);
    fprintf('delta MD_PT-RE = %0.2f, p = %0.3f; ', md_net_pretrained_net_trained_delta, p_net_pretrained_net_trained_delta);
    fprintf('theta MD_PT-RE = %0.2f, p = %0.3f; ', md_net_pretrained_net_trained_theta, p_net_pretrained_net_trained_theta);
    fprintf('alpha MD_PT-RE = %0.2f, p = %0.3f', md_net_pretrained_net_trained_alpha, p_net_pretrained_net_trained_alpha);
    fprintf(')\n');
    fprintf('%s:\n(', coi);
    fprintf('delta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_delta, p_net_trained_net_delta);
    fprintf('theta MD_RE-WS = %0.2f, p = %0.3f; ', md_net_trained_net_theta, p_net_trained_net_theta);
    fprintf('alpha MD_RE-WS = %0.2f, p = %0.3f', md_net_trained_net_alpha, p_net_trained_net_alpha);
    fprintf(')\n');
    % ylabel('AUC')
    
    ylabel('Power ratio')
    
    
end
print_local(h_f, [18.1/2, 5]);

%%
es_patience = 25;  % set in python

heldout_epoch = T_sub.heldout_epoch - 25;
across_run_epoch = T_sub.across_run_epoch - 25;
pretrained_epoch = T_sub.pretrained_epoch - 25;
%%

h_f = figure('Name', sprintf('epoch_n'));clf;
ax = subplot(1, 1, 1);
hold on;
X = [heldout_epoch, across_run_epoch];
G = [1:2] + zeros(size(across_run_epoch));
G = G(:);

hold on;
h_box = boxplot(X, ...
    G, ...
    'ColorGroup', G, ...
    'Colors', repmat([c.net_ta; c.net_st], 1, 1), ...
    'labels',repmat({'heldout_epoch', 'across_run_epoch'}, 1, 1), 'Symbol', '.');
box off;
set(h_box, {'linew'}, {1.75});
av = findobj(gca, 'tag', 'Lower Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.01, +0.01];
end
av = findobj(gca, 'tag', 'Upper Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.01, +0.01];
end

c_grey = 0.7*ones(1, 3);
for ix_vec_sub = 1:length(cell_sub)
    MarkerSize = 2.5;
    plot([1, 2], [heldout_epoch(ix_vec_sub), across_run_epoch(ix_vec_sub)], ...
        'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
end

y_range = [min(X(:)), max(X(:))];
d = (diff(y_range)*0.075);
tooth_length = d * 0.2;
h = 1.35;
line_height = y_range(2) + d;

p_epochs_n = plot_stars(heldout_epoch, across_run_epoch, [1, 2], line_height, tooth_length);


% ylim([-0.05, y_range(2) + 1.5*d])
ylim([0, 200])
% xtickangle(45)

% title(coi);
fprintf('---\n');
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'xtick',[])
% set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})

fprintf('%s:\n(', coi);
fprintf('re-trained network median: %d, ', median(heldout_epoch));
fprintf('within-subject trained network median: %d, ', median(across_run_epoch));
fprintf('p = %0.3f', p_epochs_n);
fprintf(')\n');
% ylabel('AUC')

ylabel('N. training epochs')
print_local(h_f, [4, 5]);
