clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('an_results');

%%
proc = getenv_check_empty('R_PROC');
d_project = 'working_eegbcg';
d.overwrite = false;
% v.str_ecg = 'ECG';
% v.use_full_time = false;
arch = 'gru_arch_general4';
%%
proc_bcgobs = fullfile('proc_bcgobs');
proc_ga = fullfile('proc_rs');
proc_temp = fullfile('proc_temp');
% T_qrs_stats = an_qrs_statistics(d_project, proc_bcgobs, proc_qrs, 'overwrite', v.overwrite);
%%
d.proc = getenv_check_empty('R_PROC');  % main processing directory, usually just corresponds to 'proc'
d.overwrite = false;  % overwrite main results of function
d.d_overwrite = struct;  % if this is passed, then it overwrite all of d structure at once
d.met_qrs = 'qrs0';
d.t_low = -0.75;
d.t_hig = +0.75;
%% Parse inputs
% [v, d] = inputParserCustom(d, varargin);clear d;
% v = inputParserStructureOverwrite(v);
v = d;
%%
proc = getenv_check_empty('R_PROC');
%%
d_target = proc_temp;
%%
d_proc_bcgobs = fullfile(getenv('D_OUT'), d_project, proc, proc_bcgobs);
d_proc_ga = fullfile(getenv('D_OUT'), d_project, proc,  proc_ga);
f_qrs = fullfile(getenv('D_OUT'), d_project, proc, d_target, 'qrs');
%% Load GA removed data QRS locked epoch
clear T_net;
T_bcgobs = get_file_table(d_proc_bcgobs, 'set');
T_rmga =  get_file_table(d_proc_ga, 'set');
qrs_epoch = [];

cell_sub = unique(T_bcgobs.str_sub)';
for ix_cell_sub = 1:length(cell_sub)
    str_sub = cell_sub{ix_cell_sub};
    case_sub = strcmpi(T_bcgobs.str_sub, str_sub);
    T_sub = T_bcgobs(case_sub, :);
    
    vec_run = unique(T_sub.ix_run);
    qrs_epoch_sub = [];
    for ix_vec_run = 1:length(vec_run)
        case_run = vec_run(ix_vec_run) == T_bcgobs.ix_run;
        T_sub_run = T_bcgobs(case_run & case_sub, :);
        EEG_bcgobs = pop_loadset('filename', T_sub_run.file{1});
        
        case_rmga = and(T_rmga.ix_run == T_sub_run.ix_run, T_rmga.ix_sub == T_sub_run.ix_sub);
        EEG_rs = pop_loadset('filename', T_rmga.file(case_rmga));
        
        assert(all(size(EEG_rs.data) == size(EEG_bcgobs.data)), 'size mismatch');
        
        % transplant events from bcg removed to not bcg removed...
        EEG_rs.event = EEG_bcgobs.event;
        
        EEG_rs_epoched = pop_epoch( EEG_rs, {v.met_qrs},...
            [v.t_low, v.t_hig]);
        
        EEG_rs_epoched = pop_chanedit(EEG_rs_epoched, 'lookup',fullfile(getenv('D_EEGLAB'),'plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc'));
        for ix_chan = 1:length(EEG_rs_epoched.chanlocs)
            EEG_rs_epoched.chanlocs(ix_chan).type = 'EEG';
        end
        case_ecg = strcmpi({EEG_rs_epoched.chanlocs.labels}, 'ECG');
        EEG_rs_epoched.chanlocs(case_ecg).type = 'ECG';
        EEG_rs_epoched = eeg_checkset( EEG_rs_epoched );
        
        EEG_rs_epoched = pop_select(EEG_rs_epoched, 'nochannel', {'r0', 'r1', 'r2', 't0', 't1', 't2'});
        
        qrs_epoch_sub = cat(3, qrs_epoch_sub, EEG_rs_epoched.data);
    end
    
    qrs_epoch = cat(3, qrs_epoch, mean(qrs_epoch_sub, 3));
    
    if ix_cell_sub==1
        
        t = EEG_rs_epoched.times/1000;
        chanlocs = EEG_rs_epoched.chanlocs;
        chaninfo = EEG_rs_epoched.chaninfo;
    end
end
%%
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig01_supp');
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
vobs.figformat = 'svg';
vobs.figdir = pwd;
vobs.figsave = true;
if not(vobs.figsave)
    warning('Not saving figures!');
end
print_local = @(f, dim) printForPub(f, sprintf('f_intro_%s', f.Name), 'doPrint', vobs.figsave,...
    'fformat', vobs.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig);
% cmap = [[1, 0, 0];[0, 0, 1]];
c_obs_st = [091, 194, 062]/255;
c_net_st = [068, 120, 070]/255;
c_obs_ta = [101, 136, 205]/255;
c_net_ta = [073, 069, 164]/255;
c_neutral= [177, 177, 176]/255;
c_ekg = [0.6350    0.0780    0.1840];
cmap = [c_ekg; c_neutral; c_neutral; c_neutral];

%%
t_slice = [-0.25, -0.1, -0.03, 0.1, 0.25];
ix_slice = nan(size(t_slice));
for ix_t_slice = 1:length(t_slice)
    [~, ix_slice(ix_t_slice)] = min(abs(t_slice(ix_t_slice) - t));
end
%%
mea_ecg_epoched = mean(ecg_epoched, 2);
sem_ecg_epoched = std_error(ecg_epoched, 0, 2);
%%
h_f1 = figure('Name', 'intro_supp_time');clf;
hold on;
ecg_epoched = squeeze(qrs_epoch(strcmpi({chanlocs.labels}, 'ECG'), :, :));
plot(t, mea_ecg_epoched, 'k-', 'lineWidth', 2);
ciplot(mea_ecg_epoched - sem_ecg_epoched, mea_ecg_epoched + sem_ecg_epoched, t, [0, 0, 0], 0.5);


axis tight;
ylim([-400 400])
for ix_t_slice = 1:length(t_slice)
    plot(t_slice(ix_t_slice) * ones(1, 2), get(gca, 'ylim'), '--', 'color', c_neutral);
end

print_local(h_f1, [18.1, 3]);


%%
h_f2 = figure('Name', 'intro_supp_topo');clf;
maplims = [-40, +40];
for ix_t_slice = 1:length(t_slice)
    subplot(1, length(t_slice), ix_t_slice);cla;
    electrodes = 'off'; %pts
    topoplot(qrs_epoch(:, ix_slice(ix_t_slice)), chanlocs , ...
        'style', 'fill', 'electrodes', electrodes, 'conv', 'on', 'chantype', 'EEG', 'chaninfo', chaninfo, ...
        'maplimits', maplims, ...
        'plotrad', 0.55, 'colormap', parula, 'plotdisk', 'on');
    title(sprintf('%0.3fs', t_slice(ix_t_slice)))
end
print_local(h_f2, [18.1, 5]);
