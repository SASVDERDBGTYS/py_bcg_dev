clearvars -except switch_case;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%
d_project = 'working_eegbcg';
t_pct_baseline = NaN;t_low = -0.5;t_hig = 1.5; % defaults
%%
reref = 'laplace';
% spectral_band = 'alpha_beta1_beta2'; %n.b. order matters!
% spectral_band = 'alpha_beta1'; % examples
% spectral_band = 'alpha_square'; % examples
% spectral_band = 'alpha'; % examples
spectral_band = ''; % examples
% spectral_band = 'beta-motor_pls_notch_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
% spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
% spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
% spectral_band = 'beta-motor_pls_notch_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
% spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
% spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];

proc = getenv_check_empty('R_PROC');
%%
if (exist('switch_case', 'var')==1)
    switch switch_case
        case 1, spectral_band = 'beta-motor_pls_notch_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 2, spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 3, spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [-0.75, -0.25];
        case 4, spectral_band = 'beta-motor_pls_notch_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
        case 5, spectral_band = 'mu-high_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
        case 6, spectral_band = 'mu-low_pls_ersp'; t_low = -1.5; t_hig = 3;t_pct_baseline = [Inf];
    end
end
%%
overwrite = false;

%%
data_source_gar = fullfile('proc_rs');
d_proc_bcg_gar = fullfile(getenv('D_OUT'), d_project, proc, data_source_gar);
[str_source_gar, str_type_gar] = gen_source(data_source_gar);

data_source_obs = fullfile('proc_bcgobs');
d_proc_bcg_obs = fullfile(getenv('D_OUT'), d_project, proc, data_source_obs);
[str_source_obs, str_type_obs] = gen_source(data_source_obs);

data_source_net = fullfile('proc_bcgnet', 'multi_run', 'gru_arch_general4'); % use this format to classify on networks!
d_proc_bcg_net = fullfile(getenv('D_OUT'), d_project, proc, data_source_net);
[str_sourc_net, str_type_net] = gen_source(data_source_net);

%%

[d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar] = an_epoch(d_project, d_proc_bcg_gar, ...
    'reref', reref, 'str_source', str_source_gar, 'str_merged', str_type_gar, ...
    't_low', t_low, 't_hig', t_hig, 'spectral_band', spectral_band, 'overwrite', overwrite);

[d_proc_epoch_obs, get_f_epoched_obs, vec_sub_obs] = an_epoch(d_project, d_proc_bcg_obs, ...
    'reref', reref, 'str_source', str_source_obs, 'str_merged', str_type_obs, ...
    't_low', t_low, 't_hig', t_hig, 'spectral_band', spectral_band, 'overwrite', overwrite);

[d_proc_epoch_net, get_f_epoched_net, vec_sub_net] = an_epoch(d_project, d_proc_bcg_net, ...
    'reref', reref, 'str_source', str_sourc_net, 'str_merged', str_type_net, ...
    't_low', t_low, 't_hig', t_hig, 'spectral_band', spectral_band, 'overwrite', overwrite);

%%

[std_standard_gar, std_target_gar, mea_standard_gar, mea_target_gar, med_standard_gar, med_target_gar, ...
    se2_standard_gar, se2_target_gar, t1, e1, topo_locs] = an_plot_evoked(d_project, ...
    d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar, 'str_source', str_source_gar, ...
    't_baseline', t_pct_baseline);

[std_standard_obs, std_target_obs, mea_standard_obs, mea_target_obs, med_standard_obs, med_target_obs, ...
    se2_standard_obs, se2_target_obs] = an_plot_evoked(d_project, ...
    d_proc_epoch_obs, get_f_epoched_obs, vec_sub_obs, 'str_source', str_source_obs, ...
    't_baseline', t_pct_baseline);

[std_standard_net, std_target_net, mea_standard_net, mea_target_net, med_standard_net, med_target_net, ...
    se2_standard_net, se2_target_net, t, eeg_labels] = an_plot_evoked(d_project, ...
    d_proc_epoch_net, get_f_epoched_net, vec_sub_net, 'str_source', str_sourc_net, ...
    't_baseline', t_pct_baseline);

%%
vec_sub = vec_sub_net;
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = '_5ronly';
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = '';
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';

if all(isinf(t_pct_baseline))
    es = sprintf('%s%s', es, '_inf_baseline');
end
%%

coi = {'T8', 'Pz'};
coi_full = { ...
    'Fz','F1','F2','F3','F4', ...
    'FC1','FC2','FC3','FC4', ...
    'FT7', 'FT8', ...
    'Cz','C1','C2','C3','C4', ...
    'T7', 'T8', ...
    'CPz','CP1','CP2','CP3','CP4', ...
    'TP7', 'TP8', ...
    'Pz','P1','P2','P3','P4', ...
    'Oz','O1','O2', ...
    };

if strcmpi(reref, 'laplace')
    a_units = 'µV/m²';
else
    a_units = 'µV';
end
%%
if isempty(spectral_band)
    str_spectral_band = '';
else
    str_spectral_band = sprintf('_%s', spectral_band);
    cell_spectral_band = strsplit(spectral_band, '_');
    if contains(spectral_band, 'ersp')
    else
        coi_temp = {};
        for ix_sb = 1:length(cell_spectral_band)
            for ix_coi = 1:length(coi)
                coi_temp_instance = sprintf('%s_%s', coi{ix_coi}, cell_spectral_band{ix_sb});
                coi_temp = [coi_temp, coi_temp_instance];
            end
        end
        coi = coi_temp;
    end
end

for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    if ix_vec_sub == 1
        nb_chan = size(std_standard_gar.(str_sub), 1);
        nb_methods = 3; %obs, net_based
        nb_stimuli = 2; % standard, target
        mat_mea = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_med = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_std = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        mat_se2 = nan(nb_chan, length(t), length(vec_sub), nb_methods, nb_stimuli);
        
    end
    mat_mea(:, :, ix_vec_sub, 1, 1) = mea_standard_gar.(str_sub);
    mat_mea(:, :, ix_vec_sub, 1, 2) = mea_target_gar.(str_sub);
    mat_mea(:, :, ix_vec_sub, 2, 1) = mea_standard_obs.(str_sub);
    mat_mea(:, :, ix_vec_sub, 2, 2) = mea_target_obs.(str_sub);
    mat_mea(:, :, ix_vec_sub, 3, 1) = mea_standard_net.(str_sub);
    mat_mea(:, :, ix_vec_sub, 3, 2) = mea_target_net.(str_sub);
    
    mat_med(:, :, ix_vec_sub, 1, 1) = med_standard_gar.(str_sub);
    mat_med(:, :, ix_vec_sub, 1, 2) = med_target_gar.(str_sub);
    mat_med(:, :, ix_vec_sub, 2, 1) = med_standard_obs.(str_sub);
    mat_med(:, :, ix_vec_sub, 2, 2) = med_target_obs.(str_sub);
    mat_med(:, :, ix_vec_sub, 3, 1) = med_standard_net.(str_sub);
    mat_med(:, :, ix_vec_sub, 3, 2) = med_target_net.(str_sub);
    
    mat_std(:, :, ix_vec_sub, 1, 1) = std_standard_gar.(str_sub);
    mat_std(:, :, ix_vec_sub, 1, 2) = std_target_gar.(str_sub);
    mat_std(:, :, ix_vec_sub, 2, 1) = std_standard_obs.(str_sub);
    mat_std(:, :, ix_vec_sub, 2, 2) = std_target_obs.(str_sub);
    mat_std(:, :, ix_vec_sub, 3, 1) = std_standard_net.(str_sub);
    mat_std(:, :, ix_vec_sub, 3, 2) = std_target_net.(str_sub);
    
    mat_se2(:, :, ix_vec_sub, 1, 1) = se2_standard_gar.(str_sub);
    mat_se2(:, :, ix_vec_sub, 1, 2) = se2_target_gar.(str_sub);
    mat_se2(:, :, ix_vec_sub, 2, 1) = se2_standard_obs.(str_sub);
    mat_se2(:, :, ix_vec_sub, 2, 2) = se2_target_obs.(str_sub);
    mat_se2(:, :, ix_vec_sub, 3, 1) = se2_standard_net.(str_sub);
    mat_se2(:, :, ix_vec_sub, 3, 2) = se2_target_net.(str_sub);
end

%%

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig06', ...
    sprintf('c_%s%s_vs%s%s', reref, gen_source(data_source_obs), gen_source(data_source_net), str_spectral_band));
% p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig06', ...
%     sprintf('c_%s%s%s', reref, gen_source(data_source_gar), str_spectral_band));
% p_fig = '~/Desktop/temp/';

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'svg';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
print_local = @(h, dim) printForPub(h, sprintf('f_evo_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;

% cmap = [c.obs_st; c.net_st; c.obs_ta; c.net_ta; c.neutral];
%%
case_t = and(t>0.25, t<0.75);
%%

h_f = figure('Name', sprintf('%s', 'time_res_global_std'));clf;
ns = numSubplots(length(vec_sub));
for ix_vec_sub = 1:length(vec_sub)
    subplot(ns(1), ns(2), ix_vec_sub);cla;hold on;
    h_standard_obs = plot(t, mean(mat_std(:, :, ix_vec_sub, 2, 1), 1)', '--', 'Color', c.obs_st);
    h_target_obs = plot(t, mean(mat_std(:, :, ix_vec_sub, 2, 2), 1)', '-', 'Color', c.obs_ta);
    
    h_standard_net = plot(t, mean(mat_std(:, :, ix_vec_sub, 3, 1), 1)', '--', 'Color', c.net_st);
    h_target_net = plot(t, mean(mat_std(:, :, ix_vec_sub, 3, 2), 1)', '-', 'Color', c.obs_ta);
    title(sprintf('sub%02d', vec_sub(ix_vec_sub)));
end
xlabel('Time (s)');
ylabel('Std. (μV?)');
legend([h_standard_obs, h_target_obs], {'OBS standard', 'OBS target'}, 'Location', 'Best');
print_local(h_f, [50, 30]);

%%

h_f = figure('Name', sprintf('%s', 'box_global_std_obs_net'));clf;

ns = numSubplots(length(coi));

for ix_coi = 1:length(coi)
    if ix_coi==length(coi)+1
        case_ch = 1:length(eeg_labels, coi{ix_coi});
    else
        case_ch = strcmpi(eeg_labels, coi{ix_coi});
    end
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    std_standard_gar = zeros(length(vec_sub), 1);
    std_target_gar = zeros(length(vec_sub), 1);
    std_standard_obs = zeros(length(vec_sub), 1);
    std_target_obs = zeros(length(vec_sub), 1);
    std_standard_net = zeros(length(vec_sub), 1);
    std_target_net = zeros(length(vec_sub), 1);
    for ix_vec_sub = 1:length(vec_sub)
        std_standard_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 1), 1), 2);
        std_target_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 2), 1), 2);
        
        std_standard_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 1), 1), 2);
        std_target_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 2), 1), 2);
        
        std_standard_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 1), 1), 2);
        std_target_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 2), 1), 2);
    end
    
    X = [std_standard_obs; std_standard_net;std_target_obs ; std_target_net];
    
    hold on;
    G = [2 * ones(size(std_standard_obs)); 3 * ones(size(std_standard_net)); ...
        5 * ones(size(std_target_obs));6 * ones(size(std_target_net))];
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [c.obs_st; c.net_st;c.obs_ta;c.net_ta], ...
        'labels',{'OBS', 'BCGNet', 'OBS', 'BCGNet'});
    box off;
    %     set(findobj(h_box,'LineStyle','--'),'LineStyle','-')
    set(h_box, {'linew'}, {1.75});
    av = findobj(gca, 'tag', 'Lower Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.1, +0.1];
    end
    av = findobj(gca, 'tag', 'Upper Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.1, +0.1];
    end
    
    c_grey = 0.7*ones(1, 3);
    for ix_vec_sub = 1:length(vec_sub)
        plot([1, 2], [std_standard_obs(ix_vec_sub), std_standard_net(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', 5.5, 'MarkerEdgeColor', [1, 1, 1]);
        plot([3, 4], [std_target_obs(ix_vec_sub), std_target_net(ix_vec_sub)], ...
            'o-', 'MarkerFaceColor', c_grey, 'Color', c_grey, 'MarkerSize', 5.5, 'MarkerEdgeColor', [1, 1, 1]);
    end
    p_standard = signrank(std_standard_obs, std_standard_net);
    p_target = signrank(std_target_obs, std_target_net);
    % [~, p_standard] = ttest(std_standard_obs, std_standard_net);
    % [~, p_target] = ttest(std_target_obs, std_target_net);
    
    y_range = [min(X(:)), max(X(:))];
    line_height = y_range(2) + 2.5;
    
    [p_st_obs_net, ~, stats] = signrank(std_standard_obs, std_standard_net);
    [p_ta_obs_net, ~, stats] = signrank(std_target_obs, std_target_net);
    [p_st_obs_net_c, p_ta_obs_net_c] = deal_bonf_holm([p_st_obs_net, p_ta_obs_net]);
    
    p_st_obs_net_c = plot_stars(std_standard_obs, std_standard_net, [1, 2], line_height, 1, p_st_obs_net_c);
    p_ta_obs_net_c = plot_stars(std_target_obs, std_target_net, [3, 4], line_height, 1, p_ta_obs_net_c);
    
    ylim([y_range(1) - 5, y_range(2) + 6])
    
    title_ = sprintf('%s', coi{ix_coi});
    fprintf('%s, signrank obs/net: standard p: %0.3f, target p: %0.3f\n', title_, p_st_obs_net_c, p_ta_obs_net_c);
    %     title(title_);
    ylabel(sprintf('Evoked standard dev. (%s)', a_units))
end
print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', sprintf('%s', 'box_global_std_gar_obs_net'));clf;

ns = numSubplots(length(coi)+1);
fprintf('---------\n');

for ix_coi = 1:length(coi)+1
    if ix_coi==length(coi)+1
        case_ch = 1:length(eeg_labels);
        title_ = sprintf('%s', 'ALL');
    else
        case_ch = strcmpi(eeg_labels, coi{ix_coi});
        title_ = sprintf('%s', coi{ix_coi});
        %         if strcmpi(coi{ix_coi}, 'Fz'), d = 10;else d = 5;end
    end
    ax = subplot(ns(1), ns(2), ix_coi);cla;hold on;
    
    std_standard_gar = zeros(length(vec_sub), 1);
    std_target_gar = zeros(length(vec_sub), 1);
    std_standard_obs = zeros(length(vec_sub), 1);
    std_target_obs = zeros(length(vec_sub), 1);
    std_standard_net = zeros(length(vec_sub), 1);
    std_target_net = zeros(length(vec_sub), 1);
    for ix_vec_sub = 1:length(vec_sub)
        std_standard_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 1), 1), 2);
        std_target_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 2), 1), 2);
        
        std_standard_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 1), 1), 2);
        std_target_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 2), 1), 2);
        
        std_standard_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 1), 1), 2);
        std_target_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 2), 1), 2);
    end
    
    X = [std_standard_gar; std_standard_obs; std_standard_net; std_target_gar; std_target_obs; std_target_net];
    
    hold on;
    G = [1 * ones(size(std_standard_gar));2 * ones(size(std_standard_obs)); 3 * ones(size(std_standard_net)); ...
        4 * ones(size(std_target_gar));5 * ones(size(std_target_obs));6 * ones(size(std_target_net))];
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [c.gar_st; c.obs_st; c.net_st; c.gar_ta; c.obs_ta; c.net_ta], ...
        'labels',{'BCE', 'OBS', 'BCGNet', 'BCE', 'OBS', 'BCGNet'}, 'Symbol', '');
    box off;
    %     set(findobj(h_box,'LineStyle','--'),'LineStyle','-')
    set(h_box, {'linew'}, {1.75});
    av = findobj(gca, 'tag', 'Lower Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.1, +0.1];
    end
    av = findobj(gca, 'tag', 'Upper Adjacent Value');
    for ix = 1:length(av)
        av(ix).XData = av(ix).XData + [-0.1, +0.1];
    end
    
    c_grey = 0.7*ones(1, 3);
    c_alpha = 0.4;
    MarkerSize = 6;
    MarkerEdgeColor = [1,1,1];
    % note that in inkscape you then need to add a 0.1mm border to each
    % point (find property circle)
    
    for ix_vec_sub = 1:length(vec_sub)
        h = plot([1, 2], [std_standard_gar(ix_vec_sub), std_standard_obs(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        h = plot([2, 3], [std_standard_obs(ix_vec_sub), std_standard_net(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        h = plot([4, 5], [std_target_gar(ix_vec_sub), std_target_obs(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        h = plot([5, 6], [std_target_obs(ix_vec_sub), std_target_net(ix_vec_sub)], ...
            '-', 'Color', c_grey);
        h.Color(4) = c_alpha;
        
        for ix = 1:6
            switch ix
                case 1, std_local = std_standard_gar(ix_vec_sub);
                case 2, std_local = std_standard_obs(ix_vec_sub);
                case 3, std_local = std_standard_net(ix_vec_sub);
                case 4, std_local = std_target_gar(ix_vec_sub);
                case 5, std_local = std_target_obs(ix_vec_sub);
                case 6, std_local = std_target_net(ix_vec_sub);
            end
            
            plot(ix, std_local, ...
                '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
                'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
        end
    end
    p_standard = signrank(std_standard_obs, std_standard_net);
    p_target = signrank(std_target_obs, std_target_net);
    
    y_range = [min(X(:)), max(X(:))];
    d = ceil(diff(y_range)*0.075);
    tooth_length = d * 0.25;
    line_height = y_range(2) + d;
    
    [p_st_obs_net, ~, stats] = signrank(std_standard_obs, std_standard_net);
    [p_ta_obs_net, ~, stats] = signrank(std_target_obs, std_target_net);
    [p_st_gar_obs, ~, stats] = signrank(std_standard_gar, std_standard_obs);
    [p_ta_gar_obs, ~, stats] = signrank(std_target_gar, std_target_obs);
    [p_st_gar_net, ~, stats] = signrank(std_standard_gar, std_standard_net);
    [p_ta_gar_net, ~, stats] = signrank(std_target_gar, std_target_net);
    
    [p_st_obs_net_c, p_ta_obs_net_c, p_st_gar_obs_c, p_ta_gar_obs_c, p_st_gar_net_c, p_ta_gar_net_c] = ...
        deal_bonf_holm([p_st_obs_net, p_ta_obs_net, p_st_gar_obs, p_ta_gar_obs, p_st_gar_net, p_ta_gar_net]);
    
    plot_stars(std_standard_obs, std_standard_net, [2, 3], line_height, tooth_length, p_st_obs_net_c);
    plot_stars(std_target_obs, std_target_net, [5, 6], line_height, tooth_length, p_ta_obs_net_c);
    plot_stars(std_standard_gar, std_standard_obs, [1, 2], line_height, tooth_length, p_st_gar_obs_c);
    plot_stars(std_target_gar, std_target_obs, [4, 5], line_height, tooth_length, p_ta_gar_obs_c);
    plot_stars(std_standard_gar, std_standard_net, [1, 3], line_height + 2*d, tooth_length, p_st_gar_net_c);
    plot_stars(std_target_gar, std_target_net, [4, 6], line_height + 2*d, tooth_length, p_ta_gar_net_c);
    
    xtickangle(45)
    
    ylim_ = [y_range(1) - 5, y_range(2) + 4.5*d];
    ylim_ = round([floor(ylim_(1)), ceil(ylim_(2))]/10)*10;
    %     ylim_ = ylim_ + [-1, +1];
    title(title_);
    
    fprintf('%s:\n(', title_);
    fprintf('BCE/OBS st: p = %s, ta: p = %s; ', p_to_str(p_st_gar_obs_c), p_to_str(p_ta_gar_obs_c));
    fprintf('BCE/BCGNet st: p = %s, ta: p = %s; ', p_to_str(p_st_gar_net_c), p_to_str(p_ta_gar_net_c));
    fprintf('OBS/BCGNet st: p = %s, ta: p = %s', p_to_str(p_st_obs_net_c), p_to_str(p_ta_obs_net_c));
    fprintf(')\n');
    
    %     title(title_);
    ylabel(sprintf('Evoked standard dev. (%s)', a_units))
    ax.XAxis.Visible = 'off';
    if range(ylim_)>100
        ylim_step = 20;
    else
        ylim_step = 10;
    end
    ax.YTick = [ylim_(1):ylim_step:ylim_(end)];
    ylim(ylim_)
    
    
end

fprintf('---------\n');
print_local(h_f, [18.1, 5]);

%%

h_f = figure('Name', sprintf('%s', 'time_res_global_mea'));clf;
ns = numSubplots(length(vec_sub));
for ix_vec_sub = 1:length(vec_sub)
    subplot(ns(1), ns(2), ix_vec_sub);cla;hold on;
    h_standard_obs = plot(t, mean(mat_mea(:, :, ix_vec_sub, 2, 1), 1)', '--', 'Color', c.obs_st);
    h_target_obs = plot(t, mean(mat_mea(:, :, ix_vec_sub, 2, 2), 1)', '-', 'Color', c.obs_ta);
    
    h_standard_net = plot(t, mean(mat_mea(:, :, ix_vec_sub, 3, 1), 1)', '--', 'Color', c.net_st);
    h_target_net = plot(t, mean(mat_mea(:, :, ix_vec_sub, 3, 2), 1)', '-', 'Color', c.net_ta);
    title(sprintf('sub%02d', vec_sub(ix_vec_sub)));
end
xlabel('Time (s)');
ylabel(sprintf('Amplitude (%s)', a_units))
legend([h_standard_obs, h_target_obs], {'OBS standard', 'OBS target'}, 'Location', 'Best');
legend('boxoff');
print_local(h_f, [50, 30]);

%%

h_f = figure('Name', sprintf('%s', 'time_res_coi'));clf;

ns = numSubplots(length(coi));
for ix_coi = 1:length(coi)
    case_ch = strcmpi(eeg_labels, coi{ix_coi});
    
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    mea_standard_obs = mean(mat_mea(case_ch, :, :, 2, 1), 3);
    mea_target_obs = mean(mat_mea(case_ch, :, :, 2, 2), 3);
    se2_standard_obs = std_error(mat_mea(case_ch, :, :, 2, 1), 0, 3);
    se2_target_obs = std_error(mat_mea(case_ch, :, :, 2, 2), 0, 3);
    
    mea_standard_net = mean(mat_mea(case_ch, :, :, 3, 1), 3);
    mea_target_net = mean(mat_mea(case_ch, :, :, 3, 2), 3);
    se2_standard_net = std_error(mat_mea(case_ch, :, :, 3, 1), 0, 3);
    se2_target_net = std_error(mat_mea(case_ch, :, :, 3, 2), 0, 3);
    
    %     ciplot(mea_standard_obs - se2_standard_obs, mea_standard_obs + se2_standard_obs, t, c.obs_st, 0.15);
    %     ciplot(mea_target_obs - se2_target_obs, mea_target_obs + se2_target_obs, t, c.obs_ta, 0.15);
    %
    %     ciplot(mea_standard_net - se2_standard_net, mea_standard_net + se2_standard_net, t, c.net_st, 0.15);
    %     ciplot(mea_target_net - se2_target_net, mea_target_net + se2_target_net, t, c.net_ta, 0.15);
    
    lw = 2;
    
    h_standard_obs = plot(t, mea_standard_obs, '-',  'Color', c.obs_st);
    h_standard_obs.LineWidth = lw;
    h_target_obs = plot(t, mea_target_obs, '-', 'Color', c.obs_ta);
    h_target_obs.LineWidth = lw;
    
    h_standard_net = plot(t, mea_standard_net, '--',  'Color', c.net_st);
    h_standard_net.LineWidth = lw;
    h_target_net = plot(t, mea_target_net, '--',  'Color', c.net_ta);
    h_target_net.LineWidth = lw;
    
    title(strrep(sprintf('%s', coi{ix_coi}), '_', ' '));
    xlim([t(1) t(end)])
    if isempty(spectral_band)
        ylim(40 * [-1, 1])
        s_f = [18.1, 4];
    else
        ylim(0.41 * [-1, 1])
        s_f = [24, 8];
    end
    xlabel('Time (s)');
    ylabel(sprintf('Amplitude (%s)', a_units))
    plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
end
legend([h_standard_obs, h_target_obs, h_standard_net, h_target_net], ...
    {'OBS standard', 'OBS target', 'BCGNet standard', 'BCGNet target'}, 'Location', 'SouthEast');
legend('boxoff');
print_local(h_f, s_f);

%%

%
h_f = figure('Name', sprintf('%s', 'time_res_coi_gar'));clf;

ns = numSubplots(length(coi));
for ix_coi = 1:length(coi)
    if ix_coi==length(coi)+1
        case_ch = 1:length(eeg_labels);
        title_ = strrep(sprintf('%s', 'ALL'), '_', ' ');
    else
        case_ch = strcmpi(eeg_labels, coi{ix_coi});
        title_ = strrep(sprintf('%s', coi{ix_coi}), '_', ' ');
    end
    
    subplot(ns(1), ns(2), ix_coi);cla;hold on;
    mea_standard_gar = mean(mean(mat_mea(case_ch, :, :, 2, 1), 1), 3);
    mea_target_gar = mean(mean(mat_mea(case_ch, :, :, 2, 2), 1), 3);
    se2_standard_gar = std_error(mean(mat_mea(case_ch, :, :, 2, 1), 1), 0, 3);
    se2_target_gar = std_error(mean(mat_mea(case_ch, :, :, 2, 2), 1), 0, 3);
    
    ciplot(mea_standard_gar - se2_standard_gar, mea_standard_gar + se2_standard_gar, t, c.gar_st, 0.15);
    ciplot(mea_target_gar - se2_target_gar, mea_target_gar + se2_target_gar, t, c.gar_ta, 0.15);
    
    lw = 2;
    
    h_standard_gar = plot(t, mea_standard_gar, '-',  'Color', c.gar_st);
    h_standard_gar.LineWidth = lw;
    h_target_gar = plot(t, mea_target_gar, '-', 'Color', c.gar_ta);
    h_target_gar.LineWidth = lw;
    title(title_);
    xlim([t(1) t(end)])
    if isempty(spectral_band)
        ylim(40 * [-1, 1])
        s_f = [18.1 * (9/12), 4];
    else
        ylim(0.41 * [-1, 1])
        s_f = [24, 8];
    end
    xlabel('Time (s)');
    ylabel(sprintf('Amplitude (%s)', a_units))
    plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
end
legend([h_standard_gar, h_target_gar], ...
    {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
legend('boxoff');
print_local(h_f, s_f);


%%
h_f = figure('Name', sprintf('%s', 'topo_box_global_std_gar_obs_net'));clf;

ns = numSubplots(length(coi)+1);
fprintf('---------\n');

for ix_coi = 1:length(coi_full)+1
    if ix_coi==length(coi_full)+1
        case_ch = 1:length(eeg_labels);
        title_ = sprintf('%s', 'ALL');
    else
        case_ch = strcmpi(eeg_labels, coi_full{ix_coi});
        title_ = sprintf('%s', coi_full{ix_coi});
        %         if strcmpi(coi{ix_coi}, 'Fz'), d = 10;else d = 5;end
        %     ax = subplot(ns(1), ns(2), ix_coi);cla;hold on;
        %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
        
        ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
        
        std_standard_gar = zeros(length(vec_sub), 1);
        std_target_gar = zeros(length(vec_sub), 1);
        std_standard_obs = zeros(length(vec_sub), 1);
        std_target_obs = zeros(length(vec_sub), 1);
        std_standard_net = zeros(length(vec_sub), 1);
        std_target_net = zeros(length(vec_sub), 1);
        for ix_vec_sub = 1:length(vec_sub)
            std_standard_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 1), 1), 2);
            std_target_gar(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 1, 2), 1), 2);
            
            std_standard_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 1), 1), 2);
            std_target_obs(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 2, 2), 1), 2);
            
            std_standard_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 1), 1), 2);
            std_target_net(ix_vec_sub) = mean(mean(mat_std(case_ch, case_t, ix_vec_sub, 3, 2), 1), 2);
        end
        
        X = [std_standard_gar; std_standard_obs; std_standard_net; std_target_gar; std_target_obs; std_target_net];
        
        hold on;
        G = [1 * ones(size(std_standard_gar));2 * ones(size(std_standard_obs)); 3 * ones(size(std_standard_net)); ...
            4 * ones(size(std_target_gar));5 * ones(size(std_target_obs));6 * ones(size(std_target_net))];
        h_box = boxplot(X, ...
            G, ...
            'ColorGroup', G, ...
            'Colors', [c.gar_st; c.obs_st; c.net_st; c.gar_ta; c.obs_ta; c.net_ta], ...
            'labels',{'BCE', 'OBS', 'BCGNet', 'BCE', 'OBS', 'BCGNet'}, 'Symbol', '');
        box off;
        set(findobj(h_box,'LineStyle','--'),'LineStyle','-')
        set(h_box, {'linew'}, {1.00});
        av = findobj(gca, 'tag', 'Lower Adjacent Value');
        for ix = 1:length(av)
            av(ix).XData = av(ix).XData + [-0.1, +0.1];
        end
        av = findobj(gca, 'tag', 'Upper Adjacent Value');
        for ix = 1:length(av)
            av(ix).XData = av(ix).XData + [-0.1, +0.1];
        end
        
        c_grey = 0.7*ones(1, 3);
        c_alpha = 0.4;
        MarkerSize = 3;
        MarkerEdgeColor = [0.5,0.5,0.5];
        % note that in inkscape you then need to add a 0.1mm border to each
        % point (find property circle)
        
        for ix_vec_sub = 1:length(vec_sub)
            %         h = plot([1, 2], [std_standard_gar(ix_vec_sub), std_standard_obs(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %         h = plot([2, 3], [std_standard_obs(ix_vec_sub), std_standard_net(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %         h = plot([4, 5], [std_target_gar(ix_vec_sub), std_target_obs(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %         h = plot([5, 6], [std_target_obs(ix_vec_sub), std_target_net(ix_vec_sub)], ...
            %             '-', 'Color', c_grey);
            %         h.Color(4) = c_alpha;
            %
            %             av = findobj(gca, 'tag', 'Upper Whisker');
            
            for ix = 1:6
                switch ix
                    case 1, std_local = std_standard_gar(ix_vec_sub);
                    case 2, std_local = std_standard_obs(ix_vec_sub);
                    case 3, std_local = std_standard_net(ix_vec_sub);
                    case 4, std_local = std_target_gar(ix_vec_sub);
                    case 5, std_local = std_target_obs(ix_vec_sub);
                    case 6, std_local = std_target_net(ix_vec_sub);
                end
                a = get(h_box(1, ix));
                a = a.YData(end);
                %             a = av(ix).YData(end)
                if std_local > a
                    plot(ix, std_local, ...
                        '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
                        'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
                end
            end
        end
        p_standard = signrank(std_standard_obs, std_standard_net);
        p_target = signrank(std_target_obs, std_target_net);
        
        y_range = [min(X(:)), 1.1 * max(X(:))];
        d = ceil(diff(y_range)*0.075);
        tooth_length = 0;
        
        
        [p_st_obs_net, ~, stats] = signrank(std_standard_obs, std_standard_net);
        [p_ta_obs_net, ~, stats] = signrank(std_target_obs, std_target_net);
        [p_st_gar_obs, ~, stats] = signrank(std_standard_gar, std_standard_obs);
        [p_ta_gar_obs, ~, stats] = signrank(std_target_gar, std_target_obs);
        [p_st_gar_net, ~, stats] = signrank(std_standard_gar, std_standard_net);
        [p_ta_gar_net, ~, stats] = signrank(std_target_gar, std_target_net);
        
        [p_st_obs_net_c, p_ta_obs_net_c, p_st_gar_obs_c, p_ta_gar_obs_c, p_st_gar_net_c, p_ta_gar_net_c] = ...
            deal_bonf_holm([p_st_obs_net, p_ta_obs_net, p_st_gar_obs, p_ta_gar_obs, p_st_gar_net, p_ta_gar_net]);
        
        
        %
        %     xtickangle(45)
        if not(isempty(str_spectral_band))
            line_height = 0.25;
            d_step = 0.1;
        else
            line_height = y_range(1) - 3*d;
            ylim_ = [y_range(1) - 4*d, y_range(2)];
            ylim_ = round([0, ceil(ylim_(2))]/10)*10;
            
            d_step = 0.04*range(ylim_);
        end
        
        plot_siglines(std_standard_obs, std_standard_net, [2, 3], line_height, tooth_length, p_st_obs_net_c);
        plot_siglines(std_target_obs, std_target_net, [5, 6], line_height, tooth_length, p_ta_obs_net_c);
        plot_siglines(std_standard_gar, std_standard_obs, [1, 2], line_height, tooth_length, p_st_gar_obs_c);
        plot_siglines(std_target_gar, std_target_obs, [4, 5], line_height, tooth_length, p_ta_gar_obs_c);
        plot_siglines(std_standard_gar, std_standard_net, [1, 3], line_height - d_step, tooth_length, p_st_gar_net_c);
        plot_siglines(std_target_gar, std_target_net, [4, 6], line_height - d_step, tooth_length, p_ta_gar_net_c);
        
        %     ylim_ = ylim_ + [-1, +1];
        title(title_);
        
        fprintf('%s:\n(', title_);
        fprintf('BCE/OBS st: p = %s, ta: p = %s; ', p_to_str(p_st_gar_obs_c), p_to_str(p_ta_gar_obs_c));
        fprintf('BCE/BCGNet st: p = %s, ta: p = %s; ', p_to_str(p_st_gar_net_c), p_to_str(p_ta_gar_net_c));
        fprintf('OBS/BCGNet st: p = %s, ta: p = %s', p_to_str(p_st_obs_net_c), p_to_str(p_ta_obs_net_c));
        fprintf(')\n');
        
        %     title(title_);
        %     ylabel(sprintf('Evoked standard dev. (%s)', a_units))
        %     ax.XAxis.Visible = 'off';
        
        %         ylim(ylim_)
        if not(isempty(str_spectral_band))
            ylim_ = [0, 2.5];
            ax.YTick = [ylim_(1):1:ylim_(end)];
            
            ylim(ylim_)
        else
            if strcmpi(reref, 'laplace')
                ylim_step=ylim_(2)/2;
            else
                ylim_step = 10;
            end
            ax.YTick = [0:ylim_step:ylim_(end)];
            ylim([-10, ylim_(end)])
        end
    end
    %     axis off;
    ax.Color = 'none';
    ax.XColor = 'none';
    %turn back on!
    %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
    
end

fprintf('---------\n');
print_local(h_f, [18.1, 22]);

%%
c = get_cmap(true);
h_f = figure('Name', sprintf('%s', 'topo_time_res_coi_gar'));clf;

ns = numSubplots(length(coi_full));
lw = 1.5;

for ix_coi = 1:length(coi_full)+1
    if ix_coi==length(coi_full)+1
        subplot('Position',[ ...
            0.8, 0.1, ...
            topo_locs.topoW, ...
            topo_locs.topoH, ...
            ]);hold on;
        title('bits');
        h_standard_gar = plot(t, mea_standard_gar, '-',  'Color', c.gar_st);
        h_standard_gar.LineWidth = lw;
        h_target_gar = plot(t, mea_target_gar, '-', 'Color', c.gar_ta);
        h_target_gar.LineWidth = lw;
        legend([h_standard_gar, h_target_gar], ...
            {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
        legend('boxoff');
    else
        case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
        title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
        
        
        %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
        ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
        
        cla;hold on;
        mea_standard_gar = mean(mean(mat_mea(case_ch, :, :, 2, 1), 1), 3);
        mea_target_gar = mean(mean(mat_mea(case_ch, :, :, 2, 2), 1), 3);
        se2_standard_gar = std_error(mean(mat_mea(case_ch, :, :, 2, 1), 1), 0, 3);
        se2_target_gar = std_error(mean(mat_mea(case_ch, :, :, 2, 2), 1), 0, 3);
        
        ciplot(mea_standard_gar - se2_standard_gar, mea_standard_gar + se2_standard_gar, t, c.gar_st, 0.15);
        ciplot(mea_target_gar - se2_target_gar, mea_target_gar + se2_target_gar, t, c.gar_ta, 0.15);
        
        
        h_standard_gar = plot(t, mea_standard_gar, '-',  'Color', c.gar_st);
        h_standard_gar.LineWidth = lw;
        h_target_gar = plot(t, mea_target_gar, '-', 'Color', c.gar_ta);
        h_target_gar.LineWidth = lw;
        title(title_);
        xlim([t(1) t(end)])
        
        if ix_coi==length(coi_full)+1
            xlabel('Time (s)');
            ylabel(sprintf('Amplitude (%s)', a_units))
        else
            %             axis off;
            ax.Color = 'none';
            ax.XColor = 'none';
            %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
        end
    end
    axis tight;
    if isempty(spectral_band)
        if strcmpi(reref, 'car')
            ylim(8 * [-1, 1])
        else
            ylim(40 * [-1, 1])
        end
        s_f = [18.1, 22];
        plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
        
    else
        ylim(0.41 * [-1, 1])
        s_f = [24, 8];
    end
end

print_local(h_f, s_f);

%%
c = get_cmap(false);
coi_motor = {'C3', 'Cz', 'C2', 'Pz'};
lw = 2 ;
%
cell_met = {'GAR', 'OBS', 'BCGNet'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('time_res_coi_%s_band', str_met));clf;
    ns = numSubplots(length(coi_motor));
    for ix_coi = 1:length(coi_motor)
        if ix_coi==length(coi_motor)+1
            case_ch = 1:length(eeg_labels);
            title_ = strrep(sprintf('%s', 'ALL'), '_', ' ');
        else
            case_ch = strcmpi(eeg_labels, coi_motor{ix_coi});
            title_ = strrep(sprintf('%s', coi_motor{ix_coi}), '_', ' ');
        end
        
        subplot(ns(1), ns(2), ix_coi);cla;hold on;
        
        if strcmpi(str_met, 'GAR')
            mea_standard = mean(mat_med(case_ch, :, :, 1, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 1, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 1, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 1, 2), 0, 3);
            c_st = c.gar_st;
            c_ta = c.gar_ta;
        elseif strcmpi(str_met, 'OBS')
            mea_standard = mean(mat_med(case_ch, :, :, 2, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 2, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 2, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 2, 2), 0, 3);
            c_st = c.obs_st;
            c_ta = c.obs_ta;
        elseif strcmpi(str_met, 'BCGNet')
            mea_standard = mean(mat_med(case_ch, :, :, 3, 1), 3);
            mea_target = mean(mat_med(case_ch, :, :, 3, 2), 3);
            se2_standard = std_error(mat_med(case_ch, :, :, 3, 1), 0, 3);
            se2_target = std_error(mat_med(case_ch, :, :, 3, 2), 0, 3);
            c_st = c.net_st;
            c_ta = c.net_ta;
        end
        ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
        ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
        
        
        h_standard = plot(t, mea_standard, '-',  'Color', c_st);
        h_standard.LineWidth = lw;
        h_target = plot(t, mea_target, '-', 'Color', c_ta);
        h_target.LineWidth = lw;
        title(title_);
        
        if isempty(spectral_band)
            s_f = [18.1 * (9/12), 4];
        else
            s_f = [18.1 * (9/12), 9];
        end
        xlabel('Time (s)');
        ylabel(sprintf('Amplitude (%s)', a_units))
        plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
        axis tight;
        xlim([-0.25, 2.75])
    end
    legend([h_standard, h_target], ...
        {sprintf('%s standard', str_met), sprintf('%s target', str_met)}, 'Location', 'SouthEast');
    legend('boxoff');
    print_local(h_f, s_f);
end
%%

ns = numSubplots(length(coi_full));
lw = 1;
c = get_cmap(true);
cell_met = {'GAR', 'OBS', 'BCGNet'};
for ix_met = 1:length(cell_met)
    str_met = cell_met{ix_met};
    h_f = figure('Name', sprintf('topo_time_res_coi_%s_band', str_met));clf;
    for ix_coi = 1:length(coi_full)+1
        if ix_coi==length(coi_full)+1
            subplot('Position',[ ...
                0.8, 0.1, ...
                topo_locs.topoW, ...
                topo_locs.topoH, ...
                ]);hold on;
            title('bits');
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            legend([h_standard, h_target], ...
                {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
            legend('boxoff');
        else
            case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
            title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
            
            
            %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
            ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
            
            cla;hold on;
            if strcmpi(str_met, 'GAR')
                mea_standard = mean(mat_med(case_ch, :, :, 1, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 1, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 1, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 1, 2), 0, 3);
                c_st = c.gar_st;
                c_ta = c.gar_ta;
            elseif strcmpi(str_met, 'OBS')
                mea_standard = mean(mat_med(case_ch, :, :, 2, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 2, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 2, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 2, 2), 0, 3);
                c_st = c.obs_st;
                c_ta = c.obs_ta;
            elseif strcmpi(str_met, 'BCGNet')
                mea_standard = mean(mat_med(case_ch, :, :, 3, 1), 3);
                mea_target = mean(mat_med(case_ch, :, :, 3, 2), 3);
                se2_standard = std_error(mat_med(case_ch, :, :, 3, 1), 0, 3);
                se2_target = std_error(mat_med(case_ch, :, :, 3, 2), 0, 3);
                c_st = c.net_st;
                c_ta = c.net_ta;
            end
            ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
            ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
            
            h_standard = plot(t, mea_standard, '-',  'Color', c_st);
            h_standard.LineWidth = lw;
            h_target = plot(t, mea_target, '-', 'Color', c_ta);
            h_target.LineWidth = lw;
            title(title_);
            xlim([t(1) t(end)])
            
            if ix_coi==length(coi_full)+1
                xlabel('Time (s)');
                ylabel(sprintf('Amplitude (%s)', a_units))
            else
                %             axis off;
                ax.Color = 'none';
                ax.XColor = 'none';
                %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
            end
        end
        axis tight;
        if isempty(spectral_band)
            s_f = [18.1, 22];
            
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        else
            s_f = [18.1, 22];
            xlim([-0.25, 2.5])
            plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
            
        end
    end
    
    print_local(h_f, s_f);
end

%%
c = get_cmap(false);
for ix_vec_sub = 1:length(vec_sub)
    
    ns = numSubplots(length(coi_full));
    lw = 1;
    cell_met = {'GAR', 'OBS', 'BCGNet'};
    for ix_met = 1:length(cell_met)
        str_met = cell_met{ix_met};
        %     h_f = figure('Name', sprintf('topo_time_res_coi_%s_band', str_met));clf;
        h_f = figure('Name', sprintf('_sub%02d_topo_time_res_coi_%s_band', vec_sub(ix_vec_sub), str_met));clf;
        for ix_coi = 1:length(coi_full)+1
            if ix_coi==length(coi_full)+1
                subplot('Position',[ ...
                    0.8, 0.1, ...
                    topo_locs.topoW, ...
                    topo_locs.topoH, ...
                    ]);hold on;
                title('bits');
                h_standard = plot(t, mea_standard, '-',  'Color', c_st);
                h_standard.LineWidth = lw;
                h_target = plot(t, mea_target, '-', 'Color', c_ta);
                h_target.LineWidth = lw;
                legend([h_standard, h_target], ...
                    {'BCE standard', 'BCE target'}, 'Location', 'SouthEast');
                legend('boxoff');
            else
                case_ch = strcmpi(coi_full{ix_coi}, eeg_labels);
                title_ = strrep(sprintf('%s', coi_full{ix_coi}), '_', ' ');
                
                
                %     subplot(ns(1), ns(2), ix_coi);cla;hold on;
                ax = get_ax_topo(case_ch, coi_full{ix_coi}, topo_locs);
                
                cla;hold on;
                if strcmpi(str_met, 'GAR')
                    mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 1, 1), 3);
                    mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 1, 2), 3);
                    se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 1, 1), 0, 3);
                    se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 1, 2), 0, 3);
                    c_st = c.gar_st;
                    c_ta = c.gar_ta;
                elseif strcmpi(str_met, 'OBS')
                    mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 2, 1), 3);
                    mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 2, 2), 3);
                    se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 2, 1), 0, 3);
                    se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 2, 2), 0, 3);
                    c_st = c.obs_st;
                    c_ta = c.obs_ta;
                elseif strcmpi(str_met, 'BCGNet')
                    mea_standard = mean(mat_med(case_ch, :, ix_vec_sub, 3, 1), 3);
                    mea_target = mean(mat_med(case_ch, :, ix_vec_sub, 3, 2), 3);
                    se2_standard = std_error(mat_med(case_ch, :, ix_vec_sub, 3, 1), 0, 3);
                    se2_target = std_error(mat_med(case_ch, :, ix_vec_sub, 3, 2), 0, 3);
                    c_st = c.net_st;
                    c_ta = c.net_ta;
                end
                ciplot(mea_standard - se2_standard, mea_standard + se2_standard, t, c_st, 0.15);
                ciplot(mea_target - se2_target, mea_target + se2_target, t, c_ta, 0.15);
                
                h_standard = plot(t, mea_standard, '-',  'Color', c_st);
                h_standard.LineWidth = lw;
                h_target = plot(t, mea_target, '-', 'Color', c_ta);
                h_target.LineWidth = lw;
                title(title_);
                xlim([t(1) t(end)])
                
                if ix_coi==length(coi_full)+1
                    xlabel('Time (s)');
                    ylabel(sprintf('Amplitude (%s)', a_units))
                else
                    %             axis off;
                    ax.Color = 'none';
                    ax.XColor = 'none';
                    %     ytick = get(gca, 'ytick');set(gca, 'ytick', round(linspace(0, ytick(end), 3)));
                end
            end
            axis tight;
            if isempty(spectral_band)
                s_f = [18.1, 22];
                
                plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
                
            else
                s_f = [18.1, 22];
                xlim([-0.25, 2.5])
                plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
                
            end
        end
        
        print_local(h_f, s_f);
    end
end