clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%

d_project = 'working_eegbcg';
reref = 'laplace';
arch = 'gru_arch_general4';
d_output = 'proc_classified';
training_type = 'multi_run';
% training_type = 'multi_sub';
spectral_band = '';
% spectral_band = 'delta';
% spectral_band = 'theta';
% spectral_band = 'alpha';
% spectral_band = 'alpha_beta1'; %n.b. order matters!
% spectral_band = 'delta_square';
% spectral_band = 'theta_square';
% spectral_band = 'alpha_square';
proc = getenv_check_empty('R_PROC');
overwrite = false;

%%

data_source_gar = fullfile('proc_rs');
d_proc_bcg_gar = fullfile(getenv('D_OUT'), d_project, proc, data_source_gar);
[str_source_gar, str_type_gar] = gen_source(data_source_gar);

data_source_obs = fullfile('proc_bcgobs');
d_proc_bcg_obs = fullfile(getenv('D_OUT'), d_project, proc, data_source_obs);
[str_source_obs, str_type_obs] = gen_source(data_source_obs);

data_source_net = fullfile('proc_bcgnet', training_type, arch); % use this format to classify on networks!
d_proc_bcg_net = fullfile(getenv('D_OUT'), d_project, proc, data_source_net);
[str_sourc_net, str_type_net] = gen_source(data_source_net);
%%
[d_proc_epoch_gar, get_f_epoched_gar, vec_sub_gar] = an_epoch(d_project, d_proc_bcg_gar, ...
    'reref', reref, 'str_source', str_source_gar, 'str_merged', str_type_gar, 'spectral_band', spectral_band);

[d_proc_epoch_obs, get_f_epoched_obs, vec_sub_obs] = an_epoch(d_project, d_proc_bcg_obs, ...
    'reref', reref, 'str_source', str_source_obs, 'str_merged', str_type_obs, 'spectral_band', spectral_band);

[d_proc_epoch_net, get_f_epoched_net, vec_sub_net] = an_epoch(d_project, d_proc_bcg_net, ...
    'reref', reref, 'str_source', str_sourc_net, 'str_merged', str_type_net, 'spectral_band', spectral_band);

%%
vec_sub = vec_sub_net;
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = '_5ronly';
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = '';
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
%%
[t, auc_gar, d_proc_classified_gar, vgar] = an_classify(d_project, ...
    d_proc_epoch_gar, get_f_epoched_gar, vec_sub, 'd_output', d_output,...
    'str_source', str_source_gar, 'overwrite', overwrite, ...
    'alg', 'ridgeglmnet', 'lambda', nan);

[t, auc_obs, d_proc_classified, vobs] = an_classify(d_project, ...
    d_proc_epoch_obs, get_f_epoched_obs, vec_sub, 'd_output', d_output,...
    'str_source', str_source_obs, 'overwrite', overwrite, ...
    'alg', 'ridgeglmnet', 'lambda', nan);

[t, auc_net, d_proc_classified_net, vnet] = an_classify(d_project, ...
    d_proc_epoch_net, get_f_epoched_net, vec_sub, 'd_output', d_output,...
    'str_source', str_sourc_net, 'overwrite', overwrite, ...
    'alg', 'ridgeglmnet', 'lambda', nan);

%%
es_fig = '';
if not(isempty(spectral_band))
    str_spectral_band = sprintf('_%s', spectral_band);
else
    str_spectral_band = '';
end
es_fig = [es_fig, training_type];
if not(isempty(spectral_band))
    str_training_type = sprintf('_%s', training_type);
else
    str_training_type = '';
end
es_fig = [es_fig, str_training_type];

% p_fig = fullfile(getenv('D_OUT'), d_project, proc, d_output, 'figures_response1', ...
%     sprintf('c%s_vs%s%s', gen_source(data_source_obs), gen_source(data_source_net), es_fig));

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig07', ...
    sprintf('c%s_%svs%s%s', reref, gen_source(data_source_obs), gen_source(data_source_net), es_fig));

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end

v.figformat = 'svg';
v.figdir = pwd;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;

v.figdir = p_fig;

if not(v.figsave)
    warning('Not saving figures!');
end
print_local = @(h, dim) printForPub(h, sprintf('f_class_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];

%%
case_t = and(t>0.25, t<0.75);

%%

for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    if ix_vec_sub == 1
        default_size = nan(length(vec_sub), length(t));
        
        mat_auc_diff_net_m_obs = default_size;
        mat_auc_diff_net_m_gar = default_size;
        mat_auc_obs = default_size;
        mat_auc_gar = default_size;
        mat_auc_net = default_size;
        
        mat_iauc_diff_net_m_obs = default_size;
        mat_iauc_diff_net_m_gar = default_size;
        mat_iauc_obs = default_size;
        mat_iauc_gar = default_size;
        mat_iauc_net = default_size;
    end
    
    % looking at the std was also kind of interesting...
    mat_auc_obs(ix_vec_sub, :) = nanmean(auc_obs.(str_sub), 1);
    mat_auc_gar(ix_vec_sub, :) = nanmean(auc_gar.(str_sub), 1);
    mat_auc_net(ix_vec_sub, :) = nanmean(auc_net.(str_sub), 1);
    mat_auc_diff_net_m_obs(ix_vec_sub, :) = mat_auc_net(ix_vec_sub, :) - mat_auc_obs(ix_vec_sub, :);
    mat_auc_diff_net_m_gar(ix_vec_sub, :) = mat_auc_net(ix_vec_sub, :) - mat_auc_gar(ix_vec_sub, :);
    
    isf = not(isnan(mat_auc_obs(ix_vec_sub, :)));
    mat_iauc_obs(ix_vec_sub, :) = interp1(t(isf), mat_auc_obs(ix_vec_sub, isf), t, 'pchip');
    mat_iauc_gar(ix_vec_sub, :) = interp1(t(isf), mat_auc_gar(ix_vec_sub, isf), t, 'pchip');
    mat_iauc_net(ix_vec_sub, :) = interp1(t(isf), mat_auc_net(ix_vec_sub, isf), t, 'pchip');
    mat_iauc_diff_net_m_obs(ix_vec_sub, :) = mat_iauc_net(ix_vec_sub, :) - mat_iauc_obs(ix_vec_sub, :);
    mat_iauc_diff_net_m_gar(ix_vec_sub, :) = mat_iauc_net(ix_vec_sub, :) - mat_iauc_gar(ix_vec_sub, :);
end

%%
h_f1 = figure('Name', 'auc_individuals');clf;
h_f2 = figure('Name', 'auc_individuals_diff');clf;

ns = numSubplots(length(vec_sub));

for ix_vec_sub = 1:length(vec_sub)
    str_sub = sprintf('sub%02d', vec_sub(ix_vec_sub));
    
    figure(h_f1);subplot(ns(1), ns(2), ix_vec_sub);cla;hold on;
    plot(t, mat_iauc_obs(ix_vec_sub, :), '-', 'Color', cmap(2, :));
    plot(t, mat_iauc_net(ix_vec_sub, :), '-', 'Color', cmap(3, :));
    xlim([t(1), t(end)])
    grid on;
    
    figure(h_f2);subplot(ns(1), ns(2), ix_vec_sub);cla;hold on;
    plot(t, mat_iauc_diff_net_m_obs(ix_vec_sub, :), '-', 'Color', cmap(end, :));
    xlim([t(1), t(end)])
    grid on;
    title(str_sub);
end
print_local(h_f1, [25, 8]);
print_local(h_f2, [25, 8]);

%%
h_f = figure('Name', 'auc_obs_net');clf;
hold on;lw = 1.5;

xlim([t(1), t(end)]);
% xlim([0, t(end)]);

ylim([0.45, 1.05])
plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
% plot(get(gca, 'xlim'), 0.5 * ones(1, 2), 'k--')
ciplot(nanmean(mat_iauc_obs, 1) - std_error(mat_iauc_obs, 0, 1), nanmean(mat_iauc_obs, 1) + std_error(mat_iauc_obs, 0, 1), t, cmap(2, :), 0.15);
ciplot(nanmean(mat_iauc_net, 1) - std_error(mat_iauc_net, 0, 1), nanmean(mat_iauc_net, 1) + std_error(mat_iauc_net, 0, 1), t, cmap(3, :), 0.15);

h2 = plot(t, nanmean(mat_iauc_obs, 1), 'Color', cmap(2, :), 'lineWidth', lw);
h3 = plot(t, nanmean(mat_iauc_net, 1), 'Color', cmap(3, :), 'lineWidth', lw);

ylabel('AUC');
% xlabel('Time (s)');
legend([h2, h3], 'OBS', 'BCGNet');
legend('boxoff');
print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', 'auc_gar_obs_net');clf;
hold on;lw = 1.5;

xlim([t(1), t(end)]);
% xlim([0, t(end)]);

ylim([0.45, 1.05])
plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
% plot(get(gca, 'xlim'), 0.5 * ones(1, 2), 'k--')
c_new = [0,0,0];
ciplot(nanmean(mat_iauc_gar, 1) - std_error(mat_iauc_gar, 0, 1), nanmean(mat_iauc_gar, 1) + std_error(mat_iauc_gar, 0, 1), t, cmap(1, :), 0.15);
ciplot(nanmean(mat_iauc_obs, 1) - std_error(mat_iauc_obs, 0, 1), nanmean(mat_iauc_obs, 1) + std_error(mat_iauc_obs, 0, 1), t, cmap(2, :), 0.15);
ciplot(nanmean(mat_iauc_net, 1) - std_error(mat_iauc_net, 0, 1), nanmean(mat_iauc_net, 1) + std_error(mat_iauc_net, 0, 1), t, cmap(3, :), 0.15);

h1 = plot(t, nanmean(mat_iauc_gar, 1), 'Color', cmap(1, :), 'lineWidth', lw);
h2 = plot(t, nanmean(mat_iauc_obs, 1), 'Color', cmap(2, :), 'lineWidth', lw);
h3 = plot(t, nanmean(mat_iauc_net, 1), 'Color', cmap(3, :), 'lineWidth', lw);

ylabel('AUC');
% xlabel('Time (s)');
legend([h1, h2, h3], 'BCE', 'OBS', 'BCGNet');
legend('boxoff');

print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', 'auc_gar');clf;
hold on;lw = 1.5;

xlim([t(1), t(end)]);
% xlim([-0.25 1.25]);
% xlim([0, t(end)]);

ylim([0.45, 1.05])
plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
% plot(get(gca, 'xlim'), 0.5 * ones(1, 2), 'k--')
c_new = [0,0,0];
ciplot(nanmean(mat_iauc_gar, 1) - std_error(mat_iauc_gar, 0, 1), nanmean(mat_iauc_gar, 1) + std_error(mat_iauc_gar, 0, 1), t, cmap(1, :), 0.15);
% ciplot(nanmean(mat_iauc_obs, 1) - std_error(mat_iauc_obs, 0, 1), nanmean(mat_iauc_obs, 1) + std_error(mat_iauc_obs, 0, 1), t, cmap(2, :), 0.15);
% ciplot(nanmean(mat_iauc_net, 1) - std_error(mat_iauc_net, 0, 1), nanmean(mat_iauc_net, 1) + std_error(mat_iauc_net, 0, 1), t, cmap(3, :), 0.15);

h1 = plot(t, nanmean(mat_iauc_gar, 1), 'Color', cmap(1, :), 'lineWidth', lw);

% for ix = 1:size(mat_iauc_gar, 1)
%     plot(t, mat_iauc_gar(ix, :), 'Color', 0.9*ones(1, 3), 'lineWidth', 1);
% end
% h2 = plot(t, nanmean(mat_iauc_obs, 1), 'Color', cmap(2, :), 'lineWidth', lw);
% h3 = plot(t, nanmean(mat_iauc_net, 1), 'Color', cmap(3, :), 'lineWidth', lw);

ylabel('AUC');
xlabel('Time (s)');
legend([h1], 'BCE');
legend('boxoff');

print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', 'auc_diff_obs_net');clf;
hold on;lw = 1.5;
xlim([t(1), t(end)]);
ylim([-0.05, 0.05])
plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
plot(get(gca, 'xlim'), 0 * ones(1, 2), 'k--')
ciplot(nanmean(mat_iauc_diff_net_m_obs, 1) - std_error(mat_iauc_diff_net_m_obs, 0, 1), nanmean(mat_iauc_diff_net_m_obs, 1) + std_error(mat_iauc_diff_net_m_obs, 0, 1), t, cmap(3, :), 0.5);

plot(t, nanmean(mat_iauc_diff_net_m_obs, 1), 'Color', cmap(3, :), 'lineWidth', lw);

ylabel('\DeltaAUC');
xlabel('Time (s)');

print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', 'auc_diff_gar_obs_net');clf;
hold on;lw = 1.5;
xlim([t(1), t(end)]);
ylim([-0.05, 0.05])
plot(0 * ones(1, 2), get(gca, 'ylim'), 'k--')
plot(get(gca, 'xlim'), 0 * ones(1, 2), 'k--')
ciplot(nanmean(mat_iauc_diff_net_m_gar, 1) - std_error(mat_iauc_diff_net_m_gar, 0, 1), nanmean(mat_iauc_diff_net_m_gar, 1) + std_error(mat_iauc_diff_net_m_obs, 0, 1), t, cmap(end, :), 0.5);
ciplot(nanmean(mat_iauc_diff_net_m_obs, 1) - std_error(mat_iauc_diff_net_m_obs, 0, 1), nanmean(mat_iauc_diff_net_m_obs, 1) + std_error(mat_iauc_diff_net_m_obs, 0, 1), t, cmap(end, :), 0.5);

plot(t, nanmean(mat_iauc_diff_net_m_gar, 1), 'Color', cmap(end, :), 'lineWidth', lw);
plot(t, nanmean(mat_iauc_diff_net_m_obs, 1), 'Color', cmap(end, :), 'lineWidth', lw);
ylabel('\DeltaAUC');
xlabel('Time (s)');
print_local(h_f, [8.8, 4]);

%%
h_f = figure('Name', sprintf('%s', 'auc_box_obs_net'));clf;
hold on;

vec_auc_obs = nanmean(mat_auc_obs(:, case_t), 2);
vec_auc_net = nanmean(mat_auc_net(:, case_t), 2);
X = [vec_auc_obs; vec_auc_net];


hold on;
G = [2 * ones(size(vec_auc_obs)); 3 * ones(size(vec_auc_net))];
if strcmpi(training_type, 'multi_run')
    
    h_box = boxplot([vec_auc_obs; vec_auc_net], ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [cmap(2, :);c.net_st], ...
        'labels',{'OBS', 'BCGNet'}, 'Symbol', '');
elseif strcmpi(training_type, 'multi_sub')
    h_box = boxplot([vec_auc_obs; vec_auc_net], ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [cmap(2, :);c.net_ta], ...
        'labels',{'OBS', 'Retrained'}, 'Symbol', '');
else
    error('!');
end


box off;
set(h_box, {'linew'}, {1.75});
av = findobj(gca, 'tag', 'Lower Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end
av = findobj(gca, 'tag', 'Upper Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end

c_grey = 0.7*ones(1, 3);
c_alpha = 0.4;
MarkerSize = 6;
MarkerEdgeColor = [1,1,1];
% note that in inkscape you then need to add a 0.1mm border to each
% point (find property circle)
for ix_vec_sub = 1:length(vec_sub)
    h = plot([1, 2], [vec_auc_obs(ix_vec_sub), vec_auc_net(ix_vec_sub)], ...
        '-', 'Color', cmap(end, :));
    h.Color(4) = c_alpha;
    for ix = 1:2
        switch ix
            case 1, vec_local = vec_auc_obs(ix_vec_sub);
            case 2, vec_local = vec_auc_net(ix_vec_sub);
        end
        
        plot(ix, vec_local, ...
            '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
            'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
    end
end



y_range = [min(X(:)), max(X(:))];
d = ceil((diff(y_range)*0.075)*100)/100;
tooth_length = d * 0.25;
line_height = y_range(2) + d;
[p_obs_net, ~, md_obs_net] = plot_stars(vec_auc_obs, vec_auc_net, [1, 2], line_height, tooth_length);

ylim([y_range(1) - 0.025, y_range(2) + 4.5*d])

fprintf('---\n');

fprintf('\n(');
fprintf('MD_OBS-BCGNet = %0.3f, p = %s', md_obs_net, p_to_str(p_obs_net));
fprintf(')\n');
fprintf('---\n');

%     title(title_);
ylabel('AUC')
print_local(h_f, [4.4, 4]);
%%
h_f = figure('Name', sprintf('%s', 'auc_box_obs_net_switched'));clf;
hold on;

vec_auc_obs = nanmean(mat_auc_obs(:, case_t), 2);
vec_auc_net = nanmean(mat_auc_net(:, case_t), 2);
X = [vec_auc_net, vec_auc_obs];


hold on;
G = [2 * ones(size(vec_auc_obs)); 3 * ones(size(vec_auc_net))];
if strcmpi(training_type, 'multi_run')
    
    h_box = boxplot([vec_auc_net, vec_auc_obs], ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [c.net_st; cmap(2, :)], ...
        'labels',{'BCGNet', 'OBS'}, 'Symbol', '');
elseif strcmpi(training_type, 'multi_sub')
    h_box = boxplot([vec_auc_net, vec_auc_obs], ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [c.net_ta; cmap(2, :)], ...
        'labels',{'Retrained', 'OBS'}, 'Symbol', '');
else
    error('!');
end


box off;
set(h_box, {'linew'}, {1.75});
av = findobj(gca, 'tag', 'Lower Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end
av = findobj(gca, 'tag', 'Upper Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end

c_grey = 0.7*ones(1, 3);
c_alpha = 0.4;
MarkerSize = 6;
MarkerEdgeColor = [1,1,1];
% note that in inkscape you then need to add a 0.1mm border to each
% point (find property circle)
for ix_vec_sub = 1:length(vec_sub)
    h = plot([1, 2], [vec_auc_net(ix_vec_sub), vec_auc_obs(ix_vec_sub)], ...
        '-', 'Color', cmap(end, :));
    h.Color(4) = c_alpha;
    for ix = 1:2
        switch ix
            case 1, vec_local = vec_auc_net(ix_vec_sub);
            case 2, vec_local = vec_auc_obs(ix_vec_sub);
        end
        
        plot(ix, vec_local, ...
            '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
            'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
    end
end



y_range = [min(X(:)), max(X(:))];
d = ceil((diff(y_range)*0.075)*100)/100;
tooth_length = d * 0.25;
line_height = 0.975;
[p_obs_net, ~, md_obs_net] = plot_stars(vec_auc_net, vec_auc_obs, [1, 2], line_height, tooth_length);

ylim([y_range(1) - 0.025, 1.01])

fprintf('---\n');

fprintf('\n(');
fprintf('MD_RT-OBS = %0.3f, p = %s', md_obs_net, p_to_str(p_obs_net));
fprintf(')\n');
fprintf('---\n');

%     title(title_);
ylabel('AUC')
print_local(h_f, [4.4, 4]);

%%
h_f = figure('Name', sprintf('%s', 'auc_box_gar_obs_net'));clf;
hold on;

vec_auc_gar = nanmean(mat_auc_gar(:, case_t), 2);
vec_auc_obs = nanmean(mat_auc_obs(:, case_t), 2);
vec_auc_net = nanmean(mat_auc_net(:, case_t), 2);
X = [vec_auc_gar; vec_auc_obs; vec_auc_net];

hold on;
G = [1 * ones(size(vec_auc_gar)); 2 * ones(size(vec_auc_obs)); 3 * ones(size(vec_auc_net))];

if strcmpi(training_type, 'multi_run')
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [cmap(1, :);cmap(2, :);c.net_st], ...
        'labels',{'BCE', 'OBS', 'BCGNet'}, 'Symbol', '');
elseif strcmpi(training_type, 'multi_sub')
    h_box = boxplot(X, ...
        G, ...
        'ColorGroup', G, ...
        'Colors', [cmap(1, :);cmap(2, :);c.net_ta], ...
        'labels',{'BCE', 'OBS', 'Retrained'}, 'Symbol', '');
else
    error('!');
end
box off;
set(h_box, {'linew'}, {1.75});
av = findobj(gca, 'tag', 'Lower Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end
av = findobj(gca, 'tag', 'Upper Adjacent Value');
for ix = 1:length(av)
    av(ix).XData = av(ix).XData + [-0.1, +0.1];
end


c_grey = 0.7*ones(1, 3);
c_alpha = 0.4;
MarkerSize = 6;
MarkerEdgeColor = [1,1,1];
% note that in inkscape you then need to add a 0.1mm border to each
% point (find property circle)
for ix_vec_sub = 1:length(vec_sub)
    h = plot([1, 2], [vec_auc_gar(ix_vec_sub), vec_auc_obs(ix_vec_sub)], ...
        '-', 'Color', cmap(end, :));
    h.Color(4) = c_alpha;
    h = plot([2, 3], [vec_auc_obs(ix_vec_sub), vec_auc_net(ix_vec_sub)], ...
        '-', 'Color', cmap(end, :));
    h.Color(4) = c_alpha;
    
    for ix = 1:3
        switch ix
            case 1, vec_local = vec_auc_gar(ix_vec_sub);
            case 2, vec_local = vec_auc_obs(ix_vec_sub);
            case 3, vec_local = vec_auc_net(ix_vec_sub);
        end
        
        plot(ix, vec_local, ...
            '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
            'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
    end
end

y_range = [min(X(:)), max(X(:))];

if isempty(spectral_band)
    line_height = 0.97;
    d = ceil((diff(y_range)*0.05)*100)/100;
    tooth_length = d * 0.5;
else
    line_height = 0.97;
    d = 0.02;
    tooth_length = 0.01;
end

[p_gar_obs, ~, stats] = signrank(vec_auc_gar, vec_auc_obs);
[p_gar_net, ~, stats] = signrank(vec_auc_gar, vec_auc_net);
[p_obs_net, ~, stats] = signrank(vec_auc_obs, vec_auc_net);
[p_gar_obs_c, p_gar_net_c, p_obs_net_c] = deal_bonf_holm([p_gar_obs, p_gar_net, p_obs_net]);

[p_gar_obs_c, ~, md_gar_obs_c] = ...
    plot_stars(vec_auc_gar, vec_auc_obs, [1, 2], line_height, tooth_length, p_gar_obs_c);
[p_gar_net_c, ~, md_gar_net_c] = ...
    plot_stars(vec_auc_gar, vec_auc_net, [1, 3], line_height + 3.75*d, tooth_length, p_gar_net_c);
[p_obs_net_c, ~, md_obs_net_c] = ...
    plot_stars(vec_auc_obs, vec_auc_net, [2, 3], line_height, tooth_length, p_obs_net_c);

if isempty(spectral_band)
    ylim([y_range(1) - 0.025, y_range(2) + 8*d])
else
    ylim([0.49, 1.11])
end
fprintf('---\n');

fprintf('\n(');
fprintf('MD_BCE-OBS = %0.3f, p = %s; ', md_gar_obs_c, p_to_str(p_gar_obs_c));
fprintf('MD_BCE-BCGNet = %0.3f, p = %s; ', md_gar_net_c, p_to_str(p_gar_net_c));
fprintf('MD_OBS-BCGNet = %0.3f, p = %s', md_obs_net_c, p_to_str(p_obs_net_c));
fprintf(')\n');

ylabel('AUC')
print_local(h_f, [4.4, 4]);
