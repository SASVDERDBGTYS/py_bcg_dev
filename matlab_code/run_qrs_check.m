clearvars;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('an_results');

%%
proc = getenv_check_empty('R_PROC');
d_project = 'working_eegbcg';
v.overwrite = false;
v.str_ecg = 'ECG';
v.use_full_time = false;
arch = 'gru_arch_general4';
%%
proc_bcgobs = fullfile('proc_bcgobs');
proc_qrs = fullfile('proc_qrs');
T_qrs_stats = an_qrs_statistics(d_project, proc_bcgobs, proc_qrs, 'overwrite', v.overwrite);
if v.use_full_time
    proc_rs = fullfile('proc_rs');
    proc_net = fullfile('proc_bcgnet', 'multi_run', arch);
    T = an_psd_time_series(d_project, proc_bcgobs, proc_rs, proc_net, 'overwrite', v.overwrite);
else  % use test epochs
    proc_net = fullfile('proc_test_epochs', 'multi_run', arch); % use this format to classify on networks!
    T = an_psd_test_epochs(d_project, proc_net, 'overwrite', v.overwrite);
end

%% Now add the QRS in
for ix_file = 1:height(T)
    
    case_run = T_qrs_stats.ix_run == T.ix_run(ix_file);
    case_sub = strcmpi(T_qrs_stats.str_sub, T.str_sub{ix_file});
    T.qrs_lat{ix_file} = T_qrs_stats.qrs_lat{case_sub & case_run};
    T.qrs_delta{ix_file} = T_qrs_stats.qrs_delta{case_sub & case_run};
    T.qrs_std(ix_file) = T_qrs_stats.qrs_std(case_sub & case_run);
    T.qrs_outlier(ix_file) = T_qrs_stats.qrs_outlier(case_sub & case_run);
end
T.Properties.UserData.an_qrs_statistics = T_qrs_stats.Properties.UserData.an_qrs_statistics;
%%
cell_band = {'delta', 'theta', 'alpha'};
met_comparison = 'div';
cell_coi = {'Fz', 'Pz', 'AVG'};
op_additional = {'qrs_std', 'qrs_outlier'};
[T, T_sub] = get_power_difference(T, cell_band, cell_coi, met_comparison, ...
    v.str_ecg, 'str_sub', op_additional);
%%
vec_sub = T_sub.ix_sub';
assert(length(vec_sub)==19, 'Missing subjects?');
only_use_five_runs = false;
if only_use_five_runs
    es = '_5ronly';
    vec_sub(vec_sub==15) = [];
    vec_sub(vec_sub==17) = [];
    vec_sub(vec_sub==19) = [];
    vec_sub(vec_sub==23) = [];
else
    es = '';
end
cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
%% unlike other scripts, this one does direct operations on the whole of T/T_sub
% so just remove unused subjects if there are any
ix_keep_T = false(height(T), 1);
ix_keep_T_sub = false(height(T_sub), 1);
for ix_sub = 1:length(cell_sub)
ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
end
T = T(ix_keep_T, :);
T_sub = T_sub(ix_keep_T_sub, :);

%%
met_qrs_calc = 'qrs_std';
% met_qrs_calc = 'qrs_outlier';

x = T_sub.(met_qrs_calc);
if strcmpi(met_qrs_calc, 'qrs_outlier')
    x_label = 'Fraction of outlier \DeltaQRS';
    xlim_= [-0.01, 0.15];
    xtick_ = [0:0.05:0.25];
    
elseif strcmpi(met_qrs_calc, 'qrs_std')
    x_label = 'Std. \DeltaQRS';
    xlim_= [0.04, 0.25];
    xtick_ = [xlim_(1):0.1:xlim_(end)];
    
end
c = get_cmap;
v.figsave = true;
v.figformat = 'svg';
v.fontsizeAxes = 7;
v.fontsizeText = 6;
p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', 'fig05');
if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
print_local = @(h, dim) printForPub(h, sprintf('f_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', p_fig, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

%%
for ix_band = 1:length(cell_band)
    band = cell_band{ix_band};
    met1 = sprintf('AVG_%s_p_net_over_p_gar', band);
    met2 = sprintf('AVG_%s_p_obs_over_p_gar', band);
    h_model = fitlm(x,  T_sub.(met1) - T_sub.(met2));
    
    h_f2 = figure('Name', sprintf('%s_fraction_%s_diff', met_qrs_calc, band)); clf; hold on;
    h_a2 = gca;
    
    f_temp = figure(10);
    h = h_model.plot;
    title_ = sprintf('p=%0.3f', h_model.coefTest);
    fprintf('%s\n', title_);
    copyobj(h(2:end), h_a2);
    close(f_temp);
    
    MarkerSize = 4;
    
    plot(h_a2, x, T_sub.(met1) - T_sub.(met2), ...
        'o', 'MarkerFaceColor', c.neutral, 'Color', c.neutral, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%     grid on;
    xlim([xlim_])
    xlabel(x_label)
    ylabel('\Delta(Power ratio)');
    title(title_);
    set(gca, 'xtick', xtick_);
    print_local(h_f2, [4.4, 4]);
    
    %%
    h_f1 = figure('Name', sprintf('%s_fraction_%s', met_qrs_calc, band)); clf; hold on;
    h_a1 = plot(x, T_sub.(met1), ...
        'o', 'MarkerFaceColor', c.net_st, 'Color', c.net_ta, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
    h_a2 = plot(x, T_sub.(met2), ...
        'o', 'MarkerFaceColor', c.obs_st, 'Color', c.obs_ta, 'MarkerSize', MarkerSize, 'MarkerEdgeColor', [1, 1, 1]);
%     grid on;
    xlabel(x_label)
    ylabel('Power ratio');
    xlim([xlim_])
    h_a1.DisplayName = 'BCGNet';
    h_a2.DisplayName = 'OBS';
    legend('Location', 'BestOutside');
    legend('boxoff');
    title('-');
    set(gca, 'xtick', xtick_);
    print_local(h_f1, [7.2, 4]);
end
