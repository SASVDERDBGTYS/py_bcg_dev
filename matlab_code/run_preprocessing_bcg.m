clear;
close all;
set_env;
addpath(genpath('auxf'));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');

d_project = 'working_eegbcg';
%%
sp_rmbcg(d_project);
sp_rmbcg(d_project, 'causal_method', 'ecg_subsmooth');
sp_rmbcg(d_project, 'causal_method', 'ecg_diff');

