%% Setup

clear;
clc;
close all;

set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
if isempty(which('eeglab.m')),addpath(getenv('D_EEGLAB'));eeglab;close;end
addpath('sp_preproc');

d_project = 'working_eegbcg';

%% Perform resampling

sp_resample_clayden(d_project);
