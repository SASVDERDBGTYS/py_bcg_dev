clearvars;
close all;
set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'an_results')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
% addpath('an_results');
%%

d_project = 'working_eegbcg';
proc = getenv_check_empty('R_PROC');
v.overwrite = false;
v.str_ecg = 'ECG';
%%
arch = 'gru_arch_general4';
if strcmpi(getenv('R_PROC'), 'proc_clayden')
    proc_net = fullfile('proc_bcgnet', 'single_sub', arch);
else
    proc_net = fullfile('proc_bcgnet', 'multi_run', arch);
end
proc_obs = 'proc_bcgobs';
proc_rs = 'proc_rs';
%%
d_proc_net = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_net);file_filter_net = 'bcgnet';
d_proc_obs = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_obs);file_filter_bcg = 'rmbcg';
d_proc_gar = fullfile(getenv('D_DATA'), d_project, getenv('R_PROC'), proc_rs);file_filter_rs = 'rs';

%%
coi = 'T8';
str_band = 'spect';
cell_met = {'BCE', 'OBS', 'BCGNet'};
n_met = length(cell_met);
% this loading is just a temporary convenience
f_X = fullfile(getenv('D_OUT'), d_project, getenv('R_PROC'), 'proc_spect', sprintf('%s_%s', str_band, coi));
overwrite_X = false;
do_stft_compile = generate_check_mat(f_X, overwrite_X);
if do_stft_compile
    [d_proc_stft, get_f_stft_net, vec_sub_net] = an_proc_filter(d_project, d_proc_net, file_filter_net, 'band', str_band);
    [~, get_f_stft_obs, ~] = an_proc_filter(d_project, d_proc_obs, file_filter_bcg, 'band', str_band);
    [~, get_f_stft_gar, ~] = an_proc_filter(d_project, d_proc_gar, file_filter_rs, 'band', str_band);
    %
    vec_sub = vec_sub_net;
    if strcmpi(getenv('R_PROC'), 'proc_full')
        assert(length(vec_sub)==19, 'Missing subjects?');
        only_use_five_runs = false;
        if only_use_five_runs
            es = '_5ronly';
            vec_sub(vec_sub==15) = [];
            vec_sub(vec_sub==17) = [];
            vec_sub(vec_sub==19) = [];
            vec_sub(vec_sub==23) = [];
        else
            es = '';
        end
        %         cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
    end
    
    v.str_qrs = 'qrs0';
    
    
    for ix_cell_met = 1:n_met
        str_met = cell_met{ix_cell_met};
        ns = numSubplots(length(vec_sub));
        if strcmpi(getenv('R_PROC'), 'proc_full')
            t_i = 0:1/500:1500;
        else
            t_i = 0:1/500:600;
        end
        
        X_coi_yyy = zeros(length(vec_sub), length(t_i));
        A_coi_net = X_coi_yyy;
        
        clear X_coi_yyy;
        for ix_vec_sub_local = 1:length(vec_sub)
            if strcmpi(str_met, 'BCE')
                f_use = get_f_stft_gar(d_proc_stft, vec_sub(ix_vec_sub_local));
            elseif strcmpi(str_met, 'OBS')
                f_use = get_f_stft_obs(d_proc_stft, vec_sub(ix_vec_sub_local));
            elseif strcmpi(str_met, 'BCGNet')
                f_use = get_f_stft_net(d_proc_stft, vec_sub(ix_vec_sub_local));
            else
                error('>');
            end
            EEG_use = pop_loadset(sprintf('%s%s', f_use, '.set'));
            
            ix_coi = strcmpi(coi, {EEG_use.chanlocs.labels});
            x = EEG_use.spect_data(:, :, ix_coi);
            
            if and(ix_vec_sub_local == 1, ix_cell_met == 1)
                n_max = 600 * EEG_use.spect_srate;
                g.X = nan([size(x, 1), n_max, length(vec_sub), n_met]);
                g.f = EEG_use.spect_frequencies;
                g.fs = EEG_use.spect_srate;
                g.vec_sub = vec_sub;
            end
            g.X(:, :, ix_vec_sub_local, ix_cell_met) = x(:, 1:n_max);
            
        end
    end
    save(f_X, '-struct', 'g');
else
    g = load(f_X);
end
%%
% maybe should also look at individual subs?
if strcmpi(getenv('R_PROC'), 'proc_full')
    fig_folder = 'figures_response1';
elseif strcmpi(getenv('R_PROC'), 'proc_clayden')
    fig_folder = 'figures_clayden';
end

p_fig = fullfile(getenv('D_OUT'), d_project, proc, 'figures_response1', ...
    sprintf('c%s_vs%s_%s', gen_source(proc_net), gen_source(proc_obs), str_band));
% p_fig = '~/Desktop/temp/';

if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
v.figformat = 'svg';
v.figdir = p_fig;
v.figsave = true;
v.fontsizeAxes = 7;
v.fontsizeText = 6;
if not(v.figsave)
    warning('Not saving figures!');
end
es = '';
print_local = @(h, dim) printForPub(h, sprintf('f_%s%s', h.Name, es), 'doPrint', v.figsave,...
    'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
    'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);

c = get_cmap;
%%
smooth_op = @ medfilt1;
t_sm = 0;
t = ([1:size(g.X, 2)]-1)/g.fs;

% cmap = brighten(parula(256), -0.5);
cmap = hcparula(256);
%%
for ix_vec_sub = 1:length(g.vec_sub)+1
    for ix_cell_met = 1:n_met
        if ix_vec_sub == length(g.vec_sub)+1
            str_sub = 'all';
            Xf = g.X(:, :, :, ix_cell_met);
        else
            str_sub = sprintf('sub%02d', g.vec_sub(ix_vec_sub));
            Xf = g.X(:, :, ix_vec_sub, ix_cell_met);
        end
        n_sm = round(t_sm * g.fs);
        if n_sm>1
            for ix_f = 1:size(Xf, 1)
                for ix_vec_sub_local = 1:size(Xf, 3)
                    Xf(ix_f, :, ix_vec_sub_local) = medfilt1(Xf(ix_f, :, ix_vec_sub_local), n_sm);
                end
            end
        end
        do_pct = false;
        if do_pct
            case_bl = t < 60;
            X_bl = median(Xf(:, case_bl, :), 2);
            Xf = (Xf - X_bl)./X_bl;
        else
            Xf = log(Xf);
        end
        Z = median(Xf, 3);
        str_met = cell_met{ix_cell_met};
        h_f = figure('Name', sprintf('%s_%s_%s_%s', str_band, coi, str_sub, str_met));clf;
        
        %     imagesc(t, g.f, Z);
        ylim_ = [1, 13];
        
        %         if ix_vec_sub == length(g.vec_sub)+1
        %             cl = [-2, 7.5];
        %         else
        if ix_cell_met == 1
            [~, ix_f_min] = min(abs(g.f - (ylim_(1))));
            [~, ix_f_max] = min(abs(g.f - (ylim_(2))));
            Z_slice = Z(ix_f_min:ix_f_max, :);
            cl = prctile(Z_slice(:), [1, 99]);
        end
        %         end
        imagesc(t, g.f, Z, cl);
        set(gca, 'ydir', 'normal');
        colorbar;
        if t_sm < 30
            t_ed = 30;
        else
            t_ed = t_sm;
        end
        xlim([t_ed, 600 - t_ed]);
        ylim(ylim_)
        %         xlabel('Time (s)');
        ylabel('Frequency (Hz)');
        
        colormap(h_f, cmap);
        %         print_local(h_f, [18.1, 3]);
        print_local(h_f, [18.1, 3]);
    end
end
%%
% cmap = brighten(parula(256), -0.5);
D_bce_m_net = log(g.X(:, :, :, 1)) - log(g.X(:, :, :, 3));
D_obs_m_net = log(g.X(:, :, :, 2)) - log(g.X(:, :, :, 3));
D_bce_m_obs = log(g.X(:, :, :, 1)) - log(g.X(:, :, :, 2));
for ix_vec_sub = length(g.vec_sub)+1
    if ix_vec_sub == length(g.vec_sub)+1
        str_sub = 'all';
        Xf_bce_m_net = D_bce_m_net;
        Xf_obs_m_net = D_obs_m_net;
        Xf_bce_m_obs = D_bce_m_obs;
    else
        str_sub = sprintf('sub%02d', g.vec_sub(ix_vec_sub));
        Xf_bce_m_net = D_bce_m_net(:, :, ix_vec_sub);
        Xf_obs_m_net = D_obs_m_net(:, :, ix_vec_sub);
        Xf_bce_m_obs = D_bce_m_obs(:, :, ix_vec_sub);
    end
    n_sm = round(t_sm * g.fs);
    if n_sm>1
        for ix_f = 1:size(Xf_bce_m_net, 1)
            for ix_vec_sub_local = 1:size(Xf_bce_m_net, 3)
                Xf_bce_m_net(ix_f, :, ix_vec_sub_local) = medfilt1(Xf_bce_m_net(ix_f, :, ix_vec_sub_local), n_sm);
            end
        end
        for ix_f = 1:size(Xf_obs_m_net, 1)
            for ix_vec_sub_local = 1:size(Xf_obs_m_net, 3)
                Xf_obs_m_net(ix_f, :, ix_vec_sub_local) = medfilt1(Xf_obs_m_net(ix_f, :, ix_vec_sub_local), n_sm);
            end
        end
        for ix_f = 1:size(Xf_bce_m_obs, 1)
            for ix_vec_sub_local = 1:size(Xf_bce_m_obs, 3)
                Xf_bce_m_obs(ix_f, :, ix_vec_sub_local) = medfilt1(Xf_bce_m_obs(ix_f, :, ix_vec_sub_local), n_sm);
            end
        end
    end
    
    for ix_met = 1:3
        if ix_met == 1
            str_met = 'bce_m_net';
            Z = median(Xf_bce_m_net, 3);
        elseif ix_met == 2
            str_met = 'obs_m_net';
            Z = median(Xf_obs_m_net, 3);
        elseif ix_met == 3
            str_met = 'bce_m_obs';
            Z = median(Xf_bce_m_obs, 3);
            
        end
        h_f = figure('Name', sprintf('%s_%s__%s_%s', str_band, coi, str_sub, str_met));clf;
        
        ylim_ = [1, 13];
        
        [~, ix_f_min] = min(abs(g.f - (ylim_(1))));
        [~, ix_f_max] = min(abs(g.f - (ylim_(2))));
        Z_slice = Z(ix_f_min:ix_f_max, :);
        cl = prctile(Z_slice(:), [1, 99]);
        cl = max(abs(cl)) * [-1, 1];
        
        imagesc(t, g.f, Z, cl);
        set(gca, 'ydir', 'normal');
        colorbar;
        if t_sm < 30
            t_ed = 30;
        else
            t_ed = t_sm;
        end
        xlim([t_ed, 600 - t_ed]);
        ylim(ylim_)
        %         xlabel('Time (s)');
        ylabel('Frequency (Hz)');
        
        colormap(h_f, cmap);
        %         print_local(h_f, [18.1, 3]);
        print_local(h_f, [18.1, 3]);
    end
end