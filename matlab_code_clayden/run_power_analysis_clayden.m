clearvars;
close all;
set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'an_results')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
addpath('an_results');

%%

skip_settings = true;

try
    cd(fullfile(getenv('D_GIT'), 'matlab_code'));
    run_power_analysis;
    cd(fullfile(getenv('D_GIT'), 'matlab_code_clayden'));
catch errt
    cd(fullfile(getenv('D_GIT'), 'matlab_code_clayden'));
    rethrow(errt);
end
