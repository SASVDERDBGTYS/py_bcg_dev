clear;
addpath('..');
set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));

addpath(getenv('D_EEGLAB'));
addpath('../sp_preproc');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
d_project = 'working_eegbcg';

mat_to_eeglab(d_project);
