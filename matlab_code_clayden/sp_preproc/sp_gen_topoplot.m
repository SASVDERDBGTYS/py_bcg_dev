function sp_gen_topoplot(d_project, varargin)
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');

%% Parameter setup

d.figsave = true;                        % flag for whether to save plots
d.overwrite = true;                     % flag for whether to overwrite existing data
d.d_overwrite = struct;                  % overwriting struct provided by user
d.d_dipfit = fullfile('plugins', 'dipfit');
d.proc = getenv_check_empty('R_PROC');   % output directory set in set_env/dnc_set_env
d.str_figure = fullfile('figures', 'topoplots');
%% Parse inputs

[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Obtain the path information

d_raw = fullfile(getenv('D_DATA'), d_project, 'raw_clayden');
d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);

%% Plotting the topoplots

% obtain all datasets (only in the 'raw' directories)
file_list = glob(fullfile(d_raw, '**.vhdr'));
file_list = file_list(not(contains(file_list, 'derivatives')));

% obtain all datasets (only in the 'raw' directories)
file_list = glob(fullfile(d_raw, '**.vhdr'));
file_list = file_list(not(contains(file_list, 'derivatives')));

% file_list = file_list(contains(file_list, '47'));  % debugging

% loop through files found
for ix_file = 1:length(file_list)
    
    % obtain file name, subject idx, run idx, etc
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    [~, f_for_rng] = fileparts(p_raw_eeg);
    rng(string2hash([f_for_rng, f_raw_eeg]));
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    str_sub = regexp(f_raw_eeg, '(sub-\d+)_.+', 'tokens');
    str_sub = str_sub{1}{1};
    str_sub = erase(str_sub, '-');
    ix_run = 0;
    
    % define figure directory
    f_out_topo_figure = sprintf('%s_r%02d_topoplot', str_sub, ix_run);
    p_out_topo_figure = fullfile(d_proc, v.str_figure);
    if ~(exist(p_out_topo_figure, 'dir') == 7), mkdir(p_out_topo_figure); end
    
    % define output directory and test whether to generate the plots
    d_out_topo_figure = fullfile(p_out_topo_figure, f_out_topo_figure);
    do_out_topo_figure = generate_check_eeg(d_out_topo_figure, v.overwrite);
    
    if do_out_topo_figure
        % loading the dataset
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, f_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        
        % obtain the chanlocs with channel locations
        orig_chanlocs = EEG.chanlocs;
        EEG = pop_chanedit(EEG, 'lookup', ...
        fullfile(getenv('D_EEGLAB'), v.d_dipfit, 'standard_BEM/elec/standard_1005.elc'));
        
        % saving the plot
        h_f = figure(11);clf;
        topoplot([], EEG.chanlocs, 'style','blank','plotchans', [], ...
            'electrodes','labelpoint','chaninfo',EEG.chaninfo);
        printForPub(h_f, sprintf('%s', f_out_topo_figure), 'doPrint', v.figsave,...
                'fformat','png' ,'physicalSizeCM',[15 15],'saveDir', p_out_topo_figure);
    end
end