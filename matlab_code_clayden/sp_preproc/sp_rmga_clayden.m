function sp_rmga_clayden(d_project, varargin)
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%% Parameter setup

d.str_grad = 'V';                        % event type in EEGLAB for TR markers (would argue setting it sth else other than V...)
d.str_ecg = 'ECG';                       % name of the ECG channel
d.figsave = true;                        % flag for whether to save plots
d.overwrite = false;                     % flag for whether to overwrite existing data
d.d_overwrite = struct;                  % overwriting struct provided by user
d.d_dipfit = fullfile('plugins', 'dipfit');
d.testing_mode = false;                  % flag for testing mode: running script on a test channel (currently Fp)
d.proc = getenv_check_empty('R_PROC');   % output directory set in set_env/dnc_set_env
d.tr_t = 2.160;                          % TR value in s
d.th_prct = 99;                          % amplitude thresholding percentage
d.th_frac = 0.25;                        % amplitude thresholding fraction: threshold at frac*per
d.th_shift = -10;                        % amplitude thresholding sample shift
d.th_channel = 'Cz';                     % channel used for extracting the TR marker and for testing
%%
proc_ga = 'proc_ga';
%% Parse inputs

[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Obtain the path information

d_raw = fullfile(getenv('D_DATA'), d_project, 'raw_clayden');
d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);
%%
if v.testing_mode
    proc_ga = sprintf('%s%s', proc_ga, '_test');
end
%% Perform gradient removal

% obtain all datasets (only in the 'raw' directories)
file_list = glob(fullfile(d_raw, '**.vhdr'));
file_list = file_list(not(contains(file_list, 'derivatives')));

% file_list = file_list(contains(file_list, '47'));  % debugging

% loop through files found
for ix_file = 1:length(file_list)
    
    % obtain file name, subject idx, run idx, etc
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    [~, f_for_rng] = fileparts(p_raw_eeg);
    rng(string2hash([f_for_rng, f_raw_eeg]));
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    str_sub = regexp(f_raw_eeg, '(sub-\d+)_.+', 'tokens');
    str_sub = str_sub{1}{1};
    str_sub = erase(str_sub, '-');
    ix_run = 0;
    
    % define figure directory
    f_out_ga_figure_root = sprintf('%s_r%02d_rmga', str_sub, ix_run);
    p_out_ga_figure = fullfile(d_proc, proc_ga, str_sub, 'figures');
    if ~(exist(p_out_ga_figure, 'dir') == 7), mkdir(p_out_ga_figure); end
    
    % define output directory and test whether to perform gradient removal
    f_out_ga =  sprintf('%s_r%02d_rmga', str_sub, ix_run);
    d_out_ga = fullfile(d_proc, proc_ga, str_sub, f_out_ga);
    do_rmga = generate_check_eeg(d_out_ga, v.overwrite);
    
    % perform gradient artifact removal
    if do_rmga
        
        % loading the dataset
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, f_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        EEG.setname = f_out_ga; % this should actually be placed just before the save, since eeglab modifies it
        
        EEG.etc.original_chanlocs = EEG.chanlocs;
        EEG = pop_chanedit(EEG, 'lookup', ...
            fullfile(getenv('D_EEGLAB'), v.d_dipfit, 'standard_BEM/elec/standard_1005.elc'));
        EEG.etc.full_chanlocs = EEG.chanlocs;
        
        % obtain TR value in samples (tr_ixl) and set channel types to correct type
        tr_ixl = (v.tr_t * EEG.srate);
        for ix = 1:EEG.nbchan, EEG.chanlocs(ix).type = 'EEG';end
        EEG.chanlocs(strcmpi({EEG.chanlocs.labels}, v.str_ecg)).type = v.str_ecg;
        
        %% extract TR marker based on v.th_channel
        
        % obtain data from v.th_channel
        x_raw = EEG.data(strcmpi({EEG.chanlocs.labels}, v.th_channel), :);
        tr_threshold = v.th_frac * prctile(abs(x_raw), v.th_prct);
        
        tr_ixs = find(abs(x_raw) > tr_threshold, 1, 'first');
        tr_ixe = find(abs(x_raw) > tr_threshold, 1, 'last');
        
        % FIRST TR MARKER:
        ix_tr = tr_ixs - tr_ixl + v.th_shift;
        
        % INTRODUCE ALL TR MARKERS
        % type of last TR marker set to be V_end
        while ix_tr < tr_ixe
            ix_tr = ix_tr + tr_ixl;
            
            % create new EEGLAB events that correspond to TR markers
            EEG.event(length(EEG.event) + 1).type = v.str_grad;
            EEG.event(end).latency = ix_tr;
            EEG.event(end).duration = 1;
        end
        EEG.event(end).type = 'V_end';
        
        % sort the events based on latency etc
        [~, ix_ev] = sort([EEG.event.latency]);
        EEG.event = EEG.event(ix_ev);
        EEG = rmfield(EEG, 'urevent');
        EEG.event = rmfield(EEG.event, 'bvtime');
        EEG.event = rmfield(EEG.event, 'code');
        EEG.event = rmfield(EEG.event, 'channel');
        EEG.urevent = rmfield(EEG.event, 'urevent');
        
        % checking set and type conversion for set
        EEG = eeg_checkset(EEG);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        
        % if test mode then keep only two channels (including ECG)
        % by default picks the channel for testing
        if v.testing_mode
            warning('In testing mode, keeping only two channels for speed!');
            EEG = pop_select( EEG, 'channel', {v.th_channel});
            
            % convert to double again
            EEG.times = double(EEG.times);
            EEG.data = double(EEG.data);
        end
        
        % obtain index of the ECG channel
        ch_ecg = find(strcmpi({EEG.chanlocs.labels}, v.str_ecg));
        
        % obtain the list of EEG channels
        vec_ch_eeg = 1:numel({EEG.chanlocs.labels});
        vec_ch_eeg = vec_ch_eeg(vec_ch_eeg ~= ch_ecg);
        %% GA remove
        
        % parameters for gradient artifact removal
        vs.lpf = 70;                                            % important to remember there is a 70Hz low pass here
        vs.L = round(25000/EEG.srate);                          % upsampling factor (upsample to 25 kHz)
        vs.Win = 30;                                            % number of artifacts in an average window, just the default
        vs.str_grad = v.str_grad;                               % event type for TR (slice?) markers
        vs.is_slice = 1;                                        % whether the markers are slice triggers: only way this works (i.e. does not work = 0)
        vs.anc_chk = 1;                                         % adaptive noise cancelling
        vs.trig_correct = 0;                                    % correct for missing triggers
        vs.Volumes = [];                                        % fMRI volumes: if no correction then set empty
        vs.pre_frac = [];                                        % manually shifted the event time already (no need for further shift)
        vs.ch_non_eeg = ch_ecg;                                 % index of ECG channel (empty in test mode)
        vs.NPC = 'auto';                                        % whether to perform OBS
        
        % calling fmrib FASTR algorithm and load the settings into the
        % grand parameter setting struct
        EEG_ga = pop_fmrib_fastr(EEG, vs.lpf, vs.L, vs.Win, vs.str_grad, ...
            vs.is_slice, vs.anc_chk, vs.trig_correct, vs.Volumes, [], vs.pre_frac, ...
            vs.ch_non_eeg, vs.NPC);
        v.settings = vs;
        
        if strcmp(str_sub, 'sub35')
            % if from subject 35, channel PO9 is bad so delete that channel
            % first and then interpolate that channel based on information from
            % other channels
            
            % Save the name of the bad channel in the structure
            v.str_bad_channel = {'PO9'};
            
            EEG_ga = interp_bad_channel(EEG_ga, str_sub, v);
            
        elseif strcmp(str_sub, 'sub38')
            % if from subject 38, channel TP9 is bad so delete that channel
            % first and then interpolate that channel based on information from
            % other channels
            
            % Save the name of the bad channel in the structure
            v.str_bad_channel = {'TP9'};
            
            EEG_ga = interp_bad_channel(EEG_ga, str_sub, v);
            
        elseif strcmp(str_sub, 'sub42')
            % if from subject 42, channel P8 and CP1 is bad so delete that channel
            % first and then interpolate that channel based on information from
            % other channels
            
            % Save the name of the bad channel in the structure
            v.str_bad_channel = {'P8', 'CP1'};
            
            EEG_ga = interp_bad_channel(EEG_ga, str_sub, v);
            
        elseif strcmp(str_sub, {'sub47'})
            % if from subject 47, channel FP2, Tp9 and AF3 are bad so delete that channel
            % first and then interpolate that channel based on information from
            % other channels
            
            % Save the name of the bad channel in the structure
            v.str_bad_channel = {'Fp2', 'TP9', 'AF3'};
            
            EEG_ga = interp_bad_channel(EEG_ga, str_sub, v);
        end
        
        % check for success
        if isnan(EEG_ga.data)
            str_failure = sprintf(...
                'There was a failure in sp_eeg_fix for %s, run %s\n', str_sub, ix_run);
            sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), sprintf('%s.set',d_out_ga));
            continue;
        end
        
        %% Plotting the output
        
        % If in testing mode then only plot a single channel (since there
        % is only a single channel)
        
        if v.testing_mode
            % obtain number of TR markers - 1 and plotting data prior to
            % removal
            n_markers_pre = sum(strcmpi({EEG.event.type}, v.str_grad));
            h_f = figure(11);clf;
            figure(h_f);h_s1 = subplot(2, 1, 1);cla;hold on;
            sp_eeg_plot_markers(EEG, 1, v.str_grad);
            title(sprintf('Pre GA removal, %s:%d', v.str_ecg, n_markers_pre));
            printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
                'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
            
            % plotting the results after gradient removal
            n_markers = sum(strcmpi({EEG_ga.event.type}, v.str_grad));
            figure(h_f);h_s2 = subplot(2, 1, 2);cla;hold on;
            sp_eeg_plot_markers(EEG_ga, 1, v.str_grad);
            title(sprintf('Post GA removal, %s:%d', v.str_ecg, n_markers));
            linkaxes([h_s1, h_s2], 'x');
            printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
                'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
            
            
            % If not in testing mode then plot all EEG channels
        else
            for ix = 1:numel(vec_ch_eeg)
                ix_ch = vec_ch_eeg(ix);
                f_out_ga_figure_ch = sprintf('%s_%s', f_out_ga_figure_root, EEG.chanlocs(ix_ch).labels);
                
                % obtain number of TR markers - 1 and plotting data prior to
                % removal
                n_markers_pre = sum(strcmpi({EEG.event.type}, v.str_grad));
                h_f = figure(11);clf;
                figure(h_f);h_s1 = subplot(2, 1, 1);cla;hold on;
                sp_eeg_plot_markers(EEG, ix_ch, v.str_grad);
                title(sprintf('Pre GA removal, channel %s, %s:%d', EEG.chanlocs(ix_ch).labels, v.str_ecg, n_markers_pre));
                printForPub(h_f, sprintf('%s', f_out_ga_figure_ch), 'doPrint', v.figsave,...
                    'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
                
                % plotting the results after gradient removal
                n_markers = sum(strcmpi({EEG_ga.event.type}, v.str_grad));
                figure(h_f);h_s2 = subplot(2, 1, 2);cla;hold on;
                sp_eeg_plot_markers(EEG_ga, ix_ch, v.str_grad);
                title(sprintf('Post GA removal, channel %s, %s:%d', EEG_ga.chanlocs(ix_ch).labels, v.str_ecg, n_markers));
                linkaxes([h_s1, h_s2], 'x');
                printForPub(h_f, sprintf('%s', f_out_ga_figure_ch), 'doPrint', v.figsave,...
                    'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
            end
        end
        
        %% Saving output
        
        % saving the output dataset
        EEG_ga.etc.sp_rmga = v; %store the processing conditions
        EEG_ga = eeg_checkset(EEG_ga);
        pop_saveset(EEG_ga, 'filename', d_out_ga);
    end
end
end

function EEG_ga = interp_bad_channel(EEG_ga, str_sub, v)

for ix_bc = 1:length(v.str_bad_channel)
    % Get rid of the bad channel
    fprintf('\nDeleting a bad channel %s for %s\n', ...
        v.str_bad_channel{ix_bc}, str_sub);
end
EEG_ga = pop_select(EEG_ga, 'nochannel', v.str_bad_channel);

% Perform interpolating based on the other channels
fprintf('\nPerforming interpolation for %s\n', str_sub);
EEG_ga = pop_interp(EEG_ga, EEG_ga.etc.full_chanlocs, 'invdist');
end