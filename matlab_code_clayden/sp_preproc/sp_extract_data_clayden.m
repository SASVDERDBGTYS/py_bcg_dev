% sp_extract_data_clayden: Extract the data either between the first
% and last TR marker or between the second and second last TR markers.
% Function is called in the sp_rmga_resample_clayden script

% Inputs:
%   EEG: EEGLAB format data in which gradient artifact has been removed
%
%   varargin: struct for overwritting the default parameters
%       str_grad: TR marker name
%
%       slice_boundary: boundary to extract before and after TR marker
%
%       flag_last: extract between first and last TR marker or between
%       second and second last TR marker
%           0: between second and second last TR marker
%           1: between first and last TR marker


function EEG_extract = sp_extract_data_clayden(EEG, varargin)
    %% Parameter setup
    
    % Default settings
    d.str_grad = 'V';                        % TR marker name
    d.slice_boundary = 0.2;                  % add periods of specified time around the slice
    d.flag_last = 0;                         % extract between second and second last TR marker
    d.d_overwrite = struct;                  % overwriting struct provided by user
    
    d.str_sub = '';                          % placeholder for subject idx
    d.ix_run = 0;                            % placeholder for run idx
    
    % Parse inputs to overwrite input
    [v, d] = inputParserCustom(d, varargin);clear d;
    v = inputParserStructureOverwrite(v);

    %% Preprocessing
    
    % convert time stamps and data to double in case checkset was performed
    EEG.times = double(EEG.times);
    EEG.data = double(EEG.data);

    %% Locating first and last TR marker 
    % save the original events
    EEG.etc.orig_event = EEG.event;
    
    % obtain the latency for all TR markers
    grad_lat = [EEG.event(strcmpi({EEG.event.type}, v.str_grad)).latency];
    
    % since end of last TR is denoted as v.str_grad + '_end', find that
    % event as well
    label_final_lat = sprintf('%s%s',v.str_grad, '_end');
    grad_final_lat = [EEG.event(strcmpi({EEG.event.type}, label_final_lat)).latency];
    
    % append that to the list of TR marker latencies
    grad_lat = [grad_lat, grad_final_lat];
    
    %% Extract the data in between
    
    % If extract between first and last marker
    if v.flag_last
        slice_from = floor(grad_lat(1) - EEG.srate * v.slice_boundary);
        slice_to = ceil(grad_lat(end) + EEG.srate*v.slice_boundary);
        
    % else if extract between second and second last marker
    else
        slice_from = floor(grad_lat(2) - EEG.srate * v.slice_boundary);
        slice_to = ceil(grad_lat(end - 1) + EEG.srate*v.slice_boundary);
    end
    
    % figure out the proper setname
    f_extract =  sprintf('%s_r%02d_extract', v.str_sub, v.ix_run);
    
    % extract the data
    EEG_extract = pop_select( EEG, 'point', [slice_from, slice_to]);
    
    % store the processing conditions and set setname
    EEG_extract.etc.sp_rmga_extract_data_clayden = v; 
    EEG_extract.setname = f_extract;
    
    % check set again
    EEG_extract = eeg_checkset(EEG_extract);
    
end