function sp_rmbcg_clayden(d_project, varargin)
% TODO: add causal implementation

%% Initial environment exception handling

assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');

%% Parameter setup

d.str_grad = 'V';                        % event type in EEGLAB for TR markers (would argue setting it sth else other than V...)
d.str_ecg = 'ECG';                       % name of the ECG channel
d.str_eeg = 'T8';                        % name of the EEG channel to test
d.figsave = true;                        % flag for whether to save plots
d.overwrite = false;                      % flag for whether to overwrite existing data
d.d_overwrite = struct;                  % overwriting struct provided by user
d.testing_mode = false;                  % flag for testing mode: running script on a test channel (currently Fp)
d.proc = getenv_check_empty('R_PROC');   % output directory set in set_env/dnc_set_env
d.proc_obs = 'proc_bcgobs';                 % output directory name to save obs-cleaned data to
d.f_failure_log = @(proc) fullfile(getenv('D_OUT'), d_project, proc, 'log', 'rmbcg_failure_log.txt');

d.fmrib_pas_method = 'obs';              % processing method used: obs or median, default is obs
d.met_str = 'qrs0';                      % label for the qrs markers

%% Parse inputs

[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Obtain the path information

d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);
d_proc_rs = fullfile(d_proc, 'proc_rs');

%% Modify directory name if testing mode

if v.testing_mode
    v.proc_obs = sprintf('%s%s', v.proc_obs, '_test');
end

%% Performing BCG removal using OBS method

file_list = glob(fullfile(d_proc_rs, '**_r0[0-9]_rs.set'));

    % loop through files found
    for ix_file = 1:length(file_list)

        % obtain file name, subject idx, run idx, etc
        [p_rs, f_rs, ext_rs] = fileparts(file_list{ix_file});
        rng(string2hash(f_rs));
        f_rs = sprintf('%s%s', f_rs, ext_rs);
        [~,folder_rs,~] = fileparts(p_rs);
        ix_run = extractBetween(f_rs, '_r0', '_rs.');
        ix_run = str2double(ix_run{end});
        str_sub = lower(folder_rs);

        % define figure directory
        f_out_obs_figure_root = sprintf('%s_r%02d_rmbcg', str_sub, ix_run);
        p_out_obs_figure = fullfile(d_proc, v.proc_obs, str_sub, 'figures');
        if ~(exist(p_out_obs_figure, 'dir') == 7), mkdir(p_out_obs_figure); end

        % define output directory and test whether to perform BCG removal
        p_rmbcg = fullfile(d_proc, v.proc_obs, str_sub);
        f_rmbcg = sprintf('%s_r%02d_rmbcg', str_sub, ix_run);
        d_out_obs = fullfile(p_rmbcg, f_rmbcg);
        do_rmbcg = generate_check_eeg(d_out_obs, v.overwrite);

        % perform BCG artifact removal
        if do_rmbcg
            %% Loading in the dataset

            fprintf('\n\n\nLoading:\n%s\n', f_rs);
            EEG_rs = pop_loadset(f_rs, p_rs);
            EEG_rs.times = double(EEG_rs.times);
            EEG_rs.data = double(EEG_rs.data);
            EEG_rs.setname = f_rmbcg;

            % if test mode then keep only two channels (including ECG)
            % by default picks the channel for testing
            if v.testing_mode
                warning('In testing mode, keeping only two channels for speed!');
                EEG_rs = pop_select( EEG_rs, 'channel', {v.str_ecg, v.str_eeg});
                
                EEG_rs.times = double(EEG_rs.times);
                EEG_rs.data = double(EEG_rs.data);
            end

            %% QRS Detection

            % Highpass at 2.5 Hz before performing QRS detection

            EEG_qrs = pop_eegfiltnew(EEG_rs, 2.5, [], [], 0, [], 1);
            EEG_qrs.event = [];
            EEG_qrs = eeg_checkset(EEG_qrs);

            % obtain channel number for ECG
            v.ch_ecg = find(strcmpi({EEG_qrs.chanlocs.labels}, v.str_ecg));

            % custom modifications to stop QRS detection from crashing
            % actual qrs detection
            try
                EEG_qrs = pop_fmrib_qrsdetect(EEG_qrs, v.ch_ecg, v.met_str, 'no');
            catch err
                str_failure = sprintf(...
                    'QRS detection failed for %s, run %d, message: %s\n', str_sub, ix_run, err.message);
                sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), sprintf('%s.set',d_out_obs));
                continue;
            end

            % obtain all QRS event marker latency
            choice_qrs = [EEG_qrs.event.latency];
            choice_qrs = choice_qrs(strcmpi({EEG_qrs.event.type}, v.met_str));

            % force 0 duration of QRS events to match with no bcg structure
            for ix_event = 1:length(EEG_qrs.event)
                EEG_qrs.event(ix_event).duration = 0;
            end
            
            % set name of QRS figures 
            f_out_obs_figure_qrs = sprintf('%s%s', f_out_obs_figure_root, ...
                '_qrs_complex');
            
            % Plot the QRS markers if and save figure if figsave is true
            
            choice_qrs_slice = choice_qrs(2:6);
            plot_qrs(EEG_qrs, choice_qrs_slice, v.str_ecg, v.str_eeg, 'first', ...
                v.figsave, f_out_obs_figure_qrs, p_out_obs_figure)

            choice_qrs_slice = choice_qrs(round(size(choice_qrs, 2)/2)-2:round(size(choice_qrs, 2)/2)+2);
            plot_qrs(EEG_qrs, choice_qrs_slice, v.str_ecg, v.str_eeg, 'middle', ...
                v.figsave, f_out_obs_figure_qrs, p_out_obs_figure)

            choice_qrs_slice = choice_qrs(end-5:end-1);
            plot_qrs(EEG_qrs, choice_qrs_slice, v.str_ecg, v.str_eeg, 'last', ...
                v.figsave, f_out_obs_figure_qrs, p_out_obs_figure)

            %% BCG Removal

            % Run the BCG removal script
            EEG_rmbcg = fmrib_pas(EEG_rs, choice_qrs, v.fmrib_pas_method, []);

            % Plotting the raw data pre and post removal
            % set name of pre and post OBS figures 
            f_out_obs_figure_pre_post = sprintf('%s%s', f_out_obs_figure_root, ...
                '_pre_post');
            
            % plotting the figures
            h_f = figure(11);clf;
            figure(h_f);
            plot_pre_post_obs(EEG_rmbcg, EEG_rs, v.str_ecg, v.str_eeg)
            printForPub(h_f, sprintf('%s', f_out_obs_figure_pre_post), 'doPrint', v.figsave,...
                'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_obs_figure);
            
            % Plotting the PSD pre and post removal
            % set name of PSD figures
            f_out_obs_figure_psd = sprintf('%s%s', f_out_obs_figure_root, ...
                '_psd');
            
            % plotting the figures
            h_f = figure(11);clf;
            figure(h_f);
            plot_psd(EEG_rmbcg, EEG_rs, v.str_ecg, v.str_eeg)
            printForPub(h_f, sprintf('%s', f_out_obs_figure_psd), 'doPrint', v.figsave,...
                'fformat','png' ,'physicalSizeCM',[12 10],'saveDir', p_out_obs_figure);
            
            % load the qrs markers into the event and check set again
            EEG_rmbcg.event = [EEG_rmbcg.event, EEG_qrs.event];
            EEG_rmbcg = eeg_checkset(EEG_rmbcg);
            
            % load the configuration into the dataset
            EEG_rmbcg.etc.sp_rmbcg = v;
            
            % saving the dataset
            pop_saveset(EEG_rmbcg, 'filename', f_rmbcg, 'filepath', p_rmbcg, 'version', '6');
        end
    end
end


function plot_pre_post_obs(EEG_rmbcg, EEG_rs, str_ecg, str_eeg)
    
        % Find ECG channel and test channel index
        ch_ecg = find(strcmpi({EEG_rs.chanlocs.labels}, str_ecg));
        ch_eeg = find(strcmpi({EEG_rs.chanlocs.labels}, str_eeg));
        
       
        h_s1 = subplot(2, 1, 1);cla;hold on;
        plot(EEG_rs.times(round(size(EEG_rs.data,2)/2) - 1000:round(size(EEG_rs.data,2)/2) + 1000) / 1000, ...
            EEG_rs.data(ch_ecg, round(size(EEG_rs.data,2)/2) - 1000:round(size(EEG_rs.data,2)/2) + 1000), 'DisplayName', 'BCE')
        xlabel('Time (s)');
        ylabel('Amplitude (\muV)');
        title('ECG data')
        
        h_s2 = subplot(2, 1, 2);cla;hold on;
        plot(EEG_rs.times(round(size(EEG_rs.data,2)/2) - 1000:round(size(EEG_rs.data,2)/2) + 1000) / 1000, ...
            EEG_rs.data(ch_eeg, round(size(EEG_rs.data,2)/2) - 1000:round(size(EEG_rs.data,2)/2) + 1000), 'DisplayName', 'BCE')
        hold on
        plot(EEG_rmbcg.times(round(size(EEG_rmbcg.data,2)/2) - 1000:round(size(EEG_rmbcg.data,2)/2) + 1000) / 1000, ...
            EEG_rmbcg.data(ch_eeg, round(size(EEG_rmbcg.data,2)/2) - 1000:round(size(EEG_rmbcg.data,2)/2) + 1000), 'DisplayName', 'OBS')
        hold off

        legend;
        title('EEG data pre and post BCG removal')
        xlabel('Time (s)');
        ylabel('Amplitude (\muV)');
        
        linkaxes([h_s1, h_s2], 'x');
end

function plot_psd(EEG_rmbcg, EEG_rs, str_ecg, str_eeg)
        
    % Find ECG channel and test channel index
    ch_ecg = find(strcmpi({EEG_rs.chanlocs.labels}, str_ecg));
    ch_eeg = find(strcmpi({EEG_rs.chanlocs.labels}, str_eeg));
    
    fs = EEG_rmbcg.srate;
    ECG_data = EEG_rs.data(ch_ecg, :);
    EEG_pre = EEG_rs.data(ch_eeg, :);
    EEG_post = EEG_rmbcg.data(ch_eeg,:);

    L = 1500;
    nfft = 1500;
    N = 1200;
    [pxx2, f2] = pwelch(EEG_pre, L, N, nfft, fs);
    [pxx3, f3] = pwelch(EEG_post, L, N, nfft, fs);
    
    semilogy(f2, pxx2, 'Displayname', 'BCE');
    hold on;
    semilogy(f3, pxx3, 'Displayname', 'OBS');
    xlim([0 30])
    hold off;
    
    legend;
    title(sprintf('PSD for channel %s pre and post OBS', str_eeg));
    xlabel('Frequency (Hz)');
    ylabel('PSD (\muV^2/Hz)');
end

function plot_qrs(EEG, choice_qrs_slice, str_ecg, str_eeg, mode, ...
    figsave, f_out_obs_figure_qrs, p_out_obs_figure)
    
    str_title = sprintf('%s%s', mode,' five QRS markers');
    h_f = figure(11);clf;
    figure(h_f);
    
    % index to plot prior and after the QRS markers
    ix_back = 300;
    ix_forward = 300;
    
    % Find ECG channel and test channel index
    ch_ecg = find(strcmpi({EEG.chanlocs.labels}, str_ecg));
    ch_eeg = find(strcmpi({EEG.chanlocs.labels}, str_eeg));
    
    h_s1 = subplot(2, 1, 1);cla;hold on;
    plot(EEG.times(choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward) / 1000, ...
        EEG.data(ch_ecg, choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), 'k');
    for ix = 1:length(choice_qrs_slice)
        y = get(gca, 'ylim');
        plot(ones(1, 2) * EEG.times(choice_qrs_slice(ix)) / 1000, y, 'Color', 'm');
    end
    s1 = sprintf('ECG with %s', str_title);
    title(s1)
    xlabel('Time (s)');
    ylabel('Amplitude (\muV)');
    hold off;

    h_s2 = subplot(2, 1, 2);cla;hold on;
    plot(EEG.times(choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward) / 1000, ...
        EEG.data(ch_eeg, choice_qrs_slice(1) - ix_back:choice_qrs_slice(end) + ix_forward), 'k');
    hold on;
    for ix = 1:length(choice_qrs_slice)
        y = get(gca, 'ylim');
        plot(ones(1, 2) * EEG.times(choice_qrs_slice(ix))/ 1000, y, 'Color', 'm');
    end
    s2 = sprintf('EEG channel %s with %s', str_eeg, str_title);
    title(s2)
    xlabel('Time (s)');
    ylabel('Amplitude (\muV)');
    
    linkaxes([h_s1, h_s2], 'x');
    
    printForPub(h_f, sprintf('%s_%s', f_out_obs_figure_qrs, mode), 'doPrint', figsave,...
    'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_obs_figure);
end