function sp_rmga_clayden_alt(d_project, varargin)
assert(not(isempty(getenv('D_OUT'))), 'set_env.m should be run first');
%% Parameter setup

d.str_grad = 'V';                        % event type in EEGLAB for TR markers (would argue setting it sth else other than V...)
d.str_slice = 'S';                       % event type in EEGLAB for slice markers
d.str_ecg = 'ECG';                       % name of the ECG channel
d.figsave = true;                        % flag for whether to save plots
d.overwrite = false;                     % flag for whether to overwrite existing data
d.d_overwrite = struct;                  % overwriting struct provided by user
d.testing_mode = true;                   % flag for testing mode: running script on a test channel (currently Fp)
d.proc = getenv_check_empty('R_PROC');   % output directory set in set_env/dnc_set_env
d.tr_t = 2.160;                          % TR value in s
d.slice_per_tr = 30;                     % number of slices per repetition
d.th_prct = 99;                          % amplitude thresholding percentage
d.th_frac = 0.25;                        % amplitude thresholding fraction: threshold at frac*per
d.th_shift = -10;                        % amplitude thresholding sample shift
d.th_channel = 'Cz';                     % channel used for extracting the TR marker
d.str_grad = 'V';

%% Parse inputs

[v, d] = inputParserCustom(d, varargin);clear d;
v = inputParserStructureOverwrite(v);

%% Obtain the path information

d_raw = fullfile(getenv('D_DATA'), d_project, 'raw_clayden');
d_proc = fullfile(getenv('D_OUT'), d_project, v.proc);

%% Perform gradient removal

% obtain all datasets (only in the 'raw' directories)
file_list = glob(fullfile(d_raw, '**.vhdr'));
file_list = file_list(not(contains(file_list, 'derivatives')));

% loop through files found
for ix_file = 1:length(file_list)
    
    % obtain file name, subject idx, run idx, etc
    [p_raw_eeg, f_raw_eeg, ext_raw_eeg] = fileparts(file_list{ix_file});
    [~, f_for_rng] = fileparts(p_raw_eeg);
    rng(string2hash([f_for_rng, f_raw_eeg]));
    f_raw_eeg = sprintf('%s%s', f_raw_eeg, ext_raw_eeg);
    str_sub = regexp(f_raw_eeg, '(sub-\d+)_.+', 'tokens');
    str_sub = str_sub{1}{1};
    str_sub = erase(str_sub, '-');
    ix_run = 0;
    
    % define figure directory
    f_out_ga_figure_root = sprintf('%s_r%02d_rmga', str_sub, ix_run);
    p_out_ga_figure = fullfile(d_proc, 'proc_ga_alt', str_sub, 'figures');
    if ~(exist(p_out_ga_figure, 'dir') == 7), mkdir(p_out_ga_figure); end
    
    % define output directory and test whether to perform gradient removal
    f_out_ga =  sprintf('%s_r%02d_rmga', str_sub, ix_run);
    d_out_ga = fullfile(d_proc, 'proc_ga_alt', str_sub, f_out_ga);
    do_rmga = generate_check_eeg(d_out_ga, v.overwrite);
    
    % perform gradient artifact removal
    if do_rmga
        
        % loading the dataset
        fprintf('Loading:\n%s\n', fullfile(p_raw_eeg, f_raw_eeg));
        EEG = pop_loadbv(p_raw_eeg, f_raw_eeg);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        EEG.setname = f_out_ga; % this should actually be placed just before the save, since eeglab modifies it
        
        % obtain TR value in samples (tr_ixl) and set channel types to correct type
        tr_ixl = (v.tr_t * EEG.srate);
        for ix = 1:EEG.nbchan, EEG.chanlocs(ix).type = 'EEG';end
        EEG.chanlocs(strcmpi({EEG.chanlocs.labels}, v.str_ecg)).type = v.str_ecg;
        
        %% extract TR marker based on v.th_channel (currently Fp)
        
        % obtain data from v.th_channel
        x_raw = EEG.data(strcmpi({EEG.chanlocs.labels}, v.th_channel), :);
        tr_threshold = v.th_frac * prctile(abs(x_raw), v.th_prct);
        
        tr_ixs = find(abs(x_raw) > tr_threshold, 1, 'first');
        tr_ixe = find(abs(x_raw) > tr_threshold, 1, 'last');
        
        % FIRST TR MARKER:
        ix_tr = tr_ixs - tr_ixl + v.th_shift;
        
        % INTRODUCE ALL TR MARKERS
        % type of last TR marker set to be V_end
        while ix_tr < tr_ixe
            ix_tr = ix_tr + tr_ixl;
            
            % create new EEGLAB events that correspond to TR markers
            EEG.event(length(EEG.event) + 1).type = v.str_grad;
            EEG.event(end).latency = ix_tr;
            EEG.event(end).duration = 1;
        end
        EEG.event(end).type = 'V_end';
        
        %% Extract slice markers based on TR marker
        
        % introduce the slice markers
        s_ixl = tr_ixl / v.slice_per_tr;
        s_ixs = tr_ixs;
        s_ixe = EEG.event(end - 1).latency + tr_ixl;
        
        % First slice marker
        ix_s = s_ixs - s_ixl + v.th_shift;
        
        % introduce all slice markers and set last marker to different type
        while ix_s < s_ixe
            ix_s = ix_s + s_ixl;
            
            % create new EEGLAB events that correspond to TR markers
            EEG.event(length(EEG.event) + 1).type = v.str_slice;
            EEG.event(end).latency = ix_s;
            EEG.event(end).duration = 1;
            
        end
        EEG.event(end).type = 'S_end';
        
        % sort the events based on latency etc
        [~, ix_ev] = sort([EEG.event.latency]);
        EEG.event = EEG.event(ix_ev);
        EEG = rmfield(EEG, 'urevent');
        EEG.event = rmfield(EEG.event, 'bvtime');
        EEG.event = rmfield(EEG.event, 'code');
        EEG.event = rmfield(EEG.event, 'channel');
        EEG.urevent = rmfield(EEG.event, 'urevent');
        
        % checking set and type conversion for set
        EEG = eeg_checkset(EEG);
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        
        % obtain number of TR markers - 1 and plotting data prior to
        % removal
        n_markers_pre = sum(strcmpi({EEG.event.type}, v.str_grad));
        h_f = figure(11);clf;
        figure(h_f);h_s1 = subplot(2, 1, 1);cla;hold on;
        sp_eeg_plot_markers_clayden(EEG, 1, v.str_grad);
        title(sprintf('Pre GA removal, %s:%d', v.str_ecg, n_markers_pre));
        printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
            'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
        
        % if test mode then keep only two channels (including ECG)
        % by default picks the first channel (Fp)
        if v.testing_mode
            warning('In testing mode, keeping only two channels for speed!');
            EEG = pop_select( EEG, 'channel', {EEG.chanlocs(1).labels});
        end
        
        % convert to double again
        EEG.times = double(EEG.times);
        EEG.data = double(EEG.data);
        
        % obtain index of the ECG channel
        ch_ecg = find(strcmpi({EEG.chanlocs.labels}, v.str_ecg));
        
        %% GA removal
        
        % parameters for gradient artifact removal    
        vs.lpf = 70;                                            % important to remember there is a 70Hz low pass here
        vs.L = round(25000/EEG.srate);                          % upsampling factor (upsample to 25 kHz)
        vs.Win = 30;                                            % number of artifacts in an average window, just the default
        vs.str_grad = v.str_slice;                               % event type for TR (slice?) markers
        vs.is_slice = 1;                                        % whether the markers are slice triggers: only way this works (i.e. does not work = 0)
        vs.anc_chk = 1;                                         % adaptive noise cancelling
        vs.trig_correct = 0;                                    % correct for missing triggers
        vs.Volumes = [];                                        % fMRI volumes: if no correction then set empty
        vs.pre_frac = 0;                                        % manually shifted the event time already (no need for further shift)
        vs.ch_non_eeg = ch_ecg;                                 % index of ECG channel (empty in test mode)
        vs.NPC = 'auto';                                        % whether to perform OBS

        
        % calling fmrib FASTR algorithm and load the settings into the
        % grand parameter setting struct
        EEG_ga = pop_fmrib_fastr(EEG, vs.lpf, vs.L, vs.Win, vs.str_grad, ...
            vs.is_slice, vs.anc_chk, vs.trig_correct, vs.Volumes, [], vs.pre_frac, ...
            vs.ch_non_eeg, vs.NPC);
        v.settings = vs;
        
        % check for success
        % TODO: would also add a test for amplitude
        
        %         if isnan(EEG_ga.data)
        %             str_failure = sprintf(...
        %                 'There was a failure in sp_eeg_fix for %s, run %s\n', str_sub, ix_run);
        %             sp_error_handle_custom(str_failure, v.f_failure_log(v.proc), sprintf('%s.set',d_out_ga));
        %             continue;
        %         end
        
        % plotting the results after gradient removal
        n_markers = sum(strcmpi({EEG_ga.event.type}, v.str_grad));
        figure(h_f);h_s2 = subplot(2, 1, 2);cla;hold on;
        sp_eeg_plot_markers_clayden(EEG_ga, 1, v.str_grad);
        title(sprintf('Post GA removal, %s:%d', v.str_ecg, n_markers));
        linkaxes([h_s1, h_s2], 'x');
        printForPub(h_f, sprintf('%s', f_out_ga_figure_root), 'doPrint', v.figsave,...
            'fformat','png' ,'physicalSizeCM',[30 10],'saveDir', p_out_ga_figure);
        
        % saving the output dataset
        EEG_ga.etc.sp_rmga = v; %store the processing conditions
        EEG_ga = eeg_checkset(EEG_ga);
        pop_saveset(EEG_ga, 'filename', d_out_ga);
    end
end
end