%% Setup

clear;
clc;
close all;

set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
% addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'sp_preproc')));
if isempty(which('eeglab.m')),addpath(getenv('D_EEGLAB'));eeglab;close;end
addpath('sp_preproc');

d_project = 'working_eegbcg';

%% Perform GA removal

sp_rmga_clayden(d_project);
