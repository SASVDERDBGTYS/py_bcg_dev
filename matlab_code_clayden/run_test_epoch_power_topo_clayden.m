clearvars;
close all;
set_env_clayden;
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code', 'auxf')));
addpath(getenv('D_EEGLAB'));eeglab;close;
addpath('sp_preproc');
% addpath('an_results');
addpath(genpath(fullfile(getenv('D_GIT'), 'matlab_code')));

skip_settings = true;

use_cv = true;

try
    cd(fullfile(getenv('D_GIT'), 'matlab_code'));
    run_test_epoch_power_topo;
    cd(fullfile(getenv('D_GIT'), 'matlab_code_clayden'));
catch errt
    cd(fullfile(getenv('D_GIT'), 'matlab_code_clayden'));
    rethrow(errt);
end

% %%
%
% d_project = 'working_eegbcg';
% proc = getenv_check_empty('R_PROC');
% v.overwrite = false;
% v.str_ecg = 'ECG';
% %%
% arch = 'gru_arch_general4';
% proc_net = fullfile('proc_test_epochs', 'single_sub', arch);
% T = an_psd_test_epochs(d_project, proc_net, 'str_ecg', v.str_ecg);
% %%
% cell_band = {'delta', 'theta', 'alpha'};
% met_comparison = 'div';
% cell_coi = {'T8', 'Pz', 'AVG'};
% [T, T_sub] = get_power_difference(T, cell_band, cell_coi, met_comparison, ...
%     v.str_ecg, 'str_sub');
% %%
% vec_sub = T_sub.ix_sub';
% if strcmpi(getenv('R_PROC'), 'proc_full')
% assert(length(vec_sub)==19, 'Missing subjects?');
% only_use_five_runs = false;
% if only_use_five_runs
%     es = '_5ronly';
%     vec_sub(vec_sub==15) = [];
%     vec_sub(vec_sub==17) = [];
%     vec_sub(vec_sub==19) = [];
%     vec_sub(vec_sub==23) = [];
% else
%     es = '';
% end
% elseif strcmpi(getenv('R_PROC'), 'proc_clayden')
%     es = '';
% end
% cell_sub = arrayfun(@(x) sprintf('sub%02d', x), vec_sub, 'UniformOutput', false)';
%
% %% unlike other scripts, this one does direct operations on the whole of T/T_sub
% % so just remove unused subjects if there are any
% ix_keep_T = false(height(T), 1);
% ix_keep_T_sub = false(height(T_sub), 1);
% for ix_sub = 1:length(cell_sub)
%     ix_keep_T = ix_keep_T | strcmpi(T.str_sub, cell_sub{ix_sub});
%     ix_keep_T_sub = ix_keep_T_sub | strcmpi(T_sub.str_sub, cell_sub{ix_sub});
% end
% T = T(ix_keep_T, :);
% T_sub = T_sub(ix_keep_T_sub, :);
%
% %%
% v.figformat = 'svg';
% v.figdir = pwd;
% v.figsave = true;
% % v.proc = 'proc';
% v.fontsizeAxes = 7;
% v.fontsizeText = 6;
%
% if strcmpi(getenv('R_PROC'), 'proc_full')
%     fig_folder = 'figures_response1';
% elseif strcmpi(getenv('R_PROC'), 'proc_clayden')
%     fig_folder = 'figures_clayden';
% end
% p_fig = fullfile(getenv('D_OUT'), d_project, proc, fig_folder, 'fig04');
% % p_fig = '~/Desktop/temp2/';
% if not(exist(p_fig, 'dir')==7), mkdir(p_fig);end
%
% v.figdir = p_fig;
%
% print_local = @(h, dim) printForPub(h, sprintf('f_pow_%s_%s%s', arch, h.Name, es), 'doPrint', v.figsave,...
%     'fformat', v.figformat , 'physicalSizeCM', dim, 'saveDir', v.figdir, ...
%     'fontsizeText', v.fontsizeText, 'fontsizeAxes', v.fontsizeAxes);
%
% c = get_cmap;
% cmap = [c.gar_st; c.obs_st; c.net_st; c.neutral];
% %%
% h_f = figure('Name', sprintf('epoch_power'));clf;
%
% ns = [1, length(cell_coi)];
% fprintf('---------\n');
% for ix_coi = 1:length(cell_coi)
%     coi = cell_coi{ix_coi};
%
%
%     if strcmpi(met_comparison, 'diff')
%         met1 = @(band) sprintf('%s_%s_p_gar_minus_p_obs', coi, band);
%         met2 = @(band) sprintf('%s_%s_p_gar_minus_p_net', coi, band);
%     elseif strcmpi(met_comparison, 'div')
%         met1 = @(band) sprintf('%s_%s_p_obs_over_p_gar', coi, band);
%         met2 = @(band) sprintf('%s_%s_p_net_over_p_gar', coi, band);
%     else
%         error('');
%     end
%     subplot(ns(1), ns(2), ix_coi);cla;hold on;
%
%
%     vec_obs_delta = T_sub.(met1('delta'));
%     vec_net_delta = T_sub.(met2('delta'));
%     vec_obs_theta = T_sub.(met1('theta'));
%     vec_net_theta = T_sub.(met2('theta'));
%     vec_obs_alpha = T_sub.(met1('alpha'));
%     vec_net_alpha = T_sub.(met2('alpha'));
%     X = [vec_obs_delta; vec_net_delta; vec_obs_theta; vec_net_theta; vec_obs_alpha; vec_net_alpha];
%
%     hold on;
%     G = [1 * ones(size(vec_obs_delta)); 2 * ones(size(vec_net_delta)); 3 * ones(size(vec_obs_theta)); 4 * ones(size(vec_net_theta)); 5 * ones(size(vec_obs_alpha)); 6 * ones(size(vec_net_alpha))];
%     h_box = boxplot(X, ...
%         G, ...
%         'ColorGroup', G, ...
%         'Colors', repmat([cmap(2, :);cmap(3, :)], 3, 1), ...
%         'labels',repmat({'OBS', 'BCGNet'}, 1, 3), 'Symbol', '');
%     box off;
%     set(h_box, {'linew'}, {1.75});
%     av = findobj(gca, 'tag', 'Lower Adjacent Value');
%     for ix = 1:length(av)
%         av(ix).XData = av(ix).XData + [-0.01, +0.01];
%     end
%     av = findobj(gca, 'tag', 'Upper Adjacent Value');
%     for ix = 1:length(av)
%         av(ix).XData = av(ix).XData + [-0.01, +0.01];
%     end
%
%     c_grey = 0.7*ones(1, 3);
%     c_alpha = 0.4;
%     MarkerSize = 6;
%     MarkerEdgeColor = [1,1,1];
%     % note that in inkscape you then need to add a 0.1mm border to each
%     % point (find property circle)
%
%     for ix_vec_sub = 1:length(cell_sub)
%         h = plot([1, 2], [vec_obs_delta(ix_vec_sub), vec_net_delta(ix_vec_sub)], ...
%             '-', 'Color', c_grey);
%         h.Color(4) = c_alpha;
%         h = plot([3, 4], [vec_obs_theta(ix_vec_sub), vec_net_theta(ix_vec_sub)], ...
%             '-', 'Color', c_grey);
%         h.Color(4) = c_alpha;
%         h = plot([5, 6], [vec_obs_alpha(ix_vec_sub), vec_net_alpha(ix_vec_sub)], ...
%             '-', 'Color', c_grey);
%         h.Color(4) = c_alpha;
%
%         for ix = 1:6
%             switch ix
%                 case 1, vec_local = vec_obs_delta(ix_vec_sub);
%                 case 2, vec_local = vec_net_delta(ix_vec_sub);
%                 case 3, vec_local = vec_obs_theta(ix_vec_sub);
%                 case 4, vec_local = vec_net_theta(ix_vec_sub);
%                 case 5, vec_local = vec_obs_alpha(ix_vec_sub);
%                 case 6, vec_local = vec_net_alpha(ix_vec_sub);
%             end
%
%             plot(ix, vec_local, ...
%                 '.', 'MarkerFaceColor', c_grey, 'Color', c_grey,...
%                 'MarkerSize', MarkerSize, 'MarkerEdgeColor', MarkerEdgeColor);
%         end
%     end
%
%     if strcmpi(coi, 'AVG')
%         ylim([-0.05, 1.1   ])
%         p_line_height = 1.;
%     else
%         ylim([-0.05, 1.5])
%         p_line_height = 1.35;
%     end
%
%     y_range = [min(X(:)), max(X(:))];
%     d = (diff(y_range)*0.075);
%     tooth_length = d * 0.25;
%     line_height = y_range(2) + d;
%
%     [p_obs_net_delta, ~, stats] = signrank(vec_obs_delta, vec_net_delta);
%     [p_obs_net_theta, ~, stats] = signrank(vec_obs_theta, vec_net_theta);
%     [p_obs_net_alpha, ~, stats] = signrank(vec_obs_alpha, vec_net_alpha);
%
%     [p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha] = ...
%         deal_bonf_holm([p_obs_net_delta, p_obs_net_theta, p_obs_net_alpha]);
%
%     [p_obs_net_delta, ~, md_obs_net_delta] = plot_stars(vec_obs_delta, vec_net_delta, [1, 2], p_line_height, tooth_length, p_obs_net_delta);
%     [p_obs_net_theta, ~, md_obs_net_theta] = plot_stars(vec_obs_theta, vec_net_theta, [3, 4], p_line_height, tooth_length, p_obs_net_theta);
%     [p_obs_net_alpha, ~, md_obs_net_alpha] = plot_stars(vec_obs_alpha, vec_net_alpha, [5, 6], p_line_height, tooth_length, p_obs_net_alpha);
%
%
%
%     %         xtickangle(45)
%
%     title(coi);
%     fprintf('---\n');
%     set(gca,'xtick',[])
%     set(gca,'xticklabel',[])
%     set(gca,'xtick',[1.5, 3.5, 5.5])
%     set(gca,'xticklabel', {'Delta', 'Theta', 'Alpha'})
%
%     fprintf('%s:\n(', coi);
%     fprintf('delta MD = %0.2f, p = %s; ', md_obs_net_delta, p_to_str(p_obs_net_delta));
%     fprintf('theta MD = %0.2f, p = %s; ', md_obs_net_theta, p_to_str(p_obs_net_theta));
%     fprintf('alpha MD = %0.2f, p = %s', md_obs_net_alpha, p_to_str(p_obs_net_alpha));
%     fprintf(')\n');
%     % ylabel('AUC')
%
%     ylabel('Power ratio')
%
%
% end
% print_local(h_f, [18.1, 5]);
