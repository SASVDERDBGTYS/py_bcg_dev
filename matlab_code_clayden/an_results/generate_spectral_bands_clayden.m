% I Think this is not used by anything
function EEG_spect = generate_spectral_bands_clayden(EEG, cell_spectral_band)
%% takes in normal EEG and returns band power concatenated in channel dimension
EEG_spect = EEG;
EEG_spect.data = [];
EEG_spect.chanlocs = [];
if strcmpi(cell_spectral_band{end}, 'square')
    do_square = true;
    cell_spectral_band = cell_spectral_band(1:end-1);
else
    do_square = false;
end
for ix_band = 1:length(cell_spectral_band)
    f_range = get_eeg_band(cell_spectral_band{ix_band});
    EEG_band = pop_eegfiltnew(EEG, f_range(1), f_range(2));
    EEG_band.data = hilbert(EEG_band.data.').';
    if do_square
        EEG_band.data = EEG_band.data.^2;
    else
    EEG_band.data = abs(EEG_band.data);
    end
    EEG_band.data = EEG_band.data - mean(EEG_band.data, 2);
    EEG_band.data = EEG_band.data./std(EEG_band.data, 0, 2);
    for ix_ch = 1:length(EEG_band.chanlocs)
        EEG_band.chanlocs(ix_ch).labels = sprintf('%s_%s', ...
            EEG_band.chanlocs(ix_ch).labels, cell_spectral_band{ix_band});
        EEG_band.chanlocs(ix_ch).type = cell_spectral_band{ix_band};
    end
    EEG_spect.data = cat(1, EEG_spect.data, EEG_band.data);
    EEG_spect.chanlocs = cat(2, EEG_spect.chanlocs, EEG_band.chanlocs);
end
EEG_spect.nbchan = size(EEG_spect.data, 1);
end
