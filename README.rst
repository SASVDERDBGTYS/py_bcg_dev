************
Introduction
************

:Authors: J. R. McIntosh and J. Yao
:Contact: j.mcintosh@columbia.edu

Purpose
=======

Notes
=======
- There is a variable called predict_ev that controls what training and testing data is allowed to see. This currently defaults to a regime where we are not trying to predict the actual event phase, but trying to predict the phase at a filter length prior to the actual event.
- Currently the data events are subsampled (but this is specific to the data loading function). This is so that we can use a longer acausal filter.

Features
========

- Easily extensible framework:
	- Inherit from a single function (ddm_def.m)
	

To use on cluster
========
Pull it. Copy over data, then with python on the cluster call run_cluster_functions.py


Installation
============
1) Download.  
2) ? 

Todo
====

